using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPath : MonoBehaviour
{
    //public float speed;
    //private float WaitTime;
    //public float StartTime;
    public float repath;
    public float timer;

    //public Transform Startspot;
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;

    float RandomX;
    float RandomY;

    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        //WaitTime = StartTime;
        //animator = GetComponent<Animator>();
        CalculateVector3();
    }

    public void CalculateVector3() {
        RandomX = Random.Range(minX, maxX);
        RandomY = Random.Range(minY, maxY);
        transform.position = new Vector2(RandomX, RandomY);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        //transform.position = Vector2.MoveTowards(transform.position, Startspot.position, speed * Time.deltaTime);

        if (timer > repath) {
                CalculateVector3();
                timer = 0;
                Debug.Log("1");
            }
        /*    
        if (transform.position == Startspot.position) {
            //animator.SetBool("IsIdle", true);
            Debug.Log("2");
        } else {
            //animator.SetBool("IsIdle", false);
            Debug.Log("3");
        }
        */
    }

    /*
    private void OnCollisionEnter2D(Collision2D other) {
        
        if (other.gameObject.CompareTag("Wall")) {
            Debug.Log("CollideWall");
            CalculateVector3();
        }
        Debug.Log("Collide");
        CalculateVector3();
        timer = 0;
    }

    /*
    private void OnCollisionExit2D(Collision2D other) {
        if (other.gameObject.CompareTag("Wall")) {
            timer = 0;
            Debug.Log("CollideWall2");
        }
        Debug.Log("Collide2");
        timer = 0;
    }
    */
}
