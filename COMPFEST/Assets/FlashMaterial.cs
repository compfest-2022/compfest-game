using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class FlashMaterial : MonoBehaviour
    {

        public Material flashMaterial;
        public float duration;
        private float time;

       
        private SpriteRenderer spriteRenderer;
        private Material originalMaterial;

        bool IsFlash;

        void Start() {
            spriteRenderer = GetComponent<SpriteRenderer>();
            originalMaterial = spriteRenderer.material;
        }
    
        public void StartLoop() {
            StartCoroutine(DoLoop());
        }

        public void StopLoop() {
            spriteRenderer.material = originalMaterial;
        }
        
        IEnumerator DoLoop() {
            spriteRenderer.material = flashMaterial;
            yield return new WaitForSeconds(0.08f);
            spriteRenderer.material = originalMaterial;
            yield return new WaitForSeconds(0.08f);
            spriteRenderer.enabled = false;
            yield return new WaitForSeconds(0.08f);
            spriteRenderer.enabled = true;
            spriteRenderer.material = flashMaterial;
            yield return new WaitForSeconds(0.08f);
            spriteRenderer.material = originalMaterial;
            yield return new WaitForSeconds(0.08f);
            spriteRenderer.material = flashMaterial;
            yield return new WaitForSeconds(0.08f);
            spriteRenderer.material = originalMaterial;
            yield return new WaitForSeconds(0.08f);
            spriteRenderer.enabled = false;
            yield return new WaitForSeconds(0.08f);
            spriteRenderer.enabled = true;
            spriteRenderer.material = flashMaterial;
            yield return new WaitForSeconds(0.08f);
            spriteRenderer.material = originalMaterial;
        }
    }