using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeController : MonoBehaviour
{
    Animator animator;
    public float timer;
    bool IsHurt;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        IsHurt = false;
        timer = 1;
        gameObject.tag="Untagged";
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > 3) {
            SpikeUp();
        } else if (timer <= 6) {
            animator.SetBool("Spike", false);
            gameObject.tag="Untagged";
            //IsHurt = false;
            //Physics2D.IgnoreLayerCollision(8,7, true);
        }
    }

    private void SpikeUp() {
        if (timer >= 3) {
            animator.SetBool("Spike", true);
            gameObject.tag="Spike";
//          IsHurt = true;
        } if (timer > 6) 
            timer = 0;
    }


    /*
    private void OnTriggerStay2D(Collider2D collision) {
        if (collision.gameObject.CompareTag("Player") && IsHurt == true) {
            Player2 player2 = collision.gameObject.GetComponent<Player2>();
            Debug.Log("Hit");
            player2.HurtTime();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Player") && IsHurt == true) {
            Player2 player2 = collision.gameObject.GetComponent<Player2>();
            Debug.Log("Hit");
            player2.HurtTime();
        }

        Physics2D.IgnoreLayerCollision(12,7, true);
    }
    */
}
