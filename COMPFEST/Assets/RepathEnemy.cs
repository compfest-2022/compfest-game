using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class RepathEnemy : MonoBehaviour
{
    GameObject path;
    AIDestinationSetter AIDestin;
    RandomPath randomPath;

    // Start is called before the first frame update
    void Start()
    {
        AIDestin = GetComponent<AIDestinationSetter>();
        path = AIDestin.target.gameObject;
        randomPath = path.GetComponent<RandomPath>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("pos" + " enemy" + RandomPath.transform.position);
        
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("StaticEnemy") || other.gameObject.CompareTag("FungiMonster") || other.gameObject.CompareTag("RandomPathEnemy")) {
            randomPath.CalculateVector3();
            randomPath.timer = 0;
        }
    }

   
}
