using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectibles : MonoBehaviour
{
    public AudioClip suaranya;
    public PengaturGame pg;
    
    void OnCollisionEnter2D (Collision2D target)
    {
        if (target.gameObject.tag == "Player") {
            pg.Bunyikan(suaranya);
            //GameObject.Find("ObjectPG").GetComponent<PengaturGame>().Bunyikan(suaranya);
            GameObject.Find("ObjPG").GetComponent<PengaturGame>().TambahSkor(10);
            Destroy(gameObject);
        }
    }
}
