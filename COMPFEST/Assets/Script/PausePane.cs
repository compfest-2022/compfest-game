using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausePane : MonoBehaviour
{   
    public AudioClip coin;

    public void Start() {
         GameObject pausepanel = GameObject.Find("Canvas").transform.GetChild(11).gameObject;
         pausepanel.SetActive(false);
    }

    public void PausePanel () {
        GameObject pausepanel = GameObject.Find("Canvas").transform.GetChild(11).gameObject;
        GameObject.Find("ObjPG").GetComponent<PengaturGame>().Bunyikan(coin);
        GameObject.Find("MobileSingleStickControl").GetComponent<Canvas>().enabled = false;
        pausepanel.SetActive(true);
        
    }

    public void StartPanel () {
        GameObject pausepanel = GameObject.Find("Canvas").transform.GetChild(11).gameObject;
        GameObject.Find("ObjPG").GetComponent<PengaturGame>().Bunyikan(coin);
        pausepanel.SetActive(false);
        GameObject.Find("MobileSingleStickControl").GetComponent<Canvas>().enabled = true;
    }
}
