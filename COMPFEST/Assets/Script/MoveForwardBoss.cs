using UnityEngine;
using System.Collections;

public class MoveForwardBoss : MonoBehaviour {

	public float speed;    
    private Rigidbody2D rigidbody2D;
    private float timer = 7;
    private int random;
    private int boosttime = 12;

    void Start(){
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update () {//memberikan nilai speed kepada sumbu x
        if (timer > 2){

            timer -= Time.deltaTime;
            rigidbody2D.velocity = new Vector2 (transform.localScale.x, 0) * speed;

        } else if (timer <= 2 && timer > 0) {

            timer -= Time.deltaTime;
            Debug.Log("Time has run out! 4");
            rigidbody2D.velocity = new Vector2 (transform.localScale.x, 0) * 1.5f;

        } else {

            Debug.Log("Time has run out! 0");
            timer = 7;
        }
        
	}
}
