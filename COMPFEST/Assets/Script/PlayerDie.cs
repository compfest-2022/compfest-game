﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class PlayerDie : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D target){
		if (target.gameObject.tag == "Deadly" || target.gameObject.tag == "Enemy") { //tabrakan dgn objek memiliki tag
			Die(); //panggil fungsi die()
		}
	}

	void Die(){		
		GameObject go = GameObject.Find("PengaturGame"); //cari gameobject bernama pengaturgame pada hierarchy
        ScriptPengaturGame pg = (ScriptPengaturGame)go.GetComponent(typeof(ScriptPengaturGame));//ambil scriptpengaturgame
		pg.Respawning(); //memakai metode respawning yang sudah public
		Destroy (gameObject); //destroy diri sendiri
	}
}
