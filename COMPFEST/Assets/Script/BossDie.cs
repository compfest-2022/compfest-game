using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BossDie : MonoBehaviour
{
    public float HealthSkrg = 30f;
    public RectTransform HeartBar;
    private int count = 2;
    FlashMaterial flashMaterial;
    PengaturMusic MusicControl;
    public AudioClip EnemyHurt;
    public AudioClip EnemyRegen;
    GameController gameCon;

    void Start() {
        flashMaterial = GetComponent<FlashMaterial>();
        MusicControl = GameObject.FindGameObjectWithTag("MusicCon").GetComponent<PengaturMusic>();
        gameCon = GameObject.FindGameObjectWithTag("GameCon").GetComponent<GameController>();
        gameCon.EnemyCount();
    }

    void Fixedupdate() {
        
    }

    public void Die(){		
		//memakai metode respawning yang sudah public
		if (HealthSkrg > 0) {
            MusicControl.Bunyikan(EnemyHurt);
            HealthSkrg = HealthSkrg - 1f;
            HeartBar.localScale = new Vector3(HeartBar.transform.localScale.x - (1/HealthSkrg), 1, 1);
            flashMaterial.StartLoop();
        }
        if (HealthSkrg == 25) {
            Debug.Log("REGEN");
            MusicControl.Bunyikan(EnemyRegen);
            for (int i = 0; i < 50 ; i++) {
                HeartBar.localScale = new Vector3(1, 1, 1);
                HealthSkrg += 1;
                if (HealthSkrg == 50) {
                    i = 50;
                }
            }
        }
       
        if (HealthSkrg == 0 ) {
            gameCon.EnemyDecrease();
            Destroy (gameObject); 
        }
	}

    

}
