using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quit : MonoBehaviour
{
    public AudioClip press;

    public void quitaplication() {
         Application.Quit();
         Bunyikan(press);
    }

     public void Bunyikan(AudioClip suara) {
        GetComponent<AudioSource>().PlayOneShot(suara, 1F);
    }
}
