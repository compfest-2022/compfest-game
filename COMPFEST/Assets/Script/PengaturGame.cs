using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PengaturGame : MonoBehaviour
{
    public TextMeshProUGUI teks_skor;
    public static int skorSkrg = 0;
    public static int HealthSkrg = 4;
    public GameObject Heart1;
    public GameObject Heart2;
    public GameObject Heart3;
    public AudioClip BoneCrack;
    private float time = 0.0f;
    public float interpolationPeriod = 0.1f;
    public TextMeshProUGUI price;
    public static int PRICEUP = 50;
    public float timer;
    public AudioClip Heals;

    void Start() {
        teks_skor.text=skorSkrg.ToString();
        price.text=PRICEUP.ToString();
    }

    public void TambahSkor(int skornya) {
        skorSkrg+=skornya;
        teks_skor.text=skorSkrg.ToString();
    }

    public void Bunyikan(AudioClip suara) {
        GetComponent<AudioSource>().PlayOneShot(suara, 1F);
    }

    void Update () {
        time += Time.deltaTime;
        timer += Time.deltaTime;

        if (time >= interpolationPeriod) {
            time = 0.0f;

            if (HealthSkrg == 1) {
               Heart3.SetActive(false);
               Heart2.SetActive(false);
               Heart1.SetActive(false);
            }
            if (HealthSkrg == 2) {
                Heart2.SetActive(false);
                Heart1.SetActive(false);
            }
            if (HealthSkrg == 3) {
                Heart1.SetActive(false);
            }
            teks_skor.text=skorSkrg.ToString();
            price.text=PRICEUP.ToString();
         // execute block of code here
        }  
    }

    void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("Deadly")) {
            if (timer > 0.1f) {
                StartCoroutine("Die");
                timer = 0;
            }
        }
    }

    public void buyanlifefromdemon() {
        if (HealthSkrg < 4 && skorSkrg >= PRICEUP) {
            HealthSkrg = HealthSkrg + 1;
            //GameObject.Find("ObjPG").GetComponent<PengaturGame>().Bunyikan(Heals);
            skorSkrg = skorSkrg - PRICEUP;
            PRICEUP += 20;
            

            if (HealthSkrg == 2) {
               Heart3.SetActive(true);
            }
            if (HealthSkrg == 3) {
                Heart2.SetActive(true);
                Heart3.SetActive(true);
            }
            if (HealthSkrg == 4) {
                Heart3.SetActive(true);
                Heart2.SetActive(true);
                Heart1.SetActive(true);
            }
        }
    }

    IEnumerator Die(){		
		GameObject go = GameObject.Find("PengaturGame"); //cari gameobject bernama pengaturgame pada hierarchy
        ScriptPengaturGame pg = (ScriptPengaturGame)go.GetComponent(typeof(ScriptPengaturGame));//ambil scriptpengaturgame
		//memakai metode respawning yang sudah public
		HealthSkrg = HealthSkrg - 1;
        if (HealthSkrg == 1) {
            Heart3.SetActive(false);
            //GameObject.Find("ObjPG").GetComponent<PengaturGame>().Bunyikan(BoneCrack);
        }
        if (HealthSkrg == 2) {
            Heart2.SetActive(false);
            //GameObject.Find("ObjPG").GetComponent<PengaturGame>().Bunyikan(BoneCrack);
        }
        if (HealthSkrg == 3) {
            Heart1.SetActive(false);
            //GameObject.Find("ObjPG").GetComponent<PengaturGame>().Bunyikan(BoneCrack);
        }
        if (HealthSkrg == 0 ) {
            yield return new WaitForSeconds(2);
            Destroy (gameObject);
            HealthSkrg = 4; //destroy diri sendiri
            pg.Respawning(); 
            skorSkrg = 0;
        }
	}

    public void ResetCoin() {
        skorSkrg = 0;
    }

    public void ResetHealth() {
		HealthSkrg = 4;
	}

    public void ResetPrice() {
        PRICEUP = 50;
    }

}
