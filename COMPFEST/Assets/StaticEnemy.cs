using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticEnemy : MonoBehaviour
{
    Animator animator;
    public Rigidbody2D rb2d;
    FlashMaterial flashMaterial;
    public float Health = 1;
    public float strength;
    GameObject DoorController;
    GameController EnemyCount;
    EnemyHealthBar EnemyBar;
    PengaturMusic MusicControl;
    public AudioClip EnemyHurt;

    private void Start() {
        animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        flashMaterial = GetComponent<FlashMaterial>();
        DoorController = GameObject.FindWithTag("GameCon");
        EnemyCount = DoorController.GetComponent<GameController>(); 
        EnemyCount.EnemyCount();
        EnemyBar = transform.GetChild(0).GetComponent<EnemyHealthBar>();
        MusicControl = GameObject.FindGameObjectWithTag("MusicCon").GetComponent<PengaturMusic>();
    }

    public void DamageHealth(Vector3 PlayerPos, float Damage) {
        if (Health > 0) {
            MusicControl.Bunyikan(EnemyHurt);
            Health -= Damage;
            Debug.Log(Health);
            flashMaterial.StartLoop();
            StartCoroutine("Reset");
            EnemyBar.EnemyPercent = Damage;
            EnemyBar.BarMinus();
        } else if (Health == 0) {
            Health = -2;
            MusicControl.Bunyikan(EnemyHurt);
            Debug.Log("Damn");
            flashMaterial.StartLoop();
            EnemyCount.EnemyDecrease();
            StartCoroutine("RemoveEnemy");
        }
    }

    /*
    private void OnCollisionEnter2D(Collision2D collision) {
        Physics2D.IgnoreLayerCollision(8,3, true);
    }
    */

    private IEnumerator Reset() {
        yield return new WaitForSeconds(0.15f);
        rb2d.velocity = Vector3.zero;
    }

    private IEnumerator RemoveEnemy() {
        yield return new WaitForSeconds(1);  
        Destroy(gameObject);
    }


    public void Defeated(){
        animator.SetTrigger("Defeated");
    }
}
