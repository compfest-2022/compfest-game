using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class FungiMonster : MonoBehaviour
{
    public Rigidbody2D rb2d;
    FlashMaterial flashMaterial;
    Animator animator;
    FindMonsterController FlipScript;
    AIPath aiPath;
    GameObject DoorController;
    GameController EnemyCount;
    EnemyHealthBar EnemyBar;

    public float Health;
    public float strength;
    public int TimetoDie = 1;

    public AudioClip Hurtsfx;
    public AudioClip Idlesfx;
    public AudioClip Attacksfx;
    PengaturMusic MusicControl;
    float timer;
    float randomTimer;

    private void Start() {
        rb2d = GetComponent<Rigidbody2D>();
        flashMaterial = GetComponent<FlashMaterial>();
        animator= GetComponent<Animator>();
        FlipScript = GetComponent<FindMonsterController>();
        aiPath = GetComponent<AIPath>();
        DoorController = GameObject.FindWithTag("GameCon");
        EnemyCount = DoorController.GetComponent<GameController>(); 
        MusicControl = GameObject.FindGameObjectWithTag("MusicCon").GetComponent<PengaturMusic>();
        EnemyCount.EnemyCount();
        EnemyBar = transform.GetChild(0).GetComponent<EnemyHealthBar>();
        randomTimer = 0;
    }

    public void DamageHealth(Vector3 PlayerPos, float Damage) {
        if (Health > 0) {
            MusicControl.Bunyikan(Hurtsfx);
            Health -= Damage;
            Debug.Log(Health);
            flashMaterial.StartLoop();
            StartCoroutine("Reset", PlayerPos);
            EnemyBar.EnemyPercent = Damage;
            EnemyBar.BarMinus();
        } if (Health == 0) {
            Health = -2;
            Debug.Log("Damn");
            Debug.Log(Health);
            RemoveEnemy();
        }
    }

     private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Player")) {
            if (Attacksfx) {
                MusicControl.Bunyikan(Attacksfx);
            }
        }
    }

    /*
    private void OnCollisionEnter2D(Collision2D collision) {
        Physics2D.IgnoreLayerCollision(8,3, true);
    }
    */

    private void Update() {
        timer += Time.deltaTime;

        if (timer == randomTimer) {
            MusicControl.Bunyikan(Idlesfx);
            timer = 0;
            randomTimer = Random.Range(3, 5);
        }
    }

    
     private IEnumerator Reset(Vector3 PlayerPos) {
        aiPath.canMove = false;
        yield return new WaitForSeconds(0.15f);
        rb2d.velocity = Vector3.zero;
        aiPath.canMove = true;
    }
    private void RemoveEnemy() {
        animator.SetBool("IsDead", true);
        rb2d.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY;
        rb2d.freezeRotation = true; 
        flashMaterial.StopLoop();
        FlipScript.enabled = false;
        aiPath.speed = 0;
        EnemyCount.EnemyDecrease();
    }
}
