using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootBomb : MonoBehaviour
{

    public float timer = 0;
    public GameObject bulletPrefab;
    float fireRate;
    float nextFIre;
    Animator animator;
    private Transform playerPos;
    SpriteRenderer spriteRenderer;
    UnityEngine.AI.NavMeshAgent nav;
    float oldXaxis;

    private float angle = 90;

    // Start is called before the first frame update
    void Start()
    {
        animator = transform.root.GetComponent<Animator>();
        //playerPos = GetComponent<Transform>();
        //oldXaxis = transform.position.x;
        //spriteRenderer = GetComponent<SpriteRenderer>();
        //nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        fireRate = 1f;
        nextFIre = Time.time;
    }

    private void FixedUpdate() {
        timer += Time.deltaTime;
		//memunculkan peluru pada posisi gameobject shootpos
		//memberikan dorongan peluru sebesar bulletSpeed dengan arah terbangnya bulletPos 

		if (timer >= (3.7f)) {
            animator.SetBool("IsThrow", true);
            //Debug.Log(timer);
        } if (timer >= 4.2f ) {
                    float angleMoveDirectionX1 = transform.position.x + Mathf.Sin((angle * Mathf.PI) / 180f);
                    float angleMoveDirectionY1 = transform.position.y + Mathf.Cos((angle * Mathf.PI) / 180f);

                    Vector3 bulMoveVector = new Vector3 (angleMoveDirectionX1, angleMoveDirectionY1, 0);
                    Vector2 BulDir = (bulMoveVector - transform.position).normalized;

                    GameObject Bullet = Instantiate(bulletPrefab);
                    Bullet.transform.position = transform.position;
                    Bullet.transform.rotation = transform.rotation;

                    Bullet.GetComponent<BombAction>().SetMoveDirection(BulDir);
                timer = 0;
                animator.SetBool("IsThrow", false);
        }
        /*
        if (oldXaxis > transform.position.x) {
            //Debug.Log(oldXaxis);
            //Debug.Log("Left");
            spriteRenderer.flipX = true;
            oldXaxis = transform.position.x;
        } if (oldXaxis < transform.position.x) {
            //Debug.Log(oldXaxis);
            //Debug.Log("Right");
            spriteRenderer.flipX = false;
            oldXaxis = transform.position.x;
        }
        //Debug.Log(timer);
		//counting cooldown, nanti dicek lagi
        //coolDown = Time.time + attackSpeed;
        */
    }
}
