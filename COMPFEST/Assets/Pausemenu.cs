using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pausemenu : MonoBehaviour
{
    Canvas canvas;
    bool isPause;
    Canvas MainC;

    private void Awake() {
        //DontDestroyOnLoad(this.gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        canvas = GetComponent<Canvas>();    
        MainC = GameObject.FindGameObjectWithTag("MainCanvas").GetComponent<Canvas>();
        MainC.enabled = true;
        canvas.enabled = false;
        Time.timeScale = 1;
        isPause = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if(isPause == false) {
                canvas.enabled = true;
                MainC.enabled = false;
                Time.timeScale = 0;
                isPause = true;
            } else {
                canvas.enabled = false;
                MainC.enabled = true;
                Time.timeScale = 1;
                isPause = false;
            }
        }
    }

    public void resume() {
        canvas.enabled = false;
        MainC.enabled = true;
        Time.timeScale = 1;
        isPause = false;
    }
}
