using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Price : MonoBehaviour
{
    public int price;
    public TextMeshPro Price_Text;

    // Start is called before the first frame update
    void Start()
    {
        Price_Text.text = price.ToString();
    }
}
