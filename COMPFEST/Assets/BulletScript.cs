using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour {


	public int lifetime = 4;
	private float timer;
	public bool canDestroy;
	//public AudioClip suaranya;
	//public AudioClip EEK;
	
	private void OnCollisionEnter2D(Collision2D collision) {
		if(canDestroy == true) {
			if(collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("StaticEnemy")) {
				Physics2D.IgnoreLayerCollision(8,8, true); 
			}
			Destroy(this.gameObject);
		}
	}

	IEnumerator OnCollisionExit2D(Collision2D other) {
        if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("StaticEnemy")) {
            yield return new WaitForSeconds(1);
            Physics2D.IgnoreLayerCollision(8,8, false); 
        }
    }

	void Update(){
		if(timer == 0) {
			//GameObject.Find("ObjPG").GetComponent<PengaturGame>().Bunyikan(suaranya);
		}
		timer += Time.deltaTime; //count waktu kemunculan
		if(timer > lifetime){ //bila sudah mencapai lifetime			
			timer = 0; //reset timer
			Destroy(gameObject); //hancurkan diri sendiri
		}
	}
}
