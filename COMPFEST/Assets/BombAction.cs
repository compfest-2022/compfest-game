using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombAction : MonoBehaviour
{
    public FlashMaterial flashMater;
    Animator animator;
    public float timer = 0;
    public CircleCollider2D BombCollider;
    public LayerMask LayertoHit;
    public float fieldofImpact;
    public float force;
    
    Vector2 moveDirection;
    public float moveSpeed;
    Rigidbody2D rb2d;
    

    void Start()
    {
        animator = GetComponent<Animator>();
        flashMater = GetComponent<FlashMaterial>();
        rb2d = GetComponent<Rigidbody2D>();
        timer = 0;

    }

    void Update() {
        timer += Time.deltaTime; //count waktu kemunculan

        transform.Translate(moveDirection * moveSpeed * Time.deltaTime);

        if (timer >= 4) {
            StartCoroutine("BombExplode");
        }
    }

    public void SetMoveDirection(Vector2 dir) {
        moveDirection = dir;
    }

    private IEnumerator BombExplode() {
        //BombCollider.radius = 5;
        Explode();
        animator.SetTrigger("IsExplode");
        yield return new WaitForSeconds(0.7f);
        DestroyObject(gameObject);
    }

    void Explode() {
        Collider2D[] objects = Physics2D.OverlapCircleAll(transform.position, fieldofImpact, LayertoHit);
        
        foreach (Collider2D obj in objects)
        {
            Vector2 direction = obj.transform.position - transform.position;
            obj.GetComponent<Rigidbody2D>().AddForce(direction * force);
            obj.GetComponent<Player>().isExplode();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("StaticEnemy"))
            Physics2D.IgnoreCollision(collision.collider,gameObject.GetComponent<Collider2D>());
        if (collision.gameObject.CompareTag("Player")) {
            Physics2D.IgnoreLayerCollision(8,7, true);
            //flashMater.StartLoop();
            //collider2d.enabled = false; 
        }
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, fieldofImpact);    
    }
}
