using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    Transform Player;
    public bool X = false;
    public bool Y = false;

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (X == true)
            transform.position = new Vector3 (transform.position.x, Player.position.y, transform.position.z);
        if (Y == true)
            transform.position = new Vector3 (Player.position.x, transform.position.y, transform.position.z);
    }
}
