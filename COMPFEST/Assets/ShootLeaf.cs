using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootLeaf : MonoBehaviour
{

    public float timer = 0;
    public GameObject bulletPrefab;
    float fireRate;
    float nextFIre;
    Animator animator;
    float oldXaxis;
    public AudioClip Attacksfx;
    PengaturMusic MusicControl;

    private float angle = 90;

    // Start is called before the first frame update
    void Start()
    {
        animator = transform.root.GetComponent<Animator>();
        MusicControl = GameObject.FindGameObjectWithTag("MusicCon").GetComponent<PengaturMusic>();
        //playerPos = GetComponent<Transform>();
        //oldXaxis = transform.position.x;
        //spriteRenderer = GetComponent<SpriteRenderer>();
        //nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        fireRate = 1f;
        nextFIre = Time.time;
    }

    private void FixedUpdate() {
        timer += Time.deltaTime;
		//memunculkan peluru pada posisi gameobject shootpos
		//memberikan dorongan peluru sebesar bulletSpeed dengan arah terbangnya bulletPos 

		if (timer >= (2f)) {
            animator.SetBool("IsAttack", true);
            //Debug.Log(timer);
        } if (timer >= 2.5f ) {
                    MusicControl.Bunyikan(Attacksfx);
                    float angleMoveDirectionX1 = transform.position.x + Mathf.Sin((angle * Mathf.PI) / 180f);
                    float angleMoveDirectionY1 = transform.position.y + Mathf.Cos((angle * Mathf.PI) / 180f);

                    Vector3 bulMoveVector = new Vector3 (angleMoveDirectionX1, angleMoveDirectionY1, 0);
                    Vector2 BulDir = (bulMoveVector - transform.position).normalized;

                    GameObject Bullet = Instantiate(bulletPrefab);
                    Bullet.transform.position = transform.position;
                    Bullet.transform.rotation = transform.rotation;

                    Bullet.GetComponent<Bullet>().SetMoveDirection(BulDir);
                timer = 0;
                animator.SetBool("IsAttack", false);
        }
        /*
        if (oldXaxis > transform.position.x) {
            //Debug.Log(oldXaxis);
            //Debug.Log("Left");
            spriteRenderer.flipX = true;
            oldXaxis = transform.position.x;
        } if (oldXaxis < transform.position.x) {
            //Debug.Log(oldXaxis);
            //Debug.Log("Right");
            spriteRenderer.flipX = false;
            oldXaxis = transform.position.x;
        }
        //Debug.Log(timer);
		//counting cooldown, nanti dicek lagi
        //coolDown = Time.time + attackSpeed;
        */
    }
}
