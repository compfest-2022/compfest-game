using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPatrol : MonoBehaviour
{
    public float walkSpeed, timeBTWShots, shootSpeed;
    public float health;
    private float distTolPlayer;

    public Color color1;
    public Color color2;
    public Color colorGizmos;
    public Vector3 range;
    public Transform center;
    public LayerMask LayerTohit;
    float timer;
    float timer2;
    SpriteRenderer sp;

    public AudioClip Shootsfx;
    PengaturMusic MusicControl;

    [HideInInspector]
    public bool mustPatrol;
    private bool mustTurn, canShoot;

    public Rigidbody2D rb;
    public Transform groundCheck;
    public LayerMask groundLayer;
    public Collider2D bodyCollider;
    public Transform player, shootPos;
    public GameObject bullet;

    FlashMaterial flashMaterial;

    GameObject DoorController;
    GameController EnemyCount;
    EnemyHealthBar EnemyBar;

    // Start is called before the first frame update
    void Start()
    {
        mustPatrol = true;
        canShoot = true;
        timer = 3;
        sp = GetComponent<SpriteRenderer>();
        player = GameObject.FindWithTag("Player").transform;
        flashMaterial = GetComponent<FlashMaterial>();
        DoorController = GameObject.FindWithTag("GameCon");
        EnemyCount = DoorController.GetComponent<GameController>(); 
        MusicControl = GameObject.FindGameObjectWithTag("MusicCon").GetComponent<PengaturMusic>();
        EnemyCount.EnemyCount();
        EnemyBar = transform.GetChild(0).GetComponent<EnemyHealthBar>();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        timer2 += Time.deltaTime;

        if(mustPatrol || bodyCollider.IsTouchingLayers(groundLayer)) {
            Patrol();
        }

        CheckPlayer();
    }

    private void CheckPlayer() {
        Collider2D Obj = Physics2D.OverlapBox(new Vector3(center.position.x, center.position.y), range, 0, LayerTohit);

        if (Obj != null) {
            colorGizmos = color2;
            if(player.position.x > transform.position.x && transform.localScale.x < 0 || player.position.x < transform.position.x && transform.localScale.x > 0) {
                // Jika Enemy menghadap ke kiri dan Player ada di kanan Enemy maka Enemy akan menghadap ke kanan
                    Flip();
            }
            mustPatrol = false;
            rb.velocity = Vector2.zero;
            if(canShoot){
                if (timer > 1) {
                    Shoot();
                    MusicControl.Bunyikan(Shootsfx);
                    timer = 0;
                }
            }
        } else {
            mustPatrol = true;
            colorGizmos = color1;
        }
    }

    private void FixedUpdate()
    {
        if(mustPatrol) {
            // Debug.Log("Checking OverlapCircle");
            mustTurn = Physics2D.OverlapCircle(groundCheck.position, 0.1f, groundLayer);
        }
    }

    public void TakeDamage(float Damage) {
        if (health > 0) {
            health -= Damage;
            EnemyBar.EnemyPercent = Damage;
            EnemyBar.BarMinus();
            flashMaterial.StartLoop();
        } if(health == 0){
            EnemyCount.EnemyDecrease();
            Destroy(this.gameObject);
        }
    }

    void Patrol() {
        rb.velocity = new Vector2(walkSpeed * Time.fixedDeltaTime, rb.velocity.y);
        if(mustTurn) {
            // Debug.Log("Flip = Triggered");
            if (timer2 > 0.1f) {
                Flip();
                timer2 = 0;
            }
        }
    }

    void Flip() {
        mustPatrol = false;
        transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
        //sp.flipX = true;
        walkSpeed *= -1;
        mustPatrol = true;
    }

    private void Shoot() {
        GameObject newBullet = Instantiate(bullet, shootPos.position, Quaternion.identity);
        newBullet.GetComponent<Rigidbody2D>().velocity = new Vector2(shootSpeed * walkSpeed * Time.deltaTime, 0f);
    }
    
    private void OnDrawGizmos() {
        Gizmos.color = colorGizmos;
        Gizmos.DrawCube(new Vector3(center.position.x, center.position.y), range);
        Gizmos.DrawSphere(groundCheck.transform.position, groundLayer);
    }
}