using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolBullets : MonoBehaviour
{
    float timer;

    private void Update() {
        timer += Time.deltaTime;
    }

    void OnCollisionEnter2D(Collision2D hitInfo) {
        Debug.Log(hitInfo);

            Destroy(gameObject);
    }
}
