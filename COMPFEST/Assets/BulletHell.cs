using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class BulletHell : MonoBehaviour
{
    public int BulletAmount = 24;

    private float angleStart = 0, angleEnd = 360;

    Vector2 bulletMoveDirection;
    public GameObject bulletPrefab;
    public GameObject[] enemies;
    public float xmin, xmax;
	public float ymin,ymax;
    private float objX,objY;
    public int maxtimer;
	public int mintimer;
	private float timer2;
	public float timetospawn;
    AIPath aiPath;
    AIDestinationSetter3 AIDestin;
    Animator animator;
    public float timer = 0;
    bool CanMove;
    public float timer3;

    //public float bulletPos = -1;
    //public float bulletSpeed;
    //public Rigidbody2D bulletPrefab; //objek peluru yg dimaksud
	//public GameObject shootPos1;
    //public GameObject shootPos2;
    //public GameObject shootPos3;
    //GameObject target;
    
    // Start is called before the first frame update
    void Start()
    {
        //target =  GameObject.FindGameObjectWithTag("Player");
        animator = transform.root.GetComponent<Animator>();
        aiPath = transform.root.GetComponent<AIPath>();
        CanMove = true;
        timetospawn = Random.Range(mintimer, (maxtimer / 2)); //random ulang lagi
        AIDestin = GetComponent<AIDestinationSetter3>();
        
    }

    private void Update() {
        timer += Time.deltaTime;
        timer2 += Time.deltaTime;
        timer3 += Time.deltaTime;

        if (timer3 > 5) {
            var Aidestin1 = AIDestin.PlayerPos;
            if (Aidestin1 == false) {
                AIDestin.PlayerPos = true;
                timer3 = 0;
            } if (Aidestin1 == true) {
                AIDestin.PlayerPos = false;
                timer3 = 0;
            }
        }

       
        if (timer >= 7 ) {
            animator.SetBool("IsAttack", true);
            CanMove = false;
        } if (timer >= 8f ) {
            Invoke("StopAnimate", 0.6f);
            Invoke("Fire", 0.4f);
            timer = 0;
            Invoke("LockMovement", 1f);
            
        }

        if (CanMove == true) {
            aiPath.canMove = true;
        } else {
            aiPath.canMove = false;
    }

       if(timer2 > timetospawn){ //apabila sudah mencapai waktunya spawn			
			timer2 = 0; //reset timer
			timetospawn = Random.Range(mintimer, maxtimer); //random ulang lagi
			Debug.Log(timetospawn); //cetak catatan ke debug window
			RandomPosition();
            Spawn(Random.Range(0, enemies.Length)); //random musuh yang akan dimunculkan
		}
    }

    void RandomPosition(){
		objX = Random.Range(xmin, xmax);
		objY = Random.Range(ymin, ymax);
	}


    public void LockMovement() {
        CanMove = true;
    }

    void Spawn(int index){
        Instantiate(enemies[index],new Vector3(objX,objY,0),transform.rotation); //memunculkan musuh pada index "index"
		
	}

    private void Fire() {
        float angleStep = (angleEnd - angleStart) / BulletAmount;
        float angle = angleStart;

        for (int i = 0 ; i < BulletAmount ; i++) {
            float angleMoveDirectionX1 = transform.position.x + Mathf.Sin((angle * Mathf.PI) / 180f);
            float angleMoveDirectionY1 = transform.position.y + Mathf.Cos((angle * Mathf.PI) / 180f);

            Vector3 bulMoveVector = new Vector3 (angleMoveDirectionX1, angleMoveDirectionY1, 0);
            Vector2 BulDir = (bulMoveVector - transform.position).normalized;

            GameObject Bullet = Instantiate(bulletPrefab);
            Bullet.transform.position = transform.position;
            Bullet.transform.rotation = transform.rotation;

            Bullet.GetComponent<Bullet>().SetMoveDirection(BulDir);
            angle += angleStep;
        }
    }
    /*
    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= 4.2 ) {
            //CenterSpike
            animator.SetBool("IsAttack", true);
            Invoke("StopAnimate", 0.4f);
            Vector2 moveDirection = ( target.transform.position - transform.position).normalized;
            Debug.Log(moveDirection);
            float angleMoveDirectionX1 = moveDirection.x + Mathf.Sin((90f * Mathf.PI) / 180f);
            float angleMoveDirectionY1 = moveDirection.y + Mathf.Cos((90f * Mathf.PI) / 180f);
            Vector2 moveAngle1 = new Vector2(angleMoveDirectionX1, angleMoveDirectionY1).normalized;

            Rigidbody2D bPrefab1 = Instantiate(bulletPrefab, shootPos1.transform.position, shootPos1.transform.rotation) as Rigidbody2D;
            bPrefab1.GetComponent<Rigidbody2D>().AddForce((moveDirection * bulletSpeed));
            
            //bPrefab1.transform.SetParent(Parent);
            /*
            //UpperSpikeAt 60 Degree
            float angleMoveDirectionX2 = moveDirection.x + Mathf.Sin((85f * Mathf.PI) / 180f);
            float angleMoveDirectionY2 = moveDirection.y + Mathf.Cos((85f * Mathf.PI) / 180f);
            Vector2 moveAngle2 = new Vector2(angleMoveDirectionX2, angleMoveDirectionY2).normalized;

            Rigidbody2D bPrefab2 = Instantiate(bulletPrefab, shootPos2.transform.position, Quaternion.Euler(new Vector3(0, 0, 9.48f + Parent.transform.localEulerAngles.z))) as Rigidbody2D;
            bPrefab2.GetComponent<Rigidbody2D>().AddForce((moveAngle2 * bulletSpeed));
           //bPrefab2.transform.SetParent(Parent);
            
            //DownSpikeAt 60 Degree
            float angleMoveDirectionX3 = moveDirection.x + Mathf.Sin((95f * Mathf.PI) / 180f);
            float angleMoveDirectionY3 = moveDirection.y + Mathf.Cos((95f * Mathf.PI) / 180f);
            Vector2 moveAngle3 = new Vector2(angleMoveDirectionX3, angleMoveDirectionY3).normalized;

            Rigidbody2D bPrefab3 = Instantiate(bulletPrefab, shootPos3.transform.position, Quaternion.Euler(new Vector3(0, 0, -10.195f + Parent.transform.localEulerAngles.z))) as Rigidbody2D;
            bPrefab3.GetComponent<Rigidbody2D>().AddForce((moveAngle3 * bulletSpeed));
            //bPrefab3.transform.SetParent(Parent);
            
            timer = 0;
            //animator.SetBool("IsAttack", false);
        }
    }
    */

    public void StopAnimate() {
            animator.SetBool("IsAttack", false);
        }
}
