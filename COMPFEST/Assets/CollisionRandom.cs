using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionRandom : MonoBehaviour
{
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;

    float RandomX;
    float RandomY;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void CalculateVector3() {
        RandomX = Random.Range(minX, maxX);
        RandomY = Random.Range(minY, maxY);
        transform.position = new Vector2(RandomX, RandomY);
    }

    // Update is called once per frame
    void Update()
    {
        //Physics2D.IgnoreLayerCollision(6,3, true);
        Physics2D.IgnoreLayerCollision(6,8, true);
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.CompareTag("Wall")) {
            Debug.Log("change pos");
            CalculateVector3();
        }


    }
}
