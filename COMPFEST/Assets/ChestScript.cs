using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class ChestScript : MonoBehaviour
{
    public GameObject coinspawn;
    public bool CoinSpawn;
    coinspawner coinScript;
    SpriteRenderer spr;
    Animator animator;
    Collider2D coll;
    PengaturMusic MusicControl;
    public AudioClip Chest;
    Light2D Light;

    // Start is called before the first frame update
    void Start()
    {
        coinspawn = GameObject.Find("CoinSpawner");
        coinspawn.SetActive(true);
        CoinSpawn = false;
        spr = GetComponent<SpriteRenderer>();
        Light = transform.GetChild(2).GetComponent<Light2D>();
        animator = GetComponent<Animator>();
        coll = GetComponent<Collider2D>();
        coll.enabled = false;
        spr.enabled = false;
        Light.enabled = false;
        MusicControl = GameObject.FindGameObjectWithTag("MusicCon").GetComponent<PengaturMusic>();

        /*
        coinScript = coinspawn.GetComponent<coinspawner>();
        coinScript.enabled = false;
        Coins = transform.GetChild(0).GetComponent<ParticleSystem>();
        */
    }

    public void lvlDone() {
        coll.enabled = true;
        spr.enabled = true;
        Light.enabled = true;
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.CompareTag("Player")) {
            CoinSpawn = true;
        }
        //Physics2D.IgnoreLayerCollision(16,7, true); 
    }

    private void Update() {
        if (CoinSpawn == true) {
            StartCoroutine("ChestOpen");
        } 
    }

    public IEnumerator ChestOpen() {
        animator.SetTrigger("OpenChest");
        MusicControl.Bunyikan(Chest);
        yield return new WaitForSeconds(0.7f);
        coinspawn.SetActive(false);

    }

}
