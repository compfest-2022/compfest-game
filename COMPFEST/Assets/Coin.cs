using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    PengaturMusic MusicControl;
    public AudioClip CoinSound;

    // Start is called before the first frame update
    void Start()
    {
        MusicControl = GameObject.FindGameObjectWithTag("MusicCon").GetComponent<PengaturMusic>();
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.CompareTag("Player")) {
            Player player = other.gameObject.GetComponent<Player>();
            player.CoinUp();
            MusicControl.Bunyikan(CoinSound);
        }
        Destroy(gameObject);
    }
}
