using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LEVEL : MonoBehaviour
{
	public string newLevel;
	public bool CanPass;
	Canvas canvas;
	DestroyLoad dl;
	
	private void Start() {
		CanPass = false;
		canvas = GameObject.FindGameObjectWithTag("Warning").GetComponent<Canvas>();
        canvas.enabled = false;
		dl = GetComponent<DestroyLoad>();
	}

	public void CanPassTrue() {
		CanPass = true;
	}

	void OnCollisionEnter2D (Collision2D player)
	{
		if(player.gameObject.tag == "Player") {
			if (CanPass ==  true) {
				if (dl) {
					SceneManager.LoadScene(newLevel);
					dl.destroyObject();
				} else {
					SceneManager.LoadScene(newLevel);
				}
				
			} if (CanPass == false) {
				StartCoroutine("CanvasVisible");
			}
		}
	}	

	public void changelevel() {
		SceneManager.LoadScene(newLevel);
	}

	public IEnumerator CanvasVisible() {
        canvas.enabled = true;
        yield return new WaitForSeconds(2);
        canvas.enabled = false;
    }
}


