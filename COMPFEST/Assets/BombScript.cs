using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombScript : MonoBehaviour
{
    // Start is called before the first frame update
    float moveSpeed = 15f;
    Rigidbody2D rb;

    GameObject target;
    //Vector2 moveDirection;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        target =  GameObject.FindGameObjectWithTag("Player");
		Vector2 moveDirection = (target.transform.position - transform.position).normalized * moveSpeed;
        rb.velocity = new Vector2 (moveDirection.x, moveDirection.y);
        
    }

    private void Update() {
       
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //transform.position = Vector2.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime); 
        //transform.position = new Vector2(moveDirection.x, moveDirection.y);
        //rb.AddForce(moveDirection * moveSpeed, ForceMode2D.Impulse);
    }

}
