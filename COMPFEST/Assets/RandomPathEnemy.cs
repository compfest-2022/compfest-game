using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class RandomPathEnemy : MonoBehaviour
{
    public Rigidbody2D rb2d;
    AIPath aiPath;
    FlashMaterial flashMaterial;
    GameObject DoorController;
    GameController EnemyCount;
    EnemyHealthBar EnemyBar;

    public AudioClip Hurtsfx;
    public AudioClip Idlesfx;
    PengaturMusic MusicControl;
    float timer;
    float randomTimer;

    public float Health;
    public float strength;
    public int TimetoDie = 1;

    private void Start() {
        flashMaterial = GetComponent<FlashMaterial>();
        aiPath = GetComponent<AIPath>();
        DoorController = GameObject.FindWithTag("GameCon");
        EnemyCount = DoorController.GetComponent<GameController>(); 
        EnemyCount.EnemyCount();
        EnemyBar = transform.GetChild(0).GetComponent<EnemyHealthBar>();
            MusicControl = GameObject.FindGameObjectWithTag("MusicCon").GetComponent<PengaturMusic>();
        randomTimer = 2;
    }

    public void DamageHealth(Vector3 PlayerPos, float Damage) {
        MusicControl.Bunyikan(Hurtsfx);
        if (Health > 0) {
            Health -= Damage;
            Debug.Log("Enemy " + (Health));
            flashMaterial.StartLoop();
            StartCoroutine("Reset", PlayerPos);
            EnemyBar.EnemyPercent = Damage;
            EnemyBar.BarMinus();
        } else if (Health == 0) {
            Health -= Damage;
            Debug.Log("Enemy " + "Damn");
            aiPath.canMove = false;
            flashMaterial.StartLoop();
            EnemyCount.EnemyDecrease();
            StartCoroutine("RemoveEnemy");
        }
    }

    private IEnumerator Reset(Vector3 PlayerPos) {
        aiPath.canMove = false;
        yield return new WaitForSeconds(0.15f);
        aiPath.canMove = true;
    }

    private IEnumerator RemoveEnemy() {
        yield return new WaitForSeconds(TimetoDie);  
        Destroy(transform.root.gameObject);
        Debug.Log("Die");
    }
}
