using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BIGROOMSCRIPT : MonoBehaviour
{
    Transform Player;
    private bool canMove;
    public float MaxX;
    public float MinX;
    public float MaxY;
    public float MinY;
    

    // Start is called before the first frame update
    void Start() {
        Player = GameObject.FindGameObjectWithTag("Player").transform;
        canMove = true;
    }

    // Update is called once per frame
    void Update() {
        transform.position = new Vector3(Mathf.Clamp(Player.position.x, MaxX, MinX), Mathf.Clamp(Player.position.y, MaxY, MinY), transform.position.z);
        
    }
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.CompareTag("Wall")) {
            canMove = false;
            Debug.Log("NotMove");
        }
    }
    
    private void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.CompareTag("Wall")) {
            canMove = true;
            Debug.Log("Move");
        }
    }
}
