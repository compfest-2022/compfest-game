using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Yontōryū : MonoBehaviour
{
    private int BulletAmount = 4;

    private float angleStart = 75, angleEnd = 115;

    Vector2 bulletMoveDirection;
    public GameObject bulletPrefab;
    AIPath aiPath;
    Animator animator;
    public Transform Parent;
    public float timer = 0;
    bool CanMove;
    PengaturMusic MusicControl;
    public AudioClip SwordSound;

    //public float bulletPos = -1;
    //public float bulletSpeed;
    //public Rigidbody2D bulletPrefab; //objek peluru yg dimaksud
	//public GameObject shootPos1;
    //public GameObject shootPos2;
    //public GameObject shootPos3;
    //GameObject target;
    
    // Start is called before the first frame update
    void Start()
    {
        //target =  GameObject.FindGameObjectWithTag("Player");
        animator = transform.root.GetComponent<Animator>();
        aiPath = transform.root.GetComponent<AIPath>();
        CanMove = true;
        MusicControl = GameObject.FindGameObjectWithTag("MusicCon").GetComponent<PengaturMusic>();
    }

    private void Update() {
       timer += Time.deltaTime;
       if (timer >= 4.2 ) {
            animator.SetBool("IsAttack", true);
            Invoke("StopAnimate", 0.6f);
            Invoke("Fire", 0.4f);
            MusicControl.Bunyikan(SwordSound);
            timer = 0;
            CanMove = false;
            Invoke("LockMovement", 0.4f);
       }

       if (CanMove == true) {
            aiPath.canMove = true;
       } else {
            aiPath.canMove = false;
       }
    }

    public void LockMovement() {
        CanMove = true;
    }

    private void Fire() {
        float angleStep = (angleEnd - angleStart) / BulletAmount;
        float angle = angleStart;

        for (int i = 0 ; i < BulletAmount ; i++) {
            float angleMoveDirectionX1 = transform.position.x + Mathf.Sin((angle * Mathf.PI) / 180f);
            float angleMoveDirectionY1 = transform.position.y + Mathf.Cos((angle * Mathf.PI) / 180f);

            Vector3 bulMoveVector = new Vector3 (angleMoveDirectionX1, angleMoveDirectionY1, 0);
            Vector2 BulDir = (bulMoveVector - transform.position).normalized;

            GameObject Bullet = Instantiate(bulletPrefab);
            Bullet.transform.position = transform.position;
            Bullet.transform.rotation = transform.rotation;

            Bullet.GetComponent<Bullet>().SetMoveDirection(BulDir);
            angle += angleStep;
        }
    }
    /*
    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= 4.2 ) {
            //CenterSpike
            animator.SetBool("IsAttack", true);
            Invoke("StopAnimate", 0.4f);
            Vector2 moveDirection = ( target.transform.position - transform.position).normalized;
            Debug.Log(moveDirection);
            float angleMoveDirectionX1 = moveDirection.x + Mathf.Sin((90f * Mathf.PI) / 180f);
            float angleMoveDirectionY1 = moveDirection.y + Mathf.Cos((90f * Mathf.PI) / 180f);
            Vector2 moveAngle1 = new Vector2(angleMoveDirectionX1, angleMoveDirectionY1).normalized;

            Rigidbody2D bPrefab1 = Instantiate(bulletPrefab, shootPos1.transform.position, shootPos1.transform.rotation) as Rigidbody2D;
            bPrefab1.GetComponent<Rigidbody2D>().AddForce((moveDirection * bulletSpeed));
            
            //bPrefab1.transform.SetParent(Parent);
            /*
            //UpperSpikeAt 60 Degree
            float angleMoveDirectionX2 = moveDirection.x + Mathf.Sin((85f * Mathf.PI) / 180f);
            float angleMoveDirectionY2 = moveDirection.y + Mathf.Cos((85f * Mathf.PI) / 180f);
            Vector2 moveAngle2 = new Vector2(angleMoveDirectionX2, angleMoveDirectionY2).normalized;

            Rigidbody2D bPrefab2 = Instantiate(bulletPrefab, shootPos2.transform.position, Quaternion.Euler(new Vector3(0, 0, 9.48f + Parent.transform.localEulerAngles.z))) as Rigidbody2D;
            bPrefab2.GetComponent<Rigidbody2D>().AddForce((moveAngle2 * bulletSpeed));
           //bPrefab2.transform.SetParent(Parent);
            
            //DownSpikeAt 60 Degree
            float angleMoveDirectionX3 = moveDirection.x + Mathf.Sin((95f * Mathf.PI) / 180f);
            float angleMoveDirectionY3 = moveDirection.y + Mathf.Cos((95f * Mathf.PI) / 180f);
            Vector2 moveAngle3 = new Vector2(angleMoveDirectionX3, angleMoveDirectionY3).normalized;

            Rigidbody2D bPrefab3 = Instantiate(bulletPrefab, shootPos3.transform.position, Quaternion.Euler(new Vector3(0, 0, -10.195f + Parent.transform.localEulerAngles.z))) as Rigidbody2D;
            bPrefab3.GetComponent<Rigidbody2D>().AddForce((moveAngle3 * bulletSpeed));
            //bPrefab3.transform.SetParent(Parent);
            
            timer = 0;
            //animator.SetBool("IsAttack", false);
        }
    }
    */

    public void StopAnimate() {
            animator.SetBool("IsAttack", false);
        }
}
