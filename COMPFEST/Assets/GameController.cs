using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    int Enemy;
    public GameObject[] Doors;
    LEVEL levels;
    ChestScript ch;
    Canvas canvas;
    

    // Start is called before the first frame update
    void Start()
    {
        Doors = GameObject.FindGameObjectsWithTag("Door");
        ch = GameObject.FindGameObjectWithTag("Chest").GetComponent<ChestScript>();
        canvas = GameObject.FindGameObjectWithTag("Warning").GetComponent<Canvas>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Enemy == 0) {
            
            foreach (GameObject Door in Doors) {
                levels = Door.GetComponent<LEVEL>();
                levels.CanPassTrue();
            }

            if (ch) {
                ch.lvlDone();
            }
        }

        Debug.Log(Enemy);
    }

    public void EnemyCount() {
        Enemy += 1;
        //Debug.Log("Enemy Count " + Enemy);
    }

    public void EnemyDecrease() {
        Enemy -= 1;
        Debug.Log("Enemy Count " + Enemy);
    }
}
