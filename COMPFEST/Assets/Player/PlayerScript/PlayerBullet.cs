using UnityEngine;
using System.Collections;

public class PlayerBullet : MonoBehaviour {


	public int lifetime = 1;
	private float timer;
	Collider2D BulletCollider;
	Transform goal;
	FungiMonster fungmonster;
	public float Damage;
	//public AudioClip suaranya;
	//public AudioClip EEK;

	private void Start() {
		//BulletCollider = GetComponent<BoxCollider2D>();
		goal = GameObject.FindGameObjectWithTag("Player").transform;
		//StartCoroutine("ColliderOff");
	}

	/*
	IEnumerator ColliderOff() {
		BulletCollider.enabled = false;
		yield return new WaitForSeconds(0.2f);
		BulletCollider.enabled = true;
	}
	*/
	
	void OnCollisionEnter2D(Collision2D target){ //cek tabrakan tanpa is trigger
		if (target.gameObject.CompareTag("Enemy")) {
			Debug.Log("Hit");
            Enemy enemy = target.gameObject.GetComponent<Enemy>();
            if (enemy) {
				enemy.DamageHealth(goal.transform.position, Damage);
			} if (!enemy) {
				Destroy(gameObject);
			}
		} if (target.gameObject.CompareTag("StaticEnemy")) {
			Debug.Log("Hit");
            StaticEnemy Staticenemy = target.gameObject.GetComponent<StaticEnemy>();
            Staticenemy.DamageHealth(goal.transform.position, Damage);
		} if (target.gameObject.CompareTag("FungiMonster")) {
			Debug.Log("Hit");
			FungiMonster fungimonster = target.gameObject.GetComponent<FungiMonster>();
			fungimonster.DamageHealth(goal.transform.position, Damage);
		} if (target.gameObject.CompareTag("RandomPathEnemy")) {
			Debug.Log("Hit");
			RandomPathEnemy randompathenemy = target.gameObject.GetComponent<RandomPathEnemy>();
			randompathenemy.DamageHealth(goal.transform.position, Damage);
		} if (target.gameObject.CompareTag("Boss")) {
			Debug.Log("Hit");
			BossDie bossdie = target.gameObject.GetComponent<BossDie>();
			bossdie.Die();
		}
		Destroy(gameObject); //destroy diri sendiri
	}

	void Update(){
		if(timer == 0) {
			//GameObject.Find("ObjPG").GetComponent<PengaturGame>().Bunyikan(suaranya);
		}
		timer += Time.deltaTime; //count waktu kemunculan
		if(timer > lifetime){ //bila sudah mencapai lifetime			
			Destroy(gameObject); //hancurkan diri sendiri
			timer = 0; //reset timer
		}
	}
}
