using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordAttack : MonoBehaviour
{
    public Collider2D swordCollider;
    public Transform playerPos;
    public float damage = 3;
    Vector2 rightAttackOffset;
    float timer;
    float timer2 = 0;

    private void Start() {
        //playerPos = GetComponent<Transform>();
        rightAttackOffset = playerPos.position - transform.position;
        swordCollider.enabled = false;
        Debug.Log("PLAYER POS" + playerPos.transform.position);
        
    }

    private void Update() {
        timer = Time.deltaTime;
    }

    public void AttackRight() {
        swordCollider.enabled = true;
        transform.localPosition = new Vector3(rightAttackOffset.x * -1, rightAttackOffset.y);
        StartCoroutine("StopAttack");
    }

    public void AttackLeft() {
        swordCollider.enabled = true;
        transform.localPosition = rightAttackOffset;
        StartCoroutine("StopAttack");
    }

     public void AttackUp() {
        swordCollider.enabled = true;
        transform.localPosition = new Vector3(rightAttackOffset.x * 0, rightAttackOffset.y + 1.15f);
        StartCoroutine("StopAttack");
    }
    
     public void AttackDown() {
        swordCollider.enabled = true;
        transform.localPosition = new Vector3(rightAttackOffset.x * 0, rightAttackOffset.y - 1.15f);
        StartCoroutine("StopAttack");
    }
    
    public IEnumerator StopAttack() {
        yield return new WaitForSeconds(1);
        swordCollider.enabled = false;
    }


    private void OnTriggerEnter2D(Collider2D target) {
        if (target.gameObject.CompareTag("Enemy")) {
			Debug.Log("Hit");
            Enemy enemy = target.gameObject.GetComponent<Enemy>();
            enemy.DamageHealth(playerPos.position, 1);
		} if (target.gameObject.CompareTag("StaticEnemy")) {
			Debug.Log("Hit");
            StaticEnemy enemy = target.gameObject.GetComponent<StaticEnemy>();
            enemy.DamageHealth(playerPos.position, 1);
		} if (target.gameObject.CompareTag("FungiMonster")) {
			Debug.Log("Hit");
			FungiMonster fungimonster = target.gameObject.GetComponent<FungiMonster>();
			fungimonster.DamageHealth(playerPos.position, 1);
		} if (target.gameObject.CompareTag("RandomPathEnemy")) {
			Debug.Log("Hit");
			RandomPathEnemy randompathenemy = target.gameObject.GetComponent<RandomPathEnemy>();
			randompathenemy.DamageHealth(playerPos.position, 1);
		}
    }
}
