using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Enemy : MonoBehaviour
{
    public Rigidbody2D rb2d;
    AIPath aiPath;
    FlashMaterial flashMaterial;
    GameObject DoorController;
    GameController EnemyCount;
    EnemyHealthBar EnemyBar;

    public AudioClip Hurtsfx;
    public AudioClip Idlesfx;
    public AudioClip Attacksfx;
    PengaturMusic MusicControl;
    float timer;
    float randomTimer;

    public float Health;
    public float strength;
    public int TimetoDie = 1;

    
    private void Start() {
        flashMaterial = GetComponent<FlashMaterial>();
        aiPath = transform.root.GetComponent<AIPath>();
        DoorController = GameObject.FindWithTag("GameCon");
        EnemyCount = DoorController.GetComponent<GameController>();
        MusicControl = GameObject.FindGameObjectWithTag("MusicCon").GetComponent<PengaturMusic>();
        EnemyCount.EnemyCount();
        EnemyBar = transform.GetChild(0).GetComponent<EnemyHealthBar>();
        randomTimer = 2;
    }

    private void Update() {
        timer += Time.deltaTime;

        if (timer == randomTimer) {
            MusicControl.Bunyikan(Idlesfx);
            timer = 0;
            randomTimer = Random.Range(3, 10);
        }
    }


    public void DamageHealth(Vector3 PlayerPos, float Damage) {
        MusicControl.Bunyikan(Hurtsfx);
        if (!aiPath) {
            if (Health > 0) {
                Health -= Damage;
                Debug.Log("Enemy " + (Health));
                flashMaterial.StartLoop();
                StartCoroutine("Reset", PlayerPos);
                EnemyBar.EnemyPercent = Damage;
                EnemyBar.BarMinus();
            } else if (Health == 0) {
                Health -= Damage;
                Debug.Log("Enemy " + "Damn");
                flashMaterial.StartLoop();
                EnemyCount.EnemyDecrease();
                Destroy(transform.root.gameObject);
                //StartCoroutine("RemoveEnemy");
            }
        } if (aiPath) {
            if (Health > 0) {
                Health -= Damage;
                Debug.Log("Enemy " + (Health));
                flashMaterial.StartLoop();
                StartCoroutine("Reset", PlayerPos);
                EnemyBar.EnemyPercent = Damage;
                EnemyBar.BarMinus();
            } else if (Health == 0) {
                Health = -2;
                aiPath.canMove = false;
                Debug.Log("Enemy " + "Damn");
                EnemyCount.EnemyDecrease();
                flashMaterial.StartLoop();
                Destroy(transform.root.gameObject);
                //StartCoroutine("RemoveEnemy");
            }
        }
    }

    
    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Player")) {
            if (Attacksfx) {
                MusicControl.Bunyikan(Attacksfx);
            }
        }
        Physics2D.IgnoreLayerCollision(8,3, true);
        Physics2D.IgnoreLayerCollision(8,9, true);
        Physics2D.IgnoreLayerCollision(8,11, true);
        Physics2D.IgnoreLayerCollision(8,12, true);

    }

    private IEnumerator Reset(Vector3 PlayerPos) {
        if (!aiPath) {
            Vector2 direction = (transform.position - PlayerPos).normalized;
            rb2d.AddForce(direction * strength, ForceMode2D.Impulse);
            rb2d.velocity = Vector3.zero;
            yield return new WaitForSeconds(0.15f);
            rb2d.velocity = Vector3.zero;
        } if (aiPath) {
            aiPath.canMove = false;
            Vector2 direction = (transform.position - PlayerPos).normalized;
            rb2d.AddForce(direction * strength, ForceMode2D.Impulse);
            rb2d.velocity = Vector3.zero;
            yield return new WaitForSeconds(0.15f);
            rb2d.velocity = Vector3.zero;
            aiPath.canMove = true;
        }
    }

    private IEnumerator RemoveEnemy() {
        yield return new WaitForSeconds(TimetoDie);  
        Destroy(transform.root.gameObject);
        Debug.Log("Die");
    }
}
