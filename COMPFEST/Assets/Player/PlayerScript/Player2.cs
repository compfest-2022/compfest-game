using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.InputSystem;

// Takes and handles input and movement for a player character
public class Player2 : MonoBehaviour
{
    public float moveSpeed = 5f;
    Rigidbody2D rb;
    Vector2 movementInput;
    SpriteRenderer spriteRenderer;
    public SwordAttack swordAttack;
    FlashMaterial flashMaterial;
    public ShootPos shootPos;
    public GameObject Player;
    bool InRadius;
    float Rotation;
    public Collider2D collider2d;
    float Health;
    float ticktime;
    float ticktime2;
    
    Animator animator;

    private string WeaponType;
    private string WeaponTypeTag;

    bool canMove = true;

    HealthCounter healthsys;
    GameObject health;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        collider2d = GetComponent<BoxCollider2D>();
        flashMaterial = GetComponent<FlashMaterial>();

        WeaponType = "Pistol";
        Rotation = 1;
    }

    private void Update() {
        movementInput.x = Input.GetAxisRaw("Horizontal");
        movementInput.y = Input.GetAxisRaw("Vertical");

        if (Input.GetKeyDown("e") && InRadius == true) {
            WeaponType = WeaponTypeTag;
        }

        if (WeaponType == "Sword") {
            if (Input.GetKeyDown(KeyCode.RightArrow)) {
                swordAttack.AttackRight();
                animator.SetTrigger("swordAttackRight");
            } if (Input.GetKeyDown(KeyCode.LeftArrow)) {
                swordAttack.AttackLeft();
                animator.SetTrigger("swordAttackLeft");
            } if (Input.GetKeyDown(KeyCode.UpArrow)) {
                swordAttack.AttackUp();
                animator.SetTrigger("SwordAttackYaxisUp");
            } if (Input.GetKeyDown(KeyCode.DownArrow)) {
                swordAttack.AttackDown();
                animator.SetTrigger("SwordAttackYaxisDown");
            }
        } if (WeaponType == "Pistol") {
            if (Input.GetKeyDown(KeyCode.RightArrow)) {
                if (movementInput.x == -1 || movementInput.y == -1) {
                    shootPos.GunRight();
                    StartCoroutine("FlipXY");
                } if (movementInput.x == 1 || movementInput.sqrMagnitude == 0 || movementInput.y == 1) {
                    shootPos.GunRight();
                    StartCoroutine("NotFlipX");
                }
            } if (Input.GetKeyDown(KeyCode.LeftArrow)) {
                if (movementInput.x == 1 || movementInput.y == 1) {
                    shootPos.GunLeft();
                    StartCoroutine("FlipXY");
                } if (movementInput.x == -1 || movementInput.sqrMagnitude == 0 || movementInput.y == -1) {
                    shootPos.GunLeft();
                    StartCoroutine("NotFlipX");
                }
            } if (Input.GetKeyDown(KeyCode.UpArrow)) {
                if (movementInput.y == -1 || movementInput.x == -1) {
                    shootPos.GunUp();
                    StartCoroutine("FlipXY");
                } if (movementInput.y == 1 || movementInput.sqrMagnitude == 0 || movementInput.x == 1) {
                    shootPos.GunUp();
                    StartCoroutine("NotFlipY");
                }
            } if (Input.GetKeyDown(KeyCode.DownArrow)) {
                if (movementInput.y == 1 || movementInput.x == 1) {
                    shootPos.GunDown();
                    StartCoroutine("FlipXY");
                } if (movementInput.y == -1 || movementInput.sqrMagnitude == 0 || movementInput.x == -1) {
                    shootPos.GunDown();
                    StartCoroutine("NotFlipY");
                }
            } 
            //Debug.Log("Pistol bang bang");
        }
    }

    private void FixedUpdate() {
        if (canMove) {
            rb.MovePosition(rb.position + movementInput * moveSpeed * Time.deltaTime);

            animator.SetFloat("Horizontal", movementInput.x * Rotation);
            animator.SetFloat("Vertical", movementInput.y * Rotation);

            animator.SetFloat("speed", movementInput.sqrMagnitude);
        } else  {
            animator.SetFloat("speed", movementInput.magnitude);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Sword") {
            InRadius = true;
            WeaponTypeTag = "Sword"; 
        } if (other.gameObject.tag == "Pistol") {
            InRadius = true;
            WeaponTypeTag = "Pistol";
        }
    }

    private IEnumerator FlipXY() {
        Rotation = -1;
        yield return new WaitForSeconds(0.2f);
        Rotation = 1;
    }

    private IEnumerator NotFlipX() {
        animator.SetFloat("Horizontal",  movementInput.x);
        yield return new WaitForSeconds(0.2f);
    }

    private IEnumerator NotFlipY() {
        animator.SetFloat("Horizontal",  movementInput.y);
        yield return new WaitForSeconds(0.2f);
    }

    public void LockMovement() {
        canMove = false;
    }

    public void EndSwordAttack() {
        UnlockMovement();
        swordAttack.StopAttack();
    }

     public void UnlockMovement() {
        canMove = true;
    }


}
