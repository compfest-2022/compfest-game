﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScriptPengaturGame : MonoBehaviour
{
    public int respawnTime=2;
    public string level1;
    string respawnlevel;
    public GameObject[] DontDestroyObject;

    private void Start() {
        blockArray();
        if (level1 == "") {
            respawnlevel = "Scene Outdoor 1";
        } else {
            respawnlevel = level1;
        }
    }

    private void Update() {
        Debug.Log(level1);
        Debug.Log(respawnlevel);
    }

    private void Awake() {
        blockArray();
    }

    public void Respawning(){
        StartCoroutine(DelayedRespawn()); //menjalankan coroutine, yaitu untuk delay execution script
    }

    // Start is called before the first frame update
    IEnumerator DelayedRespawn(){
		yield return new WaitForSeconds(respawnTime); //delay bbrp second sesuai variabel
        for (int i = 0; i < DontDestroyObject.Length; i++) {
            Destroy(DontDestroyObject[i]);
        }
		SceneManager.LoadScene(respawnlevel);
	}

     void blockArray()
         {
            DontDestroyObject = new GameObject[4];
            DontDestroyObject[0] = GameObject.FindGameObjectWithTag("Warning");
            DontDestroyObject[1] = GameObject.FindGameObjectWithTag("Music");
            DontDestroyObject[2] = GameObject.FindGameObjectWithTag("MusicCon");
            DontDestroyObject[3] = GameObject.FindGameObjectWithTag("EventSystem");
         }
}   
