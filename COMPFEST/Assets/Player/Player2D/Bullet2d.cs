using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet2d : MonoBehaviour
{
    public float speed = 20f;
    public Rigidbody2D rb;
    public float damage;
    float timer;
    float timer2;
    float timer3;

    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.right * speed;
    }

    private void Update() {
        Debug.Log(damage);
        timer += Time.deltaTime; //count waktu kemunculan
        timer2 += Time.deltaTime;
        timer3 += Time.deltaTime;
        if(timer > 1){ //bila sudah mencapai lifetime			
			timer = 0; //reset timer
			Destroy(gameObject); //hancurkan diri sendiri
		}
    }

    void OnCollisionEnter2D(Collision2D hitInfo) {
        Debug.Log(hitInfo);
        if(hitInfo.gameObject.CompareTag("Enemy")) {
            AIPatrol enemy = hitInfo.gameObject.GetComponent<AIPatrol>();
            Debug.Log("Kena Musuh");
            if (timer2 > 0.01f) {
                enemy.TakeDamage(damage);
                timer2 = 0;
            }
            Destroy(this.gameObject);
        }
        Destroy(this.gameObject);
    }
}
