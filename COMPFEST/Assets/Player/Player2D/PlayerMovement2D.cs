using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement2D : MonoBehaviour
{

    public Char2DController controller;
    public Animator animator;

    float horizontalMove = 1;
    public float runSpeed = 40f;
    bool jump = false;
    // bool crouch = false;

    // Buat Dash 1
    // private Rigidbody2D rb;
    // public float dashSpeed;
    // private float dashTime;
    // public float startDashTime;
    // private int direction;

    // Buat Dash 2
    // [Header("Dashing")]
    // [SerializeField] private float _dashingVelocity = 14f;
    // [SerializeField] private float _dashingTime = 0.5f;
    // [SerializeField] private Transform _groundCheck;
    // [SerializeField] private float _groundCheckRadius = 0.05f;
    // [SerializeField] private LayerMask _collisionMask;
    // private Vector2 _dashingDir;
    // private bool _isDashing;
    // private bool _canDash = true;
    // private TrailRenderer _trailRenderer;
    // private Rigidbody2D rb;

    // Buat Dash 3
    // private float activeMoveSpeed;
    // public float dashSpeed;
    // public float dashLength = 0.5f, dashCooldown = 1f;
    // private float dashCounter;
    // private float dashCoolCounter;
    // public Rigidbody2D rb2d;
    // private Vector2 moveInput;

    // Buat Dash 4
    // private bool canDash = true;
    // private bool isDashing;
    // public float dashingPower = 5f;
    // private float dashingTime = 0.1f;
    // private float dashingCooldown = 1f;
    // public Rigidbody2D rb;

    void Start()
    {
        // Dash 1
        // rb = GetComponent<Rigidbody2D>();
        // dashTime = startDashTime;

        // Dash 2
        // _trailRenderer = GetComponent<TrailRenderer>();

        // Dash 3
        // activeMoveSpeed = runSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        // Jalan kanan kiri
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        // Animasi
        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));
        
        // Lompat
        if(Input.GetButtonDown("Jump"))
        {
            Debug.Log("Jump Buttons Working");
            jump = true;
            animator.SetBool("IsJumping", true);
        }

        
        // Dash 1
        // if(direction == 0)
        // {
        //     if(Input.GetKeyDown(KeyCode.LeftArrow)) {
        //         direction = 1;
        //     } else if(Input.GetKeyDown(KeyCode.RightArrow)) {
        //         direction = 2;
        //     } else if(Input.GetKeyDown(KeyCode.UpArrow)) {
        //         direction = 3;
        //     } else if(Input.GetKeyDown(KeyCode.DownArrow)) {
        //         direction = 4;
        //     }
        // } else {
        //     if(direction <= 0) {
        //         direction = 0;
        //         dashTime = startDashTime;
        //         rb.velocity = Vector2.zero;
        //     } else {
        //         dashTime -= Time.deltaTime;
                
        //         if(direction == 1) {
        //             rb.velocity = Vector2.left * dashSpeed;
        //         } else if(direction == 2) {
        //             rb.velocity = Vector2.right * dashSpeed;
        //         } else if(direction == 3) {
        //             rb.velocity = Vector2.up * dashSpeed;
        //         } else if(direction == 4) {
        //             rb.velocity = Vector2.down * dashSpeed;
        //         }
        //     }
        // }

        // Dash 2
        // var dashInput = Input.GetButtonDown("Dash");
        // if(dashInput && _canDash) {
        //     _isDashing = true;
        //     _canDash = false;
        //     _trailRenderer.emitting = true;
        //     _dashingDir = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        //     if(_dashingDir == Vector2.zero) {
        //         _dashingDir = new Vector2(transform.localScale.x, 0);
        //     }
        //  // Add Stopping Dash
        //     StartCoroutine(StopDashing());
        // }
        // if(_isDashing) {
        //     rb.velocity = _dashingDir.normalized * _dashingVelocity;
        //     return;
        // }
        // if(IsGrounded()){
        //     _canDash = true;
        // }

        // Dash 3
        // rb2d.velocity = moveInput * activeMoveSpeed;
        // if(Input.GetButtonDown("Dash")) {
        //     Debug.Log("LeftShift Button Working");
        //     if(dashCoolCounter <= 0 && dashCounter <= 0){
        //         activeMoveSpeed = dashSpeed;
        //         dashCounter = dashLength;
        //     }
        // }
        // if(dashCounter > 0) {
        //     dashCounter -= Time.deltaTime;

        //     if(dashCounter <= 0) {
        //         activeMoveSpeed = runSpeed;
        //         dashCoolCounter = dashCooldown;
        //     }
        // }
        // if(dashCoolCounter > 0) {
        //     dashCoolCounter -= Time.deltaTime;
        // }

        // Dash 4
        // if(isDashing) {
        //     return;
        // }
        // Aktifkan Dash 4
        // if(Input.GetKeyDown(KeyCode.LeftShift) && canDash) {
        //     StartCoroutine(Dash());
        // }


        // Crouch
        // if(Input.GetButtonDown("Crouch"))
        // {
        //     Debug.Log("Crouch Buttons Working");
        //     crouch = true;
        // } else if(Input.GetButtonDown("Crouch"))
        // {
        //     crouch = false;
        // }
    }

    // Dash 2
    // private IEnumerator StopDashing() {
    //     yield return new WaitForSeconds(_dashingTime);
    //     _trailRenderer.emitting = false;
    //     _isDashing = false;
    // }
    // private bool IsGrounded() {
    //     // Check player menyentuh tanah
    //     return Physics2D.OverlapCircle(_groundCheck.position, _groundCheckRadius, _collisionMask);
    // }

    public void OnLanding()
    {
        // Jika animasi menempel di tanah dia akan berubah ke animasi "idle" atau "run"
        animator.SetBool("IsJumping", false);
    }

    void FixedUpdate()
    {
        // Crouch
        // controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
        controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
        jump = false;

        // Dash 4
        // if(isDashing) {
        //     return;
        // }
    }

    // Dash 4
    // private IEnumerator Dash() {
    //     canDash = false;
    //     isDashing = true;
    //     float originalGravity = rb.gravityScale;
    //     rb.gravityScale = 0f;
    //     rb.velocity = new Vector2(transform.localScale.x * dashingPower, 0f);
    //     yield return new WaitForSeconds(dashingTime);
    //     rb.gravityScale = originalGravity;
    //     isDashing = false;
    //     yield return new WaitForSeconds(dashingCooldown);
    //     canDash = true;
    // }
}
