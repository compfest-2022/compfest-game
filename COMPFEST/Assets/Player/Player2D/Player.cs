using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{
    public TextMeshProUGUI teks_skor;
   // public TextMeshProUGUI teks_Health;
    public  static int HealthSkrg = 4;   
    public static int Coinskrng = 0;
    public static float Damage;
    public GameObject Heart1;
    public GameObject Heart2;
    public GameObject Heart3;
    //public AudioClip BoneCrack;
    private float time = 0.0f;
    public float interpolationPeriod = 0.1f;
    //public TextMeshProUGUI price;
    public static int PRICEUP = 50;
    float ticktime;
    float ticktime2;
    Rigidbody2D rb2d;
    FlashMaterial flashMaterial;
    public Animator animator;
    public float timer;
    PengaturMusic MusicControl;
    public AudioClip PlayerHurt;	
    public AudioClip PotionGet;

    //public AudioClip Heals;

    void Start() {
        teks_skor.text=Coinskrng.ToString();
        //teks_Health.text=HealthSkrg.ToString();
        //price.text=PRICEUP.ToString();
        rb2d = GetComponent<Rigidbody2D>();
        MusicControl = GameObject.FindGameObjectWithTag("MusicCon").GetComponent<PengaturMusic>();
        flashMaterial = GetComponent<FlashMaterial>();
        Physics2D.IgnoreLayerCollision(7,8, false); 
        Physics2D.IgnoreLayerCollision(7,9, false);
        Physics2D.IgnoreLayerCollision(7,11, false);
        Physics2D.IgnoreLayerCollision(7,12, false);
        Damage = 1;
    }

    /*
    public void TambahSkor(int skornya) {
        skorSkrg+=skornya;
        teks_skor.text=skorSkrg.ToString();
        rb2d = GetComponent<Rigidbody2D>();
        flashMaterial = GetComponent<FlashMaterial>();
    }
    */

    void Update () {
        Debug.Log("Health " + HealthSkrg);
        time += Time.deltaTime;
        timer += Time.deltaTime;
        
        if (time >= interpolationPeriod) {
            time = 0.0f;

            if (HealthSkrg == 1) {
               Heart3.SetActive(false);
               Heart2.SetActive(false);
               Heart1.SetActive(false);
            }
            if (HealthSkrg == 2) {
                Heart2.SetActive(false);
                Heart1.SetActive(false);
            }
            if (HealthSkrg == 3) {
                Heart1.SetActive(false);
            }
            //teks_skor.text=skorSkrg.ToString();
            //price.text=PRICEUP.ToString();
         // execute block of code here
        }  
    }

    void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("FungiMonster") || other.gameObject.CompareTag("EnemyBullet") || other.gameObject.CompareTag("RandomPathEnemy") || other.gameObject.CompareTag("Boss")) {
            if (timer > 0.1f) {
                StartCoroutine("Die");
                timer = 0;
            }
            Physics2D.IgnoreLayerCollision(7,8, true);
            Physics2D.IgnoreLayerCollision(7,9, true);
            Physics2D.IgnoreLayerCollision(7,11, true);
            Physics2D.IgnoreLayerCollision(7,12, true);
            animator.SetBool("IsHurt", false);
            //collider2d.enabled = false; 
        }
        Physics2D.IgnoreLayerCollision(7,12, true);
        if (other.gameObject.CompareTag("PotionDemeg")) {
            Price priceSc = other.gameObject.GetComponent<Price>();
            if (Coinskrng >= priceSc.price) {
                Damage = 1.5f;
                Destroy (other.gameObject);
                MusicControl.Bunyikan(PotionGet);
                Coinskrng = Coinskrng - priceSc.price;
                teks_skor.text=Coinskrng.ToString();
            }// else Physics2D.IgnoreLayerCollision(7, 19, true);
        }
        if (other.gameObject.CompareTag("PotionHelt")) {
            Price priceSc = other.gameObject.GetComponent<Price>();
            if (Coinskrng >= priceSc.price) {
                if (HealthSkrg < 4) {
                    addHelt();
                    MusicControl.Bunyikan(PotionGet);
                    Destroy(other.gameObject);
                    Coinskrng = Coinskrng - priceSc.price;
                    teks_skor.text=Coinskrng.ToString();
                    if (HealthSkrg == 2) {
                        Heart3.SetActive(true);
                    }
                    if (HealthSkrg == 3) {
                        Heart2.SetActive(true);
                        Heart3.SetActive(true);
                    }
                    if (HealthSkrg == 4) {
                        Heart3.SetActive(true);
                        Heart2.SetActive(true);
                        Heart1.SetActive(true);
                    }
                    } else {
                        Destroy(other.gameObject);
                        MusicControl.Bunyikan(PotionGet);
                        Coinskrng = Coinskrng - priceSc.price;
                        teks_skor.text=Coinskrng.ToString();
                    }
            } //else Physics2D.IgnoreLayerCollision(18, 7, true);
        }
    }

    IEnumerator OnCollisionExit2D(Collision2D other) {
        if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("StaticEnemy") || other.gameObject.CompareTag("FungiMonster") || other.gameObject.CompareTag("EnemyBullet") || other.gameObject.CompareTag("RandomPathEnemy") || other.gameObject.CompareTag("Boss")) {
            yield return new WaitForSeconds(1);
            Physics2D.IgnoreLayerCollision(7,8, false); 
            Physics2D.IgnoreLayerCollision(7,9, false);
            Physics2D.IgnoreLayerCollision(7,12, false);
            Physics2D.IgnoreLayerCollision(7,11, false);
            Physics2D.IgnoreLayerCollision(7, 19, false);
        }
    }

    void OnTriggerStay2D(Collider2D other) {
        
        if (other.gameObject.CompareTag("ToxicArea") || other.gameObject.CompareTag("Spike") || other.gameObject.CompareTag("EnemyBullet")) {
            HurtTime();
        }
    }

    public void isExplode() {
            ticktime -= Time.deltaTime;
            if (ticktime <= 0) {
                StartCoroutine("Die");
                flashMaterial.StartLoop();
                ticktime = 1.0f;
                
            }
    }

    public void HurtTime() {
        //Debug.Log(Health + " Player");
            ticktime -= Time.deltaTime;
            flashMaterial.StartLoop();
            if (ticktime <= 0) {
                StartCoroutine("Die");
                ticktime = 1.0f;
                Debug.Log("AAA");
            }
            animator.SetBool("IsHurt", false);
        }

    public void addHelt() {
        HealthSkrg = HealthSkrg + 1;
    }

    /*
    public void buyanlifefromdemon() {
        if (HealthSkrg < 4 && skorSkrg >= PRICEUP) {
            HealthSkrg = HealthSkrg + 1;
            //GameObject.Find("ObjPG").GetComponent<PengaturGame>().Bunyikan(Heals);
            skorSkrg = skorSkrg - PRICEUP;
            PRICEUP += 20;
            

            if (HealthSkrg == 2) {
               Heart3.SetActive(true);
            }
            if (HealthSkrg == 3) {
                Hearta2.SetActive(true);
                Heart3.SetActive(true);
            }
            if (HealthSkrg == 4) {
                Heart3.SetActive(true);
                Heart2.SetActive(true);
                Heart1.SetActive(true);
            }
        }
    }
    */

    IEnumerator Die(){		
		GameObject go = GameObject.Find("PengaturGame"); //cari gameobject bernama pengaturgame pada hierarchy
        ScriptPengaturGame pg = (ScriptPengaturGame)go.GetComponent(typeof(ScriptPengaturGame));//ambil scriptpengaturgame
		//memakai metode respawning yang sudah public
		if (HealthSkrg > 0) {
            HealthSkrg = HealthSkrg - 1;
            MusicControl.Bunyikan(PlayerHurt);
            //teks_Health.text=HealthSkrg.ToString();
            flashMaterial.StartLoop();
            if (HealthSkrg == 1) {
                Heart3.SetActive(false);
                //GameObject.Find("ObjPG").GetComponent<PengaturGame>().Bunyikan(BoneCrack);
            }
            if (HealthSkrg == 2) {
                Heart2.SetActive(false);
                //GameObject.Find("ObjPG").GetComponent<PengaturGame>().Bunyikan(BoneCrack);
            }
            if (HealthSkrg == 3) {
                Heart1.SetActive(false);
                //GameObject.Find("ObjPG").GetComponent<PengaturGame>().Bunyikan(BoneCrack);
            }
        }
        if (HealthSkrg == 0 ) {
            animator.SetTrigger("IsDie");
            rb2d.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY;
            rb2d.freezeRotation = true; 
            yield return new WaitForSeconds(2);
            Destroy (gameObject);
            pg.Respawning(); 
            ResetHealth();
            ResetCoin();
        }
	}

    public void CoinUp() {
        Coinskrng += 1;
        teks_skor.text = Coinskrng.ToString();
    }

    public void ResetCoin() {
        Coinskrng = 0;
    }

    public void ResetHealth() {
		HealthSkrg = 4;
	}

    public void ResetPrice() {
        PRICEUP = 50;
    }

}
