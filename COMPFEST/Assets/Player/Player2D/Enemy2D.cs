using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2D : MonoBehaviour
{
    public float health = 100;
    public Animator anim;
    public Transform firePoint;
    public GameObject bulletPrefab;
    float timer = 1;
    float delay = 1;

    private void Start() {
        anim = GetComponent<Animator>();
    }

    private void Update() {
        timer += Time.deltaTime;
        //Debug.Log(timer);
        if(timer > delay) {
            Debug.Log("Enemy Shoot");
            Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            timer = 0;
        }
    }

    // public GameObject deathEffect;
    public void TakeDamage(float damage) {
        health -= damage;

        if(health <= 0){
            StartCoroutine(Die());
        }
    }

    IEnumerator Die() {
        // Instantiate(deathEffect, transform.position, Quaternion.identity);
        anim.SetBool("deathEffect", true);
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }
}
