using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public Transform firePoint;
    public GameObject bulletPrefab;
    public Player player;
    public float timer;
    public float Damage;
    public AudioClip Shootsfx;
    PengaturMusic MusicControl;

    private void Start() {
        player.GetComponent<Player>();
        MusicControl = GameObject.FindGameObjectWithTag("MusicCon").GetComponent<PengaturMusic>();
    }

    // Update is called once per frame
    void Update()
    {
        Damage = Player.Damage;
        timer += Time.deltaTime;

        if(Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow)) {
            Shoot();
        }
    }

    void Shoot() {
        // Shooting Logic
        if (timer > 0.3f) {
            bulletPrefab.GetComponent<Bullet2d>().damage = Damage;
            Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            MusicControl.Bunyikan(Shootsfx);
            timer = 0;
        }
    }
}
