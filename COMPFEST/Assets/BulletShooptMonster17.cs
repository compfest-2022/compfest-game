using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShooptMonster17 : MonoBehaviour
{
    private int BulletAmount = 3;

    private float angleStart = 75, angleEnd = 115;

    Vector2 bulletMoveDirection;
    public GameObject bulletPrefab;
    Animator animator;
    public Transform Parent;
    public float timer = 0;
    PengaturMusic MusicControl;
    public AudioClip SwordSound;
    
    // Start is called before the first frame update
    void Start()
    {
        //target =  GameObject.FindGameObjectWithTag("Player");
        animator = Parent.GetComponent<Animator>();
        MusicControl = GameObject.FindGameObjectWithTag("MusicCon").GetComponent<PengaturMusic>();
    }

    private void Update() {
       timer += Time.deltaTime;
       if (timer >= 4.2 ) {
        animator.SetBool("IsAttack", true);
        Invoke("StopAnimate", 0.6f);
        Invoke("Fire", 0.4f);
        MusicControl.Bunyikan(SwordSound);
        timer = 0;
       }
    }

    private void Fire() {
        float angleStep = (angleEnd - angleStart) / BulletAmount;
        float angle = angleStart;

        for (int i = 0 ; i < BulletAmount ; i++) {
            float angleMoveDirectionX1 = transform.position.x + Mathf.Sin((angle * Mathf.PI) / 180f);
            float angleMoveDirectionY1 = transform.position.y + Mathf.Cos((angle * Mathf.PI) / 180f);

            Vector3 bulMoveVector = new Vector3 (angleMoveDirectionX1, angleMoveDirectionY1, 0);
            Vector2 BulDir = (bulMoveVector - transform.position).normalized;

            GameObject Bullet = Instantiate(bulletPrefab);
            Bullet.transform.position = transform.position;
            Bullet.transform.rotation = transform.rotation;

            Bullet.GetComponent<Bullet>().SetMoveDirection(BulDir);
            angle += angleStep;
        }
    }

    public void StopAnimate() {
            animator.SetBool("IsAttack", false);
        }
}
