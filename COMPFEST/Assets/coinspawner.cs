using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coinspawner : MonoBehaviour
{
    public GameObject coin;

    public Transform chest;
    public float MinX;
    public float MaxX;
    public float MinY;
    public float MaxY;
    public float rangeX;
    public float rangeY;
    public Vector3 coinpos;
    int many;
    public ParticleSystem Coins;


    /// <summary>
    /// This function is called when the object becomes enabled and active.
    /// </summary>
    private void OnDisable() {
        onCoin();
    }

    public void Start() {
        Coins = GameObject.Find("CoinParticle").GetComponent<ParticleSystem>();
        MinX = chest.transform.position.x - 2.5f;
        MaxX = chest.transform.position.x + 2.5f;
        MinY = chest.transform.position.y;
        MaxY = chest.transform.position.y - 2.5f;
        Debug.Log(chest.transform.position);

        many = Random.Range(1, 7);
        CalculateCoinPos();
    }

    void onCoin() {
        Coins.Play();
        //yield return new WaitForSeconds(0.9f);
        Invoke("SpawmCoim", 0.9f);
    }

    public void SpawmCoim() {
        for (int i = 0; i < many; i++) {
            GameObject coinspawn = Instantiate(coin, coinpos, transform.rotation);
            coinspawn.transform.parent = chest;
            CalculateCoinPos();
        }
    }

    

    public void CalculateCoinPos() {
        rangeX = Random.Range(MinX, MaxX);
        rangeY = Random.Range(MinY, MaxY);

        coinpos = new Vector3 (rangeX, rangeY, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
