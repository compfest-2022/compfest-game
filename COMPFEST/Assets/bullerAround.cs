using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class bullerAround : MonoBehaviour
{
    private float angle = 20f;
    public GameObject bulletPrefab;
    float timer;
    private Animator animator;
    AIPath aiPath;
    bool CanMove;
    Rigidbody2D rb2d;
    public bool isStatic;
    PengaturMusic MusicControl;
    public AudioClip ShootSound;
    // Start is called before the first frame update
    void Start()
    {
        CanMove = true;
        timer = 5;
        animator = GetComponent<Animator>();
        aiPath = GetComponent<AIPath>();
        rb2d = GetComponent<Rigidbody2D>();
        MusicControl = GameObject.FindGameObjectWithTag("MusicCon").GetComponent<PengaturMusic>();
    }


    // Update is called once per frame
    void Update() {
        timer += Time.deltaTime;

       // rb2d.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY;
        rb2d.freezeRotation = true; 

        if (timer >= 10 ) {
            //animator.SetBool("IsAttack", true);
            //Invoke("StopAnimate", 0.6f);
            StartCoroutine("FireAction");
            timer = 0;
            //Invoke("Fire", 0.6f);
       } 

       if (CanMove == true) {
            aiPath.canMove = true;
       } else {
            aiPath.canMove = false;
       }
    }

    public IEnumerator FireAction() {
        animator.SetBool("IsAttack", true);
        rb2d.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY;
        rb2d.freezeRotation = true; 
        CanMove = false;
        InvokeRepeating("Fire", 0f, 0.1f);
        yield return new WaitForSeconds(5);
        animator.SetBool("IsAttack", false);
        CancelInvoke(); 
        if (isStatic == true) {
            rb2d.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY;
            rb2d.freezeRotation = true; 
        } if (isStatic == false) {
            rb2d.constraints = RigidbodyConstraints2D.None;
            rb2d.freezeRotation = true; 
        }
        CanMove = true; 
    }

    

    private void Fire() {
        MusicControl.Bunyikan(ShootSound);
        float BulDirX = transform.position.x + Mathf.Sin((angle * Mathf.PI) / 180f);
        float BulDirY = transform.position.y + Mathf.Cos((angle * Mathf.PI) / 180f);

        Vector3 BulMove = new Vector3(BulDirX, BulDirY, 0f);
        Vector2 BulDir = (BulMove - transform.position).normalized;

        GameObject Bullet = Instantiate(bulletPrefab);
        Bullet.transform.position = transform.position;
        Bullet.transform.rotation = transform.rotation;

        Bullet.GetComponent<Bullet>().SetMoveDirection(BulDir);
        angle += 20f;
    }

    private void TimerReset() {
        timer = 0;
    }
}
