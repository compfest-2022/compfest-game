using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletRotationToPlayer : MonoBehaviour
{

    Transform Player;
    Transform Parent;
    // Start is called before the first frame update
    void Start()
    {
        /*
        Parent = GetComponentInParent<Transform>();
        Debug.Log(Parent.transform.localEulerAngles.z);
        transform.rotation = Quaternion.Euler(0, 0, Parent.transform.localEulerAngles.z);
        */

        Player = GameObject.FindGameObjectWithTag("Player").transform;
         
    }

    // Update is called once per frame
    void Update()
    {
         Vector3 difference = Player.position - transform.position;
        float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotationZ);
        //transform.rotation = Quaternion.Euler(0, 0, Parent.transform.localEulerAngles.z * -1);
    }
}
