using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelUp : MonoBehaviour
{
	public string newLevel;

	void OnCollisionEnter2D (Collision2D player)
	{
		if(player.gameObject.tag == "Player")
		{
			SceneManager.LoadScene(newLevel);
		}
	}	
}


