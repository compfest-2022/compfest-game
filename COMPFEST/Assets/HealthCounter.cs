using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HealthCounter : MonoBehaviour
{
    public static float Health;
    public TextMeshProUGUI teks_Health;

    // Start is called before the first frame update
    void Start()
    {
        teks_Health.text = Health.ToString();
    }

    public void updateHealth(float health) {
        Health = health;
        teks_Health.text = Health.ToString();

        if (Health == 0) {
            Destroy(this.gameObject);
        }   
    }

    private void Update() {
        Debug.Log(Health);
    }

    
}
