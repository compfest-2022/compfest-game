using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class ParticleStart : MonoBehaviour
{
    ParticleSystem Eyes;
    ParticleSystem Toxic;
    Animator animator;
    SpriteRenderer spriteRenderer;
    SpriteRenderer spriteRenderer2;
    Collider2D coll;
    GameObject DoorController;
    GameController EnemyCount;
    AIPath aiPath;

    // Start is called before the first frame update
    void Start() {
        Eyes = transform.GetChild(0).GetComponent<ParticleSystem>();
        Toxic = transform.GetChild(1).GetComponent<ParticleSystem>();
        animator =  gameObject.GetComponentInParent<Animator>();
        spriteRenderer2 = transform.GetChild(2).GetComponent<SpriteRenderer>();
        coll = transform.GetChild(2).GetComponent<Collider2D>();
        spriteRenderer = transform.GetComponentInParent<SpriteRenderer>();
        spriteRenderer2.enabled = false;
        coll.enabled = false;
        DoorController = GameObject.FindWithTag("GameCon");
        EnemyCount = DoorController.GetComponent<GameController>();
        aiPath = transform.GetComponentInParent<AIPath>();
    }

    private void OnEnable() {
        StartCoroutine("Dying");
        aiPath.maxSpeed = 0;
    }

    private IEnumerator Dying() {
        animator.SetTrigger("IsDie");
        yield return new WaitForSeconds(2);
        Eyes.Play();
        Toxic.Play();
        spriteRenderer.enabled = false;
        spriteRenderer2.enabled  =true;
        coll.enabled = true;
        yield return new WaitForSeconds(5);
        Destroy(transform.root.gameObject);
        //EnemyCount.EnemyDecrease();
    }
}
