using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class SwitchHandler : MonoBehaviour{
  private int switchState = 1;
  public GameObject switchBtn;
  
  public void OnSwitchButtonClicked(){
       switchBtn.transform.position = new Vector3 (switchBtn.transform.position.x * -1, switchBtn.transform.position.y, switchBtn.transform.position.z);
       Debug.Log("Clicked");
  }
  
}