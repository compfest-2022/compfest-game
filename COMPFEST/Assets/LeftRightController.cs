using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class LeftRightController : MonoBehaviour
{
    float oldXaxis;
    private Transform playerPos;
    SpriteRenderer spriteRenderer;
    AIPath aiPath;
    float timer;
    Animator animator;
    public float RunninSpeed;

    
    // Start is called before the first frame update
    void Start()
    {
        playerPos = GetComponent<Transform>();
        oldXaxis = transform.position.x;
        spriteRenderer = GetComponent<SpriteRenderer>();
        aiPath = GetComponent<AIPath>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        timer += Time.deltaTime;

        if (oldXaxis > transform.position.x) {
            //Debug.Log(oldXaxis);
            //Debug.Log("Left");
            spriteRenderer.flipX = true;
            oldXaxis = transform.position.x;
        } if (oldXaxis < transform.position.x) {
            //Debug.Log(oldXaxis);
            //Debug.Log("Right");
            spriteRenderer.flipX = false;
            oldXaxis = transform.position.x;
        }

        //Debug.Log(timer);

        if (timer >= 5) {
            aiPath.maxSpeed = RunninSpeed;
            animator.SetBool("IsRunning", true);
            if (timer >= 9) {
                timer = 0;
            }
        } else {
            animator.SetBool("IsRunning", false);
            aiPath.maxSpeed = 2.5f;
        }
        
    }
}
