using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicBGM : MonoBehaviour
 {
    public bool Music = true;
    public AudioSource _AudioSource;
 
    public AudioClip _AudioClip1;
    public AudioClip _AudioClip2;
    
    private void Awake() {
        DontDestroyOnLoad(this.gameObject);
    }

     void Start() 
     {
 
         _AudioSource.clip = _AudioClip1;
 
         _AudioSource.Play();
     
     }
     
 
     void Update () 
     {
 
         if (Music == false)
         {
 
             if (_AudioSource.clip == _AudioClip1)
             {
 
                 _AudioSource.clip = _AudioClip2;
 
                 _AudioSource.Play();
 
             }
 
         }

     }
 
 }
 

