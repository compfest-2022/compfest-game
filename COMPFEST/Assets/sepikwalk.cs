using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sepikwalk : MonoBehaviour
{
    public GameObject[] Checkpoint;
    public float speed;
    Vector2 moveDirection;
    int i;
    public int waypointindex = 0;
    Vector2 targetposition;
    Vector2 ObjectPosition;
    public AudioClip Spike;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = Checkpoint[waypointindex].transform.position;
        i= 0;
    }

    // Update is called once per frame
    void Update()
    {
        move();
        if (transform.position == Checkpoint[waypointindex + 1].transform.position) {
            Debug.Log("aaa" + waypointindex + Checkpoint.Length);
             if (waypointindex < Checkpoint.Length - 1) {
                waypointindex++;
            }
            if (waypointindex == Checkpoint.Length - 1) {
                waypointindex = -1;
            }
        } 
    }

    public void move() {
        //int waypointindex = 0;

        if (transform.position != Checkpoint[waypointindex + 1].transform.position) {
            transform.position = Vector2.MoveTowards(transform.position, Checkpoint[waypointindex + 1].transform.position, speed * Time.deltaTime);
        }
    }

    void OnCollisionEnter2D(Collision2D other) {
        Physics2D.IgnoreLayerCollision(8,8, true);
        Physics2D.IgnoreLayerCollision(8,9, true);
    }
}
