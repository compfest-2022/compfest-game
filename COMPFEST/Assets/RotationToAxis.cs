using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationToAxis : MonoBehaviour
{
    Transform Axis;


    // Start is called before the first frame update
    void Start()
    {
        Axis = transform.root.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Euler( new Vector3 (transform.rotation.x, transform.rotation.y, transform.rotation.z * -1));
    }
}
