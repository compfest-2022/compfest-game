using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyLoad : MonoBehaviour
{
    public GameObject[] DontDestroyObject;
    Player player;

    // Start is called before the first frame update
    void Start()
    {
        blockArray();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    // Update is called once per frame

    public void blockArray()
         {
            DontDestroyObject = new GameObject[4];
            DontDestroyObject[0] = GameObject.FindGameObjectWithTag("Warning");
            DontDestroyObject[1] = GameObject.FindGameObjectWithTag("Music");
            DontDestroyObject[2] = GameObject.FindGameObjectWithTag("MusicCon");
            DontDestroyObject[3] = GameObject.FindGameObjectWithTag("EventSystem");
         }
    
    public void destroyObject() {
        for (int i = 0; i < DontDestroyObject.Length; i++) {
            Destroy(DontDestroyObject[i]);
        }
        player.ResetCoin();
        player.ResetHealth();
    }
}
