using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PengaturMusic : MonoBehaviour
{
    // Start is called before the first frame update
    private void Awake() {
        DontDestroyOnLoad(this.gameObject);
    }

    public void Bunyikan(AudioClip suara) {
        GetComponent<AudioSource>().PlayOneShot(suara, 1F);
    }

}
