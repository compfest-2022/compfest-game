using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;


public class patrolandattack : MonoBehaviour
{
    public LayerMask LayertoHit;
    public float fieldofImpact;
    AIDestinationSetter3 ai;
    Transform Random;

    // Start is called before the first frame update
    void Start()
    {
        ai = GetComponent<AIDestinationSetter3>();
    }

    // Update is called once per frame
    void Update()
    {
        PlayerDetection();
    }

    void PlayerDetection() {
        Collider2D objects = Physics2D.OverlapCircle(transform.position, fieldofImpact, LayertoHit);
        
        if (objects != null) {
            ai.PlayerPos = true;
            //transform.Translate((objects.transform.position - transform.position) * 1 * Time.deltaTime);
        } else
        {
            ai.PlayerPos = false;
        }
        
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, fieldofImpact);    
    }
}
