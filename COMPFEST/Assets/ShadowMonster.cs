using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class ShadowMonster : MonoBehaviour
{
    Animator animator;
    AIPath aiPath;
    public Collider2D collider2d;
    float timer;
    Enemy enemy;
    EnemyHealthBar EnemyBar;

    // Start is called before the first frame update
    void Start()
    {
        aiPath = GetComponent<AIPath>();
        animator = GetComponent<Animator>();
        collider2d = GetComponent<BoxCollider2D>();  
        enemy = GetComponent<Enemy>();
        EnemyBar = transform.GetChild(0).GetComponent<EnemyHealthBar>();
        timer = 0;  
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
    }

    private void FixedUpdate() {
        if (timer > 8) {
            StartCoroutine("Shadow");
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.CompareTag("Player")) {
            animator.SetTrigger("IsAttack");
        }
    }
    
    private IEnumerator Shadow() {
        Debug.Log("Hidin");
        animator.SetBool("IsHide", true);
        EnemyBar.HideBar();
        aiPath.maxSpeed = 1.5f;
        collider2d.enabled = false;
        enemy.enabled = false;
        yield return new WaitForSeconds(10);
        Build();
    }

    private void Build() {
        animator.SetBool("IsHide", false);
        EnemyBar.ShowBar();
        collider2d.enabled = true;
        enemy.enabled = true;
        aiPath.maxSpeed = 2;
        timer = 0;
    }
}
