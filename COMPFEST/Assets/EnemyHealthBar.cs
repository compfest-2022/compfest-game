using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthBar : MonoBehaviour
{
    public float EnemyPercent;
    SpriteRenderer GreenBarSprite;
    SpriteRenderer RedBar;
    SpriteRenderer FrameBar;
    GameObject GreenBar;
    int i;
    
    float frameLimit;

    // Start is called before the first frame update
    void Start()
    {
        GreenBarSprite = transform.GetChild(2).transform.GetChild(0).GetComponent<SpriteRenderer>();
        RedBar = transform.GetChild(1).GetComponent<SpriteRenderer>();
        FrameBar = transform.GetChild(0).GetComponent<SpriteRenderer>();
        GreenBar = transform.GetChild(2).gameObject;
        GreenBarSprite.enabled = false;
        RedBar.enabled = false;
        FrameBar.enabled = false;
        Debug.Log("Enemypercent" + EnemyPercent);
    }

    // Update is called once per frame
    void Update()
    {
        //GreenBar.transform.localScale = new Vector3(1 - (1 * EnemyPercent/3), 1, 1);
    }

    public void BarMinus() {
        Debug.Log(EnemyPercent);
        GreenBarSprite.enabled = true;
        RedBar.enabled = true;
        FrameBar.enabled = true;
        GreenBar.transform.localScale = new Vector3(GreenBar.transform.localScale.x - (1 * EnemyPercent/3), 1, 1);
        ///EnemyPercent += EnemyPercent;

    }

    public void HideBar() {
        GreenBarSprite.enabled = false;
        RedBar.enabled = false;
        FrameBar.enabled = false;
    }

    public void ShowBar() {
        GreenBarSprite.enabled = true;
        RedBar.enabled = true;
        FrameBar.enabled = true;
    }
}
