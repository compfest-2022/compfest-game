using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnSpawn : MonoBehaviour
{
    GameObject GameController;
    GameController GameControllerScript;

    // Start is called before the first frame update
    void Start()
    {
        GameController = GameObject.Find("GameController");
        GameControllerScript = GameController.GetComponent<GameController>();
        GameControllerScript.EnemyCount();
    }

    // Update is called once per frame
    public void OnEnable()
    {
        
    }
}
