using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    public float rortationSpeed;
    public GameObject bulletPrefab; //objek peluru yg dimaksud
    public GameObject shootPos; //letak munculnya peluru terhadap gameobject
    public GameObject shootPos1;
    public GameObject shootPos2;
    public Transform Parent;
    public Transform Parent2;
    float timer;
    Animator animator;
    float Delay;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponentInParent<Animator>();
        Delay = 5;
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
       timer += Time.deltaTime;
       transform.position = Parent2.transform.position;
       transform.Rotate(new Vector3(0, 0, rortationSpeed) * Time.deltaTime);
    }

    private void FixedUpdate() {
        
        if (timer > Delay) {
            StartCoroutine("AttackSpin");
            Delay += 8;
            timer = 0;
        }
    }

    private IEnumerator AttackSpin() {
            animator.SetTrigger("IsAttack");
            yield return new WaitForSeconds(0.4f);
            GameObject bPrefab = Instantiate(bulletPrefab, shootPos.transform.position, Quaternion.Euler(new Vector3(0, 0, 180)));
            bPrefab.transform.SetParent(Parent);
            GameObject bPrefab2 = Instantiate(bulletPrefab, shootPos1.transform.position, Quaternion.Euler(new Vector3(0, 0, 180)));
            bPrefab2.transform.SetParent(Parent);
            GameObject bPrefab3 = Instantiate(bulletPrefab, shootPos2.transform.position, Quaternion.Euler(new Vector3(0, 0, 180)));
            bPrefab3.transform.SetParent(Parent);
            Object.Destroy(bPrefab, 8.0f);
            Object.Destroy(bPrefab2, 8.0f);
            Object.Destroy(bPrefab3, 8.0f);
    }
    
}
