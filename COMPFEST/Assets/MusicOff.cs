using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MusicOff : MonoBehaviour
{
    AudioSource AC;
    bool isClick;
    public TextMeshProUGUI teks_Music;

    // Start is called before the first frame update
    void Start()
    {
        AC = GameObject.FindGameObjectWithTag("Music").GetComponent<AudioSource>(); 
        teks_Music.text = "Music On";  
    }

    // Update is called once per frame
    public void Off() {
        AC.mute = !AC.mute;
        if (AC.mute == true) {
            teks_Music.text = "Music Off";
        } else {
            teks_Music.text = "Music On";
        }
    }
}
