using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    Vector2 moveDirection;
    public float moveSpeed;
    float timer;
    public int lifetime = 4;
    Rigidbody2D rb2d;
    
    // Start is called before the first frame update
    void Start() {
        rb2d = GetComponent<Rigidbody2D>();
        timer = 0;
    }

    // Update is called once per frame
    void Update() {
        timer += Time.deltaTime; //count waktu kemunculan

        transform.Translate(moveDirection * moveSpeed * Time.deltaTime);
        
		if(timer == 0) {
			//GameObject.Find("ObjPG").GetComponent<PengaturGame>().Bunyikan(suaranya);
		}

        if (timer > 0.5f) {
            rb2d.isKinematic = false;
        } else {
            rb2d.isKinematic = true;
        }
		
		if(timer > lifetime){ //bila sudah mencapai lifetime			
			timer = 0; //reset timer
			Destroy(gameObject); //hancurkan diri sendiri
		}
    }

    public void SetMoveDirection(Vector2 dir) {
        moveDirection = dir;
    }
    
    private void OnCollisionEnter2D(Collision2D collision) {
		if (timer > 0.1f) {
            Destroy(this.gameObject);
            timer = 0;
        }
        Physics2D.IgnoreLayerCollision(11,8, true);
        Physics2D.IgnoreLayerCollision(11,10, true);
        Physics2D.IgnoreLayerCollision(11,9, true);  
            
          
	}
}
