using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeBullet: MonoBehaviour
{
    float bulletAmount = 3;
    private float Startangle =60f, Endangle = 120f; 
    public GameObject bulletPrefab;
    

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Fire", 0f, 0.18f); 
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Fire() {
        float angleStep = (Endangle - Startangle) / bulletAmount;
        float angle = Startangle;


        for (int i = 0 ; i < bulletAmount ; i++) {
            float BulDirX = transform.position.x + Mathf.Sin((angle * Mathf.PI) / 180f);
            float BulDirY = transform.position.y + Mathf.Cos((angle * Mathf.PI) / 180f);

            Vector3 BulMove = new Vector3(BulDirX, BulDirY, 0f);
            Vector2 BulDir = (BulMove - transform.position).normalized;

            GameObject Bullet = Instantiate(bulletPrefab);
            Bullet.transform.position = transform.position;
            Bullet.transform.rotation = transform.rotation;

            Bullet.GetComponent<Bullet>().SetMoveDirection(BulDir);
            angle += angleStep;
        }
    }
}
