using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class ThwainMonster : MonoBehaviour
{
    AIPath aiPath;
    Animator animator;
    public Collider2D collider2d;
    float timer;
    SpriteRenderer spriteRenderer;
    Rigidbody2D rb2d;
    ParticleSystem Eyes;
    ParticleSystem Toxic;
    GameObject childObj;
    //GameObject HealthBar;
    RandomPathEnemy enemy;
    float Health;

    
    
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        collider2d = GetComponent<BoxCollider2D>();  
        aiPath = GetComponent<AIPath>();
        enemy = GetComponent<RandomPathEnemy>();
        childObj = GameObject.Find("Particle");
        //HealthBar = GameObject
        Eyes = transform.GetChild(0).GetComponent<Transform>().transform.GetChild(0).GetComponent<ParticleSystem>();
        Toxic = transform.GetChild(0).GetComponent<Transform>().transform.GetChild(1).GetComponent<ParticleSystem>();
        childObj.SetActive(false);
        //Debug.Log("TwwainMonster" + Health);
        timer = 0;  
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        var oneTime = false;
        Health = enemy.Health;
        if (Health < 0) {
                childObj.SetActive(true);
        }
    }

    private void FixedUpdate() {
        if (timer > 8) {
            StartCoroutine("Thawin");
        }
        //Debug.Log(Health + "Update");
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Enemy"))
            Physics2D.IgnoreCollision(collision.collider,gameObject.GetComponent<Collider2D>());
    }

    private IEnumerator Thawin() {
        animator.SetBool("IsThaw", true);
        rb2d.isKinematic = true;
        aiPath.canMove = false;
        //enemy.enabled = false;
        collider2d.enabled = false;
        yield return new WaitForSeconds(10);
        Build();
    }

    private void Build() {
        animator.SetBool("IsThaw", false);
        rb2d.isKinematic = false;
        collider2d.enabled = true;
        //enemy.enabled = true;
        aiPath.canMove = true;
        timer = 0;
    }

    /*
    private IEnumerator ParticleSystem() {
        Eyes.Play();
        Toxic.Play();
        yield return new WaitForSeconds(4);
        //Toxic.Stop();
    }
    */


}
