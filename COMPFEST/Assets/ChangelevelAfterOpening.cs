using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Playables;

public class ChangelevelAfterOpening : MonoBehaviour
{
	public string newLevel;
    PlayableDirector director;
	
	private void Start() {
		director = GetComponent<PlayableDirector>();
	}

    private void Update() {
        if (director.state == PlayState.Paused) {
            Debug.Log(director.state);
            SceneManager.LoadScene(newLevel);
        }
    }
}	

