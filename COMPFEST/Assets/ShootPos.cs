using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootPos : MonoBehaviour
{
    public GameObject bulletPrefab; //objek peluru yg dimaksud
    public GameObject shootPos; //letak munculnya peluru terhadap gameobject
    public GameObject shootPos1;
    public GameObject shootPos2;
    public GameObject shootPos3;
    Transform playerPos;
    Vector2 rightAttackOffset;
    Player player;
    public float bulletPos = 1;
    public int bulletSpeed = 100;
    float timer = 1;
    float timer1 = 1;
    float timer2 = 1;
    float timer3 = 1;
    float damage;
    PengaturMusic MusicControl;
    public AudioClip ShootSfx;

    private void Start() {
        //playerPos = GetComponent<Transform>();
        //rightAttackOffset = playerPos.position - transform.position; 
        Debug.Log("PLAYER POS" + rightAttackOffset);
        MusicControl = GameObject.FindGameObjectWithTag("MusicCon").GetComponent<PengaturMusic>();
        playerPos = GetComponent<Transform>();
        player = GetComponent<Player>();
    }

    private void Update() {
        //Debug.Log("PLAYER POS" + rightAttackOffset);
        timer += Time.deltaTime;
        timer1 += Time.deltaTime;
        timer2 += Time.deltaTime;
        timer3 += Time.deltaTime;
        damage = Player.Damage;

    }

     public void GunRight() {
        if (timer > 0.3f) {
            GameObject bPrefab2 = Instantiate(bulletPrefab, shootPos.transform.position, Quaternion.Euler(new Vector3(0, 0, 270))) as GameObject;
            bPrefab2.GetComponent<Rigidbody2D>().AddForce(new Vector2 (bulletPos * bulletSpeed, 0));
            bPrefab2.GetComponent<PlayerBullet>().Damage = damage;
            MusicControl.Bunyikan(ShootSfx);
            timer = 0;
        }
        
        //Debug.Log(shootPos.transform.position);
        Debug.Log(transform.position);
        //StartCoroutine("StopShoot");
    }

    public void GunLeft() {
        if (timer1 > 0.3f) {
            GameObject bPrefab2 = Instantiate(bulletPrefab, shootPos1.transform.position, Quaternion.Euler(new Vector3(0, 0, 90))) as GameObject;
            bPrefab2.GetComponent<Rigidbody2D>().AddForce(new Vector2 (bulletPos * bulletSpeed * -1, 0));
            bPrefab2.GetComponent<PlayerBullet>().Damage = damage;
            MusicControl.Bunyikan(ShootSfx);
            timer1 = 0;
        }
        Debug.Log(transform.localPosition);
       // StartCoroutine("StopShoot");
    }

     public void GunDown() {
        if (timer2 > 0.3f) {
            GameObject bPrefab2 = Instantiate(bulletPrefab, shootPos2.transform.position, Quaternion.Euler(new Vector3(0, 0, 180))) as GameObject;
            bPrefab2.GetComponent<Rigidbody2D>().AddForce(new Vector2 (0, bulletPos * bulletSpeed * -1));
            bPrefab2.GetComponent<PlayerBullet>().Damage = damage;
            MusicControl.Bunyikan(ShootSfx);
            timer2 = 0;
        }
        Debug.Log(transform.localPosition);
        //StartCoroutine("StopShoot");
    }
    
     public void GunUp() {
        if (timer3 > 0.3f) {
            GameObject bPrefab2 = Instantiate(bulletPrefab, shootPos3.transform.position, Quaternion.Euler(new Vector3(0, 0, 0))) as GameObject;
            bPrefab2.GetComponent<Rigidbody2D>().AddForce(new Vector2 (0, bulletPos * bulletSpeed));
            bPrefab2.GetComponent<PlayerBullet>().Damage = damage;
            MusicControl.Bunyikan(ShootSfx);
            timer3 = 0;
        }
        Debug.Log(transform.localPosition);
        //StartCoroutine("StopShoot");
    }
}
