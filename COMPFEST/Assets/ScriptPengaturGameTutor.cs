using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScriptPengaturGameTutor : MonoBehaviour
{
    public int respawnTime=2;
    public string level1;
    public GameObject[] DontDestroyObject;

    private void Start() {
        blockArray();
    }

    private void Awake() {
        blockArray();
    }

    public void Respawning(){
        StartCoroutine(DelayedRespawn()); //menjalankan coroutine, yaitu untuk delay execution script
    }

    // Start is called before the first frame update
    IEnumerator DelayedRespawn(){
		yield return new WaitForSeconds(respawnTime); //delay bbrp second sesuai variabel
        for (int i = 0; i < DontDestroyObject.Length; i++) {
            Destroy(DontDestroyObject[i]);
        }
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

     void blockArray()
         {
            DontDestroyObject = new GameObject[4];
            DontDestroyObject[0] = GameObject.FindGameObjectWithTag("Warning");
            DontDestroyObject[1] = GameObject.FindGameObjectWithTag("Music");
            DontDestroyObject[2] = GameObject.FindGameObjectWithTag("MusicCon");
            DontDestroyObject[3] = GameObject.FindGameObjectWithTag("EventSystem");
         }
}   
