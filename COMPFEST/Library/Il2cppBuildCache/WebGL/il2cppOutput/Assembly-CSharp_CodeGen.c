﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void BIGROOMSCRIPT::Start()
extern void BIGROOMSCRIPT_Start_mA5AE2F212455D0C098491C75F50A6C4369414B5B (void);
// 0x00000002 System.Void BIGROOMSCRIPT::Update()
extern void BIGROOMSCRIPT_Update_mD28ED5E802BCED44CE61E33D95E35FFC6DF1DFFA (void);
// 0x00000003 System.Void BIGROOMSCRIPT::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void BIGROOMSCRIPT_OnTriggerEnter2D_mDC132E497596FB4D45D327A48F7106AC2D8234E6 (void);
// 0x00000004 System.Void BIGROOMSCRIPT::OnTriggerExit2D(UnityEngine.Collider2D)
extern void BIGROOMSCRIPT_OnTriggerExit2D_m1D083333C270AD657513FE5EF9DDDF55B7570B1C (void);
// 0x00000005 System.Void BIGROOMSCRIPT::.ctor()
extern void BIGROOMSCRIPT__ctor_m3A038027784ACE324EB4475C02EFEEAE9C75DDC0 (void);
// 0x00000006 System.Void ReadmeVE3::.ctor()
extern void ReadmeVE3__ctor_mDC9CB74E2B03BC3D384AD9664B3CA75BF96EA8CE (void);
// 0x00000007 System.Void ReadmeVE3/Section::.ctor()
extern void Section__ctor_m5AB9E62F43D397F29091ACA0944408FA649F0F8A (void);
// 0x00000008 System.Void BombAction::Start()
extern void BombAction_Start_mE8CE93E9CFBF0745C0D3CFE7AED363C0112FA576 (void);
// 0x00000009 System.Void BombAction::Update()
extern void BombAction_Update_m67A6CF122A439057F17F77C3A71E37DD34AB0045 (void);
// 0x0000000A System.Void BombAction::SetMoveDirection(UnityEngine.Vector2)
extern void BombAction_SetMoveDirection_m73A344FBBE62107E98953586CB2C5E4CF7029F3F (void);
// 0x0000000B System.Collections.IEnumerator BombAction::BombExplode()
extern void BombAction_BombExplode_mBBCB6A87E2BD076879BE0B674CD60DFEECB12E1A (void);
// 0x0000000C System.Void BombAction::Explode()
extern void BombAction_Explode_mC686A1B4FCF8B422AAC5243AD6998AB97C88C07C (void);
// 0x0000000D System.Void BombAction::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void BombAction_OnCollisionEnter2D_m853654A86D191FD414F06D9BF37BBAAB03B751C2 (void);
// 0x0000000E System.Void BombAction::OnDrawGizmos()
extern void BombAction_OnDrawGizmos_m24BA93D4AF79175EC7DBE08D83217047DD663C7F (void);
// 0x0000000F System.Void BombAction::.ctor()
extern void BombAction__ctor_mBAB45B282630878D1A12ECDA80D9472EB7114A07 (void);
// 0x00000010 System.Void BombAction/<BombExplode>d__13::.ctor(System.Int32)
extern void U3CBombExplodeU3Ed__13__ctor_m923CB05D2C511E33FDCFB085649F5DAA1CCAF7BD (void);
// 0x00000011 System.Void BombAction/<BombExplode>d__13::System.IDisposable.Dispose()
extern void U3CBombExplodeU3Ed__13_System_IDisposable_Dispose_m15C4650CE46C34DFC8C5830A307D37CC569AAE0F (void);
// 0x00000012 System.Boolean BombAction/<BombExplode>d__13::MoveNext()
extern void U3CBombExplodeU3Ed__13_MoveNext_m65B303C3A263089B2CFD846E8D61972E871D2B3A (void);
// 0x00000013 System.Object BombAction/<BombExplode>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBombExplodeU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72951C1ADD21C1776A9F6F9FBBB1784F6CCF401A (void);
// 0x00000014 System.Void BombAction/<BombExplode>d__13::System.Collections.IEnumerator.Reset()
extern void U3CBombExplodeU3Ed__13_System_Collections_IEnumerator_Reset_m623ED216F0519489B9F92EFEB501539FBE710AE2 (void);
// 0x00000015 System.Object BombAction/<BombExplode>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CBombExplodeU3Ed__13_System_Collections_IEnumerator_get_Current_mD297AFE797BA2B44311CCDB9548E4366A41D37FA (void);
// 0x00000016 System.Void BombScript::Start()
extern void BombScript_Start_m837BDF2F01719906AA59E1FFC7791DBA3E86DE75 (void);
// 0x00000017 System.Void BombScript::Update()
extern void BombScript_Update_m3255BE52A49ADE17C35892C2C0506D754D93E25F (void);
// 0x00000018 System.Void BombScript::FixedUpdate()
extern void BombScript_FixedUpdate_mEC9B9A7A4CC93BF92008B351C3663448DCB5062E (void);
// 0x00000019 System.Void BombScript::.ctor()
extern void BombScript__ctor_m2A65AA9D6A32A3897038DDD5AC7798F26A9276F3 (void);
// 0x0000001A System.Void Bullet::Start()
extern void Bullet_Start_m58181B46F80FE1ABECE1AFF455C253B51B7EB0E4 (void);
// 0x0000001B System.Void Bullet::Update()
extern void Bullet_Update_mB82408CA535D8533168045E7C0321090448B596B (void);
// 0x0000001C System.Void Bullet::SetMoveDirection(UnityEngine.Vector2)
extern void Bullet_SetMoveDirection_mB2F5E5A9D3647F263218A80AC50894734C544BC4 (void);
// 0x0000001D System.Void Bullet::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Bullet_OnCollisionEnter2D_m4CC80D190EE3A6B6E2EDA524A121942D0285D0A9 (void);
// 0x0000001E System.Void Bullet::.ctor()
extern void Bullet__ctor_mC7D931FE508342F413FBA79525A4819D4114B3EC (void);
// 0x0000001F System.Void BulletHell::Start()
extern void BulletHell_Start_mC04D30FBA960AA1B19EA9E941CD82468C390BFE1 (void);
// 0x00000020 System.Void BulletHell::Update()
extern void BulletHell_Update_mDA120AE754C8F90C302E25E29BBB0D3824245FCE (void);
// 0x00000021 System.Void BulletHell::RandomPosition()
extern void BulletHell_RandomPosition_m2F2FF558EEAAEBF2661F42902CE35044F6EE0F56 (void);
// 0x00000022 System.Void BulletHell::LockMovement()
extern void BulletHell_LockMovement_m58BD253063F504BE3A63BB7CBECF682C89CCC7D1 (void);
// 0x00000023 System.Void BulletHell::Spawn(System.Int32)
extern void BulletHell_Spawn_m5D55FB3B9D8779803CCBF6892E4197216A886135 (void);
// 0x00000024 System.Void BulletHell::Fire()
extern void BulletHell_Fire_mC568115CB04C6EDC545709E907FD53363F2374B5 (void);
// 0x00000025 System.Void BulletHell::StopAnimate()
extern void BulletHell_StopAnimate_m8B4AEC8230A9B55318F089C0FCDD926949E21F14 (void);
// 0x00000026 System.Void BulletHell::.ctor()
extern void BulletHell__ctor_m797485160FABC324EB3E13D8B0F02F5F3E55B167 (void);
// 0x00000027 System.Void BulletRotationToPlayer::Start()
extern void BulletRotationToPlayer_Start_m01E4EC481205DE680B3D7B99B04C8A5F0A00AED3 (void);
// 0x00000028 System.Void BulletRotationToPlayer::Update()
extern void BulletRotationToPlayer_Update_m5199561978B30628DBE38CB3CFC12341D918D116 (void);
// 0x00000029 System.Void BulletRotationToPlayer::.ctor()
extern void BulletRotationToPlayer__ctor_mBE7D0C6C49AFB600ACCB789CBFC3A899757B3737 (void);
// 0x0000002A System.Void BulletScript::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void BulletScript_OnCollisionEnter2D_m8050C14632E7C2C21DB373C5EB984C5C4DA0730C (void);
// 0x0000002B System.Collections.IEnumerator BulletScript::OnCollisionExit2D(UnityEngine.Collision2D)
extern void BulletScript_OnCollisionExit2D_m39DD1005BC9F225C87231175DC010294F39C09B4 (void);
// 0x0000002C System.Void BulletScript::Update()
extern void BulletScript_Update_mEEA5A09DE25F213593101C9F9B0BBB3D20348EAF (void);
// 0x0000002D System.Void BulletScript::.ctor()
extern void BulletScript__ctor_m100E1C957FFAB49CD0E873E7A47EF15F8DCD5E19 (void);
// 0x0000002E System.Void BulletScript/<OnCollisionExit2D>d__4::.ctor(System.Int32)
extern void U3COnCollisionExit2DU3Ed__4__ctor_m14AF5686030543F733A7415FDCAD1BF3AB949BEC (void);
// 0x0000002F System.Void BulletScript/<OnCollisionExit2D>d__4::System.IDisposable.Dispose()
extern void U3COnCollisionExit2DU3Ed__4_System_IDisposable_Dispose_m73D6E1249D30E89DDD8894FCC44BF8D01F448D00 (void);
// 0x00000030 System.Boolean BulletScript/<OnCollisionExit2D>d__4::MoveNext()
extern void U3COnCollisionExit2DU3Ed__4_MoveNext_mB86C0D8D4B9AABC20D47227F1645335011D63E73 (void);
// 0x00000031 System.Object BulletScript/<OnCollisionExit2D>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnCollisionExit2DU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A272FB97A0973E2DF45FAE555831D5023594805 (void);
// 0x00000032 System.Void BulletScript/<OnCollisionExit2D>d__4::System.Collections.IEnumerator.Reset()
extern void U3COnCollisionExit2DU3Ed__4_System_Collections_IEnumerator_Reset_m9337004461BBF6ECF0723DBD6E0EBDE65CA9ADE2 (void);
// 0x00000033 System.Object BulletScript/<OnCollisionExit2D>d__4::System.Collections.IEnumerator.get_Current()
extern void U3COnCollisionExit2DU3Ed__4_System_Collections_IEnumerator_get_Current_mC32AD54E8C558A329810C8701029A8B9B31E46B1 (void);
// 0x00000034 System.Void BulletShooptMonster17::Start()
extern void BulletShooptMonster17_Start_m227B7A31ED639B2CC078CC570583412E53EF489F (void);
// 0x00000035 System.Void BulletShooptMonster17::Update()
extern void BulletShooptMonster17_Update_m68D1ED6D6F60215BC8777C520529AC2ECE27FD60 (void);
// 0x00000036 System.Void BulletShooptMonster17::Fire()
extern void BulletShooptMonster17_Fire_mCCBE636673A572FA01C900FA9F099C0F1DAA389D (void);
// 0x00000037 System.Void BulletShooptMonster17::StopAnimate()
extern void BulletShooptMonster17_StopAnimate_mAE13C84A7FD3D3CBEEC98152663F90A396C61B3F (void);
// 0x00000038 System.Void BulletShooptMonster17::.ctor()
extern void BulletShooptMonster17__ctor_m638792FBC3049D0F5241BFDCD6506DA37DD2F0A2 (void);
// 0x00000039 System.Void Change::Start()
extern void Change_Start_m597BAEC4C93963C2512C2DB12D35B1801D3103B2 (void);
// 0x0000003A System.Void Change::Update()
extern void Change_Update_mF3E920C21344D8BCA1D53E4A4F0CC78CE296DBAC (void);
// 0x0000003B System.Void Change::.ctor()
extern void Change__ctor_m8F66987EAF95B4B918CF8A7DB6617C3B08D5B9E6 (void);
// 0x0000003C System.Void ChangelevelAfterOpening::Start()
extern void ChangelevelAfterOpening_Start_m152724376B6DCBCA98C90A5501C96C8130F11AC3 (void);
// 0x0000003D System.Void ChangelevelAfterOpening::Update()
extern void ChangelevelAfterOpening_Update_m65F23F573D2635C129199A1C97F6BFDD50A8881B (void);
// 0x0000003E System.Void ChangelevelAfterOpening::.ctor()
extern void ChangelevelAfterOpening__ctor_mC4E223EBFF275D0F3DE2628E15302AE54A34A578 (void);
// 0x0000003F System.Void ChestScript::Start()
extern void ChestScript_Start_m090F1D5743839491430CE04A288085FFF53799EA (void);
// 0x00000040 System.Void ChestScript::lvlDone()
extern void ChestScript_lvlDone_mE8307EC34AD965C93AFE799F1B398DD2A3D05091 (void);
// 0x00000041 System.Void ChestScript::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void ChestScript_OnCollisionEnter2D_mAF36E8A58D323C6DD3809AD966563B4DA2C9822A (void);
// 0x00000042 System.Void ChestScript::Update()
extern void ChestScript_Update_mE06D3FB0616D0459BE8E3E56DE229C189839C76A (void);
// 0x00000043 System.Collections.IEnumerator ChestScript::ChestOpen()
extern void ChestScript_ChestOpen_m61B1715BA969C81616E4C65A3A17BE4015C5C2AC (void);
// 0x00000044 System.Void ChestScript::.ctor()
extern void ChestScript__ctor_m8B91E6120D30A883C0DE05EBEA0F67C920BC55A1 (void);
// 0x00000045 System.Void ChestScript/<ChestOpen>d__13::.ctor(System.Int32)
extern void U3CChestOpenU3Ed__13__ctor_m47FE64B53C9CBFBD01E777FE699028D93D2F4E31 (void);
// 0x00000046 System.Void ChestScript/<ChestOpen>d__13::System.IDisposable.Dispose()
extern void U3CChestOpenU3Ed__13_System_IDisposable_Dispose_m3AEB6E997AB70866D1CAC9FD6B4E4461CA83EDF7 (void);
// 0x00000047 System.Boolean ChestScript/<ChestOpen>d__13::MoveNext()
extern void U3CChestOpenU3Ed__13_MoveNext_m0EA7179CBA7AC34D4978FD502071E2D843B13DA1 (void);
// 0x00000048 System.Object ChestScript/<ChestOpen>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CChestOpenU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m773A3042410FA3B4CF8542A5404809996DE56296 (void);
// 0x00000049 System.Void ChestScript/<ChestOpen>d__13::System.Collections.IEnumerator.Reset()
extern void U3CChestOpenU3Ed__13_System_Collections_IEnumerator_Reset_m0642610982D6DB7B358B9E0AC3349535664FC592 (void);
// 0x0000004A System.Object ChestScript/<ChestOpen>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CChestOpenU3Ed__13_System_Collections_IEnumerator_get_Current_mCED6EEA482BDF5E3F2D25500341C365D9597E9B5 (void);
// 0x0000004B System.Void crocoAnimations::Start()
extern void crocoAnimations_Start_m45839F1BE51C51554C3A70C520BEA2B53CD82ECA (void);
// 0x0000004C System.Void crocoAnimations::Update()
extern void crocoAnimations_Update_m46314E2731EDDF1A23C57809110DE561B75788FE (void);
// 0x0000004D System.Void crocoAnimations::walkPress()
extern void crocoAnimations_walkPress_mC127AB5DCF7B46FDF500ADD9E8EC1358F85A13BC (void);
// 0x0000004E System.Void crocoAnimations::idlePress()
extern void crocoAnimations_idlePress_mA6E8AF66681549D2B1B42659AE1D5FBF2AF8FF43 (void);
// 0x0000004F System.Void crocoAnimations::runPress()
extern void crocoAnimations_runPress_m788C43D3FEED0B2B8CE2DC1AE2F06BE03A123A8A (void);
// 0x00000050 System.Void crocoAnimations::attackPress()
extern void crocoAnimations_attackPress_mE9D570A02C662D2013B7B65A526F15014F33A15D (void);
// 0x00000051 System.Void crocoAnimations::deffencePress()
extern void crocoAnimations_deffencePress_mECDAD0C5850EEC2F74173C7F6CCE1BC4380841B6 (void);
// 0x00000052 System.Void crocoAnimations::deadPress()
extern void crocoAnimations_deadPress_m588FAB20C2298EFE9FE9D30D52FFF04D388FDFB7 (void);
// 0x00000053 System.Void crocoAnimations::RotatePress()
extern void crocoAnimations_RotatePress_m06DD2B6C6BE485DE32A659206EB581F7AD2EA4FE (void);
// 0x00000054 System.Void crocoAnimations::.ctor()
extern void crocoAnimations__ctor_m5EE0F60E6B20DFB92CE4DE6AC0A384DBCC21C596 (void);
// 0x00000055 System.Void Coin::Start()
extern void Coin_Start_m514F3321444C25610980620A5AE926E8C5293C01 (void);
// 0x00000056 System.Void Coin::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Coin_OnCollisionEnter2D_m802812D629F532E79B772AB5E3B3FC62C1658D55 (void);
// 0x00000057 System.Void Coin::.ctor()
extern void Coin__ctor_mF977D2CE7F9F781D0E5B1226BD2757CC523DB637 (void);
// 0x00000058 System.Void CollisionRandom::Start()
extern void CollisionRandom_Start_m1FB8FBE4AEB581A7517E5676437A3F98166E5D11 (void);
// 0x00000059 System.Void CollisionRandom::CalculateVector3()
extern void CollisionRandom_CalculateVector3_m2771C8DB49771B9397C0BEA22D26457443713A51 (void);
// 0x0000005A System.Void CollisionRandom::Update()
extern void CollisionRandom_Update_m05A3A0362E4DDD05CBC11EB99CD8ADE64611C6E6 (void);
// 0x0000005B System.Void CollisionRandom::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void CollisionRandom_OnCollisionEnter2D_m05F6D995F1280C095EBD4FACC550709C5C067563 (void);
// 0x0000005C System.Void CollisionRandom::.ctor()
extern void CollisionRandom__ctor_mCB398F6D6FAA1BACA9DE64DCBFEF727D9E6A8406 (void);
// 0x0000005D System.Void DestroyBGM::Start()
extern void DestroyBGM_Start_m8BAB27A0FF4698048F89E4634FBDDDB01C9A3691 (void);
// 0x0000005E System.Void DestroyBGM::.ctor()
extern void DestroyBGM__ctor_mA74BC2F1208C0C6D76ED5F96A6D0287DA52D5A05 (void);
// 0x0000005F System.Void DestroyLoad::Start()
extern void DestroyLoad_Start_m366BC6A74D67B562A19F17E013D56013346028A3 (void);
// 0x00000060 System.Void DestroyLoad::blockArray()
extern void DestroyLoad_blockArray_mDD3559F5E586763E0A26D6480A0B03DC6DCFD38B (void);
// 0x00000061 System.Void DestroyLoad::destroyObject()
extern void DestroyLoad_destroyObject_mEEAAFB1AEC8356C6A599997C587A97C6971D287A (void);
// 0x00000062 System.Void DestroyLoad::.ctor()
extern void DestroyLoad__ctor_mDEF9FB62579332136B6E41E8476A0209D1A55F27 (void);
// 0x00000063 System.Void DontDestroy::Awake()
extern void DontDestroy_Awake_mFD3677E85209771C579A5FFF6E50A5DB7D5D7076 (void);
// 0x00000064 System.Void DontDestroy::Update()
extern void DontDestroy_Update_m0C65A725225EB715FE18760F1A0EC1F8BC2CD5D7 (void);
// 0x00000065 System.Void DontDestroy::.ctor()
extern void DontDestroy__ctor_mCDCDC2D7D6B681407E7391AE87E7DD8BE35A7A45 (void);
// 0x00000066 System.Void EnemyHealthBar::Start()
extern void EnemyHealthBar_Start_mAC5072644CD25461F54283A494648B93E10740AD (void);
// 0x00000067 System.Void EnemyHealthBar::Update()
extern void EnemyHealthBar_Update_m0D89B3B8999217593AA991F95BC0CB9D0DFF3B4B (void);
// 0x00000068 System.Void EnemyHealthBar::BarMinus()
extern void EnemyHealthBar_BarMinus_m684458FF25E8D814F37FB2CBEB5FF7151655AD19 (void);
// 0x00000069 System.Void EnemyHealthBar::HideBar()
extern void EnemyHealthBar_HideBar_m5B10CDD73099F09008196731B04BB55804EB9B0C (void);
// 0x0000006A System.Void EnemyHealthBar::ShowBar()
extern void EnemyHealthBar_ShowBar_m4C8E729D9F8B0C393FF5F15D7806DC0DFB0DB135 (void);
// 0x0000006B System.Void EnemyHealthBar::.ctor()
extern void EnemyHealthBar__ctor_mA53148773B4E9CA545B3C7D4564ADAF769B50C8B (void);
// 0x0000006C System.Void FindMonsterController::Start()
extern void FindMonsterController_Start_mAF4446A6A9C6E7D8E5A6F49EC3C90C380E2435DD (void);
// 0x0000006D System.Void FindMonsterController::FixedUpdate()
extern void FindMonsterController_FixedUpdate_mB7730B3E2178D9C3CAE7035CEDFB95A118888E89 (void);
// 0x0000006E System.Void FindMonsterController::.ctor()
extern void FindMonsterController__ctor_m90606917C329CAB2D62D82F145E106BC503F58C2 (void);
// 0x0000006F System.Void FlashMaterial::Start()
extern void FlashMaterial_Start_m3FD2C06D2A470956F90E419A84DF9BE2961E1AC9 (void);
// 0x00000070 System.Void FlashMaterial::StartLoop()
extern void FlashMaterial_StartLoop_mBCD2FB0298EB6DDB4D99CDA9539F0B0F25A8BA13 (void);
// 0x00000071 System.Void FlashMaterial::StopLoop()
extern void FlashMaterial_StopLoop_m82ACD012BD1C358E8018746C6A5209BDD2BAE8BB (void);
// 0x00000072 System.Collections.IEnumerator FlashMaterial::DoLoop()
extern void FlashMaterial_DoLoop_m5AC8BAF3E232FAD720F467C4553C008A657F9B93 (void);
// 0x00000073 System.Void FlashMaterial::.ctor()
extern void FlashMaterial__ctor_m2D93F039E4B43FEA86B6CB586848AE678A49546B (void);
// 0x00000074 System.Void FlashMaterial/<DoLoop>d__9::.ctor(System.Int32)
extern void U3CDoLoopU3Ed__9__ctor_m80FB7782169D5C9434E6BB332F0875E552A86EE3 (void);
// 0x00000075 System.Void FlashMaterial/<DoLoop>d__9::System.IDisposable.Dispose()
extern void U3CDoLoopU3Ed__9_System_IDisposable_Dispose_mA1D883D6198A472B861CDD8E69C87389B202711E (void);
// 0x00000076 System.Boolean FlashMaterial/<DoLoop>d__9::MoveNext()
extern void U3CDoLoopU3Ed__9_MoveNext_m8B8A02C18B41A7302229029D5211438F8CE1ACD7 (void);
// 0x00000077 System.Object FlashMaterial/<DoLoop>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDoLoopU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m161A940F778B3E1A8EB5FF11524329B4ED2B24E9 (void);
// 0x00000078 System.Void FlashMaterial/<DoLoop>d__9::System.Collections.IEnumerator.Reset()
extern void U3CDoLoopU3Ed__9_System_Collections_IEnumerator_Reset_m03E7421E1933ED063603056D3A8AA90432B2EBA5 (void);
// 0x00000079 System.Object FlashMaterial/<DoLoop>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CDoLoopU3Ed__9_System_Collections_IEnumerator_get_Current_m92E236931F2A16F330962D928B271976E1A5AD91 (void);
// 0x0000007A System.Void FollowPlayer::Start()
extern void FollowPlayer_Start_mFDD4B7A728742CA2842F6BDC8EEC8E518B0C064C (void);
// 0x0000007B System.Void FollowPlayer::Update()
extern void FollowPlayer_Update_m02579F8DD00972C9A5FD0546F0181118692F93E7 (void);
// 0x0000007C System.Void FollowPlayer::.ctor()
extern void FollowPlayer__ctor_mE71062263690B516F460CF232D5144C1C85017AA (void);
// 0x0000007D System.Void CamerController::Start()
extern void CamerController_Start_mF3EE5A7ED1FA5A2E3BB7257E9632946E3342D362 (void);
// 0x0000007E System.Void CamerController::Update()
extern void CamerController_Update_m5E203AFDC614861140E58F94050F39E04FAD0152 (void);
// 0x0000007F System.Void CamerController::.ctor()
extern void CamerController__ctor_m0621CBE36C46A9D0253B7593F504F73D8A4E65CC (void);
// 0x00000080 System.Void FungiMonster::Start()
extern void FungiMonster_Start_m06E91CF4BA4CEFC8FCE51A9BF87A94E6C530D31E (void);
// 0x00000081 System.Void FungiMonster::DamageHealth(UnityEngine.Vector3,System.Single)
extern void FungiMonster_DamageHealth_mD83BD6F09C0F102C696E41F45290390DF78EAFD4 (void);
// 0x00000082 System.Void FungiMonster::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void FungiMonster_OnCollisionEnter2D_m67FF8D1524F9F151F647BA44A4E4DC5EBB87F176 (void);
// 0x00000083 System.Void FungiMonster::Update()
extern void FungiMonster_Update_m79CE12B06BF347AB9AA72AB770889B9418F6A670 (void);
// 0x00000084 System.Collections.IEnumerator FungiMonster::Reset(UnityEngine.Vector3)
extern void FungiMonster_Reset_m811B8C555ADE3F36B97FB168CBCF5DD0FA6D62B4 (void);
// 0x00000085 System.Void FungiMonster::RemoveEnemy()
extern void FungiMonster_RemoveEnemy_m1A541A349EDE16C119CDFC322ACE243804EFD80D (void);
// 0x00000086 System.Void FungiMonster::.ctor()
extern void FungiMonster__ctor_mB51C575BCD81C7689680C1F341405507B2BE26ED (void);
// 0x00000087 System.Void FungiMonster/<Reset>d__21::.ctor(System.Int32)
extern void U3CResetU3Ed__21__ctor_m769F0D7F760AE690DF6B43BEA616B6470E69A540 (void);
// 0x00000088 System.Void FungiMonster/<Reset>d__21::System.IDisposable.Dispose()
extern void U3CResetU3Ed__21_System_IDisposable_Dispose_mF3B71B7BD6282934BD973F3FF905DA11CD7AF34A (void);
// 0x00000089 System.Boolean FungiMonster/<Reset>d__21::MoveNext()
extern void U3CResetU3Ed__21_MoveNext_m7BA94954A18A6244D9508D0760E06201B13C7B94 (void);
// 0x0000008A System.Object FungiMonster/<Reset>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResetU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m40A8093877EEA9DD3CF5DA5BBF315DC68B1FFE7A (void);
// 0x0000008B System.Void FungiMonster/<Reset>d__21::System.Collections.IEnumerator.Reset()
extern void U3CResetU3Ed__21_System_Collections_IEnumerator_Reset_m476796199DB1FB09AECC198FA41250799FC574AE (void);
// 0x0000008C System.Object FungiMonster/<Reset>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CResetU3Ed__21_System_Collections_IEnumerator_get_Current_mA568910A33A57FBBB89BE715EC8C8E1014881F71 (void);
// 0x0000008D System.Void GameController::Start()
extern void GameController_Start_mCC3F0292799528323E2217A12DB08D98CDD492AE (void);
// 0x0000008E System.Void GameController::Update()
extern void GameController_Update_mA76A2CE1F2AC1AACCDBF913CA6E1EA73DC621CD0 (void);
// 0x0000008F System.Void GameController::EnemyCount()
extern void GameController_EnemyCount_mEC85023C5B37DC8FE9CB2418F83DEC9BF55ECB86 (void);
// 0x00000090 System.Void GameController::EnemyDecrease()
extern void GameController_EnemyDecrease_m9FF862AF384DCA060390DDBA1F1AA92B56A98185 (void);
// 0x00000091 System.Void GameController::.ctor()
extern void GameController__ctor_m9D952052C0A7234373FA5531292FCA8855BE2643 (void);
// 0x00000092 System.Void HealthCounter::Start()
extern void HealthCounter_Start_m8DA95F34BD4A3BC087198B9096000539741496A5 (void);
// 0x00000093 System.Void HealthCounter::updateHealth(System.Single)
extern void HealthCounter_updateHealth_m5D68D8016359D8AC26197845DD084FB2744905E1 (void);
// 0x00000094 System.Void HealthCounter::Update()
extern void HealthCounter_Update_m1B50D5B12F3044D9754D782D19175C3D8199C240 (void);
// 0x00000095 System.Void HealthCounter::.ctor()
extern void HealthCounter__ctor_m07E0A1C3D3FF8CE384FAC16AE61E55C7DDC8C4A0 (void);
// 0x00000096 System.Void LEVEL::Start()
extern void LEVEL_Start_mE22120A5462CBF505601714FF4C6401E13D9EA60 (void);
// 0x00000097 System.Void LEVEL::CanPassTrue()
extern void LEVEL_CanPassTrue_m24325AF305E18B39244DE8729BD285B68E929085 (void);
// 0x00000098 System.Void LEVEL::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void LEVEL_OnCollisionEnter2D_mA4EB422141010FB9CF2E2A4A636D48CE930DD3DE (void);
// 0x00000099 System.Void LEVEL::changelevel()
extern void LEVEL_changelevel_m34A04CC5A45D0A1B1B99A2B60E3D3D356D9EA24D (void);
// 0x0000009A System.Collections.IEnumerator LEVEL::CanvasVisible()
extern void LEVEL_CanvasVisible_mA6259459DCA7F69EF82C18AED18304BB8D4214A6 (void);
// 0x0000009B System.Void LEVEL::.ctor()
extern void LEVEL__ctor_m2BA25203A4E9939958A865F2BFD71995FAA5973D (void);
// 0x0000009C System.Void LEVEL/<CanvasVisible>d__8::.ctor(System.Int32)
extern void U3CCanvasVisibleU3Ed__8__ctor_mB747C6375F58AE06D33940068E0429813343D23B (void);
// 0x0000009D System.Void LEVEL/<CanvasVisible>d__8::System.IDisposable.Dispose()
extern void U3CCanvasVisibleU3Ed__8_System_IDisposable_Dispose_m2B04E17B955D1CE7679FF6F2A6CDF01CDA2AA2EF (void);
// 0x0000009E System.Boolean LEVEL/<CanvasVisible>d__8::MoveNext()
extern void U3CCanvasVisibleU3Ed__8_MoveNext_m4E28D597E29A4AEDA0E9920D8F5B24ED82F16BC2 (void);
// 0x0000009F System.Object LEVEL/<CanvasVisible>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCanvasVisibleU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC1B4F1E8D5035F6F1FEF26BEF3D2B6D4F16B882D (void);
// 0x000000A0 System.Void LEVEL/<CanvasVisible>d__8::System.Collections.IEnumerator.Reset()
extern void U3CCanvasVisibleU3Ed__8_System_Collections_IEnumerator_Reset_m60B70917051546AC0423BB783D2146FE7F07A3DC (void);
// 0x000000A1 System.Object LEVEL/<CanvasVisible>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CCanvasVisibleU3Ed__8_System_Collections_IEnumerator_get_Current_m331FFE5EF7E5CBD34180C12B89A4DC1286C16E5D (void);
// 0x000000A2 System.Void LeftRightController::Start()
extern void LeftRightController_Start_mC4768CFB30913BE489CD248AFAED3A8BC1863ED2 (void);
// 0x000000A3 System.Void LeftRightController::FixedUpdate()
extern void LeftRightController_FixedUpdate_m8107B677F77D02A728C9A33CC0883DA953283EDE (void);
// 0x000000A4 System.Void LeftRightController::.ctor()
extern void LeftRightController__ctor_mAF95BBEBB78C0404E6AC088CEA88A86B2F64077E (void);
// 0x000000A5 System.Void LevelUp::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void LevelUp_OnCollisionEnter2D_mD32EF2EC1A4AC94EA9ACE068920F7C1D983D8BE4 (void);
// 0x000000A6 System.Void LevelUp::.ctor()
extern void LevelUp__ctor_mF1B86C987369AF31D9854B18CA6C4192C6D0F80E (void);
// 0x000000A7 System.Void LookAtEnemy::Start()
extern void LookAtEnemy_Start_m0088C545741ED0F39A3F6BAA2EBA62305EBAA048 (void);
// 0x000000A8 System.Void LookAtEnemy::Update()
extern void LookAtEnemy_Update_m1229017C0E3D83C163F627BCC93AC21045329C7D (void);
// 0x000000A9 System.Void LookAtEnemy::.ctor()
extern void LookAtEnemy__ctor_m781880F0265A69734DFF4622A8277C5D5E045116 (void);
// 0x000000AA System.Void MovePlayer::Start()
extern void MovePlayer_Start_mA96A9BB0DFF4CD84A7E19BB64D3B3FA3E30138B1 (void);
// 0x000000AB System.Void MovePlayer::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void MovePlayer_OnCollisionEnter2D_m10BBE5D907D6FCE9D8509B069F60A03B59F7A8D5 (void);
// 0x000000AC System.Void MovePlayer::.ctor()
extern void MovePlayer__ctor_mABC1924DC986EA24C51BF8636ED18DDDA5073D22 (void);
// 0x000000AD System.Void MusicBGM::Awake()
extern void MusicBGM_Awake_m0BE285FEEEAE7BBDF2F32B15F847A5C1F470A512 (void);
// 0x000000AE System.Void MusicBGM::Start()
extern void MusicBGM_Start_mFC8B59661A336BA37F3B7F8E73647C69AFFB7421 (void);
// 0x000000AF System.Void MusicBGM::Update()
extern void MusicBGM_Update_mA4735E4B9E98084EABDAB73D3B5FFAA9EEF63F1B (void);
// 0x000000B0 System.Void MusicBGM::.ctor()
extern void MusicBGM__ctor_mE105C84DB62B73EF04909C88152EB9B7F5FE3436 (void);
// 0x000000B1 System.Void MusicOff::Start()
extern void MusicOff_Start_m1A37F6B7DADD5CC782D134E2867E6AF49AE8AD51 (void);
// 0x000000B2 System.Void MusicOff::Off()
extern void MusicOff_Off_mF58992D363AE142B57F9420ED5BBCD34A24E819A (void);
// 0x000000B3 System.Void MusicOff::.ctor()
extern void MusicOff__ctor_m5FC4F3022C41A684CBF257FD98445B7F4FF3A35B (void);
// 0x000000B4 System.Void OnSpawn::Start()
extern void OnSpawn_Start_m0AB713FE36559E5127439B0830CE26EC7A169503 (void);
// 0x000000B5 System.Void OnSpawn::OnEnable()
extern void OnSpawn_OnEnable_mBF5476DE586EA1C3A593871380DBC2A78436C4E4 (void);
// 0x000000B6 System.Void OnSpawn::.ctor()
extern void OnSpawn__ctor_m0DC36293F1F7393C6A981BB8770E229C2694720D (void);
// 0x000000B7 System.Void ParticleStart::Start()
extern void ParticleStart_Start_m0B241BE7B9F212B2895EC61BE647DAD6E9CDE869 (void);
// 0x000000B8 System.Void ParticleStart::OnEnable()
extern void ParticleStart_OnEnable_m604147FB3FDDEDE48E0768D0C8B9F7A9FCC7361F (void);
// 0x000000B9 System.Collections.IEnumerator ParticleStart::Dying()
extern void ParticleStart_Dying_m5D0B1E9994445919D0F0185BB8E7E050D71E5AD0 (void);
// 0x000000BA System.Void ParticleStart::.ctor()
extern void ParticleStart__ctor_m410F4F6C3FD5B8A0E63123FDD61528EE676DEB26 (void);
// 0x000000BB System.Void ParticleStart/<Dying>d__11::.ctor(System.Int32)
extern void U3CDyingU3Ed__11__ctor_m58D732957B79282AA92ADF5F21813A7E85BCFFF0 (void);
// 0x000000BC System.Void ParticleStart/<Dying>d__11::System.IDisposable.Dispose()
extern void U3CDyingU3Ed__11_System_IDisposable_Dispose_mFA3AC7162FF2307CBFA845DD40FC27ED0BD267FC (void);
// 0x000000BD System.Boolean ParticleStart/<Dying>d__11::MoveNext()
extern void U3CDyingU3Ed__11_MoveNext_m4B25A8EA63A2B37710D2757EEEF2E65C8F75EA34 (void);
// 0x000000BE System.Object ParticleStart/<Dying>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDyingU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51C1911128E46618C6D1050F369891214787B76E (void);
// 0x000000BF System.Void ParticleStart/<Dying>d__11::System.Collections.IEnumerator.Reset()
extern void U3CDyingU3Ed__11_System_Collections_IEnumerator_Reset_m748FB5A362DC188E0B7A3A683E1F523A07E5EBCC (void);
// 0x000000C0 System.Object ParticleStart/<Dying>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CDyingU3Ed__11_System_Collections_IEnumerator_get_Current_m0DF20F30AB85F89DAC4C200ECF9C50B755DE0651 (void);
// 0x000000C1 System.Void PathFinding::Start()
extern void PathFinding_Start_mC5DD912FA73AE30FA6F1905C0216A2B567816F17 (void);
// 0x000000C2 System.Void PathFinding::Update()
extern void PathFinding_Update_m02852483EBD404D4339F38486B4A3A89B249E857 (void);
// 0x000000C3 System.Void PathFinding::.ctor()
extern void PathFinding__ctor_mAD680FEE693FBB0195CA6A565754C5219F395055 (void);
// 0x000000C4 System.Void Pausemenu::Awake()
extern void Pausemenu_Awake_m4218418B151104556DED3430BFD5F70BE8612A7F (void);
// 0x000000C5 System.Void Pausemenu::Start()
extern void Pausemenu_Start_m5BA8984CA18DBFC97825124953B701D6C7D13D23 (void);
// 0x000000C6 System.Void Pausemenu::Update()
extern void Pausemenu_Update_mB1A440D540419A407549F4A81B6EB557B44C6AEF (void);
// 0x000000C7 System.Void Pausemenu::resume()
extern void Pausemenu_resume_m6D4624BC79FA9004FF0BADA437A310D918B252E8 (void);
// 0x000000C8 System.Void Pausemenu::.ctor()
extern void Pausemenu__ctor_mAE16059E970694F1393D38A99F5C1FA88C045DD1 (void);
// 0x000000C9 System.Void PengaturMusic::Awake()
extern void PengaturMusic_Awake_m6CC96CD5ED718A466B109EAE5F0E90527E68DBA4 (void);
// 0x000000CA System.Void PengaturMusic::Bunyikan(UnityEngine.AudioClip)
extern void PengaturMusic_Bunyikan_m53D0C41E9E38A8ED82B647A027A152FEB37BBF18 (void);
// 0x000000CB System.Void PengaturMusic::.ctor()
extern void PengaturMusic__ctor_mC53F1DE39C2FBAB77A86F4C10A85D1E5FBBC49BD (void);
// 0x000000CC System.Void Pivot::Start()
extern void Pivot_Start_mA70ACD2DFACB9AA5C62E1A5CB00A5741A3B314AA (void);
// 0x000000CD System.Void Pivot::Update()
extern void Pivot_Update_mEF296087D06A1D972B0DFF91275F2F91409C5B17 (void);
// 0x000000CE System.Void Pivot::.ctor()
extern void Pivot__ctor_mC81037BBC6A40E24CA1B37EED8189912B58633E4 (void);
// 0x000000CF System.Void Bullet2d::Start()
extern void Bullet2d_Start_m2BC6CB256B667B0BA397B601A0FD2611DF47F1FC (void);
// 0x000000D0 System.Void Bullet2d::Update()
extern void Bullet2d_Update_m0E61DC6964E4BEFB9A0AA32AF16A3C872DCEC8CA (void);
// 0x000000D1 System.Void Bullet2d::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Bullet2d_OnCollisionEnter2D_m805264F221D35427F89D6C113FA8CD1DC7FD451A (void);
// 0x000000D2 System.Void Bullet2d::.ctor()
extern void Bullet2d__ctor_mF04CD500E656CA25BBB289FA74819C855E958070 (void);
// 0x000000D3 System.Void Char2DController::Awake()
extern void Char2DController_Awake_m37A35EA357F759E3803DD9E769E276CA45356E65 (void);
// 0x000000D4 System.Void Char2DController::Start()
extern void Char2DController_Start_m3208A97BEB9C90216919F819477F94E2F85D2A5F (void);
// 0x000000D5 System.Void Char2DController::FixedUpdate()
extern void Char2DController_FixedUpdate_m6E6B01820FF62F71727706A74B884294ADE1FC84 (void);
// 0x000000D6 System.Void Char2DController::Move(System.Single,System.Boolean,System.Boolean)
extern void Char2DController_Move_m04E59F20AFBE123396E22707420445B19A2C98FF (void);
// 0x000000D7 System.Void Char2DController::Flip()
extern void Char2DController_Flip_m537D8CEA5B388072668BD2E3C3656AFDD29B7BB7 (void);
// 0x000000D8 System.Void Char2DController::.ctor()
extern void Char2DController__ctor_mBC6D30BFFCCBF85FD0B56CE60E3C7C5ED95ABDAE (void);
// 0x000000D9 System.Void Char2DController/BoolEvent::.ctor()
extern void BoolEvent__ctor_m5824430D521751ED93958922213FC003282F4E09 (void);
// 0x000000DA System.Void Enemy2D::Start()
extern void Enemy2D_Start_m637914D140E4A19AEB83E78ACE2930332CA42F2C (void);
// 0x000000DB System.Void Enemy2D::Update()
extern void Enemy2D_Update_m1DB21F88BEB23B583D79F5203FD9F236DE2FCB00 (void);
// 0x000000DC System.Void Enemy2D::TakeDamage(System.Single)
extern void Enemy2D_TakeDamage_m6E12B0CAEF7A5531FF37CC1979062258E9BF4DA6 (void);
// 0x000000DD System.Collections.IEnumerator Enemy2D::Die()
extern void Enemy2D_Die_m3EC84CC04E8DD61905E8434CCEB48F25FD19BB56 (void);
// 0x000000DE System.Void Enemy2D::.ctor()
extern void Enemy2D__ctor_m53CDFD06F24B47991E763367C85F9196E544C841 (void);
// 0x000000DF System.Void Enemy2D/<Die>d__9::.ctor(System.Int32)
extern void U3CDieU3Ed__9__ctor_mADE2673E0AC151EEE2C2B8BE752FAF867D70790E (void);
// 0x000000E0 System.Void Enemy2D/<Die>d__9::System.IDisposable.Dispose()
extern void U3CDieU3Ed__9_System_IDisposable_Dispose_m2AE53DE0960F3C4AD19D9B539603A2F844447039 (void);
// 0x000000E1 System.Boolean Enemy2D/<Die>d__9::MoveNext()
extern void U3CDieU3Ed__9_MoveNext_m2E4E5071D4CC0733DAD2EFE42475369EEEBBE782 (void);
// 0x000000E2 System.Object Enemy2D/<Die>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDieU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51EEC3D1CC93BB56CEB071BFAEA5F897512C2FEB (void);
// 0x000000E3 System.Void Enemy2D/<Die>d__9::System.Collections.IEnumerator.Reset()
extern void U3CDieU3Ed__9_System_Collections_IEnumerator_Reset_m8DCFC9CAB164B3F18DC2EDC7E5F35DF765A182E0 (void);
// 0x000000E4 System.Object Enemy2D/<Die>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CDieU3Ed__9_System_Collections_IEnumerator_get_Current_mD3EED8BA14478BD8216B581B28EB81FEE5ECEECD (void);
// 0x000000E5 System.Void Player::Start()
extern void Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021 (void);
// 0x000000E6 System.Void Player::Update()
extern void Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3 (void);
// 0x000000E7 System.Void Player::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Player_OnCollisionEnter2D_m0448BB6F02AAB4967D3AB976DA22A24ACD8E7F90 (void);
// 0x000000E8 System.Collections.IEnumerator Player::OnCollisionExit2D(UnityEngine.Collision2D)
extern void Player_OnCollisionExit2D_m3313F2D12F1F1BA58D9E410CE7386136F5008BCF (void);
// 0x000000E9 System.Void Player::OnTriggerStay2D(UnityEngine.Collider2D)
extern void Player_OnTriggerStay2D_mC3EB58F8E44658D0E4588DD69260825E7CEF41CC (void);
// 0x000000EA System.Void Player::isExplode()
extern void Player_isExplode_m6FBDC0B247E9AEC1FA2DA508CE427F9DB761BA3C (void);
// 0x000000EB System.Void Player::HurtTime()
extern void Player_HurtTime_m44275D3999E32BD89DC9B5ECDF835753BE84CB7B (void);
// 0x000000EC System.Void Player::addHelt()
extern void Player_addHelt_m3C83D363A0C770AEFB7C916C9E95E4A6BF958AC0 (void);
// 0x000000ED System.Collections.IEnumerator Player::Die()
extern void Player_Die_m792E214E2658BA493E201D0C397C9E973457CA82 (void);
// 0x000000EE System.Void Player::CoinUp()
extern void Player_CoinUp_m35A4CA3E78D1EBB0822A62E124FCE2B13AFF61A9 (void);
// 0x000000EF System.Void Player::ResetCoin()
extern void Player_ResetCoin_mB0784D764663796543A0FED54A126377EFD68DA9 (void);
// 0x000000F0 System.Void Player::ResetHealth()
extern void Player_ResetHealth_m171F2D09DBAD09451FE6EA25E2274F6C651AC48A (void);
// 0x000000F1 System.Void Player::ResetPrice()
extern void Player_ResetPrice_m0EF10161DAE2762C5EABB030C396F75F1B0451CC (void);
// 0x000000F2 System.Void Player::.ctor()
extern void Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7 (void);
// 0x000000F3 System.Void Player::.cctor()
extern void Player__cctor_m38DB9DEEDAE88802EC9823ECFF4A17859AABF96F (void);
// 0x000000F4 System.Void Player/<OnCollisionExit2D>d__22::.ctor(System.Int32)
extern void U3COnCollisionExit2DU3Ed__22__ctor_mA8A4F25B6067CE0B9414AB9EC7C05D529E7C8386 (void);
// 0x000000F5 System.Void Player/<OnCollisionExit2D>d__22::System.IDisposable.Dispose()
extern void U3COnCollisionExit2DU3Ed__22_System_IDisposable_Dispose_mDD17E25B307F29A01D61EF5C767F10C624BA2A2A (void);
// 0x000000F6 System.Boolean Player/<OnCollisionExit2D>d__22::MoveNext()
extern void U3COnCollisionExit2DU3Ed__22_MoveNext_m9DB0774FE5DF55B27EC8BDD4A4A40533E2B910A3 (void);
// 0x000000F7 System.Object Player/<OnCollisionExit2D>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnCollisionExit2DU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m193C48EB097DA325334C0C839D3B48D9FD1238AF (void);
// 0x000000F8 System.Void Player/<OnCollisionExit2D>d__22::System.Collections.IEnumerator.Reset()
extern void U3COnCollisionExit2DU3Ed__22_System_Collections_IEnumerator_Reset_m772A97B6A8F765C1C881C238E0CA7428687C87E1 (void);
// 0x000000F9 System.Object Player/<OnCollisionExit2D>d__22::System.Collections.IEnumerator.get_Current()
extern void U3COnCollisionExit2DU3Ed__22_System_Collections_IEnumerator_get_Current_m2C711895EC942AE7CAFBAA314F76746A7B9CBE34 (void);
// 0x000000FA System.Void Player/<Die>d__27::.ctor(System.Int32)
extern void U3CDieU3Ed__27__ctor_m8857762760F3BCE10A3D88DD890FAD638A8D4702 (void);
// 0x000000FB System.Void Player/<Die>d__27::System.IDisposable.Dispose()
extern void U3CDieU3Ed__27_System_IDisposable_Dispose_mC61821F0D50A408F101518E86C449AC53FF660C0 (void);
// 0x000000FC System.Boolean Player/<Die>d__27::MoveNext()
extern void U3CDieU3Ed__27_MoveNext_m98991766CE141917F9F5F84A0F8F631084B96A03 (void);
// 0x000000FD System.Object Player/<Die>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDieU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m410D885DBDE33BCAABD4860F2043CF3E36EEBF70 (void);
// 0x000000FE System.Void Player/<Die>d__27::System.Collections.IEnumerator.Reset()
extern void U3CDieU3Ed__27_System_Collections_IEnumerator_Reset_m4D1BD6D7BA2AF7656BF349668CB2385963FD629F (void);
// 0x000000FF System.Object Player/<Die>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CDieU3Ed__27_System_Collections_IEnumerator_get_Current_m6BDD5E6FB780641B98FA16D648C42FD92BF42C05 (void);
// 0x00000100 System.Void PlayerMovement2D::Start()
extern void PlayerMovement2D_Start_m60C3A6A2CF75B0D0084F9A96DF0B747631E43E67 (void);
// 0x00000101 System.Void PlayerMovement2D::Update()
extern void PlayerMovement2D_Update_m47D0D45C40C119D5A33D3C842C962E0382F4E9CF (void);
// 0x00000102 System.Void PlayerMovement2D::OnLanding()
extern void PlayerMovement2D_OnLanding_m5E6FBA6E7FE4C54D3F97D0E1F02004EC05951EE8 (void);
// 0x00000103 System.Void PlayerMovement2D::FixedUpdate()
extern void PlayerMovement2D_FixedUpdate_m3EF1816FE4BB73220D1FA47989C535AAC5FC1A96 (void);
// 0x00000104 System.Void PlayerMovement2D::.ctor()
extern void PlayerMovement2D__ctor_mDD6A7A3899771410DCF3C21AA3C6293260610ACD (void);
// 0x00000105 System.Void Weapon::Start()
extern void Weapon_Start_m7216819D285B8CDF5DD9E18550E3DF9F1DD76D5A (void);
// 0x00000106 System.Void Weapon::Update()
extern void Weapon_Update_m03BCDE1EF0751A0F043AE300FD31A4BFA8B5D883 (void);
// 0x00000107 System.Void Weapon::Shoot()
extern void Weapon_Shoot_m8FC1208456BC2F62B9EB212EAC4D70FA5F60EE0E (void);
// 0x00000108 System.Void Weapon::.ctor()
extern void Weapon__ctor_m643DE56148B24BD987E564400E443ACDF43CDB97 (void);
// 0x00000109 System.Void Enemy::Start()
extern void Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02 (void);
// 0x0000010A System.Void Enemy::Update()
extern void Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2 (void);
// 0x0000010B System.Void Enemy::DamageHealth(UnityEngine.Vector3,System.Single)
extern void Enemy_DamageHealth_m54EF129B3FBF1BE19BFE6FC67A02426FA720F22B (void);
// 0x0000010C System.Void Enemy::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12 (void);
// 0x0000010D System.Collections.IEnumerator Enemy::Reset(UnityEngine.Vector3)
extern void Enemy_Reset_m628CD2AF324D1E1AB9149FCED22A7148CFBD056D (void);
// 0x0000010E System.Collections.IEnumerator Enemy::RemoveEnemy()
extern void Enemy_RemoveEnemy_mD2058B454847C883951FBE12A118697333EADF62 (void);
// 0x0000010F System.Void Enemy::.ctor()
extern void Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734 (void);
// 0x00000110 System.Void Enemy/<Reset>d__19::.ctor(System.Int32)
extern void U3CResetU3Ed__19__ctor_m5079434512C1D722152FD57D9692C8186A97D321 (void);
// 0x00000111 System.Void Enemy/<Reset>d__19::System.IDisposable.Dispose()
extern void U3CResetU3Ed__19_System_IDisposable_Dispose_mC4E423C14CAA780263C2CAD897FB06444822E352 (void);
// 0x00000112 System.Boolean Enemy/<Reset>d__19::MoveNext()
extern void U3CResetU3Ed__19_MoveNext_mE366C8AB633F0488B9A99D6D4A5F3BFC5457F10C (void);
// 0x00000113 System.Object Enemy/<Reset>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResetU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m165E9A0298191DAED6213B75F722A63C9C93732E (void);
// 0x00000114 System.Void Enemy/<Reset>d__19::System.Collections.IEnumerator.Reset()
extern void U3CResetU3Ed__19_System_Collections_IEnumerator_Reset_m4DFA3ED64ABCD8B3242221434025EC1D302916A7 (void);
// 0x00000115 System.Object Enemy/<Reset>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CResetU3Ed__19_System_Collections_IEnumerator_get_Current_mED384D8433E87A6CEBC3263205674A78D591C2C9 (void);
// 0x00000116 System.Void Enemy/<RemoveEnemy>d__20::.ctor(System.Int32)
extern void U3CRemoveEnemyU3Ed__20__ctor_m1E6B4D9F3646E854A30388BF4635D465618D628A (void);
// 0x00000117 System.Void Enemy/<RemoveEnemy>d__20::System.IDisposable.Dispose()
extern void U3CRemoveEnemyU3Ed__20_System_IDisposable_Dispose_m3745507487481DEB5DBAF8FA60A0AE9F0E670AB5 (void);
// 0x00000118 System.Boolean Enemy/<RemoveEnemy>d__20::MoveNext()
extern void U3CRemoveEnemyU3Ed__20_MoveNext_m58AAEAE32A23DA96A258EE9665D9992585065FAC (void);
// 0x00000119 System.Object Enemy/<RemoveEnemy>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRemoveEnemyU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAA95AD573C461C64AEAAA6315CFD544159B3B000 (void);
// 0x0000011A System.Void Enemy/<RemoveEnemy>d__20::System.Collections.IEnumerator.Reset()
extern void U3CRemoveEnemyU3Ed__20_System_Collections_IEnumerator_Reset_m56D0635675474E0F012360DB67C043F6DEE882B5 (void);
// 0x0000011B System.Object Enemy/<RemoveEnemy>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CRemoveEnemyU3Ed__20_System_Collections_IEnumerator_get_Current_m59443E41B8F69CE7426D70185454AC4A86C7DF3F (void);
// 0x0000011C System.Void Player2::Start()
extern void Player2_Start_m6D06697D194823D87339EE927BEEB0F1B2C53A2D (void);
// 0x0000011D System.Void Player2::Update()
extern void Player2_Update_mC7FF45F9D896EDD3C984AF5F636C958CB309DABD (void);
// 0x0000011E System.Void Player2::FixedUpdate()
extern void Player2_FixedUpdate_mCA5A4350D761687F49B16FA6D3C9D8A485D312FA (void);
// 0x0000011F System.Void Player2::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Player2_OnTriggerEnter2D_mE7892ACA13E10658D4884D344001110B0F30381B (void);
// 0x00000120 System.Collections.IEnumerator Player2::FlipXY()
extern void Player2_FlipXY_m2E8B59FAB876B6DFD536A9F63F4ACC0F6DA5AE8B (void);
// 0x00000121 System.Collections.IEnumerator Player2::NotFlipX()
extern void Player2_NotFlipX_m393306343FBC9E3BC569493BAA79BDC717A1B092 (void);
// 0x00000122 System.Collections.IEnumerator Player2::NotFlipY()
extern void Player2_NotFlipY_mFF387819DF73F7157B133CFE1C065061658951FA (void);
// 0x00000123 System.Void Player2::LockMovement()
extern void Player2_LockMovement_mF1AEA5A6D58E7EA094EDBB641C422912C51A1CC2 (void);
// 0x00000124 System.Void Player2::EndSwordAttack()
extern void Player2_EndSwordAttack_mE52CFDD468A8E6DB6C1E5F4454317EFFCCE5FED0 (void);
// 0x00000125 System.Void Player2::UnlockMovement()
extern void Player2_UnlockMovement_m4D417E029903D77B2CDCDB48EEBF01091AF1BCCD (void);
// 0x00000126 System.Void Player2::.ctor()
extern void Player2__ctor_m2482DA9BBDF12DCD2173A69F0661E0E4E25167F7 (void);
// 0x00000127 System.Void Player2/<FlipXY>d__24::.ctor(System.Int32)
extern void U3CFlipXYU3Ed__24__ctor_m131DB1A9C643FD92F6AF8100B1673A68E413C1C9 (void);
// 0x00000128 System.Void Player2/<FlipXY>d__24::System.IDisposable.Dispose()
extern void U3CFlipXYU3Ed__24_System_IDisposable_Dispose_m10B3BC1FED6E3FC8BCD0EB206E1AC28669F2EA2A (void);
// 0x00000129 System.Boolean Player2/<FlipXY>d__24::MoveNext()
extern void U3CFlipXYU3Ed__24_MoveNext_m7C7611677DBEDD025224E3C7AEDFCC26D83CB220 (void);
// 0x0000012A System.Object Player2/<FlipXY>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFlipXYU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAD58057BA5483CE0D43CE4B62009B70AF0DE2EB0 (void);
// 0x0000012B System.Void Player2/<FlipXY>d__24::System.Collections.IEnumerator.Reset()
extern void U3CFlipXYU3Ed__24_System_Collections_IEnumerator_Reset_mA87B6F0AEC5F1895B83ABC2DE5FC1ED6DD4BE20D (void);
// 0x0000012C System.Object Player2/<FlipXY>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CFlipXYU3Ed__24_System_Collections_IEnumerator_get_Current_m24CEFA38C44FD2871AC176AA4DA6C3F8AF5EAF8A (void);
// 0x0000012D System.Void Player2/<NotFlipX>d__25::.ctor(System.Int32)
extern void U3CNotFlipXU3Ed__25__ctor_m94F2F0163265685222ADFB187F09580678CA2145 (void);
// 0x0000012E System.Void Player2/<NotFlipX>d__25::System.IDisposable.Dispose()
extern void U3CNotFlipXU3Ed__25_System_IDisposable_Dispose_m6F4A9B4FE98D2DD965AF928F40AA6562E71DA52B (void);
// 0x0000012F System.Boolean Player2/<NotFlipX>d__25::MoveNext()
extern void U3CNotFlipXU3Ed__25_MoveNext_m1BDFD279397A8DE52FBB4249D9F7FC3A981B0C7F (void);
// 0x00000130 System.Object Player2/<NotFlipX>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNotFlipXU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13CA1D64DE97A4EBEBACA8356720DD9FE5756AB4 (void);
// 0x00000131 System.Void Player2/<NotFlipX>d__25::System.Collections.IEnumerator.Reset()
extern void U3CNotFlipXU3Ed__25_System_Collections_IEnumerator_Reset_m0E72C3BD6F7C5880500F39E88ECF4D8832FEF217 (void);
// 0x00000132 System.Object Player2/<NotFlipX>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CNotFlipXU3Ed__25_System_Collections_IEnumerator_get_Current_m307B7C1CAA37E1DAD29DD63844B00D7A5AC947C0 (void);
// 0x00000133 System.Void Player2/<NotFlipY>d__26::.ctor(System.Int32)
extern void U3CNotFlipYU3Ed__26__ctor_mD5791BAD4289B9AEA13D346B0D15A4085077458C (void);
// 0x00000134 System.Void Player2/<NotFlipY>d__26::System.IDisposable.Dispose()
extern void U3CNotFlipYU3Ed__26_System_IDisposable_Dispose_m92088D58772A6C6FAEF4B7208F110704E6488A2B (void);
// 0x00000135 System.Boolean Player2/<NotFlipY>d__26::MoveNext()
extern void U3CNotFlipYU3Ed__26_MoveNext_m098040834E2DEF50CBA62B7DE57E406972F247E4 (void);
// 0x00000136 System.Object Player2/<NotFlipY>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNotFlipYU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35E9597F83EF82C1BC20F307C6E63FE93EDA7307 (void);
// 0x00000137 System.Void Player2/<NotFlipY>d__26::System.Collections.IEnumerator.Reset()
extern void U3CNotFlipYU3Ed__26_System_Collections_IEnumerator_Reset_mC6D8CCA93FB0987B4081DED3D535729334345AF4 (void);
// 0x00000138 System.Object Player2/<NotFlipY>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CNotFlipYU3Ed__26_System_Collections_IEnumerator_get_Current_mE496955C82262A2E1E8BFDDFE55AB76A7ED97D97 (void);
// 0x00000139 System.Void PlayerBullet::Start()
extern void PlayerBullet_Start_mCC4F3931DDDF916F291C511DA657A85062BB1E3B (void);
// 0x0000013A System.Void PlayerBullet::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void PlayerBullet_OnCollisionEnter2D_mECAF6DAA52B3424483C277E4F98765732DD2E06A (void);
// 0x0000013B System.Void PlayerBullet::Update()
extern void PlayerBullet_Update_m3FF2F92BED9C38E4321B9F9C8058E23FABE58C23 (void);
// 0x0000013C System.Void PlayerBullet::.ctor()
extern void PlayerBullet__ctor_mF4C29AEE1BE6345F9B1FB4188945835E632F7243 (void);
// 0x0000013D System.Void SwordAttack::Start()
extern void SwordAttack_Start_m3A17DFBD1530A4E5C7CCAC79129E374698C191E2 (void);
// 0x0000013E System.Void SwordAttack::Update()
extern void SwordAttack_Update_mDE43BD736C4E9A6AFE2480D999368D7B472207A2 (void);
// 0x0000013F System.Void SwordAttack::AttackRight()
extern void SwordAttack_AttackRight_mADCFB6717C8D6DEDDF955F2DD77594FD8F422F89 (void);
// 0x00000140 System.Void SwordAttack::AttackLeft()
extern void SwordAttack_AttackLeft_mFD60AD9ACABD5D93FBE8C123528B12AB83D6CAC0 (void);
// 0x00000141 System.Void SwordAttack::AttackUp()
extern void SwordAttack_AttackUp_mCC2D33E9F29152F50A9E427CFBA728DC317C1751 (void);
// 0x00000142 System.Void SwordAttack::AttackDown()
extern void SwordAttack_AttackDown_mE0919A683FC30A77F5969556E6223EE499EECDF5 (void);
// 0x00000143 System.Collections.IEnumerator SwordAttack::StopAttack()
extern void SwordAttack_StopAttack_mE7DCA01D4E303DF75B5D784FDE348862D10618C9 (void);
// 0x00000144 System.Void SwordAttack::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void SwordAttack_OnTriggerEnter2D_m92C0053D7BD7A8AB7009CC2AE631D9EB89E3FAF5 (void);
// 0x00000145 System.Void SwordAttack::.ctor()
extern void SwordAttack__ctor_m2ABE5EE2DA6867F1A8BB12F57441ADAB8F9DD077 (void);
// 0x00000146 System.Void SwordAttack/<StopAttack>d__12::.ctor(System.Int32)
extern void U3CStopAttackU3Ed__12__ctor_mC892CCDD9137521226BFDB42F756F498ED9D9DE4 (void);
// 0x00000147 System.Void SwordAttack/<StopAttack>d__12::System.IDisposable.Dispose()
extern void U3CStopAttackU3Ed__12_System_IDisposable_Dispose_m050550BE33CA586AD6A3AFC4553AF767A80B7E0A (void);
// 0x00000148 System.Boolean SwordAttack/<StopAttack>d__12::MoveNext()
extern void U3CStopAttackU3Ed__12_MoveNext_m01893D5FD40E1A9F4CEE989FE2D6DCFF5C01BBF2 (void);
// 0x00000149 System.Object SwordAttack/<StopAttack>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStopAttackU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD070A2C7A382D082F76C43E10B370F5A161A1568 (void);
// 0x0000014A System.Void SwordAttack/<StopAttack>d__12::System.Collections.IEnumerator.Reset()
extern void U3CStopAttackU3Ed__12_System_Collections_IEnumerator_Reset_m7EB90774CFA286159F440225FF0844A9CA318451 (void);
// 0x0000014B System.Object SwordAttack/<StopAttack>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CStopAttackU3Ed__12_System_Collections_IEnumerator_get_Current_m8C32386D0F688C60F58C83D55D8232476A3FE9B2 (void);
// 0x0000014C System.Void ScriptPengaturGame::Start()
extern void ScriptPengaturGame_Start_m51E8EBD3D8C5110534058D1ADFA27566E870DC7F (void);
// 0x0000014D System.Void ScriptPengaturGame::Update()
extern void ScriptPengaturGame_Update_mF0D89519BE394C7FA5FE1876A019A32CC62707C0 (void);
// 0x0000014E System.Void ScriptPengaturGame::Awake()
extern void ScriptPengaturGame_Awake_m158F506795915AE35939CFDC50C9A56C304E024D (void);
// 0x0000014F System.Void ScriptPengaturGame::Respawning()
extern void ScriptPengaturGame_Respawning_m6E763DD53423B63E14B272FB71037CB72331F2DD (void);
// 0x00000150 System.Collections.IEnumerator ScriptPengaturGame::DelayedRespawn()
extern void ScriptPengaturGame_DelayedRespawn_mFAB0633263E6564AF5BB4EF59FABE7202696771E (void);
// 0x00000151 System.Void ScriptPengaturGame::blockArray()
extern void ScriptPengaturGame_blockArray_m3829B51F710B09DA07476250F8FE842114A03D4E (void);
// 0x00000152 System.Void ScriptPengaturGame::.ctor()
extern void ScriptPengaturGame__ctor_mA84248362751CA058151030963542AB7496985E1 (void);
// 0x00000153 System.Void ScriptPengaturGame/<DelayedRespawn>d__8::.ctor(System.Int32)
extern void U3CDelayedRespawnU3Ed__8__ctor_m40CD1912AF394DC2AC923A52C94357E0DF450D2B (void);
// 0x00000154 System.Void ScriptPengaturGame/<DelayedRespawn>d__8::System.IDisposable.Dispose()
extern void U3CDelayedRespawnU3Ed__8_System_IDisposable_Dispose_m870C5BECEB2FDF41D71A48F1252484616984C08D (void);
// 0x00000155 System.Boolean ScriptPengaturGame/<DelayedRespawn>d__8::MoveNext()
extern void U3CDelayedRespawnU3Ed__8_MoveNext_m668CB66C74874BE0AF17B24D2BD0BBA716FF48E7 (void);
// 0x00000156 System.Object ScriptPengaturGame/<DelayedRespawn>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayedRespawnU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m704FE12609434A38D732427EB89149F0E243013B (void);
// 0x00000157 System.Void ScriptPengaturGame/<DelayedRespawn>d__8::System.Collections.IEnumerator.Reset()
extern void U3CDelayedRespawnU3Ed__8_System_Collections_IEnumerator_Reset_mD5F85137107697D53140B274A3025147FCC06881 (void);
// 0x00000158 System.Object ScriptPengaturGame/<DelayedRespawn>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CDelayedRespawnU3Ed__8_System_Collections_IEnumerator_get_Current_mAA4466DE52655D1610F3E77D6DFF672364ADD55D (void);
// 0x00000159 System.Void Price::Start()
extern void Price_Start_m2BFA9180697CF979D8D77846074A3C44736590B5 (void);
// 0x0000015A System.Void Price::.ctor()
extern void Price__ctor_m6B6CD92F3A77D7478F182354DC16AC8B5D4A9CFE (void);
// 0x0000015B System.Void RandomPath::Start()
extern void RandomPath_Start_m967208E92FF229E97A8A80F89E87FB76B5706F04 (void);
// 0x0000015C System.Void RandomPath::CalculateVector3()
extern void RandomPath_CalculateVector3_m21985AD15D315C3670EAAB24E0414B94224DC6EC (void);
// 0x0000015D System.Void RandomPath::Update()
extern void RandomPath_Update_mBF7506F14129ECDFC8E603145020F6708F3CBFFB (void);
// 0x0000015E System.Void RandomPath::.ctor()
extern void RandomPath__ctor_m36E247123B9A44417B27A31AD20E6F022C23AF9C (void);
// 0x0000015F System.Void RandomPathEnemy::Start()
extern void RandomPathEnemy_Start_m6582250456D617EDFA8193B473A0774A0D859363 (void);
// 0x00000160 System.Void RandomPathEnemy::DamageHealth(UnityEngine.Vector3,System.Single)
extern void RandomPathEnemy_DamageHealth_m07F536EB04C245172459B5C06231056ED7797D8D (void);
// 0x00000161 System.Collections.IEnumerator RandomPathEnemy::Reset(UnityEngine.Vector3)
extern void RandomPathEnemy_Reset_m7AC5BB1855F12972ADA3278EE482A1C25BFFB8DD (void);
// 0x00000162 System.Collections.IEnumerator RandomPathEnemy::RemoveEnemy()
extern void RandomPathEnemy_RemoveEnemy_m3F3AA696BB27EC03B23500BA53FDA5BEE9652759 (void);
// 0x00000163 System.Void RandomPathEnemy::.ctor()
extern void RandomPathEnemy__ctor_m31F6AD2195A7339F531ACB6FA6E2EE4A00B01486 (void);
// 0x00000164 System.Void RandomPathEnemy/<Reset>d__16::.ctor(System.Int32)
extern void U3CResetU3Ed__16__ctor_m4A61B7D86F5085B4169CE600699751AD297884B6 (void);
// 0x00000165 System.Void RandomPathEnemy/<Reset>d__16::System.IDisposable.Dispose()
extern void U3CResetU3Ed__16_System_IDisposable_Dispose_mA222C493C8FD94E8FD1C9D3F7472859F5BACCB04 (void);
// 0x00000166 System.Boolean RandomPathEnemy/<Reset>d__16::MoveNext()
extern void U3CResetU3Ed__16_MoveNext_mAEB9FB96FB827D128D7D848F98321E0737EE8F72 (void);
// 0x00000167 System.Object RandomPathEnemy/<Reset>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResetU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A2CB645B1B0484C48CB991CDE86500798FAA66E (void);
// 0x00000168 System.Void RandomPathEnemy/<Reset>d__16::System.Collections.IEnumerator.Reset()
extern void U3CResetU3Ed__16_System_Collections_IEnumerator_Reset_m29FBCED23D03FA82F30CEC93EAD8BF33BA588ADF (void);
// 0x00000169 System.Object RandomPathEnemy/<Reset>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CResetU3Ed__16_System_Collections_IEnumerator_get_Current_m6A8464C3A44F86288FDC802B734BD79679BD8AC6 (void);
// 0x0000016A System.Void RandomPathEnemy/<RemoveEnemy>d__17::.ctor(System.Int32)
extern void U3CRemoveEnemyU3Ed__17__ctor_mDF5707187B155BFF83BB4292A37DC36184AE56EF (void);
// 0x0000016B System.Void RandomPathEnemy/<RemoveEnemy>d__17::System.IDisposable.Dispose()
extern void U3CRemoveEnemyU3Ed__17_System_IDisposable_Dispose_mBE1FBD45087F9AFB7A1697B658CB87630B689714 (void);
// 0x0000016C System.Boolean RandomPathEnemy/<RemoveEnemy>d__17::MoveNext()
extern void U3CRemoveEnemyU3Ed__17_MoveNext_m600AB3B8363C962FC925EC6D256CE11D148D58EB (void);
// 0x0000016D System.Object RandomPathEnemy/<RemoveEnemy>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRemoveEnemyU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC87690098968F6C42184EE1080E4E20289A85F33 (void);
// 0x0000016E System.Void RandomPathEnemy/<RemoveEnemy>d__17::System.Collections.IEnumerator.Reset()
extern void U3CRemoveEnemyU3Ed__17_System_Collections_IEnumerator_Reset_m50CF5CF07AD930842A88145B8EB6089F69155A83 (void);
// 0x0000016F System.Object RandomPathEnemy/<RemoveEnemy>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CRemoveEnemyU3Ed__17_System_Collections_IEnumerator_get_Current_m1EEF8BCDA0EE7B9BFB461F31DF62C5AA9CE89EC5 (void);
// 0x00000170 System.Void RepathEnemy::Start()
extern void RepathEnemy_Start_m3EA9F7E07FDCC9705479EEB9D16E5480A22E9F36 (void);
// 0x00000171 System.Void RepathEnemy::Update()
extern void RepathEnemy_Update_m77F75835D39785FA8F3BF66063EE032481FCE63B (void);
// 0x00000172 System.Void RepathEnemy::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void RepathEnemy_OnCollisionEnter2D_m2056470FE9E3B9B9350D3B9EDB41E105D7105400 (void);
// 0x00000173 System.Void RepathEnemy::.ctor()
extern void RepathEnemy__ctor_m2A86F3E7F7843F3DEB207A90A63404CA636FA189 (void);
// 0x00000174 System.Void Rotation::Start()
extern void Rotation_Start_mEACBFA06F8EFD7A5FDC79C980BA4D7B546B8B1B4 (void);
// 0x00000175 System.Void Rotation::Update()
extern void Rotation_Update_m9234709059E25785C44ACA1F2AE0E8D143DD6409 (void);
// 0x00000176 System.Void Rotation::FixedUpdate()
extern void Rotation_FixedUpdate_m43CDC81683CDA94FABC8416CF7AC5A5A453A818F (void);
// 0x00000177 System.Collections.IEnumerator Rotation::AttackSpin()
extern void Rotation_AttackSpin_mFCC4707E6C3F1686B797441B2CABE55A56772514 (void);
// 0x00000178 System.Void Rotation::.ctor()
extern void Rotation__ctor_mE37308CA0B03685698CC3A3EFD8EF606585B6297 (void);
// 0x00000179 System.Void Rotation/<AttackSpin>d__13::.ctor(System.Int32)
extern void U3CAttackSpinU3Ed__13__ctor_m4EA58DFAB7F07180EE63578CF3FE454562B21F9A (void);
// 0x0000017A System.Void Rotation/<AttackSpin>d__13::System.IDisposable.Dispose()
extern void U3CAttackSpinU3Ed__13_System_IDisposable_Dispose_m809F021C7039F1A4EA346723F825E2BEFFC7BB89 (void);
// 0x0000017B System.Boolean Rotation/<AttackSpin>d__13::MoveNext()
extern void U3CAttackSpinU3Ed__13_MoveNext_mB150857242B8F5593BE51A4801BDA09A4CF9EA8C (void);
// 0x0000017C System.Object Rotation/<AttackSpin>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttackSpinU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m460DF86C141E5B679E0E6964C89FFE1E7EECFD7D (void);
// 0x0000017D System.Void Rotation/<AttackSpin>d__13::System.Collections.IEnumerator.Reset()
extern void U3CAttackSpinU3Ed__13_System_Collections_IEnumerator_Reset_mC4DAB2FCF9E95B419ED3BD42CC01B3DAC3C6B5A8 (void);
// 0x0000017E System.Object Rotation/<AttackSpin>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CAttackSpinU3Ed__13_System_Collections_IEnumerator_get_Current_m48BD155E14859DB7FAA98B221A434C12740E8852 (void);
// 0x0000017F System.Void RotationBullet::Start()
extern void RotationBullet_Start_m4EB7F0AE4098EA753770A7FA2D0A03028D0B3024 (void);
// 0x00000180 System.Void RotationBullet::Update()
extern void RotationBullet_Update_m994E947B0D56EC399C30C662060EA29C07CFDDA0 (void);
// 0x00000181 System.Void RotationBullet::.ctor()
extern void RotationBullet__ctor_mCA398B771E7C5E12DB33F5C3D5B94B25946C3655 (void);
// 0x00000182 System.Void RotationToAxis::Start()
extern void RotationToAxis_Start_mA3969FC84E69D82DF2BA08DD99D08BC3D562EE42 (void);
// 0x00000183 System.Void RotationToAxis::Update()
extern void RotationToAxis_Update_mD99E5A3654D7010AE32E0A9A8E0D15AC3EE31ECC (void);
// 0x00000184 System.Void RotationToAxis::.ctor()
extern void RotationToAxis__ctor_mBD677F9300F744D125A52C8B8157A68D0523BD24 (void);
// 0x00000185 System.Void MenuSceneLoader::Awake()
extern void MenuSceneLoader_Awake_m18B4E09AAFDC9578469D066C50A8AC0054AA34AC (void);
// 0x00000186 System.Void MenuSceneLoader::.ctor()
extern void MenuSceneLoader__ctor_m27FD45CA6C6C8B579D2FA4EEDABBA35F2C7EF6BC (void);
// 0x00000187 System.Void PauseMenu::Awake()
extern void PauseMenu_Awake_mB04BCBDA84AEC654D1976EC3DF61A0CF68D2C86C (void);
// 0x00000188 System.Void PauseMenu::MenuOn()
extern void PauseMenu_MenuOn_m8973DC956DD5235381EE4ACB4A182AA5BF0EF2EA (void);
// 0x00000189 System.Void PauseMenu::MenuOff()
extern void PauseMenu_MenuOff_mCD1AF41F916B0C02D2FD6F82377DBEAFF7C30720 (void);
// 0x0000018A System.Void PauseMenu::OnMenuStatusChange()
extern void PauseMenu_OnMenuStatusChange_mD1E8BD9B9CCB274A83FFF0FE2E06E6ABD0361B4A (void);
// 0x0000018B System.Void PauseMenu::Update()
extern void PauseMenu_Update_m191CABDC11442A2CC104FC8B3244D04826E7BD57 (void);
// 0x0000018C System.Void PauseMenu::.ctor()
extern void PauseMenu__ctor_mA1A281F3359C234E5CF24FFEAC20C12C48D69018 (void);
// 0x0000018D System.Void SceneAndURLLoader::Awake()
extern void SceneAndURLLoader_Awake_m8F276157A2A5FA943EF7918D6CCDB81273317E23 (void);
// 0x0000018E System.Void SceneAndURLLoader::SceneLoad(System.String)
extern void SceneAndURLLoader_SceneLoad_m2B09BD48F419F49A6BD461DBC7B2290EC8632B06 (void);
// 0x0000018F System.Void SceneAndURLLoader::LoadURL(System.String)
extern void SceneAndURLLoader_LoadURL_m47E3E286E80F2D5B3E6A164C32F7E1B473532AE2 (void);
// 0x00000190 System.Void SceneAndURLLoader::.ctor()
extern void SceneAndURLLoader__ctor_m6DEE574FADF9E3E894594690CB2755F69D5D4BE5 (void);
// 0x00000191 System.Void CameraSwitch::OnEnable()
extern void CameraSwitch_OnEnable_mD422991EDD9D880928644EE1BC4E557EE644679C (void);
// 0x00000192 System.Void CameraSwitch::NextCamera()
extern void CameraSwitch_NextCamera_m38AB4521C129032FA1DA5154E09D36D0FE2DB257 (void);
// 0x00000193 System.Void CameraSwitch::.ctor()
extern void CameraSwitch__ctor_m550FCA9B0C24BBBC8BDBCAAAAA7BBF26312399FE (void);
// 0x00000194 System.Void LevelReset::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void LevelReset_OnPointerClick_m9625C8235343CB1DE6B089AD6F2D5ACF738072A4 (void);
// 0x00000195 System.Void LevelReset::.ctor()
extern void LevelReset__ctor_mFD2BB9AF1A9D7E795DDAAE2F33C1F0EB1FB31F07 (void);
// 0x00000196 System.Void BossDie::Start()
extern void BossDie_Start_mE761196BA9089B0D2E233065A66550315EEDA100 (void);
// 0x00000197 System.Void BossDie::Fixedupdate()
extern void BossDie_Fixedupdate_m17E71C4D8F8E4C531D60D786EBEA8C9844C6179C (void);
// 0x00000198 System.Void BossDie::Die()
extern void BossDie_Die_m046E9EE771EF362F0375A4F8749146569ED8FC6E (void);
// 0x00000199 System.Void BossDie::.ctor()
extern void BossDie__ctor_mB138FAFBD2381B4ECBCA053AF833C276EA533C57 (void);
// 0x0000019A System.Void Collectibles::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Collectibles_OnCollisionEnter2D_m0933EE2CAD2A856A60B7CA218F05E6FA4177C699 (void);
// 0x0000019B System.Void Collectibles::.ctor()
extern void Collectibles__ctor_m7872B3C2F538BB1BE3283979503FB7DCFCB1B89F (void);
// 0x0000019C System.Void Collectibles2::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Collectibles2_OnCollisionEnter2D_m2836C6193027241AE636959E2F5B31E2E5876853 (void);
// 0x0000019D System.Void Collectibles2::.ctor()
extern void Collectibles2__ctor_mFD41EF266C59AD135F8B5F72CAFA172766F7682C (void);
// 0x0000019E System.Void Collectibles3::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Collectibles3_OnCollisionEnter2D_mA9A92C11EDA764FAD82F361858921BB2D2B9FB0D (void);
// 0x0000019F System.Void Collectibles3::.ctor()
extern void Collectibles3__ctor_mC998A6E29B1F37E5E446F87BF69E1E2203765EEB (void);
// 0x000001A0 System.Void LEVELBUTTON::LEVELCHANGE()
extern void LEVELBUTTON_LEVELCHANGE_mE68B3FAC76FB2C5BF7C86D1B988790406D2C0927 (void);
// 0x000001A1 System.Void LEVELBUTTON::Bunyikan(UnityEngine.AudioClip)
extern void LEVELBUTTON_Bunyikan_m64A71F59CF43B798B69DCCBA49297A58A0CC693F (void);
// 0x000001A2 System.Void LEVELBUTTON::.ctor()
extern void LEVELBUTTON__ctor_m91361DFE9A90AACAC3617BCB0785A75FC3016D41 (void);
// 0x000001A3 System.Void LookForwardBoss::Update()
extern void LookForwardBoss_Update_m06A0A8342DBCB69189AD0DE1AEB0DB99E6CFC26C (void);
// 0x000001A4 System.Void LookForwardBoss::.ctor()
extern void LookForwardBoss__ctor_m0D463475B9A49C0590940C475A0891F5ABE3E981 (void);
// 0x000001A5 System.Void LookForwardatYaxis::Update()
extern void LookForwardatYaxis_Update_m5DEAFE9F7FEFC660D887EF87E370F2F2A9E521DE (void);
// 0x000001A6 System.Void LookForwardatYaxis::.ctor()
extern void LookForwardatYaxis__ctor_m0971DA7B1976EB77E6DECFC9F1793F0C2B38B777 (void);
// 0x000001A7 System.Void MoveForward::Start()
extern void MoveForward_Start_m01A14393269317C9EC0DE9C8B34C219475B5B9D0 (void);
// 0x000001A8 System.Void MoveForward::Update()
extern void MoveForward_Update_m1D1EA1D175B6E5FED262E5D66BD0F05A5A6C1B1E (void);
// 0x000001A9 System.Void MoveForward::.ctor()
extern void MoveForward__ctor_m79E20E057C5117B7FC19B2CB3146FCAF61ABE123 (void);
// 0x000001AA System.Void MoveForwardBoss::Start()
extern void MoveForwardBoss_Start_mEC433C5F0B199FEAFA2FF695ECE02C6DF59746F0 (void);
// 0x000001AB System.Void MoveForwardBoss::Update()
extern void MoveForwardBoss_Update_m0DCDB82FC7D4D4980DF2654FE710E647232DDAB4 (void);
// 0x000001AC System.Void MoveForwardBoss::.ctor()
extern void MoveForwardBoss__ctor_mFE304D24337FEA48909A2E49D3050D39544CF9C2 (void);
// 0x000001AD System.Void MoveForwardatYaxis::Start()
extern void MoveForwardatYaxis_Start_m3F359ADACC29592E697EE6A9521D409B202CB639 (void);
// 0x000001AE System.Void MoveForwardatYaxis::Update()
extern void MoveForwardatYaxis_Update_mAFE83CE4595D9F592AA3039015EB9FDB4A471C3A (void);
// 0x000001AF System.Void MoveForwardatYaxis::.ctor()
extern void MoveForwardatYaxis__ctor_m64A49E2381EF37D9076F5177BDAB55490F324221 (void);
// 0x000001B0 System.Void PausePane::Start()
extern void PausePane_Start_m59154AEA07D590F94A86169C9B5D50E30A864621 (void);
// 0x000001B1 System.Void PausePane::PausePanel()
extern void PausePane_PausePanel_mF2E057447C8451A4929D7C1E482A9724C509A009 (void);
// 0x000001B2 System.Void PausePane::StartPanel()
extern void PausePane_StartPanel_m16AC8968CC80B968A374CFFCA34C7A6CBBCEA628 (void);
// 0x000001B3 System.Void PausePane::.ctor()
extern void PausePane__ctor_m2432C2F54E28A4AB29B47ED16078535AC8D1110B (void);
// 0x000001B4 System.Void PengaturGame::Start()
extern void PengaturGame_Start_m2A5C82EAF8C0020B969E944507F0397742BD9CF6 (void);
// 0x000001B5 System.Void PengaturGame::TambahSkor(System.Int32)
extern void PengaturGame_TambahSkor_mDC364AE0457B13172465FBAEE34443FD01F4FCA1 (void);
// 0x000001B6 System.Void PengaturGame::Bunyikan(UnityEngine.AudioClip)
extern void PengaturGame_Bunyikan_m67FA0089716082B333648B0079E6F9DC75C84C4D (void);
// 0x000001B7 System.Void PengaturGame::Update()
extern void PengaturGame_Update_mDE3F609BD2301A561456EAE438FCC5EB9735F42E (void);
// 0x000001B8 System.Void PengaturGame::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void PengaturGame_OnCollisionEnter2D_m21275C883E00FF21C3EAD7E901B7E758940A90A1 (void);
// 0x000001B9 System.Void PengaturGame::buyanlifefromdemon()
extern void PengaturGame_buyanlifefromdemon_m532E7378C87D22B5DA67E4B1A58FCD0647370900 (void);
// 0x000001BA System.Collections.IEnumerator PengaturGame::Die()
extern void PengaturGame_Die_mB7093D4526DA560D2C9393D4984E3892F75B2574 (void);
// 0x000001BB System.Void PengaturGame::ResetCoin()
extern void PengaturGame_ResetCoin_m73AE359F615D521F003E44297C20ABD0A45E5D56 (void);
// 0x000001BC System.Void PengaturGame::ResetHealth()
extern void PengaturGame_ResetHealth_m27C1DB7B763BB00F97BEF6950893B1704184A759 (void);
// 0x000001BD System.Void PengaturGame::ResetPrice()
extern void PengaturGame_ResetPrice_m1ED8CE47F8A8C0D789ED128445B2EDCBADD8B031 (void);
// 0x000001BE System.Void PengaturGame::.ctor()
extern void PengaturGame__ctor_mF3B10B1B243FCF5CC9037BC99A07C5F4E34A5FBE (void);
// 0x000001BF System.Void PengaturGame::.cctor()
extern void PengaturGame__cctor_m61ED5A79741D9A734B6C53BF598D15A98E5D9B80 (void);
// 0x000001C0 System.Void PengaturGame/<Die>d__19::.ctor(System.Int32)
extern void U3CDieU3Ed__19__ctor_mCD289E975B415CDFE7F03327361CA771A9ACC945 (void);
// 0x000001C1 System.Void PengaturGame/<Die>d__19::System.IDisposable.Dispose()
extern void U3CDieU3Ed__19_System_IDisposable_Dispose_mA8D3F31A7FA44478E9DB61A959929BB795A8F824 (void);
// 0x000001C2 System.Boolean PengaturGame/<Die>d__19::MoveNext()
extern void U3CDieU3Ed__19_MoveNext_m2CDF825234F118CC76363457A7C475BD47BC01EA (void);
// 0x000001C3 System.Object PengaturGame/<Die>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDieU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5DB6F0BF77F660F069C9F3A2A60580D58BC136BF (void);
// 0x000001C4 System.Void PengaturGame/<Die>d__19::System.Collections.IEnumerator.Reset()
extern void U3CDieU3Ed__19_System_Collections_IEnumerator_Reset_m6C4B4656507A3C75D780F9043623876E56B280C6 (void);
// 0x000001C5 System.Object PengaturGame/<Die>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CDieU3Ed__19_System_Collections_IEnumerator_get_Current_m42A1443B0A7558052BB999AEA2950E2422012F8D (void);
// 0x000001C6 System.Void PlayerDie::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void PlayerDie_OnCollisionEnter2D_mCB73AE931EBC1329CBFFDE55CFEE615236B75E5E (void);
// 0x000001C7 System.Void PlayerDie::Die()
extern void PlayerDie_Die_mBDE9A00F9B4556CB5CC7C139353F4CA2AED729E5 (void);
// 0x000001C8 System.Void PlayerDie::.ctor()
extern void PlayerDie__ctor_m541544430EAA9AB2B1BDB199D128D8BEFEA0E52D (void);
// 0x000001C9 System.Void Quit::quitaplication()
extern void Quit_quitaplication_m37A555868257BE5B443279654015B5B12E8419AA (void);
// 0x000001CA System.Void Quit::Bunyikan(UnityEngine.AudioClip)
extern void Quit_Bunyikan_m6B39BEE86085666A2BED476C0643F2B4EED28542 (void);
// 0x000001CB System.Void Quit::.ctor()
extern void Quit__ctor_mEB6E7FE6F53362692AD96E4DEE1D3906A592A7C4 (void);
// 0x000001CC System.Void move::Start()
extern void move_Start_m68C6847F59645AD1B22C2CB5742DB521F7792586 (void);
// 0x000001CD System.Void move::Update()
extern void move_Update_mC6DF3D01C8AF42E268DC74B7C40A271D2295ABEC (void);
// 0x000001CE System.Void move::.ctor()
extern void move__ctor_mEA4EAF1DF04BED632395EC3F0721A0273A838739 (void);
// 0x000001CF System.Void playercamera::Update()
extern void playercamera_Update_m7CC33D7CF04E98D3BF3975B51D70666888555D8B (void);
// 0x000001D0 System.Void playercamera::.ctor()
extern void playercamera__ctor_m845AF78ED07EAA29634146805EB1B939D7759890 (void);
// 0x000001D1 System.Void ScriptPengaturGameTutor::Start()
extern void ScriptPengaturGameTutor_Start_m18D71E5D67BE39C75AC2818D0FD9EF98373FC072 (void);
// 0x000001D2 System.Void ScriptPengaturGameTutor::Awake()
extern void ScriptPengaturGameTutor_Awake_m942555D7CA8911988CEE74177F85E94301221C2C (void);
// 0x000001D3 System.Void ScriptPengaturGameTutor::Respawning()
extern void ScriptPengaturGameTutor_Respawning_m0F34EC9C15E364B29445D1A503D0E98C8837C9DA (void);
// 0x000001D4 System.Collections.IEnumerator ScriptPengaturGameTutor::DelayedRespawn()
extern void ScriptPengaturGameTutor_DelayedRespawn_m951994477B5FC95D9C1824E4C044B548A9F12869 (void);
// 0x000001D5 System.Void ScriptPengaturGameTutor::blockArray()
extern void ScriptPengaturGameTutor_blockArray_mDFB637B71B42807FCF0443D7496A2BC63BC5410D (void);
// 0x000001D6 System.Void ScriptPengaturGameTutor::.ctor()
extern void ScriptPengaturGameTutor__ctor_mFC05FC196998E63980DA000E87778536266FAA8C (void);
// 0x000001D7 System.Void ScriptPengaturGameTutor/<DelayedRespawn>d__6::.ctor(System.Int32)
extern void U3CDelayedRespawnU3Ed__6__ctor_m832D756A5E55217164F680E64AF1B43041752E1E (void);
// 0x000001D8 System.Void ScriptPengaturGameTutor/<DelayedRespawn>d__6::System.IDisposable.Dispose()
extern void U3CDelayedRespawnU3Ed__6_System_IDisposable_Dispose_m3ACCA8E0A4F478BFD532407AAF5C4458EB6D8131 (void);
// 0x000001D9 System.Boolean ScriptPengaturGameTutor/<DelayedRespawn>d__6::MoveNext()
extern void U3CDelayedRespawnU3Ed__6_MoveNext_mF60B2F6FF40A2C53A037E112E6BC983127208D5C (void);
// 0x000001DA System.Object ScriptPengaturGameTutor/<DelayedRespawn>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayedRespawnU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m48E50B3154FC7795127B06517938CAB13B89D71A (void);
// 0x000001DB System.Void ScriptPengaturGameTutor/<DelayedRespawn>d__6::System.Collections.IEnumerator.Reset()
extern void U3CDelayedRespawnU3Ed__6_System_Collections_IEnumerator_Reset_m814E90D8FD4532733B793DA5FDAF9FF3A57B2CDC (void);
// 0x000001DC System.Object ScriptPengaturGameTutor/<DelayedRespawn>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CDelayedRespawnU3Ed__6_System_Collections_IEnumerator_get_Current_m4949E5B7B143356AED4CB5677BBD70C93385687E (void);
// 0x000001DD System.Void ShadowMonster::Start()
extern void ShadowMonster_Start_m44D3A8FE491BD4C3932011C1EF74AE8593EA30C9 (void);
// 0x000001DE System.Void ShadowMonster::Update()
extern void ShadowMonster_Update_m17F530419193B6A5D01F2FCECC1E5C33F610DA8C (void);
// 0x000001DF System.Void ShadowMonster::FixedUpdate()
extern void ShadowMonster_FixedUpdate_m274D9657B0FC3DCF2D9ACD43B142D1EEDD341042 (void);
// 0x000001E0 System.Void ShadowMonster::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void ShadowMonster_OnCollisionEnter2D_m5549790B5145E49F8A9A34068C399B0F48DE3944 (void);
// 0x000001E1 System.Collections.IEnumerator ShadowMonster::Shadow()
extern void ShadowMonster_Shadow_m62273387B5D608A04893DADA04C1DF233B336B48 (void);
// 0x000001E2 System.Void ShadowMonster::Build()
extern void ShadowMonster_Build_m079BF377D106CB3E1A7D23A8F4F9BF6CE125048E (void);
// 0x000001E3 System.Void ShadowMonster::.ctor()
extern void ShadowMonster__ctor_mFDD35A68BC8B11FDA285F3D77DC60E50CBDE4BB2 (void);
// 0x000001E4 System.Void ShadowMonster/<Shadow>d__10::.ctor(System.Int32)
extern void U3CShadowU3Ed__10__ctor_m09156293353FA2A514EC009449AC2E4F8A7BA07E (void);
// 0x000001E5 System.Void ShadowMonster/<Shadow>d__10::System.IDisposable.Dispose()
extern void U3CShadowU3Ed__10_System_IDisposable_Dispose_m7D1378040012088EFC8545115F4E26B8BE9AD013 (void);
// 0x000001E6 System.Boolean ShadowMonster/<Shadow>d__10::MoveNext()
extern void U3CShadowU3Ed__10_MoveNext_mC2243B84AFCFFC700DCF851039648E2A0D5BC6CA (void);
// 0x000001E7 System.Object ShadowMonster/<Shadow>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShadowU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m158249148A41952D3D36E40F719C17F88C16E8F2 (void);
// 0x000001E8 System.Void ShadowMonster/<Shadow>d__10::System.Collections.IEnumerator.Reset()
extern void U3CShadowU3Ed__10_System_Collections_IEnumerator_Reset_mC690E70BB7DD3145DC2A8F2E682A15819094A724 (void);
// 0x000001E9 System.Object ShadowMonster/<Shadow>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CShadowU3Ed__10_System_Collections_IEnumerator_get_Current_m3B88444077C83B3FD6A4913F0ABB99A182145512 (void);
// 0x000001EA System.Void ShootLeaf::Start()
extern void ShootLeaf_Start_mC6FF02AC82D5C3A90FF14D2D2CB3D29D46C34AA9 (void);
// 0x000001EB System.Void ShootLeaf::FixedUpdate()
extern void ShootLeaf_FixedUpdate_m56F5A8837C3BFB46A4BFE99DBF0C5B8BF5003D66 (void);
// 0x000001EC System.Void ShootLeaf::.ctor()
extern void ShootLeaf__ctor_mFD57AF8D2D25246C6CCA0B8569EFDDF2D4E1ADE5 (void);
// 0x000001ED System.Void ShootPos::Start()
extern void ShootPos_Start_m9CB1B1CDE368364481F1F935813BF1E2FA1AAFF9 (void);
// 0x000001EE System.Void ShootPos::Update()
extern void ShootPos_Update_m3204B5E49E2C8A8B1BE3DAC8BE289AFD56BD596D (void);
// 0x000001EF System.Void ShootPos::GunRight()
extern void ShootPos_GunRight_m6358217619E4EAE2CD0E1C33C3BB62787BAB725A (void);
// 0x000001F0 System.Void ShootPos::GunLeft()
extern void ShootPos_GunLeft_m03F8C9E569ACD61FB2E2C8AAD84E914D0548F2BD (void);
// 0x000001F1 System.Void ShootPos::GunDown()
extern void ShootPos_GunDown_m223F86E1457897FAF12FE951A04922402FC6A020 (void);
// 0x000001F2 System.Void ShootPos::GunUp()
extern void ShootPos_GunUp_mBCA85CC9F4BE4D55A73B3FA62B34BE96C720DF46 (void);
// 0x000001F3 System.Void ShootPos::.ctor()
extern void ShootPos__ctor_m8E1B8EA0DDB1E000D3A0F6700FB7D62B6187E514 (void);
// 0x000001F4 System.Void SpikSound::Start()
extern void SpikSound_Start_m6572A4DE30CFB0894199DE7F922A2ABD880314E8 (void);
// 0x000001F5 System.Void SpikSound::Update()
extern void SpikSound_Update_mED1241CFFDE071448E2E6C6C4031D66A1A57F0C6 (void);
// 0x000001F6 System.Void SpikSound::SpikeUp()
extern void SpikSound_SpikeUp_m4C4289F78080151F9E271CC259F39E56ED04734D (void);
// 0x000001F7 System.Void SpikSound::.ctor()
extern void SpikSound__ctor_m00E2E93935390928E8F69BCE061B15C37E17B91D (void);
// 0x000001F8 System.Void SpikeBullet::Start()
extern void SpikeBullet_Start_m18B1112E15E25DE40A454B093A5C242487B3EDCE (void);
// 0x000001F9 System.Void SpikeBullet::Update()
extern void SpikeBullet_Update_mA25F2509DE76DA55174D9E07F69F6F994D5F05F6 (void);
// 0x000001FA System.Void SpikeBullet::Fire()
extern void SpikeBullet_Fire_m77A7910D429BCA1047CD5D9FE47B5F075EDD5CEF (void);
// 0x000001FB System.Void SpikeBullet::.ctor()
extern void SpikeBullet__ctor_m76CE956C301CB08780266BC15837B19EB1C6CB4E (void);
// 0x000001FC System.Void SpikeController::Start()
extern void SpikeController_Start_m5C8F2D44C0814D0E9175B7E3B32F0B51257C2139 (void);
// 0x000001FD System.Void SpikeController::Update()
extern void SpikeController_Update_m16FA14B230D1033D0C377C4BC7CBFDD18C898DBC (void);
// 0x000001FE System.Void SpikeController::SpikeUp()
extern void SpikeController_SpikeUp_mE654F6872E416547EAAEF1BBA981F2FCA25A9202 (void);
// 0x000001FF System.Void SpikeController::.ctor()
extern void SpikeController__ctor_mBB64FE5B499197E7772F33D8CB08E3A452FB0BD9 (void);
// 0x00000200 System.Void BGM::Awake()
extern void BGM_Awake_m3AFF9C1EC8C25A01D8C26676A3415E79C4BAB9C9 (void);
// 0x00000201 System.Void BGM::.ctor()
extern void BGM__ctor_mBB8F34CDDED6D1689C51EDB0028FBC9F14F9D9A5 (void);
// 0x00000202 System.Void BackgroundScroller::Start()
extern void BackgroundScroller_Start_mE4167530D769FE00F53556574705AE761E4D6544 (void);
// 0x00000203 System.Void BackgroundScroller::Update()
extern void BackgroundScroller_Update_m4DA04F8EB8FE8ED936C1EAA93EFCB904AA8AA457 (void);
// 0x00000204 System.Void BackgroundScroller::.ctor()
extern void BackgroundScroller__ctor_m5694164FDC22FFE5A19A3538CC6A0BA7FE328933 (void);
// 0x00000205 System.Void SceneLoad::QuitGame()
extern void SceneLoad_QuitGame_m639BF6FBD129BC3C6236FDB3CDF179C48D1D2DDE (void);
// 0x00000206 System.Void SceneLoad::StartGame()
extern void SceneLoad_StartGame_m9BF28A848B53F6C784041CB87109F95AFFF75F26 (void);
// 0x00000207 System.Void SceneLoad::.ctor()
extern void SceneLoad__ctor_m04E349D364791AB763A6418CF6B93D7403FDF65B (void);
// 0x00000208 System.Void StaticEnemy::Start()
extern void StaticEnemy_Start_m99639045D2042DC66FDBF97EE43023689D70F7C2 (void);
// 0x00000209 System.Void StaticEnemy::DamageHealth(UnityEngine.Vector3,System.Single)
extern void StaticEnemy_DamageHealth_m41918C78CED9398E41CAD6EFB0988C2532EDD40E (void);
// 0x0000020A System.Collections.IEnumerator StaticEnemy::Reset()
extern void StaticEnemy_Reset_m4A777D62276DC732D3191E4F48D267CF16B3D9A7 (void);
// 0x0000020B System.Collections.IEnumerator StaticEnemy::RemoveEnemy()
extern void StaticEnemy_RemoveEnemy_mF7FE9C88D58ADC37D2418A9AE8005C915DA206A6 (void);
// 0x0000020C System.Void StaticEnemy::Defeated()
extern void StaticEnemy_Defeated_m579E8D433FBAA42AB1F77FE522119B73352EE47B (void);
// 0x0000020D System.Void StaticEnemy::.ctor()
extern void StaticEnemy__ctor_mE920C944496AD4C1C9D3791643FF8D2BCB203F08 (void);
// 0x0000020E System.Void StaticEnemy/<Reset>d__12::.ctor(System.Int32)
extern void U3CResetU3Ed__12__ctor_m3BDA33E3CBC8E74B1D63ECE7624726D2F0ED5B33 (void);
// 0x0000020F System.Void StaticEnemy/<Reset>d__12::System.IDisposable.Dispose()
extern void U3CResetU3Ed__12_System_IDisposable_Dispose_m3A795EBB07033590127C289B6B5F5E49A5E567FC (void);
// 0x00000210 System.Boolean StaticEnemy/<Reset>d__12::MoveNext()
extern void U3CResetU3Ed__12_MoveNext_m195741AF6D30A042A89D3E0339AF96A23311C725 (void);
// 0x00000211 System.Object StaticEnemy/<Reset>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResetU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1061A8FD8BBDC1BD073A7C5D7EE759104350B430 (void);
// 0x00000212 System.Void StaticEnemy/<Reset>d__12::System.Collections.IEnumerator.Reset()
extern void U3CResetU3Ed__12_System_Collections_IEnumerator_Reset_mF82998DB5D5B49AFD9CE752D7397D9B67C176AB3 (void);
// 0x00000213 System.Object StaticEnemy/<Reset>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CResetU3Ed__12_System_Collections_IEnumerator_get_Current_m13B64BC583A8FE1918C67F176750728CD09C888F (void);
// 0x00000214 System.Void StaticEnemy/<RemoveEnemy>d__13::.ctor(System.Int32)
extern void U3CRemoveEnemyU3Ed__13__ctor_mFE8832B3C4256C83BAC43813904C447C2929A4F0 (void);
// 0x00000215 System.Void StaticEnemy/<RemoveEnemy>d__13::System.IDisposable.Dispose()
extern void U3CRemoveEnemyU3Ed__13_System_IDisposable_Dispose_mF1DA594D7D588A39061219178E77EC059C949BEB (void);
// 0x00000216 System.Boolean StaticEnemy/<RemoveEnemy>d__13::MoveNext()
extern void U3CRemoveEnemyU3Ed__13_MoveNext_mD6F0AC1B1CF0160B30A754BEC0248999DA44BCE2 (void);
// 0x00000217 System.Object StaticEnemy/<RemoveEnemy>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRemoveEnemyU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m439C009B65DD373B6730486E9589933915789BA3 (void);
// 0x00000218 System.Void StaticEnemy/<RemoveEnemy>d__13::System.Collections.IEnumerator.Reset()
extern void U3CRemoveEnemyU3Ed__13_System_Collections_IEnumerator_Reset_m245ABFC3107FFFC827EBDCCF997886EECB6439E8 (void);
// 0x00000219 System.Object StaticEnemy/<RemoveEnemy>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CRemoveEnemyU3Ed__13_System_Collections_IEnumerator_get_Current_m650FDB7D270B20CA532F4E1E0486C1783B84A4B1 (void);
// 0x0000021A System.Void SwitchHandler::OnSwitchButtonClicked()
extern void SwitchHandler_OnSwitchButtonClicked_mBB193A839F85C6AA5CB2D6EFF7A189C557365AD3 (void);
// 0x0000021B System.Void SwitchHandler::.ctor()
extern void SwitchHandler__ctor_mAC003FD2641DA42242DA11E7903F545AC011C824 (void);
// 0x0000021C System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2 (void);
// 0x0000021D System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B (void);
// 0x0000021E System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371 (void);
// 0x0000021F System.Void ChatController::.ctor()
extern void ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026 (void);
// 0x00000220 System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6 (void);
// 0x00000221 System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9 (void);
// 0x00000222 System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93 (void);
// 0x00000223 System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE (void);
// 0x00000224 System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC (void);
// 0x00000225 System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23 (void);
// 0x00000226 System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B (void);
// 0x00000227 System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE (void);
// 0x00000228 System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E (void);
// 0x00000229 System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB (void);
// 0x0000022A System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF (void);
// 0x0000022B System.Void ThwainMonster::Start()
extern void ThwainMonster_Start_mA186EDBD410AC6858AD9C0FC8214BEFF432F1C7D (void);
// 0x0000022C System.Void ThwainMonster::Update()
extern void ThwainMonster_Update_m2950A0B9A6BAC43C859A555C94CBEEC1D7793372 (void);
// 0x0000022D System.Void ThwainMonster::FixedUpdate()
extern void ThwainMonster_FixedUpdate_mCDA589FFE76A12BBE3735BA6037B2587380D886E (void);
// 0x0000022E System.Void ThwainMonster::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void ThwainMonster_OnCollisionEnter2D_mF792837DF4871983D54707DA37776F9558CBD3B4 (void);
// 0x0000022F System.Collections.IEnumerator ThwainMonster::Thawin()
extern void ThwainMonster_Thawin_m6C86C7E85FA477214831CC8EF072EE5F6EF506C3 (void);
// 0x00000230 System.Void ThwainMonster::Build()
extern void ThwainMonster_Build_m05D35675E0786B759B17C0AF2F067DDF76D4B693 (void);
// 0x00000231 System.Void ThwainMonster::.ctor()
extern void ThwainMonster__ctor_m25562718E78B182BC5BEFA95AFFAB267ED6AF1CF (void);
// 0x00000232 System.Void ThwainMonster/<Thawin>d__15::.ctor(System.Int32)
extern void U3CThawinU3Ed__15__ctor_mEF2892EA017B7E62EF004982AE3F56F38E6980A1 (void);
// 0x00000233 System.Void ThwainMonster/<Thawin>d__15::System.IDisposable.Dispose()
extern void U3CThawinU3Ed__15_System_IDisposable_Dispose_m6DF0994E7AD74DDC24DEAD3FAB93CB232F26AFDD (void);
// 0x00000234 System.Boolean ThwainMonster/<Thawin>d__15::MoveNext()
extern void U3CThawinU3Ed__15_MoveNext_m5C88BEA04F3B1E9441418243757FDCA998C9A4AF (void);
// 0x00000235 System.Object ThwainMonster/<Thawin>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CThawinU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF6D7461EC85545A6F83AB1D9F38638DCCCB7BDE3 (void);
// 0x00000236 System.Void ThwainMonster/<Thawin>d__15::System.Collections.IEnumerator.Reset()
extern void U3CThawinU3Ed__15_System_Collections_IEnumerator_Reset_m4C12473748C08E79BAC8333D134EE3E2E140773D (void);
// 0x00000237 System.Object ThwainMonster/<Thawin>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CThawinU3Ed__15_System_Collections_IEnumerator_get_Current_mC850258D3CE2A05A9A334F09705A22526894714D (void);
// 0x00000238 System.Void Yont?ry?::Start()
extern void YontU14DryU16B_Start_m91D08B18915498D327859B5E6A1ACE1C5812EEE7 (void);
// 0x00000239 System.Void Yont?ry?::Update()
extern void YontU14DryU16B_Update_m47807411507470E66AB14CBA31106FDA0DEA5F4E (void);
// 0x0000023A System.Void Yont?ry?::LockMovement()
extern void YontU14DryU16B_LockMovement_m8F50C5703F1829E2BA77CF037335BC15B4657896 (void);
// 0x0000023B System.Void Yont?ry?::Fire()
extern void YontU14DryU16B_Fire_mAEA2700A7D21F47718B21FB4B00962F875E3A720 (void);
// 0x0000023C System.Void Yont?ry?::StopAnimate()
extern void YontU14DryU16B_StopAnimate_m36A1DDC430B1929D6D30FE9FAE3B0CA570A8EEDC (void);
// 0x0000023D System.Void Yont?ry?::.ctor()
extern void YontU14DryU16B__ctor_m76A58247EFA381461A64BB7082A0B48E88DB9352 (void);
// 0x0000023E System.Void bullerAround::Start()
extern void bullerAround_Start_m2F9E951F2527DA531A3A3C6C3004F8D989723F88 (void);
// 0x0000023F System.Void bullerAround::Update()
extern void bullerAround_Update_m9A59D7128F58F4DAB1B70A96B178FBE16E84E172 (void);
// 0x00000240 System.Collections.IEnumerator bullerAround::FireAction()
extern void bullerAround_FireAction_mC13433AB56F7308581EBBD3C26B46E252AD6E812 (void);
// 0x00000241 System.Void bullerAround::Fire()
extern void bullerAround_Fire_mCABB15003FEEEC19D2425D4E400C0C48F5582252 (void);
// 0x00000242 System.Void bullerAround::TimerReset()
extern void bullerAround_TimerReset_mA030CA2549C400DF857B794FAA620EF0511D6263 (void);
// 0x00000243 System.Void bullerAround::.ctor()
extern void bullerAround__ctor_mCAB517DF82316EBAE0E76BDC11854A36E741155C (void);
// 0x00000244 System.Void bullerAround/<FireAction>d__12::.ctor(System.Int32)
extern void U3CFireActionU3Ed__12__ctor_m4D9A49C5E7CB1A36B37E5ADB70C420D347F72159 (void);
// 0x00000245 System.Void bullerAround/<FireAction>d__12::System.IDisposable.Dispose()
extern void U3CFireActionU3Ed__12_System_IDisposable_Dispose_m071496B05C3F27B2E7F7D606A63C698F0A69D2CF (void);
// 0x00000246 System.Boolean bullerAround/<FireAction>d__12::MoveNext()
extern void U3CFireActionU3Ed__12_MoveNext_m77B4161569C86AB013C018E9F8AD83BC04207A9D (void);
// 0x00000247 System.Object bullerAround/<FireAction>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFireActionU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF851ED90860DB287F90CA430F16C4DB28016597E (void);
// 0x00000248 System.Void bullerAround/<FireAction>d__12::System.Collections.IEnumerator.Reset()
extern void U3CFireActionU3Ed__12_System_Collections_IEnumerator_Reset_mE82756C227D55D51245493565A1CF7FF96305500 (void);
// 0x00000249 System.Object bullerAround/<FireAction>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CFireActionU3Ed__12_System_Collections_IEnumerator_get_Current_m5DE0834A1C06844043D41DBF2DB921DDE4C22A04 (void);
// 0x0000024A System.Void coinspawner::OnDisable()
extern void coinspawner_OnDisable_m8A6DAB43ED04819A0C04D4B98844A62C3E4D0A02 (void);
// 0x0000024B System.Void coinspawner::Start()
extern void coinspawner_Start_m16D6E23125805650829BBD61D88FFC6E58FF3E8F (void);
// 0x0000024C System.Void coinspawner::onCoin()
extern void coinspawner_onCoin_m7D2EECF31F20B81EAF6A40E9771C6C375EBFF82E (void);
// 0x0000024D System.Void coinspawner::SpawmCoim()
extern void coinspawner_SpawmCoim_mF16F8712DE127768ABE26EB5CC375AA35E9F7910 (void);
// 0x0000024E System.Void coinspawner::CalculateCoinPos()
extern void coinspawner_CalculateCoinPos_m3B8A77B493444DA73AA2B2EF845BF75C40D2EF4C (void);
// 0x0000024F System.Void coinspawner::Update()
extern void coinspawner_Update_m58C501B202650554152DE84A488201D48DD37806 (void);
// 0x00000250 System.Void coinspawner::.ctor()
extern void coinspawner__ctor_mE3A8B09E2E2A142EBCFF14171EED3F772B52DCFB (void);
// 0x00000251 System.Void AIPatrol::Start()
extern void AIPatrol_Start_m48A31A72A57AE182CC4E6852D87BE6C192A4E1A8 (void);
// 0x00000252 System.Void AIPatrol::Update()
extern void AIPatrol_Update_m387F4AF26FC4DA0D3503A142D9EFFFEF9F1A29DD (void);
// 0x00000253 System.Void AIPatrol::CheckPlayer()
extern void AIPatrol_CheckPlayer_m42C6A5BB1E69CC70B6240BD3EF4CE39021094F72 (void);
// 0x00000254 System.Void AIPatrol::FixedUpdate()
extern void AIPatrol_FixedUpdate_m3DB3CA34E9E89F890A6A355D5E122F63EC2C7226 (void);
// 0x00000255 System.Void AIPatrol::TakeDamage(System.Single)
extern void AIPatrol_TakeDamage_m94682AE31EF8E0B6F82AD859A360A98002DB85B0 (void);
// 0x00000256 System.Void AIPatrol::Patrol()
extern void AIPatrol_Patrol_m0927D4D505A6FB898E858F0616C3A95FCB961388 (void);
// 0x00000257 System.Void AIPatrol::Flip()
extern void AIPatrol_Flip_mCF7B762E04C0434E337BD34A2FA37AD3C4E5B332 (void);
// 0x00000258 System.Void AIPatrol::Shoot()
extern void AIPatrol_Shoot_m2D2C97E97A4A28D9D5CDDD682133123F905524E5 (void);
// 0x00000259 System.Void AIPatrol::OnDrawGizmos()
extern void AIPatrol_OnDrawGizmos_m68C0D29DCB5F246799B24315D0C533EB29CF5A22 (void);
// 0x0000025A System.Void AIPatrol::.ctor()
extern void AIPatrol__ctor_m0C69C3AEC5EA2C2EE0A086515055353A892FEAF2 (void);
// 0x0000025B System.Void PatrolBullets::Update()
extern void PatrolBullets_Update_m3675A77E11C8569B2C23773310D999ABCB7CCD1A (void);
// 0x0000025C System.Void PatrolBullets::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void PatrolBullets_OnCollisionEnter2D_m4CF9C65267915ECBEAD1CF7B38CC1E85D63C8FF8 (void);
// 0x0000025D System.Void PatrolBullets::.ctor()
extern void PatrolBullets__ctor_mB8E659F44688B51D1EE44E1B2EDC42EE9F1BCA05 (void);
// 0x0000025E System.Void pathfindingMonster::Start()
extern void pathfindingMonster_Start_mC3EA7174909E38EE78FE2E5DB394DA01AA027CB5 (void);
// 0x0000025F System.Void pathfindingMonster::Update()
extern void pathfindingMonster_Update_mB4BC4EE85998B4CD6077956A1FD8BCE64F2824DB (void);
// 0x00000260 System.Void pathfindingMonster::.ctor()
extern void pathfindingMonster__ctor_m9BC200F27CEA1802A1F056F3D7165D1AA6BF96E3 (void);
// 0x00000261 System.Void patrolandattack::Start()
extern void patrolandattack_Start_m8D19AEE91EBE18A58C25D451BAADCA9ECD72BD40 (void);
// 0x00000262 System.Void patrolandattack::Update()
extern void patrolandattack_Update_m620E83B2EAE6945259EBBA12F175294A29D0D4DB (void);
// 0x00000263 System.Void patrolandattack::PlayerDetection()
extern void patrolandattack_PlayerDetection_mE0E093751CFF25BC3A46E43CE619973511E96E37 (void);
// 0x00000264 System.Void patrolandattack::OnDrawGizmos()
extern void patrolandattack_OnDrawGizmos_mCC5D258D34D757317804E29797092881C0313236 (void);
// 0x00000265 System.Void patrolandattack::.ctor()
extern void patrolandattack__ctor_mE27A6E7CFFD282F2C7C7F731FB73548ABCBD16CD (void);
// 0x00000266 System.Void sepikwalk::Start()
extern void sepikwalk_Start_m74A35D23CD9AB70D4862D8C2B4EB3CA23A6595EF (void);
// 0x00000267 System.Void sepikwalk::Update()
extern void sepikwalk_Update_m72FCC0781BB9C49E6E87C0555CE3D7F1A54513CE (void);
// 0x00000268 System.Void sepikwalk::move()
extern void sepikwalk_move_mC4BCCF4B9C94068AA28C7567DA0392671FBD8583 (void);
// 0x00000269 System.Void sepikwalk::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void sepikwalk_OnCollisionEnter2D_mD17EAB3559013368220A9220F058A96B9E026B43 (void);
// 0x0000026A System.Void sepikwalk::.ctor()
extern void sepikwalk__ctor_m9ABC7103B213C0A8165578AFDF96EACCBB1BA466 (void);
// 0x0000026B System.Void shoot::Start()
extern void shoot_Start_mF5D7A829DFA7BC756DBD1DD8E52A11F134A47C31 (void);
// 0x0000026C System.Void shoot::Update()
extern void shoot_Update_m1442464A9FAECA5BA29D383CEC20C43B8CB69FBA (void);
// 0x0000026D System.Void shoot::FixedUpdate()
extern void shoot_FixedUpdate_m3279C63D80E255ACD4EA9A23A723069129DF17F1 (void);
// 0x0000026E System.Void shoot::.ctor()
extern void shoot__ctor_m36FCAE46441E2DCB7DBAA4F9B0398E8A045D901E (void);
// 0x0000026F System.Void shootBomb::Start()
extern void shootBomb_Start_m021A1853108881626E17824581D754608B625D99 (void);
// 0x00000270 System.Void shootBomb::FixedUpdate()
extern void shootBomb_FixedUpdate_m3991FEF0F3D00C85BA12C6DD9291D577FE14618A (void);
// 0x00000271 System.Void shootBomb::.ctor()
extern void shootBomb__ctor_m0D78F8673A839341DB28D2508FA9C9E9094A6CBC (void);
// 0x00000272 System.Void AnimatedTexture::Start()
extern void AnimatedTexture_Start_m971F531771EF3E15A907589C370E23651DEE5BBB (void);
// 0x00000273 System.Void AnimatedTexture::NextFrame()
extern void AnimatedTexture_NextFrame_mC674616396C363BC25FE982ACA8F5C4F2EFF8FA6 (void);
// 0x00000274 System.Void AnimatedTexture::.ctor()
extern void AnimatedTexture__ctor_m248560E947A15180F03C79868049A70054439CFD (void);
// 0x00000275 System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226 (void);
// 0x00000276 System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E (void);
// 0x00000277 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1 (void);
// 0x00000278 System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3 (void);
// 0x00000279 TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385 (void);
// 0x0000027A System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4 (void);
// 0x0000027B TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B (void);
// 0x0000027C System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788 (void);
// 0x0000027D TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A (void);
// 0x0000027E System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076 (void);
// 0x0000027F TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D (void);
// 0x00000280 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA (void);
// 0x00000281 TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB (void);
// 0x00000282 System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5 (void);
// 0x00000283 System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70 (void);
// 0x00000284 System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11 (void);
// 0x00000285 System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC (void);
// 0x00000286 System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44 (void);
// 0x00000287 System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC (void);
// 0x00000288 System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53 (void);
// 0x00000289 System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66 (void);
// 0x0000028A System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43 (void);
// 0x0000028B System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77 (void);
// 0x0000028C System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8 (void);
// 0x0000028D System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7 (void);
// 0x0000028E System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2 (void);
// 0x0000028F System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C (void);
// 0x00000290 System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018 (void);
// 0x00000291 System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0 (void);
// 0x00000292 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C (void);
// 0x00000293 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F (void);
// 0x00000294 System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB (void);
// 0x00000295 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73 (void);
// 0x00000296 System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C (void);
// 0x00000297 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591 (void);
// 0x00000298 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B (void);
// 0x00000299 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E (void);
// 0x0000029A System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4 (void);
// 0x0000029B System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376 (void);
// 0x0000029C System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651 (void);
// 0x0000029D System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561 (void);
// 0x0000029E System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1 (void);
// 0x0000029F System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD (void);
// 0x000002A0 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA (void);
// 0x000002A1 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2 (void);
// 0x000002A2 System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046 (void);
// 0x000002A3 System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5 (void);
// 0x000002A4 System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77 (void);
// 0x000002A5 System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516 (void);
// 0x000002A6 System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE (void);
// 0x000002A7 System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9 (void);
// 0x000002A8 System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C (void);
// 0x000002A9 System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E (void);
// 0x000002AA System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610 (void);
// 0x000002AB System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373 (void);
// 0x000002AC System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3 (void);
// 0x000002AD System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124 (void);
// 0x000002AE System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA (void);
// 0x000002AF System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3 (void);
// 0x000002B0 System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC (void);
// 0x000002B1 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8 (void);
// 0x000002B2 System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2 (void);
// 0x000002B3 System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469 (void);
// 0x000002B4 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64 (void);
// 0x000002B5 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A (void);
// 0x000002B6 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B (void);
// 0x000002B7 System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF (void);
// 0x000002B8 System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390 (void);
// 0x000002B9 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B (void);
// 0x000002BA System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362 (void);
// 0x000002BB System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F (void);
// 0x000002BC System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5 (void);
// 0x000002BD System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697 (void);
// 0x000002BE System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB (void);
// 0x000002BF System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F (void);
// 0x000002C0 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3 (void);
// 0x000002C1 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3 (void);
// 0x000002C2 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86 (void);
// 0x000002C3 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F (void);
// 0x000002C4 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF (void);
// 0x000002C5 System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF (void);
// 0x000002C6 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1 (void);
// 0x000002C7 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F (void);
// 0x000002C8 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A (void);
// 0x000002C9 System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4 (void);
// 0x000002CA System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB (void);
// 0x000002CB System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12 (void);
// 0x000002CC System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3 (void);
// 0x000002CD System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720 (void);
// 0x000002CE System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21 (void);
// 0x000002CF System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1 (void);
// 0x000002D0 System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C (void);
// 0x000002D1 System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF (void);
// 0x000002D2 System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A (void);
// 0x000002D3 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D (void);
// 0x000002D4 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0 (void);
// 0x000002D5 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B (void);
// 0x000002D6 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363 (void);
// 0x000002D7 System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C (void);
// 0x000002D8 System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B (void);
// 0x000002D9 System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE (void);
// 0x000002DA System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943 (void);
// 0x000002DB System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD (void);
// 0x000002DC System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0 (void);
// 0x000002DD System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66 (void);
// 0x000002DE System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF (void);
// 0x000002DF System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A (void);
// 0x000002E0 System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB (void);
// 0x000002E1 System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781 (void);
// 0x000002E2 System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910 (void);
// 0x000002E3 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691 (void);
// 0x000002E4 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D (void);
// 0x000002E5 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F (void);
// 0x000002E6 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F (void);
// 0x000002E7 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC (void);
// 0x000002E8 System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2 (void);
// 0x000002E9 System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33 (void);
// 0x000002EA System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E (void);
// 0x000002EB System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944 (void);
// 0x000002EC System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749 (void);
// 0x000002ED System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647 (void);
// 0x000002EE System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84 (void);
// 0x000002EF System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC (void);
// 0x000002F0 System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB (void);
// 0x000002F1 System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF (void);
// 0x000002F2 System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE (void);
// 0x000002F3 System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7 (void);
// 0x000002F4 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF (void);
// 0x000002F5 System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629 (void);
// 0x000002F6 System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9 (void);
// 0x000002F7 System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715 (void);
// 0x000002F8 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B (void);
// 0x000002F9 System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8 (void);
// 0x000002FA System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261 (void);
// 0x000002FB System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0 (void);
// 0x000002FC System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA (void);
// 0x000002FD System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50 (void);
// 0x000002FE System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712 (void);
// 0x000002FF System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8 (void);
// 0x00000300 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534 (void);
// 0x00000301 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4 (void);
// 0x00000302 System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848 (void);
// 0x00000303 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D (void);
// 0x00000304 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C (void);
// 0x00000305 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3 (void);
// 0x00000306 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83 (void);
// 0x00000307 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691 (void);
// 0x00000308 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F (void);
// 0x00000309 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0 (void);
// 0x0000030A System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8 (void);
// 0x0000030B System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210 (void);
// 0x0000030C System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB (void);
// 0x0000030D System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F (void);
// 0x0000030E System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27 (void);
// 0x0000030F System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046 (void);
// 0x00000310 System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1 (void);
// 0x00000311 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7 (void);
// 0x00000312 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF (void);
// 0x00000313 System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E (void);
// 0x00000314 System.Void TMPro.Examples.TextMeshProFloatingText::.cctor()
extern void TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8 (void);
// 0x00000315 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B (void);
// 0x00000316 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C (void);
// 0x00000317 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28 (void);
// 0x00000318 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963 (void);
// 0x00000319 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8 (void);
// 0x0000031A System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B (void);
// 0x0000031B System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20 (void);
// 0x0000031C System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3 (void);
// 0x0000031D System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A (void);
// 0x0000031E System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598 (void);
// 0x0000031F System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5 (void);
// 0x00000320 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6 (void);
// 0x00000321 System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB (void);
// 0x00000322 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA (void);
// 0x00000323 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1 (void);
// 0x00000324 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC (void);
// 0x00000325 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66 (void);
// 0x00000326 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42 (void);
// 0x00000327 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9 (void);
// 0x00000328 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F (void);
// 0x00000329 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007 (void);
// 0x0000032A System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6 (void);
// 0x0000032B System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51 (void);
// 0x0000032C System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932 (void);
// 0x0000032D System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52 (void);
// 0x0000032E System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F (void);
// 0x0000032F System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825 (void);
// 0x00000330 System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846 (void);
// 0x00000331 System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97 (void);
// 0x00000332 System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549 (void);
// 0x00000333 System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16 (void);
// 0x00000334 System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE (void);
// 0x00000335 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8 (void);
// 0x00000336 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099 (void);
// 0x00000337 System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B (void);
// 0x00000338 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479 (void);
// 0x00000339 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D (void);
// 0x0000033A System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF (void);
// 0x0000033B System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2 (void);
// 0x0000033C System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA (void);
// 0x0000033D System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2 (void);
// 0x0000033E System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE (void);
// 0x0000033F System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E (void);
// 0x00000340 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599 (void);
// 0x00000341 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62 (void);
// 0x00000342 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2 (void);
// 0x00000343 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81 (void);
// 0x00000344 System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F (void);
// 0x00000345 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE (void);
// 0x00000346 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA (void);
// 0x00000347 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E (void);
// 0x00000348 System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5 (void);
// 0x00000349 System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971 (void);
// 0x0000034A System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4 (void);
// 0x0000034B System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1 (void);
// 0x0000034C System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5 (void);
// 0x0000034D System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87 (void);
// 0x0000034E System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC (void);
// 0x0000034F System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760 (void);
// 0x00000350 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC (void);
// 0x00000351 System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E (void);
// 0x00000352 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D (void);
// 0x00000353 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB (void);
// 0x00000354 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD (void);
// 0x00000355 System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C (void);
// 0x00000356 System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC (void);
// 0x00000357 System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884 (void);
// 0x00000358 System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C (void);
// 0x00000359 System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F (void);
// 0x0000035A System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED (void);
// 0x0000035B System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2 (void);
// 0x0000035C System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348 (void);
// 0x0000035D System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107 (void);
// 0x0000035E System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2 (void);
// 0x0000035F System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F (void);
// 0x00000360 System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66 (void);
// 0x00000361 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9 (void);
// 0x00000362 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C (void);
// 0x00000363 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F (void);
// 0x00000364 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0 (void);
// 0x00000365 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4 (void);
// 0x00000366 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB (void);
// 0x00000367 System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2 (void);
// 0x00000368 System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099 (void);
// 0x00000369 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49 (void);
// 0x0000036A System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60 (void);
// 0x0000036B System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84 (void);
// 0x0000036C System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1 (void);
// 0x0000036D System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535 (void);
// 0x0000036E System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25 (void);
// 0x0000036F System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Awake()
extern void ParticleSceneControls_Awake_m3268C5B5B81D4EE59C85C27AED5CA3956C284DA4 (void);
// 0x00000370 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::OnDisable()
extern void ParticleSceneControls_OnDisable_mEB5EAF7BC1928EBEFD83E651459327948255D811 (void);
// 0x00000371 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Previous()
extern void ParticleSceneControls_Previous_mC2FE564898A6019238C5611E63B5EC3581CEAA63 (void);
// 0x00000372 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Next()
extern void ParticleSceneControls_Next_m4AB6C18CBE0148C4D354FDCAD5EE45D472577B8E (void);
// 0x00000373 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Update()
extern void ParticleSceneControls_Update_m7B5894AE70D3733C901BFFF529195AC1467D632B (void);
// 0x00000374 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::KeyboardInput()
extern void ParticleSceneControls_KeyboardInput_m29C494EC6ACF4EE139686EF47C3A167D1007970F (void);
// 0x00000375 System.Boolean UnityStandardAssets.SceneUtils.ParticleSceneControls::CheckForGuiCollision()
extern void ParticleSceneControls_CheckForGuiCollision_mB8EDC5E34CE1B59986991852009A011D9514A8FB (void);
// 0x00000376 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Select(System.Int32)
extern void ParticleSceneControls_Select_m123BA8121C7C5C454EF649A13C724F2622B4517A (void);
// 0x00000377 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::.ctor()
extern void ParticleSceneControls__ctor_mFE76B488C636F213B77193EDFF38F514A3186631 (void);
// 0x00000378 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::.cctor()
extern void ParticleSceneControls__cctor_mBA868BA9BC2400AD1AB420B3D0AB42D863358338 (void);
// 0x00000379 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::.ctor()
extern void DemoParticleSystem__ctor_mA046B73C934441755CA5D57991D7C453645AD8CE (void);
// 0x0000037A System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList::.ctor()
extern void DemoParticleSystemList__ctor_mAFBB16272EB2E695826211DD030553401DF6E83B (void);
// 0x0000037B System.Void UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::Update()
extern void PlaceTargetWithMouse_Update_m28E68FE68FA0185C0D20A6001F4C262868348BE3 (void);
// 0x0000037C System.Void UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::.ctor()
extern void PlaceTargetWithMouse__ctor_m3582FA7DC3CF8198244F9EB014FE12AC97985FE7 (void);
// 0x0000037D System.Void UnityStandardAssets.SceneUtils.SlowMoButton::Start()
extern void SlowMoButton_Start_m8F334286C4BEE772EAA4842F431E01BB4929A04C (void);
// 0x0000037E System.Void UnityStandardAssets.SceneUtils.SlowMoButton::OnDestroy()
extern void SlowMoButton_OnDestroy_mE8D51330ED641B12795E984B6F0ECCB00536DBDB (void);
// 0x0000037F System.Void UnityStandardAssets.SceneUtils.SlowMoButton::ChangeSpeed()
extern void SlowMoButton_ChangeSpeed_m0C19576C039AA7BBBC478C87A6C964376BA58EAB (void);
// 0x00000380 System.Void UnityStandardAssets.SceneUtils.SlowMoButton::.ctor()
extern void SlowMoButton__ctor_m993A85A43C7462445119B63636FD65A6F8C0F6BF (void);
// 0x00000381 UnityEngine.Color Krivodeling.UI.Effects.UIBlur::get_Color()
extern void UIBlur_get_Color_m37E56FDDFFE88F05C1AE6170EF2F23D3EF00A1FC (void);
// 0x00000382 System.Void Krivodeling.UI.Effects.UIBlur::set_Color(UnityEngine.Color)
extern void UIBlur_set_Color_mAE65462AD56D9DEC9E4E9B3FD1DA4B48FE8837EC (void);
// 0x00000383 Krivodeling.UI.Effects.FlipMode Krivodeling.UI.Effects.UIBlur::get_BuildFlipMode()
extern void UIBlur_get_BuildFlipMode_m6074D1333B72835ECE7C82DA6F8755D354CB0E06 (void);
// 0x00000384 System.Void Krivodeling.UI.Effects.UIBlur::set_BuildFlipMode(Krivodeling.UI.Effects.FlipMode)
extern void UIBlur_set_BuildFlipMode_m87C88BDF66A8EDEF80FD3A3C5A33590C8A0B0EC4 (void);
// 0x00000385 System.Single Krivodeling.UI.Effects.UIBlur::get_Intensity()
extern void UIBlur_get_Intensity_m70C8F8524F7CEE8889890DF69F5374225A73B5F4 (void);
// 0x00000386 System.Void Krivodeling.UI.Effects.UIBlur::set_Intensity(System.Single)
extern void UIBlur_set_Intensity_mA9B78D055CAF1C795D12C7673B4562902C0E8945 (void);
// 0x00000387 System.Single Krivodeling.UI.Effects.UIBlur::get_Multiplier()
extern void UIBlur_get_Multiplier_m3D17DC09769C0172C99A00D3C357C23B91DC2136 (void);
// 0x00000388 System.Void Krivodeling.UI.Effects.UIBlur::set_Multiplier(System.Single)
extern void UIBlur_set_Multiplier_m2128C4A1A5573151229751FACE73473DC7F6AE1D (void);
// 0x00000389 UnityEngine.Events.UnityEvent Krivodeling.UI.Effects.UIBlur::get_OnBeginBlur()
extern void UIBlur_get_OnBeginBlur_m4958233556FE6E69B9CFB10D00424A47F2602870 (void);
// 0x0000038A System.Void Krivodeling.UI.Effects.UIBlur::set_OnBeginBlur(UnityEngine.Events.UnityEvent)
extern void UIBlur_set_OnBeginBlur_m807E67E72612CAFF9C83FBA3A71B7A1981809AFB (void);
// 0x0000038B UnityEngine.Events.UnityEvent Krivodeling.UI.Effects.UIBlur::get_OnEndBlur()
extern void UIBlur_get_OnEndBlur_mF1CC15C438CBC43578415BA52504BA52ACD31E81 (void);
// 0x0000038C System.Void Krivodeling.UI.Effects.UIBlur::set_OnEndBlur(UnityEngine.Events.UnityEvent)
extern void UIBlur_set_OnEndBlur_m75277025B310055875C5BA20B8BD9EC666F1DAE4 (void);
// 0x0000038D Krivodeling.UI.Effects.UIBlur/BlurChangedEvent Krivodeling.UI.Effects.UIBlur::get_OnBlurChanged()
extern void UIBlur_get_OnBlurChanged_m8D0199CE8649433E1624FE2DD8B873AD1A94D0E0 (void);
// 0x0000038E System.Void Krivodeling.UI.Effects.UIBlur::set_OnBlurChanged(Krivodeling.UI.Effects.UIBlur/BlurChangedEvent)
extern void UIBlur_set_OnBlurChanged_m3936099CD2FA129F2E86CAF978D2F4B71D41FDFC (void);
// 0x0000038F System.Void Krivodeling.UI.Effects.UIBlur::UpdateBlur()
extern void UIBlur_UpdateBlur_m742EA33D6268AC62F517B418AE70EF8D69FB053C (void);
// 0x00000390 System.Void Krivodeling.UI.Effects.UIBlur::SetBlur(UnityEngine.Color,System.Single,System.Single)
extern void UIBlur_SetBlur_m74FEEE5B25F18A974936240A1B48F5CB843F3B31 (void);
// 0x00000391 System.Void Krivodeling.UI.Effects.UIBlur::BeginBlur(System.Single)
extern void UIBlur_BeginBlur_mAA4C8C9EC13B2346ED694F17DF6D99E51C7ABEA4 (void);
// 0x00000392 System.Void Krivodeling.UI.Effects.UIBlur::EndBlur(System.Single)
extern void UIBlur_EndBlur_m8C470ACEF4EEAFCA2023A1C335B1DC0CDC98D456 (void);
// 0x00000393 System.Void Krivodeling.UI.Effects.UIBlur::Displayarrow()
extern void UIBlur_Displayarrow_m6EAC69ACDCAF3164041DC55B77FA280483E3A2A2 (void);
// 0x00000394 System.Void Krivodeling.UI.Effects.UIBlur::Start()
extern void UIBlur_Start_mDFC44231F35C1B7BF4FEF7BFE2FBDF009B326471 (void);
// 0x00000395 System.Void Krivodeling.UI.Effects.UIBlur::SetComponents()
extern void UIBlur_SetComponents_mF86FCEE03FAC73A10A42E4D81C62BD03FFAE008B (void);
// 0x00000396 UnityEngine.Material Krivodeling.UI.Effects.UIBlur::FindMaterial()
extern void UIBlur_FindMaterial_m37CD281AB7975FF9B2B82735D6083DDB54B273C8 (void);
// 0x00000397 System.Void Krivodeling.UI.Effects.UIBlur::UpdateColor()
extern void UIBlur_UpdateColor_m564C88F0622C2B31F1A50B976B4FFBF1D272144B (void);
// 0x00000398 System.Void Krivodeling.UI.Effects.UIBlur::UpdateIntensity()
extern void UIBlur_UpdateIntensity_m3572315EC60F41642B334006B0F8C758C0E7FE32 (void);
// 0x00000399 System.Void Krivodeling.UI.Effects.UIBlur::UpdateMultiplier()
extern void UIBlur_UpdateMultiplier_mB5AA78F2C943181E69938DF30E92C59D43F00DE6 (void);
// 0x0000039A System.Void Krivodeling.UI.Effects.UIBlur::UpdateFlipMode()
extern void UIBlur_UpdateFlipMode_mDD7A50A15EA84DA2F59AAA0138BB7663BAC343E5 (void);
// 0x0000039B System.Collections.IEnumerator Krivodeling.UI.Effects.UIBlur::BeginBlurCoroutine(System.Single)
extern void UIBlur_BeginBlurCoroutine_m22E1A8448AD6438DAA161116C8B9AF7A5A1C7A5B (void);
// 0x0000039C System.Collections.IEnumerator Krivodeling.UI.Effects.UIBlur::EndBlurCoroutine(System.Single)
extern void UIBlur_EndBlurCoroutine_m636D49D1F1297AE5D5C1211455CDDABD524FAC35 (void);
// 0x0000039D System.Void Krivodeling.UI.Effects.UIBlur::.ctor()
extern void UIBlur__ctor_mA94F5595C4240506E12CF90C0746F0D81824212C (void);
// 0x0000039E System.Void Krivodeling.UI.Effects.UIBlur/BlurChangedEvent::.ctor()
extern void BlurChangedEvent__ctor_m5403806F9B87938C93AF8CF3A10C275100A30C5B (void);
// 0x0000039F System.Void Krivodeling.UI.Effects.UIBlur/<BeginBlurCoroutine>d__46::.ctor(System.Int32)
extern void U3CBeginBlurCoroutineU3Ed__46__ctor_m27D06DFB52FDBF0B49DA318368D0759928E1D1BF (void);
// 0x000003A0 System.Void Krivodeling.UI.Effects.UIBlur/<BeginBlurCoroutine>d__46::System.IDisposable.Dispose()
extern void U3CBeginBlurCoroutineU3Ed__46_System_IDisposable_Dispose_mF76EBD1A9FC137D2B4030A5EA027BE799790E4BE (void);
// 0x000003A1 System.Boolean Krivodeling.UI.Effects.UIBlur/<BeginBlurCoroutine>d__46::MoveNext()
extern void U3CBeginBlurCoroutineU3Ed__46_MoveNext_mCF71C0A082230AF52104FAEC5FE289586C57A92C (void);
// 0x000003A2 System.Object Krivodeling.UI.Effects.UIBlur/<BeginBlurCoroutine>d__46::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBeginBlurCoroutineU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD8D05546E56ECD61023F4A491559495A373F9488 (void);
// 0x000003A3 System.Void Krivodeling.UI.Effects.UIBlur/<BeginBlurCoroutine>d__46::System.Collections.IEnumerator.Reset()
extern void U3CBeginBlurCoroutineU3Ed__46_System_Collections_IEnumerator_Reset_mA7BFC75A2A8264EA28152664634CACF8494C525B (void);
// 0x000003A4 System.Object Krivodeling.UI.Effects.UIBlur/<BeginBlurCoroutine>d__46::System.Collections.IEnumerator.get_Current()
extern void U3CBeginBlurCoroutineU3Ed__46_System_Collections_IEnumerator_get_Current_m950F37AAD895B5E9640CEC4651660A2682611759 (void);
// 0x000003A5 System.Void Krivodeling.UI.Effects.UIBlur/<EndBlurCoroutine>d__47::.ctor(System.Int32)
extern void U3CEndBlurCoroutineU3Ed__47__ctor_m0AF52C6A23B8451C701EA167E43C7025E0E85856 (void);
// 0x000003A6 System.Void Krivodeling.UI.Effects.UIBlur/<EndBlurCoroutine>d__47::System.IDisposable.Dispose()
extern void U3CEndBlurCoroutineU3Ed__47_System_IDisposable_Dispose_mA8C03B32893FC08A9DC63FF29DB65C218216B925 (void);
// 0x000003A7 System.Boolean Krivodeling.UI.Effects.UIBlur/<EndBlurCoroutine>d__47::MoveNext()
extern void U3CEndBlurCoroutineU3Ed__47_MoveNext_mB73842F9774403E72A835B269A7FF64F883B9919 (void);
// 0x000003A8 System.Object Krivodeling.UI.Effects.UIBlur/<EndBlurCoroutine>d__47::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEndBlurCoroutineU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D4DFA555688E4463316E76AA7186C4650E8B20C (void);
// 0x000003A9 System.Void Krivodeling.UI.Effects.UIBlur/<EndBlurCoroutine>d__47::System.Collections.IEnumerator.Reset()
extern void U3CEndBlurCoroutineU3Ed__47_System_Collections_IEnumerator_Reset_mF6F0D7B092B4B08D62A8FC1FFE80F1236D268AAF (void);
// 0x000003AA System.Object Krivodeling.UI.Effects.UIBlur/<EndBlurCoroutine>d__47::System.Collections.IEnumerator.get_Current()
extern void U3CEndBlurCoroutineU3Ed__47_System_Collections_IEnumerator_get_Current_m777A091BCC1224A89508F8FD90276D7D6B1A8E6B (void);
// 0x000003AB System.Void Krivodeling.UI.Effects.Examples.MovingImage::Awake()
extern void MovingImage_Awake_m0D31880152C000DEF08A0E8C3D45DFAFDF3B598C (void);
// 0x000003AC System.Void Krivodeling.UI.Effects.Examples.MovingImage::Update()
extern void MovingImage_Update_mD1DCF8CC3FED5BEF3B46110E686674A3F505627F (void);
// 0x000003AD System.Void Krivodeling.UI.Effects.Examples.MovingImage::Move()
extern void MovingImage_Move_m866068146BA793161988CD41AE02AE580E9AE0A6 (void);
// 0x000003AE System.Collections.IEnumerator Krivodeling.UI.Effects.Examples.MovingImage::SetTargetCoroutine()
extern void MovingImage_SetTargetCoroutine_m5CAB8DC1170D3816EA911BC37EEB6319647200EC (void);
// 0x000003AF UnityEngine.Vector2 Krivodeling.UI.Effects.Examples.MovingImage::GetRandomTarget()
extern void MovingImage_GetRandomTarget_m66C8B7576FB36656899C6F52E8B45369782200C4 (void);
// 0x000003B0 UnityEngine.WaitForSeconds Krivodeling.UI.Effects.Examples.MovingImage::GetRandomWait()
extern void MovingImage_GetRandomWait_mF76DCC7EFA6456A09EBEA77C28CDE6E2ED7480D8 (void);
// 0x000003B1 System.Void Krivodeling.UI.Effects.Examples.MovingImage::.ctor()
extern void MovingImage__ctor_m73C9F985C03D1AB09E0D72132B0BE532E42B731D (void);
// 0x000003B2 System.Void Krivodeling.UI.Effects.Examples.MovingImage/<SetTargetCoroutine>d__12::.ctor(System.Int32)
extern void U3CSetTargetCoroutineU3Ed__12__ctor_m2DAA4DD13FEF3D7932EC3D4F52022DAA679B411A (void);
// 0x000003B3 System.Void Krivodeling.UI.Effects.Examples.MovingImage/<SetTargetCoroutine>d__12::System.IDisposable.Dispose()
extern void U3CSetTargetCoroutineU3Ed__12_System_IDisposable_Dispose_m4058148DD9EDFD7B905B0E3FA12486310F63B784 (void);
// 0x000003B4 System.Boolean Krivodeling.UI.Effects.Examples.MovingImage/<SetTargetCoroutine>d__12::MoveNext()
extern void U3CSetTargetCoroutineU3Ed__12_MoveNext_mA87788B18112C925B36D387EB036589EA8E54018 (void);
// 0x000003B5 System.Object Krivodeling.UI.Effects.Examples.MovingImage/<SetTargetCoroutine>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetTargetCoroutineU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A22C7DB41C1C1B32FF82AE0C0D7A4A26AC23750 (void);
// 0x000003B6 System.Void Krivodeling.UI.Effects.Examples.MovingImage/<SetTargetCoroutine>d__12::System.Collections.IEnumerator.Reset()
extern void U3CSetTargetCoroutineU3Ed__12_System_Collections_IEnumerator_Reset_m4590115325DA446C3B122D94B7DF7088DCB71F38 (void);
// 0x000003B7 System.Object Krivodeling.UI.Effects.Examples.MovingImage/<SetTargetCoroutine>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CSetTargetCoroutineU3Ed__12_System_Collections_IEnumerator_get_Current_m57C6B9D83546D5B22A1CAEE1B5F4E36334AF53E6 (void);
// 0x000003B8 System.Void Krivodeling.UI.Effects.Examples.UIBlurWithCanvasGroup::Start()
extern void UIBlurWithCanvasGroup_Start_m058FF7DCFC3D67A023A77B575BF41AEE54AF23BD (void);
// 0x000003B9 System.Void Krivodeling.UI.Effects.Examples.UIBlurWithCanvasGroup::SetComponents()
extern void UIBlurWithCanvasGroup_SetComponents_m96EE62BFC8705EA45BC5D14FE0CF1C226C32C18D (void);
// 0x000003BA System.Void Krivodeling.UI.Effects.Examples.UIBlurWithCanvasGroup::OnBeginBlur()
extern void UIBlurWithCanvasGroup_OnBeginBlur_m506912E4B986F28783729995C9771DE52B2976CE (void);
// 0x000003BB System.Void Krivodeling.UI.Effects.Examples.UIBlurWithCanvasGroup::OnBlurChanged(System.Single)
extern void UIBlurWithCanvasGroup_OnBlurChanged_m5DA32D6507DA517C29F73B7B0044A539C794D32E (void);
// 0x000003BC System.Void Krivodeling.UI.Effects.Examples.UIBlurWithCanvasGroup::OnEndBlur()
extern void UIBlurWithCanvasGroup_OnEndBlur_m149372157F72C96A9CF0859591488F4736FFFB8E (void);
// 0x000003BD System.Void Krivodeling.UI.Effects.Examples.UIBlurWithCanvasGroup::.ctor()
extern void UIBlurWithCanvasGroup__ctor_m0D93D37E6B6227863F5FB5FECF082A64733E91D1 (void);
// 0x000003BE System.Void JohnFarmer.KeyboardMouseSprites.Demo.Demo::Start()
extern void Demo_Start_m6657AB6877315735FC76B862C074502316882C35 (void);
// 0x000003BF System.Void JohnFarmer.KeyboardMouseSprites.Demo.Demo::OnGUI()
extern void Demo_OnGUI_m46E304B45F44828E960CE39B061CFF330BAA8DC5 (void);
// 0x000003C0 System.Void JohnFarmer.KeyboardMouseSprites.Demo.Demo::.ctor()
extern void Demo__ctor_m01A972C49C10179F83548F643CFF42C79A93E1B5 (void);
// 0x000003C1 System.Void Cainos.PixelArtTopDown_Basic.CameraFollow::Start()
extern void CameraFollow_Start_m7594BB7CDA736ED26755A569D73CD0974E1ED0AC (void);
// 0x000003C2 System.Void Cainos.PixelArtTopDown_Basic.CameraFollow::Update()
extern void CameraFollow_Update_m6ABA506E5AFC5631684C34E4C2C3ACCAC1A5E658 (void);
// 0x000003C3 System.Void Cainos.PixelArtTopDown_Basic.CameraFollow::.ctor()
extern void CameraFollow__ctor_mEB9340C858CE1A69F7E2016F752318396B6B67B5 (void);
// 0x000003C4 System.Void Cainos.PixelArtTopDown_Basic.LayerTrigger::OnTriggerExit2D(UnityEngine.Collider2D)
extern void LayerTrigger_OnTriggerExit2D_m8D2DFF92361F34E8EC6CDAF2CFFE69EC67B2772E (void);
// 0x000003C5 System.Void Cainos.PixelArtTopDown_Basic.LayerTrigger::.ctor()
extern void LayerTrigger__ctor_m4FAD7D9F152317D404D4A254991ABBF4484795B1 (void);
// 0x000003C6 System.Void Cainos.PixelArtTopDown_Basic.PropsAltar::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void PropsAltar_OnTriggerEnter2D_m1151A65ECD1659EC2B4557829E68BD2CF05AD0F1 (void);
// 0x000003C7 System.Void Cainos.PixelArtTopDown_Basic.PropsAltar::OnTriggerExit2D(UnityEngine.Collider2D)
extern void PropsAltar_OnTriggerExit2D_m9C4940E7BCF1B43C4E2E328D6FBB31BC7178F0E8 (void);
// 0x000003C8 System.Void Cainos.PixelArtTopDown_Basic.PropsAltar::Update()
extern void PropsAltar_Update_mC55F80C6BC6D2E28B2177C9BB4132695C46BF116 (void);
// 0x000003C9 System.Void Cainos.PixelArtTopDown_Basic.PropsAltar::.ctor()
extern void PropsAltar__ctor_mCD0C6E769E9A4E769513F9E69968690838137E17 (void);
// 0x000003CA System.Void Cainos.PixelArtTopDown_Basic.SpriteColorAnimation::Start()
extern void SpriteColorAnimation_Start_mD642B1D49DE0FFBB495D0B3C67F38833FB8553C5 (void);
// 0x000003CB System.Void Cainos.PixelArtTopDown_Basic.SpriteColorAnimation::Update()
extern void SpriteColorAnimation_Update_mFA5366D38B7542D73E1120DB4A51A3488BA4E905 (void);
// 0x000003CC System.Void Cainos.PixelArtTopDown_Basic.SpriteColorAnimation::.ctor()
extern void SpriteColorAnimation__ctor_m686C271C1E3977E544BEBEDEB24308E66EE9F14D (void);
// 0x000003CD System.Void Cainos.PixelArtTopDown_Basic.TopDownCharacterController::Start()
extern void TopDownCharacterController_Start_m06C85C7B1F2AA613EF490A0D02747E2448BFA850 (void);
// 0x000003CE System.Void Cainos.PixelArtTopDown_Basic.TopDownCharacterController::Update()
extern void TopDownCharacterController_Update_mA557EC76CB2058E2B74353C611F9E3290063F856 (void);
// 0x000003CF System.Void Cainos.PixelArtTopDown_Basic.TopDownCharacterController::.ctor()
extern void TopDownCharacterController__ctor_m3CC0CCA46D3ACF9D4FA020490EE0CAB07DBB525C (void);
// 0x000003D0 System.Void Pathfinding.AIDestinationSetter2::Start()
extern void AIDestinationSetter2_Start_m5BCEB60D888F8BAA78B448BDD7536DD431995E2A (void);
// 0x000003D1 System.Void Pathfinding.AIDestinationSetter2::OnEnable()
extern void AIDestinationSetter2_OnEnable_mB323EA5D333D6F6BE8BC4DA8705ECF327D5CC28A (void);
// 0x000003D2 System.Void Pathfinding.AIDestinationSetter2::OnDisable()
extern void AIDestinationSetter2_OnDisable_mD1B14D4AB7DE3A3F3296DB039D6CEA0C0B2BC766 (void);
// 0x000003D3 System.Void Pathfinding.AIDestinationSetter2::Update()
extern void AIDestinationSetter2_Update_mE3A989203C22682953A23BD8418B089FF36DD97B (void);
// 0x000003D4 System.Void Pathfinding.AIDestinationSetter2::.ctor()
extern void AIDestinationSetter2__ctor_m670EC8F565336F84C5D30D680AEF03D35528938C (void);
// 0x000003D5 System.Void Pathfinding.AIDestinationSetter3::Start()
extern void AIDestinationSetter3_Start_mAE6545A4FD163030D2F400A84395280E5EA8F033 (void);
// 0x000003D6 System.Void Pathfinding.AIDestinationSetter3::OnEnable()
extern void AIDestinationSetter3_OnEnable_m167D845C6296D14A9B7DFCB24288F040177F6C49 (void);
// 0x000003D7 System.Void Pathfinding.AIDestinationSetter3::OnDisable()
extern void AIDestinationSetter3_OnDisable_mBCF0ED02563E7216AE9B0537E4DCDB0EA4FB2992 (void);
// 0x000003D8 System.Void Pathfinding.AIDestinationSetter3::Update()
extern void AIDestinationSetter3_Update_mB4E3EFAD147420423C8203DAD6534D92AF740AF0 (void);
// 0x000003D9 System.Void Pathfinding.AIDestinationSetter3::.ctor()
extern void AIDestinationSetter3__ctor_mF3779A47BA831FB4AC6500DD0EAAB7E3998B5AD8 (void);
static Il2CppMethodPointer s_methodPointers[985] = 
{
	BIGROOMSCRIPT_Start_mA5AE2F212455D0C098491C75F50A6C4369414B5B,
	BIGROOMSCRIPT_Update_mD28ED5E802BCED44CE61E33D95E35FFC6DF1DFFA,
	BIGROOMSCRIPT_OnTriggerEnter2D_mDC132E497596FB4D45D327A48F7106AC2D8234E6,
	BIGROOMSCRIPT_OnTriggerExit2D_m1D083333C270AD657513FE5EF9DDDF55B7570B1C,
	BIGROOMSCRIPT__ctor_m3A038027784ACE324EB4475C02EFEEAE9C75DDC0,
	ReadmeVE3__ctor_mDC9CB74E2B03BC3D384AD9664B3CA75BF96EA8CE,
	Section__ctor_m5AB9E62F43D397F29091ACA0944408FA649F0F8A,
	BombAction_Start_mE8CE93E9CFBF0745C0D3CFE7AED363C0112FA576,
	BombAction_Update_m67A6CF122A439057F17F77C3A71E37DD34AB0045,
	BombAction_SetMoveDirection_m73A344FBBE62107E98953586CB2C5E4CF7029F3F,
	BombAction_BombExplode_mBBCB6A87E2BD076879BE0B674CD60DFEECB12E1A,
	BombAction_Explode_mC686A1B4FCF8B422AAC5243AD6998AB97C88C07C,
	BombAction_OnCollisionEnter2D_m853654A86D191FD414F06D9BF37BBAAB03B751C2,
	BombAction_OnDrawGizmos_m24BA93D4AF79175EC7DBE08D83217047DD663C7F,
	BombAction__ctor_mBAB45B282630878D1A12ECDA80D9472EB7114A07,
	U3CBombExplodeU3Ed__13__ctor_m923CB05D2C511E33FDCFB085649F5DAA1CCAF7BD,
	U3CBombExplodeU3Ed__13_System_IDisposable_Dispose_m15C4650CE46C34DFC8C5830A307D37CC569AAE0F,
	U3CBombExplodeU3Ed__13_MoveNext_m65B303C3A263089B2CFD846E8D61972E871D2B3A,
	U3CBombExplodeU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72951C1ADD21C1776A9F6F9FBBB1784F6CCF401A,
	U3CBombExplodeU3Ed__13_System_Collections_IEnumerator_Reset_m623ED216F0519489B9F92EFEB501539FBE710AE2,
	U3CBombExplodeU3Ed__13_System_Collections_IEnumerator_get_Current_mD297AFE797BA2B44311CCDB9548E4366A41D37FA,
	BombScript_Start_m837BDF2F01719906AA59E1FFC7791DBA3E86DE75,
	BombScript_Update_m3255BE52A49ADE17C35892C2C0506D754D93E25F,
	BombScript_FixedUpdate_mEC9B9A7A4CC93BF92008B351C3663448DCB5062E,
	BombScript__ctor_m2A65AA9D6A32A3897038DDD5AC7798F26A9276F3,
	Bullet_Start_m58181B46F80FE1ABECE1AFF455C253B51B7EB0E4,
	Bullet_Update_mB82408CA535D8533168045E7C0321090448B596B,
	Bullet_SetMoveDirection_mB2F5E5A9D3647F263218A80AC50894734C544BC4,
	Bullet_OnCollisionEnter2D_m4CC80D190EE3A6B6E2EDA524A121942D0285D0A9,
	Bullet__ctor_mC7D931FE508342F413FBA79525A4819D4114B3EC,
	BulletHell_Start_mC04D30FBA960AA1B19EA9E941CD82468C390BFE1,
	BulletHell_Update_mDA120AE754C8F90C302E25E29BBB0D3824245FCE,
	BulletHell_RandomPosition_m2F2FF558EEAAEBF2661F42902CE35044F6EE0F56,
	BulletHell_LockMovement_m58BD253063F504BE3A63BB7CBECF682C89CCC7D1,
	BulletHell_Spawn_m5D55FB3B9D8779803CCBF6892E4197216A886135,
	BulletHell_Fire_mC568115CB04C6EDC545709E907FD53363F2374B5,
	BulletHell_StopAnimate_m8B4AEC8230A9B55318F089C0FCDD926949E21F14,
	BulletHell__ctor_m797485160FABC324EB3E13D8B0F02F5F3E55B167,
	BulletRotationToPlayer_Start_m01E4EC481205DE680B3D7B99B04C8A5F0A00AED3,
	BulletRotationToPlayer_Update_m5199561978B30628DBE38CB3CFC12341D918D116,
	BulletRotationToPlayer__ctor_mBE7D0C6C49AFB600ACCB789CBFC3A899757B3737,
	BulletScript_OnCollisionEnter2D_m8050C14632E7C2C21DB373C5EB984C5C4DA0730C,
	BulletScript_OnCollisionExit2D_m39DD1005BC9F225C87231175DC010294F39C09B4,
	BulletScript_Update_mEEA5A09DE25F213593101C9F9B0BBB3D20348EAF,
	BulletScript__ctor_m100E1C957FFAB49CD0E873E7A47EF15F8DCD5E19,
	U3COnCollisionExit2DU3Ed__4__ctor_m14AF5686030543F733A7415FDCAD1BF3AB949BEC,
	U3COnCollisionExit2DU3Ed__4_System_IDisposable_Dispose_m73D6E1249D30E89DDD8894FCC44BF8D01F448D00,
	U3COnCollisionExit2DU3Ed__4_MoveNext_mB86C0D8D4B9AABC20D47227F1645335011D63E73,
	U3COnCollisionExit2DU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A272FB97A0973E2DF45FAE555831D5023594805,
	U3COnCollisionExit2DU3Ed__4_System_Collections_IEnumerator_Reset_m9337004461BBF6ECF0723DBD6E0EBDE65CA9ADE2,
	U3COnCollisionExit2DU3Ed__4_System_Collections_IEnumerator_get_Current_mC32AD54E8C558A329810C8701029A8B9B31E46B1,
	BulletShooptMonster17_Start_m227B7A31ED639B2CC078CC570583412E53EF489F,
	BulletShooptMonster17_Update_m68D1ED6D6F60215BC8777C520529AC2ECE27FD60,
	BulletShooptMonster17_Fire_mCCBE636673A572FA01C900FA9F099C0F1DAA389D,
	BulletShooptMonster17_StopAnimate_mAE13C84A7FD3D3CBEEC98152663F90A396C61B3F,
	BulletShooptMonster17__ctor_m638792FBC3049D0F5241BFDCD6506DA37DD2F0A2,
	Change_Start_m597BAEC4C93963C2512C2DB12D35B1801D3103B2,
	Change_Update_mF3E920C21344D8BCA1D53E4A4F0CC78CE296DBAC,
	Change__ctor_m8F66987EAF95B4B918CF8A7DB6617C3B08D5B9E6,
	ChangelevelAfterOpening_Start_m152724376B6DCBCA98C90A5501C96C8130F11AC3,
	ChangelevelAfterOpening_Update_m65F23F573D2635C129199A1C97F6BFDD50A8881B,
	ChangelevelAfterOpening__ctor_mC4E223EBFF275D0F3DE2628E15302AE54A34A578,
	ChestScript_Start_m090F1D5743839491430CE04A288085FFF53799EA,
	ChestScript_lvlDone_mE8307EC34AD965C93AFE799F1B398DD2A3D05091,
	ChestScript_OnCollisionEnter2D_mAF36E8A58D323C6DD3809AD966563B4DA2C9822A,
	ChestScript_Update_mE06D3FB0616D0459BE8E3E56DE229C189839C76A,
	ChestScript_ChestOpen_m61B1715BA969C81616E4C65A3A17BE4015C5C2AC,
	ChestScript__ctor_m8B91E6120D30A883C0DE05EBEA0F67C920BC55A1,
	U3CChestOpenU3Ed__13__ctor_m47FE64B53C9CBFBD01E777FE699028D93D2F4E31,
	U3CChestOpenU3Ed__13_System_IDisposable_Dispose_m3AEB6E997AB70866D1CAC9FD6B4E4461CA83EDF7,
	U3CChestOpenU3Ed__13_MoveNext_m0EA7179CBA7AC34D4978FD502071E2D843B13DA1,
	U3CChestOpenU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m773A3042410FA3B4CF8542A5404809996DE56296,
	U3CChestOpenU3Ed__13_System_Collections_IEnumerator_Reset_m0642610982D6DB7B358B9E0AC3349535664FC592,
	U3CChestOpenU3Ed__13_System_Collections_IEnumerator_get_Current_mCED6EEA482BDF5E3F2D25500341C365D9597E9B5,
	crocoAnimations_Start_m45839F1BE51C51554C3A70C520BEA2B53CD82ECA,
	crocoAnimations_Update_m46314E2731EDDF1A23C57809110DE561B75788FE,
	crocoAnimations_walkPress_mC127AB5DCF7B46FDF500ADD9E8EC1358F85A13BC,
	crocoAnimations_idlePress_mA6E8AF66681549D2B1B42659AE1D5FBF2AF8FF43,
	crocoAnimations_runPress_m788C43D3FEED0B2B8CE2DC1AE2F06BE03A123A8A,
	crocoAnimations_attackPress_mE9D570A02C662D2013B7B65A526F15014F33A15D,
	crocoAnimations_deffencePress_mECDAD0C5850EEC2F74173C7F6CCE1BC4380841B6,
	crocoAnimations_deadPress_m588FAB20C2298EFE9FE9D30D52FFF04D388FDFB7,
	crocoAnimations_RotatePress_m06DD2B6C6BE485DE32A659206EB581F7AD2EA4FE,
	crocoAnimations__ctor_m5EE0F60E6B20DFB92CE4DE6AC0A384DBCC21C596,
	Coin_Start_m514F3321444C25610980620A5AE926E8C5293C01,
	Coin_OnCollisionEnter2D_m802812D629F532E79B772AB5E3B3FC62C1658D55,
	Coin__ctor_mF977D2CE7F9F781D0E5B1226BD2757CC523DB637,
	CollisionRandom_Start_m1FB8FBE4AEB581A7517E5676437A3F98166E5D11,
	CollisionRandom_CalculateVector3_m2771C8DB49771B9397C0BEA22D26457443713A51,
	CollisionRandom_Update_m05A3A0362E4DDD05CBC11EB99CD8ADE64611C6E6,
	CollisionRandom_OnCollisionEnter2D_m05F6D995F1280C095EBD4FACC550709C5C067563,
	CollisionRandom__ctor_mCB398F6D6FAA1BACA9DE64DCBFEF727D9E6A8406,
	DestroyBGM_Start_m8BAB27A0FF4698048F89E4634FBDDDB01C9A3691,
	DestroyBGM__ctor_mA74BC2F1208C0C6D76ED5F96A6D0287DA52D5A05,
	DestroyLoad_Start_m366BC6A74D67B562A19F17E013D56013346028A3,
	DestroyLoad_blockArray_mDD3559F5E586763E0A26D6480A0B03DC6DCFD38B,
	DestroyLoad_destroyObject_mEEAAFB1AEC8356C6A599997C587A97C6971D287A,
	DestroyLoad__ctor_mDEF9FB62579332136B6E41E8476A0209D1A55F27,
	DontDestroy_Awake_mFD3677E85209771C579A5FFF6E50A5DB7D5D7076,
	DontDestroy_Update_m0C65A725225EB715FE18760F1A0EC1F8BC2CD5D7,
	DontDestroy__ctor_mCDCDC2D7D6B681407E7391AE87E7DD8BE35A7A45,
	EnemyHealthBar_Start_mAC5072644CD25461F54283A494648B93E10740AD,
	EnemyHealthBar_Update_m0D89B3B8999217593AA991F95BC0CB9D0DFF3B4B,
	EnemyHealthBar_BarMinus_m684458FF25E8D814F37FB2CBEB5FF7151655AD19,
	EnemyHealthBar_HideBar_m5B10CDD73099F09008196731B04BB55804EB9B0C,
	EnemyHealthBar_ShowBar_m4C8E729D9F8B0C393FF5F15D7806DC0DFB0DB135,
	EnemyHealthBar__ctor_mA53148773B4E9CA545B3C7D4564ADAF769B50C8B,
	FindMonsterController_Start_mAF4446A6A9C6E7D8E5A6F49EC3C90C380E2435DD,
	FindMonsterController_FixedUpdate_mB7730B3E2178D9C3CAE7035CEDFB95A118888E89,
	FindMonsterController__ctor_m90606917C329CAB2D62D82F145E106BC503F58C2,
	FlashMaterial_Start_m3FD2C06D2A470956F90E419A84DF9BE2961E1AC9,
	FlashMaterial_StartLoop_mBCD2FB0298EB6DDB4D99CDA9539F0B0F25A8BA13,
	FlashMaterial_StopLoop_m82ACD012BD1C358E8018746C6A5209BDD2BAE8BB,
	FlashMaterial_DoLoop_m5AC8BAF3E232FAD720F467C4553C008A657F9B93,
	FlashMaterial__ctor_m2D93F039E4B43FEA86B6CB586848AE678A49546B,
	U3CDoLoopU3Ed__9__ctor_m80FB7782169D5C9434E6BB332F0875E552A86EE3,
	U3CDoLoopU3Ed__9_System_IDisposable_Dispose_mA1D883D6198A472B861CDD8E69C87389B202711E,
	U3CDoLoopU3Ed__9_MoveNext_m8B8A02C18B41A7302229029D5211438F8CE1ACD7,
	U3CDoLoopU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m161A940F778B3E1A8EB5FF11524329B4ED2B24E9,
	U3CDoLoopU3Ed__9_System_Collections_IEnumerator_Reset_m03E7421E1933ED063603056D3A8AA90432B2EBA5,
	U3CDoLoopU3Ed__9_System_Collections_IEnumerator_get_Current_m92E236931F2A16F330962D928B271976E1A5AD91,
	FollowPlayer_Start_mFDD4B7A728742CA2842F6BDC8EEC8E518B0C064C,
	FollowPlayer_Update_m02579F8DD00972C9A5FD0546F0181118692F93E7,
	FollowPlayer__ctor_mE71062263690B516F460CF232D5144C1C85017AA,
	CamerController_Start_mF3EE5A7ED1FA5A2E3BB7257E9632946E3342D362,
	CamerController_Update_m5E203AFDC614861140E58F94050F39E04FAD0152,
	CamerController__ctor_m0621CBE36C46A9D0253B7593F504F73D8A4E65CC,
	FungiMonster_Start_m06E91CF4BA4CEFC8FCE51A9BF87A94E6C530D31E,
	FungiMonster_DamageHealth_mD83BD6F09C0F102C696E41F45290390DF78EAFD4,
	FungiMonster_OnCollisionEnter2D_m67FF8D1524F9F151F647BA44A4E4DC5EBB87F176,
	FungiMonster_Update_m79CE12B06BF347AB9AA72AB770889B9418F6A670,
	FungiMonster_Reset_m811B8C555ADE3F36B97FB168CBCF5DD0FA6D62B4,
	FungiMonster_RemoveEnemy_m1A541A349EDE16C119CDFC322ACE243804EFD80D,
	FungiMonster__ctor_mB51C575BCD81C7689680C1F341405507B2BE26ED,
	U3CResetU3Ed__21__ctor_m769F0D7F760AE690DF6B43BEA616B6470E69A540,
	U3CResetU3Ed__21_System_IDisposable_Dispose_mF3B71B7BD6282934BD973F3FF905DA11CD7AF34A,
	U3CResetU3Ed__21_MoveNext_m7BA94954A18A6244D9508D0760E06201B13C7B94,
	U3CResetU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m40A8093877EEA9DD3CF5DA5BBF315DC68B1FFE7A,
	U3CResetU3Ed__21_System_Collections_IEnumerator_Reset_m476796199DB1FB09AECC198FA41250799FC574AE,
	U3CResetU3Ed__21_System_Collections_IEnumerator_get_Current_mA568910A33A57FBBB89BE715EC8C8E1014881F71,
	GameController_Start_mCC3F0292799528323E2217A12DB08D98CDD492AE,
	GameController_Update_mA76A2CE1F2AC1AACCDBF913CA6E1EA73DC621CD0,
	GameController_EnemyCount_mEC85023C5B37DC8FE9CB2418F83DEC9BF55ECB86,
	GameController_EnemyDecrease_m9FF862AF384DCA060390DDBA1F1AA92B56A98185,
	GameController__ctor_m9D952052C0A7234373FA5531292FCA8855BE2643,
	HealthCounter_Start_m8DA95F34BD4A3BC087198B9096000539741496A5,
	HealthCounter_updateHealth_m5D68D8016359D8AC26197845DD084FB2744905E1,
	HealthCounter_Update_m1B50D5B12F3044D9754D782D19175C3D8199C240,
	HealthCounter__ctor_m07E0A1C3D3FF8CE384FAC16AE61E55C7DDC8C4A0,
	LEVEL_Start_mE22120A5462CBF505601714FF4C6401E13D9EA60,
	LEVEL_CanPassTrue_m24325AF305E18B39244DE8729BD285B68E929085,
	LEVEL_OnCollisionEnter2D_mA4EB422141010FB9CF2E2A4A636D48CE930DD3DE,
	LEVEL_changelevel_m34A04CC5A45D0A1B1B99A2B60E3D3D356D9EA24D,
	LEVEL_CanvasVisible_mA6259459DCA7F69EF82C18AED18304BB8D4214A6,
	LEVEL__ctor_m2BA25203A4E9939958A865F2BFD71995FAA5973D,
	U3CCanvasVisibleU3Ed__8__ctor_mB747C6375F58AE06D33940068E0429813343D23B,
	U3CCanvasVisibleU3Ed__8_System_IDisposable_Dispose_m2B04E17B955D1CE7679FF6F2A6CDF01CDA2AA2EF,
	U3CCanvasVisibleU3Ed__8_MoveNext_m4E28D597E29A4AEDA0E9920D8F5B24ED82F16BC2,
	U3CCanvasVisibleU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC1B4F1E8D5035F6F1FEF26BEF3D2B6D4F16B882D,
	U3CCanvasVisibleU3Ed__8_System_Collections_IEnumerator_Reset_m60B70917051546AC0423BB783D2146FE7F07A3DC,
	U3CCanvasVisibleU3Ed__8_System_Collections_IEnumerator_get_Current_m331FFE5EF7E5CBD34180C12B89A4DC1286C16E5D,
	LeftRightController_Start_mC4768CFB30913BE489CD248AFAED3A8BC1863ED2,
	LeftRightController_FixedUpdate_m8107B677F77D02A728C9A33CC0883DA953283EDE,
	LeftRightController__ctor_mAF95BBEBB78C0404E6AC088CEA88A86B2F64077E,
	LevelUp_OnCollisionEnter2D_mD32EF2EC1A4AC94EA9ACE068920F7C1D983D8BE4,
	LevelUp__ctor_mF1B86C987369AF31D9854B18CA6C4192C6D0F80E,
	LookAtEnemy_Start_m0088C545741ED0F39A3F6BAA2EBA62305EBAA048,
	LookAtEnemy_Update_m1229017C0E3D83C163F627BCC93AC21045329C7D,
	LookAtEnemy__ctor_m781880F0265A69734DFF4622A8277C5D5E045116,
	MovePlayer_Start_mA96A9BB0DFF4CD84A7E19BB64D3B3FA3E30138B1,
	MovePlayer_OnCollisionEnter2D_m10BBE5D907D6FCE9D8509B069F60A03B59F7A8D5,
	MovePlayer__ctor_mABC1924DC986EA24C51BF8636ED18DDDA5073D22,
	MusicBGM_Awake_m0BE285FEEEAE7BBDF2F32B15F847A5C1F470A512,
	MusicBGM_Start_mFC8B59661A336BA37F3B7F8E73647C69AFFB7421,
	MusicBGM_Update_mA4735E4B9E98084EABDAB73D3B5FFAA9EEF63F1B,
	MusicBGM__ctor_mE105C84DB62B73EF04909C88152EB9B7F5FE3436,
	MusicOff_Start_m1A37F6B7DADD5CC782D134E2867E6AF49AE8AD51,
	MusicOff_Off_mF58992D363AE142B57F9420ED5BBCD34A24E819A,
	MusicOff__ctor_m5FC4F3022C41A684CBF257FD98445B7F4FF3A35B,
	OnSpawn_Start_m0AB713FE36559E5127439B0830CE26EC7A169503,
	OnSpawn_OnEnable_mBF5476DE586EA1C3A593871380DBC2A78436C4E4,
	OnSpawn__ctor_m0DC36293F1F7393C6A981BB8770E229C2694720D,
	ParticleStart_Start_m0B241BE7B9F212B2895EC61BE647DAD6E9CDE869,
	ParticleStart_OnEnable_m604147FB3FDDEDE48E0768D0C8B9F7A9FCC7361F,
	ParticleStart_Dying_m5D0B1E9994445919D0F0185BB8E7E050D71E5AD0,
	ParticleStart__ctor_m410F4F6C3FD5B8A0E63123FDD61528EE676DEB26,
	U3CDyingU3Ed__11__ctor_m58D732957B79282AA92ADF5F21813A7E85BCFFF0,
	U3CDyingU3Ed__11_System_IDisposable_Dispose_mFA3AC7162FF2307CBFA845DD40FC27ED0BD267FC,
	U3CDyingU3Ed__11_MoveNext_m4B25A8EA63A2B37710D2757EEEF2E65C8F75EA34,
	U3CDyingU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51C1911128E46618C6D1050F369891214787B76E,
	U3CDyingU3Ed__11_System_Collections_IEnumerator_Reset_m748FB5A362DC188E0B7A3A683E1F523A07E5EBCC,
	U3CDyingU3Ed__11_System_Collections_IEnumerator_get_Current_m0DF20F30AB85F89DAC4C200ECF9C50B755DE0651,
	PathFinding_Start_mC5DD912FA73AE30FA6F1905C0216A2B567816F17,
	PathFinding_Update_m02852483EBD404D4339F38486B4A3A89B249E857,
	PathFinding__ctor_mAD680FEE693FBB0195CA6A565754C5219F395055,
	Pausemenu_Awake_m4218418B151104556DED3430BFD5F70BE8612A7F,
	Pausemenu_Start_m5BA8984CA18DBFC97825124953B701D6C7D13D23,
	Pausemenu_Update_mB1A440D540419A407549F4A81B6EB557B44C6AEF,
	Pausemenu_resume_m6D4624BC79FA9004FF0BADA437A310D918B252E8,
	Pausemenu__ctor_mAE16059E970694F1393D38A99F5C1FA88C045DD1,
	PengaturMusic_Awake_m6CC96CD5ED718A466B109EAE5F0E90527E68DBA4,
	PengaturMusic_Bunyikan_m53D0C41E9E38A8ED82B647A027A152FEB37BBF18,
	PengaturMusic__ctor_mC53F1DE39C2FBAB77A86F4C10A85D1E5FBBC49BD,
	Pivot_Start_mA70ACD2DFACB9AA5C62E1A5CB00A5741A3B314AA,
	Pivot_Update_mEF296087D06A1D972B0DFF91275F2F91409C5B17,
	Pivot__ctor_mC81037BBC6A40E24CA1B37EED8189912B58633E4,
	Bullet2d_Start_m2BC6CB256B667B0BA397B601A0FD2611DF47F1FC,
	Bullet2d_Update_m0E61DC6964E4BEFB9A0AA32AF16A3C872DCEC8CA,
	Bullet2d_OnCollisionEnter2D_m805264F221D35427F89D6C113FA8CD1DC7FD451A,
	Bullet2d__ctor_mF04CD500E656CA25BBB289FA74819C855E958070,
	Char2DController_Awake_m37A35EA357F759E3803DD9E769E276CA45356E65,
	Char2DController_Start_m3208A97BEB9C90216919F819477F94E2F85D2A5F,
	Char2DController_FixedUpdate_m6E6B01820FF62F71727706A74B884294ADE1FC84,
	Char2DController_Move_m04E59F20AFBE123396E22707420445B19A2C98FF,
	Char2DController_Flip_m537D8CEA5B388072668BD2E3C3656AFDD29B7BB7,
	Char2DController__ctor_mBC6D30BFFCCBF85FD0B56CE60E3C7C5ED95ABDAE,
	BoolEvent__ctor_m5824430D521751ED93958922213FC003282F4E09,
	Enemy2D_Start_m637914D140E4A19AEB83E78ACE2930332CA42F2C,
	Enemy2D_Update_m1DB21F88BEB23B583D79F5203FD9F236DE2FCB00,
	Enemy2D_TakeDamage_m6E12B0CAEF7A5531FF37CC1979062258E9BF4DA6,
	Enemy2D_Die_m3EC84CC04E8DD61905E8434CCEB48F25FD19BB56,
	Enemy2D__ctor_m53CDFD06F24B47991E763367C85F9196E544C841,
	U3CDieU3Ed__9__ctor_mADE2673E0AC151EEE2C2B8BE752FAF867D70790E,
	U3CDieU3Ed__9_System_IDisposable_Dispose_m2AE53DE0960F3C4AD19D9B539603A2F844447039,
	U3CDieU3Ed__9_MoveNext_m2E4E5071D4CC0733DAD2EFE42475369EEEBBE782,
	U3CDieU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51EEC3D1CC93BB56CEB071BFAEA5F897512C2FEB,
	U3CDieU3Ed__9_System_Collections_IEnumerator_Reset_m8DCFC9CAB164B3F18DC2EDC7E5F35DF765A182E0,
	U3CDieU3Ed__9_System_Collections_IEnumerator_get_Current_mD3EED8BA14478BD8216B581B28EB81FEE5ECEECD,
	Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021,
	Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3,
	Player_OnCollisionEnter2D_m0448BB6F02AAB4967D3AB976DA22A24ACD8E7F90,
	Player_OnCollisionExit2D_m3313F2D12F1F1BA58D9E410CE7386136F5008BCF,
	Player_OnTriggerStay2D_mC3EB58F8E44658D0E4588DD69260825E7CEF41CC,
	Player_isExplode_m6FBDC0B247E9AEC1FA2DA508CE427F9DB761BA3C,
	Player_HurtTime_m44275D3999E32BD89DC9B5ECDF835753BE84CB7B,
	Player_addHelt_m3C83D363A0C770AEFB7C916C9E95E4A6BF958AC0,
	Player_Die_m792E214E2658BA493E201D0C397C9E973457CA82,
	Player_CoinUp_m35A4CA3E78D1EBB0822A62E124FCE2B13AFF61A9,
	Player_ResetCoin_mB0784D764663796543A0FED54A126377EFD68DA9,
	Player_ResetHealth_m171F2D09DBAD09451FE6EA25E2274F6C651AC48A,
	Player_ResetPrice_m0EF10161DAE2762C5EABB030C396F75F1B0451CC,
	Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7,
	Player__cctor_m38DB9DEEDAE88802EC9823ECFF4A17859AABF96F,
	U3COnCollisionExit2DU3Ed__22__ctor_mA8A4F25B6067CE0B9414AB9EC7C05D529E7C8386,
	U3COnCollisionExit2DU3Ed__22_System_IDisposable_Dispose_mDD17E25B307F29A01D61EF5C767F10C624BA2A2A,
	U3COnCollisionExit2DU3Ed__22_MoveNext_m9DB0774FE5DF55B27EC8BDD4A4A40533E2B910A3,
	U3COnCollisionExit2DU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m193C48EB097DA325334C0C839D3B48D9FD1238AF,
	U3COnCollisionExit2DU3Ed__22_System_Collections_IEnumerator_Reset_m772A97B6A8F765C1C881C238E0CA7428687C87E1,
	U3COnCollisionExit2DU3Ed__22_System_Collections_IEnumerator_get_Current_m2C711895EC942AE7CAFBAA314F76746A7B9CBE34,
	U3CDieU3Ed__27__ctor_m8857762760F3BCE10A3D88DD890FAD638A8D4702,
	U3CDieU3Ed__27_System_IDisposable_Dispose_mC61821F0D50A408F101518E86C449AC53FF660C0,
	U3CDieU3Ed__27_MoveNext_m98991766CE141917F9F5F84A0F8F631084B96A03,
	U3CDieU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m410D885DBDE33BCAABD4860F2043CF3E36EEBF70,
	U3CDieU3Ed__27_System_Collections_IEnumerator_Reset_m4D1BD6D7BA2AF7656BF349668CB2385963FD629F,
	U3CDieU3Ed__27_System_Collections_IEnumerator_get_Current_m6BDD5E6FB780641B98FA16D648C42FD92BF42C05,
	PlayerMovement2D_Start_m60C3A6A2CF75B0D0084F9A96DF0B747631E43E67,
	PlayerMovement2D_Update_m47D0D45C40C119D5A33D3C842C962E0382F4E9CF,
	PlayerMovement2D_OnLanding_m5E6FBA6E7FE4C54D3F97D0E1F02004EC05951EE8,
	PlayerMovement2D_FixedUpdate_m3EF1816FE4BB73220D1FA47989C535AAC5FC1A96,
	PlayerMovement2D__ctor_mDD6A7A3899771410DCF3C21AA3C6293260610ACD,
	Weapon_Start_m7216819D285B8CDF5DD9E18550E3DF9F1DD76D5A,
	Weapon_Update_m03BCDE1EF0751A0F043AE300FD31A4BFA8B5D883,
	Weapon_Shoot_m8FC1208456BC2F62B9EB212EAC4D70FA5F60EE0E,
	Weapon__ctor_m643DE56148B24BD987E564400E443ACDF43CDB97,
	Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02,
	Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2,
	Enemy_DamageHealth_m54EF129B3FBF1BE19BFE6FC67A02426FA720F22B,
	Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12,
	Enemy_Reset_m628CD2AF324D1E1AB9149FCED22A7148CFBD056D,
	Enemy_RemoveEnemy_mD2058B454847C883951FBE12A118697333EADF62,
	Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734,
	U3CResetU3Ed__19__ctor_m5079434512C1D722152FD57D9692C8186A97D321,
	U3CResetU3Ed__19_System_IDisposable_Dispose_mC4E423C14CAA780263C2CAD897FB06444822E352,
	U3CResetU3Ed__19_MoveNext_mE366C8AB633F0488B9A99D6D4A5F3BFC5457F10C,
	U3CResetU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m165E9A0298191DAED6213B75F722A63C9C93732E,
	U3CResetU3Ed__19_System_Collections_IEnumerator_Reset_m4DFA3ED64ABCD8B3242221434025EC1D302916A7,
	U3CResetU3Ed__19_System_Collections_IEnumerator_get_Current_mED384D8433E87A6CEBC3263205674A78D591C2C9,
	U3CRemoveEnemyU3Ed__20__ctor_m1E6B4D9F3646E854A30388BF4635D465618D628A,
	U3CRemoveEnemyU3Ed__20_System_IDisposable_Dispose_m3745507487481DEB5DBAF8FA60A0AE9F0E670AB5,
	U3CRemoveEnemyU3Ed__20_MoveNext_m58AAEAE32A23DA96A258EE9665D9992585065FAC,
	U3CRemoveEnemyU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAA95AD573C461C64AEAAA6315CFD544159B3B000,
	U3CRemoveEnemyU3Ed__20_System_Collections_IEnumerator_Reset_m56D0635675474E0F012360DB67C043F6DEE882B5,
	U3CRemoveEnemyU3Ed__20_System_Collections_IEnumerator_get_Current_m59443E41B8F69CE7426D70185454AC4A86C7DF3F,
	Player2_Start_m6D06697D194823D87339EE927BEEB0F1B2C53A2D,
	Player2_Update_mC7FF45F9D896EDD3C984AF5F636C958CB309DABD,
	Player2_FixedUpdate_mCA5A4350D761687F49B16FA6D3C9D8A485D312FA,
	Player2_OnTriggerEnter2D_mE7892ACA13E10658D4884D344001110B0F30381B,
	Player2_FlipXY_m2E8B59FAB876B6DFD536A9F63F4ACC0F6DA5AE8B,
	Player2_NotFlipX_m393306343FBC9E3BC569493BAA79BDC717A1B092,
	Player2_NotFlipY_mFF387819DF73F7157B133CFE1C065061658951FA,
	Player2_LockMovement_mF1AEA5A6D58E7EA094EDBB641C422912C51A1CC2,
	Player2_EndSwordAttack_mE52CFDD468A8E6DB6C1E5F4454317EFFCCE5FED0,
	Player2_UnlockMovement_m4D417E029903D77B2CDCDB48EEBF01091AF1BCCD,
	Player2__ctor_m2482DA9BBDF12DCD2173A69F0661E0E4E25167F7,
	U3CFlipXYU3Ed__24__ctor_m131DB1A9C643FD92F6AF8100B1673A68E413C1C9,
	U3CFlipXYU3Ed__24_System_IDisposable_Dispose_m10B3BC1FED6E3FC8BCD0EB206E1AC28669F2EA2A,
	U3CFlipXYU3Ed__24_MoveNext_m7C7611677DBEDD025224E3C7AEDFCC26D83CB220,
	U3CFlipXYU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAD58057BA5483CE0D43CE4B62009B70AF0DE2EB0,
	U3CFlipXYU3Ed__24_System_Collections_IEnumerator_Reset_mA87B6F0AEC5F1895B83ABC2DE5FC1ED6DD4BE20D,
	U3CFlipXYU3Ed__24_System_Collections_IEnumerator_get_Current_m24CEFA38C44FD2871AC176AA4DA6C3F8AF5EAF8A,
	U3CNotFlipXU3Ed__25__ctor_m94F2F0163265685222ADFB187F09580678CA2145,
	U3CNotFlipXU3Ed__25_System_IDisposable_Dispose_m6F4A9B4FE98D2DD965AF928F40AA6562E71DA52B,
	U3CNotFlipXU3Ed__25_MoveNext_m1BDFD279397A8DE52FBB4249D9F7FC3A981B0C7F,
	U3CNotFlipXU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13CA1D64DE97A4EBEBACA8356720DD9FE5756AB4,
	U3CNotFlipXU3Ed__25_System_Collections_IEnumerator_Reset_m0E72C3BD6F7C5880500F39E88ECF4D8832FEF217,
	U3CNotFlipXU3Ed__25_System_Collections_IEnumerator_get_Current_m307B7C1CAA37E1DAD29DD63844B00D7A5AC947C0,
	U3CNotFlipYU3Ed__26__ctor_mD5791BAD4289B9AEA13D346B0D15A4085077458C,
	U3CNotFlipYU3Ed__26_System_IDisposable_Dispose_m92088D58772A6C6FAEF4B7208F110704E6488A2B,
	U3CNotFlipYU3Ed__26_MoveNext_m098040834E2DEF50CBA62B7DE57E406972F247E4,
	U3CNotFlipYU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35E9597F83EF82C1BC20F307C6E63FE93EDA7307,
	U3CNotFlipYU3Ed__26_System_Collections_IEnumerator_Reset_mC6D8CCA93FB0987B4081DED3D535729334345AF4,
	U3CNotFlipYU3Ed__26_System_Collections_IEnumerator_get_Current_mE496955C82262A2E1E8BFDDFE55AB76A7ED97D97,
	PlayerBullet_Start_mCC4F3931DDDF916F291C511DA657A85062BB1E3B,
	PlayerBullet_OnCollisionEnter2D_mECAF6DAA52B3424483C277E4F98765732DD2E06A,
	PlayerBullet_Update_m3FF2F92BED9C38E4321B9F9C8058E23FABE58C23,
	PlayerBullet__ctor_mF4C29AEE1BE6345F9B1FB4188945835E632F7243,
	SwordAttack_Start_m3A17DFBD1530A4E5C7CCAC79129E374698C191E2,
	SwordAttack_Update_mDE43BD736C4E9A6AFE2480D999368D7B472207A2,
	SwordAttack_AttackRight_mADCFB6717C8D6DEDDF955F2DD77594FD8F422F89,
	SwordAttack_AttackLeft_mFD60AD9ACABD5D93FBE8C123528B12AB83D6CAC0,
	SwordAttack_AttackUp_mCC2D33E9F29152F50A9E427CFBA728DC317C1751,
	SwordAttack_AttackDown_mE0919A683FC30A77F5969556E6223EE499EECDF5,
	SwordAttack_StopAttack_mE7DCA01D4E303DF75B5D784FDE348862D10618C9,
	SwordAttack_OnTriggerEnter2D_m92C0053D7BD7A8AB7009CC2AE631D9EB89E3FAF5,
	SwordAttack__ctor_m2ABE5EE2DA6867F1A8BB12F57441ADAB8F9DD077,
	U3CStopAttackU3Ed__12__ctor_mC892CCDD9137521226BFDB42F756F498ED9D9DE4,
	U3CStopAttackU3Ed__12_System_IDisposable_Dispose_m050550BE33CA586AD6A3AFC4553AF767A80B7E0A,
	U3CStopAttackU3Ed__12_MoveNext_m01893D5FD40E1A9F4CEE989FE2D6DCFF5C01BBF2,
	U3CStopAttackU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD070A2C7A382D082F76C43E10B370F5A161A1568,
	U3CStopAttackU3Ed__12_System_Collections_IEnumerator_Reset_m7EB90774CFA286159F440225FF0844A9CA318451,
	U3CStopAttackU3Ed__12_System_Collections_IEnumerator_get_Current_m8C32386D0F688C60F58C83D55D8232476A3FE9B2,
	ScriptPengaturGame_Start_m51E8EBD3D8C5110534058D1ADFA27566E870DC7F,
	ScriptPengaturGame_Update_mF0D89519BE394C7FA5FE1876A019A32CC62707C0,
	ScriptPengaturGame_Awake_m158F506795915AE35939CFDC50C9A56C304E024D,
	ScriptPengaturGame_Respawning_m6E763DD53423B63E14B272FB71037CB72331F2DD,
	ScriptPengaturGame_DelayedRespawn_mFAB0633263E6564AF5BB4EF59FABE7202696771E,
	ScriptPengaturGame_blockArray_m3829B51F710B09DA07476250F8FE842114A03D4E,
	ScriptPengaturGame__ctor_mA84248362751CA058151030963542AB7496985E1,
	U3CDelayedRespawnU3Ed__8__ctor_m40CD1912AF394DC2AC923A52C94357E0DF450D2B,
	U3CDelayedRespawnU3Ed__8_System_IDisposable_Dispose_m870C5BECEB2FDF41D71A48F1252484616984C08D,
	U3CDelayedRespawnU3Ed__8_MoveNext_m668CB66C74874BE0AF17B24D2BD0BBA716FF48E7,
	U3CDelayedRespawnU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m704FE12609434A38D732427EB89149F0E243013B,
	U3CDelayedRespawnU3Ed__8_System_Collections_IEnumerator_Reset_mD5F85137107697D53140B274A3025147FCC06881,
	U3CDelayedRespawnU3Ed__8_System_Collections_IEnumerator_get_Current_mAA4466DE52655D1610F3E77D6DFF672364ADD55D,
	Price_Start_m2BFA9180697CF979D8D77846074A3C44736590B5,
	Price__ctor_m6B6CD92F3A77D7478F182354DC16AC8B5D4A9CFE,
	RandomPath_Start_m967208E92FF229E97A8A80F89E87FB76B5706F04,
	RandomPath_CalculateVector3_m21985AD15D315C3670EAAB24E0414B94224DC6EC,
	RandomPath_Update_mBF7506F14129ECDFC8E603145020F6708F3CBFFB,
	RandomPath__ctor_m36E247123B9A44417B27A31AD20E6F022C23AF9C,
	RandomPathEnemy_Start_m6582250456D617EDFA8193B473A0774A0D859363,
	RandomPathEnemy_DamageHealth_m07F536EB04C245172459B5C06231056ED7797D8D,
	RandomPathEnemy_Reset_m7AC5BB1855F12972ADA3278EE482A1C25BFFB8DD,
	RandomPathEnemy_RemoveEnemy_m3F3AA696BB27EC03B23500BA53FDA5BEE9652759,
	RandomPathEnemy__ctor_m31F6AD2195A7339F531ACB6FA6E2EE4A00B01486,
	U3CResetU3Ed__16__ctor_m4A61B7D86F5085B4169CE600699751AD297884B6,
	U3CResetU3Ed__16_System_IDisposable_Dispose_mA222C493C8FD94E8FD1C9D3F7472859F5BACCB04,
	U3CResetU3Ed__16_MoveNext_mAEB9FB96FB827D128D7D848F98321E0737EE8F72,
	U3CResetU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A2CB645B1B0484C48CB991CDE86500798FAA66E,
	U3CResetU3Ed__16_System_Collections_IEnumerator_Reset_m29FBCED23D03FA82F30CEC93EAD8BF33BA588ADF,
	U3CResetU3Ed__16_System_Collections_IEnumerator_get_Current_m6A8464C3A44F86288FDC802B734BD79679BD8AC6,
	U3CRemoveEnemyU3Ed__17__ctor_mDF5707187B155BFF83BB4292A37DC36184AE56EF,
	U3CRemoveEnemyU3Ed__17_System_IDisposable_Dispose_mBE1FBD45087F9AFB7A1697B658CB87630B689714,
	U3CRemoveEnemyU3Ed__17_MoveNext_m600AB3B8363C962FC925EC6D256CE11D148D58EB,
	U3CRemoveEnemyU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC87690098968F6C42184EE1080E4E20289A85F33,
	U3CRemoveEnemyU3Ed__17_System_Collections_IEnumerator_Reset_m50CF5CF07AD930842A88145B8EB6089F69155A83,
	U3CRemoveEnemyU3Ed__17_System_Collections_IEnumerator_get_Current_m1EEF8BCDA0EE7B9BFB461F31DF62C5AA9CE89EC5,
	RepathEnemy_Start_m3EA9F7E07FDCC9705479EEB9D16E5480A22E9F36,
	RepathEnemy_Update_m77F75835D39785FA8F3BF66063EE032481FCE63B,
	RepathEnemy_OnCollisionEnter2D_m2056470FE9E3B9B9350D3B9EDB41E105D7105400,
	RepathEnemy__ctor_m2A86F3E7F7843F3DEB207A90A63404CA636FA189,
	Rotation_Start_mEACBFA06F8EFD7A5FDC79C980BA4D7B546B8B1B4,
	Rotation_Update_m9234709059E25785C44ACA1F2AE0E8D143DD6409,
	Rotation_FixedUpdate_m43CDC81683CDA94FABC8416CF7AC5A5A453A818F,
	Rotation_AttackSpin_mFCC4707E6C3F1686B797441B2CABE55A56772514,
	Rotation__ctor_mE37308CA0B03685698CC3A3EFD8EF606585B6297,
	U3CAttackSpinU3Ed__13__ctor_m4EA58DFAB7F07180EE63578CF3FE454562B21F9A,
	U3CAttackSpinU3Ed__13_System_IDisposable_Dispose_m809F021C7039F1A4EA346723F825E2BEFFC7BB89,
	U3CAttackSpinU3Ed__13_MoveNext_mB150857242B8F5593BE51A4801BDA09A4CF9EA8C,
	U3CAttackSpinU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m460DF86C141E5B679E0E6964C89FFE1E7EECFD7D,
	U3CAttackSpinU3Ed__13_System_Collections_IEnumerator_Reset_mC4DAB2FCF9E95B419ED3BD42CC01B3DAC3C6B5A8,
	U3CAttackSpinU3Ed__13_System_Collections_IEnumerator_get_Current_m48BD155E14859DB7FAA98B221A434C12740E8852,
	RotationBullet_Start_m4EB7F0AE4098EA753770A7FA2D0A03028D0B3024,
	RotationBullet_Update_m994E947B0D56EC399C30C662060EA29C07CFDDA0,
	RotationBullet__ctor_mCA398B771E7C5E12DB33F5C3D5B94B25946C3655,
	RotationToAxis_Start_mA3969FC84E69D82DF2BA08DD99D08BC3D562EE42,
	RotationToAxis_Update_mD99E5A3654D7010AE32E0A9A8E0D15AC3EE31ECC,
	RotationToAxis__ctor_mBD677F9300F744D125A52C8B8157A68D0523BD24,
	MenuSceneLoader_Awake_m18B4E09AAFDC9578469D066C50A8AC0054AA34AC,
	MenuSceneLoader__ctor_m27FD45CA6C6C8B579D2FA4EEDABBA35F2C7EF6BC,
	PauseMenu_Awake_mB04BCBDA84AEC654D1976EC3DF61A0CF68D2C86C,
	PauseMenu_MenuOn_m8973DC956DD5235381EE4ACB4A182AA5BF0EF2EA,
	PauseMenu_MenuOff_mCD1AF41F916B0C02D2FD6F82377DBEAFF7C30720,
	PauseMenu_OnMenuStatusChange_mD1E8BD9B9CCB274A83FFF0FE2E06E6ABD0361B4A,
	PauseMenu_Update_m191CABDC11442A2CC104FC8B3244D04826E7BD57,
	PauseMenu__ctor_mA1A281F3359C234E5CF24FFEAC20C12C48D69018,
	SceneAndURLLoader_Awake_m8F276157A2A5FA943EF7918D6CCDB81273317E23,
	SceneAndURLLoader_SceneLoad_m2B09BD48F419F49A6BD461DBC7B2290EC8632B06,
	SceneAndURLLoader_LoadURL_m47E3E286E80F2D5B3E6A164C32F7E1B473532AE2,
	SceneAndURLLoader__ctor_m6DEE574FADF9E3E894594690CB2755F69D5D4BE5,
	CameraSwitch_OnEnable_mD422991EDD9D880928644EE1BC4E557EE644679C,
	CameraSwitch_NextCamera_m38AB4521C129032FA1DA5154E09D36D0FE2DB257,
	CameraSwitch__ctor_m550FCA9B0C24BBBC8BDBCAAAAA7BBF26312399FE,
	LevelReset_OnPointerClick_m9625C8235343CB1DE6B089AD6F2D5ACF738072A4,
	LevelReset__ctor_mFD2BB9AF1A9D7E795DDAAE2F33C1F0EB1FB31F07,
	BossDie_Start_mE761196BA9089B0D2E233065A66550315EEDA100,
	BossDie_Fixedupdate_m17E71C4D8F8E4C531D60D786EBEA8C9844C6179C,
	BossDie_Die_m046E9EE771EF362F0375A4F8749146569ED8FC6E,
	BossDie__ctor_mB138FAFBD2381B4ECBCA053AF833C276EA533C57,
	Collectibles_OnCollisionEnter2D_m0933EE2CAD2A856A60B7CA218F05E6FA4177C699,
	Collectibles__ctor_m7872B3C2F538BB1BE3283979503FB7DCFCB1B89F,
	Collectibles2_OnCollisionEnter2D_m2836C6193027241AE636959E2F5B31E2E5876853,
	Collectibles2__ctor_mFD41EF266C59AD135F8B5F72CAFA172766F7682C,
	Collectibles3_OnCollisionEnter2D_mA9A92C11EDA764FAD82F361858921BB2D2B9FB0D,
	Collectibles3__ctor_mC998A6E29B1F37E5E446F87BF69E1E2203765EEB,
	LEVELBUTTON_LEVELCHANGE_mE68B3FAC76FB2C5BF7C86D1B988790406D2C0927,
	LEVELBUTTON_Bunyikan_m64A71F59CF43B798B69DCCBA49297A58A0CC693F,
	LEVELBUTTON__ctor_m91361DFE9A90AACAC3617BCB0785A75FC3016D41,
	LookForwardBoss_Update_m06A0A8342DBCB69189AD0DE1AEB0DB99E6CFC26C,
	LookForwardBoss__ctor_m0D463475B9A49C0590940C475A0891F5ABE3E981,
	LookForwardatYaxis_Update_m5DEAFE9F7FEFC660D887EF87E370F2F2A9E521DE,
	LookForwardatYaxis__ctor_m0971DA7B1976EB77E6DECFC9F1793F0C2B38B777,
	MoveForward_Start_m01A14393269317C9EC0DE9C8B34C219475B5B9D0,
	MoveForward_Update_m1D1EA1D175B6E5FED262E5D66BD0F05A5A6C1B1E,
	MoveForward__ctor_m79E20E057C5117B7FC19B2CB3146FCAF61ABE123,
	MoveForwardBoss_Start_mEC433C5F0B199FEAFA2FF695ECE02C6DF59746F0,
	MoveForwardBoss_Update_m0DCDB82FC7D4D4980DF2654FE710E647232DDAB4,
	MoveForwardBoss__ctor_mFE304D24337FEA48909A2E49D3050D39544CF9C2,
	MoveForwardatYaxis_Start_m3F359ADACC29592E697EE6A9521D409B202CB639,
	MoveForwardatYaxis_Update_mAFE83CE4595D9F592AA3039015EB9FDB4A471C3A,
	MoveForwardatYaxis__ctor_m64A49E2381EF37D9076F5177BDAB55490F324221,
	PausePane_Start_m59154AEA07D590F94A86169C9B5D50E30A864621,
	PausePane_PausePanel_mF2E057447C8451A4929D7C1E482A9724C509A009,
	PausePane_StartPanel_m16AC8968CC80B968A374CFFCA34C7A6CBBCEA628,
	PausePane__ctor_m2432C2F54E28A4AB29B47ED16078535AC8D1110B,
	PengaturGame_Start_m2A5C82EAF8C0020B969E944507F0397742BD9CF6,
	PengaturGame_TambahSkor_mDC364AE0457B13172465FBAEE34443FD01F4FCA1,
	PengaturGame_Bunyikan_m67FA0089716082B333648B0079E6F9DC75C84C4D,
	PengaturGame_Update_mDE3F609BD2301A561456EAE438FCC5EB9735F42E,
	PengaturGame_OnCollisionEnter2D_m21275C883E00FF21C3EAD7E901B7E758940A90A1,
	PengaturGame_buyanlifefromdemon_m532E7378C87D22B5DA67E4B1A58FCD0647370900,
	PengaturGame_Die_mB7093D4526DA560D2C9393D4984E3892F75B2574,
	PengaturGame_ResetCoin_m73AE359F615D521F003E44297C20ABD0A45E5D56,
	PengaturGame_ResetHealth_m27C1DB7B763BB00F97BEF6950893B1704184A759,
	PengaturGame_ResetPrice_m1ED8CE47F8A8C0D789ED128445B2EDCBADD8B031,
	PengaturGame__ctor_mF3B10B1B243FCF5CC9037BC99A07C5F4E34A5FBE,
	PengaturGame__cctor_m61ED5A79741D9A734B6C53BF598D15A98E5D9B80,
	U3CDieU3Ed__19__ctor_mCD289E975B415CDFE7F03327361CA771A9ACC945,
	U3CDieU3Ed__19_System_IDisposable_Dispose_mA8D3F31A7FA44478E9DB61A959929BB795A8F824,
	U3CDieU3Ed__19_MoveNext_m2CDF825234F118CC76363457A7C475BD47BC01EA,
	U3CDieU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5DB6F0BF77F660F069C9F3A2A60580D58BC136BF,
	U3CDieU3Ed__19_System_Collections_IEnumerator_Reset_m6C4B4656507A3C75D780F9043623876E56B280C6,
	U3CDieU3Ed__19_System_Collections_IEnumerator_get_Current_m42A1443B0A7558052BB999AEA2950E2422012F8D,
	PlayerDie_OnCollisionEnter2D_mCB73AE931EBC1329CBFFDE55CFEE615236B75E5E,
	PlayerDie_Die_mBDE9A00F9B4556CB5CC7C139353F4CA2AED729E5,
	PlayerDie__ctor_m541544430EAA9AB2B1BDB199D128D8BEFEA0E52D,
	Quit_quitaplication_m37A555868257BE5B443279654015B5B12E8419AA,
	Quit_Bunyikan_m6B39BEE86085666A2BED476C0643F2B4EED28542,
	Quit__ctor_mEB6E7FE6F53362692AD96E4DEE1D3906A592A7C4,
	move_Start_m68C6847F59645AD1B22C2CB5742DB521F7792586,
	move_Update_mC6DF3D01C8AF42E268DC74B7C40A271D2295ABEC,
	move__ctor_mEA4EAF1DF04BED632395EC3F0721A0273A838739,
	playercamera_Update_m7CC33D7CF04E98D3BF3975B51D70666888555D8B,
	playercamera__ctor_m845AF78ED07EAA29634146805EB1B939D7759890,
	ScriptPengaturGameTutor_Start_m18D71E5D67BE39C75AC2818D0FD9EF98373FC072,
	ScriptPengaturGameTutor_Awake_m942555D7CA8911988CEE74177F85E94301221C2C,
	ScriptPengaturGameTutor_Respawning_m0F34EC9C15E364B29445D1A503D0E98C8837C9DA,
	ScriptPengaturGameTutor_DelayedRespawn_m951994477B5FC95D9C1824E4C044B548A9F12869,
	ScriptPengaturGameTutor_blockArray_mDFB637B71B42807FCF0443D7496A2BC63BC5410D,
	ScriptPengaturGameTutor__ctor_mFC05FC196998E63980DA000E87778536266FAA8C,
	U3CDelayedRespawnU3Ed__6__ctor_m832D756A5E55217164F680E64AF1B43041752E1E,
	U3CDelayedRespawnU3Ed__6_System_IDisposable_Dispose_m3ACCA8E0A4F478BFD532407AAF5C4458EB6D8131,
	U3CDelayedRespawnU3Ed__6_MoveNext_mF60B2F6FF40A2C53A037E112E6BC983127208D5C,
	U3CDelayedRespawnU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m48E50B3154FC7795127B06517938CAB13B89D71A,
	U3CDelayedRespawnU3Ed__6_System_Collections_IEnumerator_Reset_m814E90D8FD4532733B793DA5FDAF9FF3A57B2CDC,
	U3CDelayedRespawnU3Ed__6_System_Collections_IEnumerator_get_Current_m4949E5B7B143356AED4CB5677BBD70C93385687E,
	ShadowMonster_Start_m44D3A8FE491BD4C3932011C1EF74AE8593EA30C9,
	ShadowMonster_Update_m17F530419193B6A5D01F2FCECC1E5C33F610DA8C,
	ShadowMonster_FixedUpdate_m274D9657B0FC3DCF2D9ACD43B142D1EEDD341042,
	ShadowMonster_OnCollisionEnter2D_m5549790B5145E49F8A9A34068C399B0F48DE3944,
	ShadowMonster_Shadow_m62273387B5D608A04893DADA04C1DF233B336B48,
	ShadowMonster_Build_m079BF377D106CB3E1A7D23A8F4F9BF6CE125048E,
	ShadowMonster__ctor_mFDD35A68BC8B11FDA285F3D77DC60E50CBDE4BB2,
	U3CShadowU3Ed__10__ctor_m09156293353FA2A514EC009449AC2E4F8A7BA07E,
	U3CShadowU3Ed__10_System_IDisposable_Dispose_m7D1378040012088EFC8545115F4E26B8BE9AD013,
	U3CShadowU3Ed__10_MoveNext_mC2243B84AFCFFC700DCF851039648E2A0D5BC6CA,
	U3CShadowU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m158249148A41952D3D36E40F719C17F88C16E8F2,
	U3CShadowU3Ed__10_System_Collections_IEnumerator_Reset_mC690E70BB7DD3145DC2A8F2E682A15819094A724,
	U3CShadowU3Ed__10_System_Collections_IEnumerator_get_Current_m3B88444077C83B3FD6A4913F0ABB99A182145512,
	ShootLeaf_Start_mC6FF02AC82D5C3A90FF14D2D2CB3D29D46C34AA9,
	ShootLeaf_FixedUpdate_m56F5A8837C3BFB46A4BFE99DBF0C5B8BF5003D66,
	ShootLeaf__ctor_mFD57AF8D2D25246C6CCA0B8569EFDDF2D4E1ADE5,
	ShootPos_Start_m9CB1B1CDE368364481F1F935813BF1E2FA1AAFF9,
	ShootPos_Update_m3204B5E49E2C8A8B1BE3DAC8BE289AFD56BD596D,
	ShootPos_GunRight_m6358217619E4EAE2CD0E1C33C3BB62787BAB725A,
	ShootPos_GunLeft_m03F8C9E569ACD61FB2E2C8AAD84E914D0548F2BD,
	ShootPos_GunDown_m223F86E1457897FAF12FE951A04922402FC6A020,
	ShootPos_GunUp_mBCA85CC9F4BE4D55A73B3FA62B34BE96C720DF46,
	ShootPos__ctor_m8E1B8EA0DDB1E000D3A0F6700FB7D62B6187E514,
	SpikSound_Start_m6572A4DE30CFB0894199DE7F922A2ABD880314E8,
	SpikSound_Update_mED1241CFFDE071448E2E6C6C4031D66A1A57F0C6,
	SpikSound_SpikeUp_m4C4289F78080151F9E271CC259F39E56ED04734D,
	SpikSound__ctor_m00E2E93935390928E8F69BCE061B15C37E17B91D,
	SpikeBullet_Start_m18B1112E15E25DE40A454B093A5C242487B3EDCE,
	SpikeBullet_Update_mA25F2509DE76DA55174D9E07F69F6F994D5F05F6,
	SpikeBullet_Fire_m77A7910D429BCA1047CD5D9FE47B5F075EDD5CEF,
	SpikeBullet__ctor_m76CE956C301CB08780266BC15837B19EB1C6CB4E,
	SpikeController_Start_m5C8F2D44C0814D0E9175B7E3B32F0B51257C2139,
	SpikeController_Update_m16FA14B230D1033D0C377C4BC7CBFDD18C898DBC,
	SpikeController_SpikeUp_mE654F6872E416547EAAEF1BBA981F2FCA25A9202,
	SpikeController__ctor_mBB64FE5B499197E7772F33D8CB08E3A452FB0BD9,
	BGM_Awake_m3AFF9C1EC8C25A01D8C26676A3415E79C4BAB9C9,
	BGM__ctor_mBB8F34CDDED6D1689C51EDB0028FBC9F14F9D9A5,
	BackgroundScroller_Start_mE4167530D769FE00F53556574705AE761E4D6544,
	BackgroundScroller_Update_m4DA04F8EB8FE8ED936C1EAA93EFCB904AA8AA457,
	BackgroundScroller__ctor_m5694164FDC22FFE5A19A3538CC6A0BA7FE328933,
	SceneLoad_QuitGame_m639BF6FBD129BC3C6236FDB3CDF179C48D1D2DDE,
	SceneLoad_StartGame_m9BF28A848B53F6C784041CB87109F95AFFF75F26,
	SceneLoad__ctor_m04E349D364791AB763A6418CF6B93D7403FDF65B,
	StaticEnemy_Start_m99639045D2042DC66FDBF97EE43023689D70F7C2,
	StaticEnemy_DamageHealth_m41918C78CED9398E41CAD6EFB0988C2532EDD40E,
	StaticEnemy_Reset_m4A777D62276DC732D3191E4F48D267CF16B3D9A7,
	StaticEnemy_RemoveEnemy_mF7FE9C88D58ADC37D2418A9AE8005C915DA206A6,
	StaticEnemy_Defeated_m579E8D433FBAA42AB1F77FE522119B73352EE47B,
	StaticEnemy__ctor_mE920C944496AD4C1C9D3791643FF8D2BCB203F08,
	U3CResetU3Ed__12__ctor_m3BDA33E3CBC8E74B1D63ECE7624726D2F0ED5B33,
	U3CResetU3Ed__12_System_IDisposable_Dispose_m3A795EBB07033590127C289B6B5F5E49A5E567FC,
	U3CResetU3Ed__12_MoveNext_m195741AF6D30A042A89D3E0339AF96A23311C725,
	U3CResetU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1061A8FD8BBDC1BD073A7C5D7EE759104350B430,
	U3CResetU3Ed__12_System_Collections_IEnumerator_Reset_mF82998DB5D5B49AFD9CE752D7397D9B67C176AB3,
	U3CResetU3Ed__12_System_Collections_IEnumerator_get_Current_m13B64BC583A8FE1918C67F176750728CD09C888F,
	U3CRemoveEnemyU3Ed__13__ctor_mFE8832B3C4256C83BAC43813904C447C2929A4F0,
	U3CRemoveEnemyU3Ed__13_System_IDisposable_Dispose_mF1DA594D7D588A39061219178E77EC059C949BEB,
	U3CRemoveEnemyU3Ed__13_MoveNext_mD6F0AC1B1CF0160B30A754BEC0248999DA44BCE2,
	U3CRemoveEnemyU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m439C009B65DD373B6730486E9589933915789BA3,
	U3CRemoveEnemyU3Ed__13_System_Collections_IEnumerator_Reset_m245ABFC3107FFFC827EBDCCF997886EECB6439E8,
	U3CRemoveEnemyU3Ed__13_System_Collections_IEnumerator_get_Current_m650FDB7D270B20CA532F4E1E0486C1783B84A4B1,
	SwitchHandler_OnSwitchButtonClicked_mBB193A839F85C6AA5CB2D6EFF7A189C557365AD3,
	SwitchHandler__ctor_mAC003FD2641DA42242DA11E7903F545AC011C824,
	ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2,
	ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B,
	ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371,
	ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026,
	DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6,
	DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9,
	EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93,
	EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE,
	EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC,
	U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B,
	U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF,
	ThwainMonster_Start_mA186EDBD410AC6858AD9C0FC8214BEFF432F1C7D,
	ThwainMonster_Update_m2950A0B9A6BAC43C859A555C94CBEEC1D7793372,
	ThwainMonster_FixedUpdate_mCDA589FFE76A12BBE3735BA6037B2587380D886E,
	ThwainMonster_OnCollisionEnter2D_mF792837DF4871983D54707DA37776F9558CBD3B4,
	ThwainMonster_Thawin_m6C86C7E85FA477214831CC8EF072EE5F6EF506C3,
	ThwainMonster_Build_m05D35675E0786B759B17C0AF2F067DDF76D4B693,
	ThwainMonster__ctor_m25562718E78B182BC5BEFA95AFFAB267ED6AF1CF,
	U3CThawinU3Ed__15__ctor_mEF2892EA017B7E62EF004982AE3F56F38E6980A1,
	U3CThawinU3Ed__15_System_IDisposable_Dispose_m6DF0994E7AD74DDC24DEAD3FAB93CB232F26AFDD,
	U3CThawinU3Ed__15_MoveNext_m5C88BEA04F3B1E9441418243757FDCA998C9A4AF,
	U3CThawinU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF6D7461EC85545A6F83AB1D9F38638DCCCB7BDE3,
	U3CThawinU3Ed__15_System_Collections_IEnumerator_Reset_m4C12473748C08E79BAC8333D134EE3E2E140773D,
	U3CThawinU3Ed__15_System_Collections_IEnumerator_get_Current_mC850258D3CE2A05A9A334F09705A22526894714D,
	YontU14DryU16B_Start_m91D08B18915498D327859B5E6A1ACE1C5812EEE7,
	YontU14DryU16B_Update_m47807411507470E66AB14CBA31106FDA0DEA5F4E,
	YontU14DryU16B_LockMovement_m8F50C5703F1829E2BA77CF037335BC15B4657896,
	YontU14DryU16B_Fire_mAEA2700A7D21F47718B21FB4B00962F875E3A720,
	YontU14DryU16B_StopAnimate_m36A1DDC430B1929D6D30FE9FAE3B0CA570A8EEDC,
	YontU14DryU16B__ctor_m76A58247EFA381461A64BB7082A0B48E88DB9352,
	bullerAround_Start_m2F9E951F2527DA531A3A3C6C3004F8D989723F88,
	bullerAround_Update_m9A59D7128F58F4DAB1B70A96B178FBE16E84E172,
	bullerAround_FireAction_mC13433AB56F7308581EBBD3C26B46E252AD6E812,
	bullerAround_Fire_mCABB15003FEEEC19D2425D4E400C0C48F5582252,
	bullerAround_TimerReset_mA030CA2549C400DF857B794FAA620EF0511D6263,
	bullerAround__ctor_mCAB517DF82316EBAE0E76BDC11854A36E741155C,
	U3CFireActionU3Ed__12__ctor_m4D9A49C5E7CB1A36B37E5ADB70C420D347F72159,
	U3CFireActionU3Ed__12_System_IDisposable_Dispose_m071496B05C3F27B2E7F7D606A63C698F0A69D2CF,
	U3CFireActionU3Ed__12_MoveNext_m77B4161569C86AB013C018E9F8AD83BC04207A9D,
	U3CFireActionU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF851ED90860DB287F90CA430F16C4DB28016597E,
	U3CFireActionU3Ed__12_System_Collections_IEnumerator_Reset_mE82756C227D55D51245493565A1CF7FF96305500,
	U3CFireActionU3Ed__12_System_Collections_IEnumerator_get_Current_m5DE0834A1C06844043D41DBF2DB921DDE4C22A04,
	coinspawner_OnDisable_m8A6DAB43ED04819A0C04D4B98844A62C3E4D0A02,
	coinspawner_Start_m16D6E23125805650829BBD61D88FFC6E58FF3E8F,
	coinspawner_onCoin_m7D2EECF31F20B81EAF6A40E9771C6C375EBFF82E,
	coinspawner_SpawmCoim_mF16F8712DE127768ABE26EB5CC375AA35E9F7910,
	coinspawner_CalculateCoinPos_m3B8A77B493444DA73AA2B2EF845BF75C40D2EF4C,
	coinspawner_Update_m58C501B202650554152DE84A488201D48DD37806,
	coinspawner__ctor_mE3A8B09E2E2A142EBCFF14171EED3F772B52DCFB,
	AIPatrol_Start_m48A31A72A57AE182CC4E6852D87BE6C192A4E1A8,
	AIPatrol_Update_m387F4AF26FC4DA0D3503A142D9EFFFEF9F1A29DD,
	AIPatrol_CheckPlayer_m42C6A5BB1E69CC70B6240BD3EF4CE39021094F72,
	AIPatrol_FixedUpdate_m3DB3CA34E9E89F890A6A355D5E122F63EC2C7226,
	AIPatrol_TakeDamage_m94682AE31EF8E0B6F82AD859A360A98002DB85B0,
	AIPatrol_Patrol_m0927D4D505A6FB898E858F0616C3A95FCB961388,
	AIPatrol_Flip_mCF7B762E04C0434E337BD34A2FA37AD3C4E5B332,
	AIPatrol_Shoot_m2D2C97E97A4A28D9D5CDDD682133123F905524E5,
	AIPatrol_OnDrawGizmos_m68C0D29DCB5F246799B24315D0C533EB29CF5A22,
	AIPatrol__ctor_m0C69C3AEC5EA2C2EE0A086515055353A892FEAF2,
	PatrolBullets_Update_m3675A77E11C8569B2C23773310D999ABCB7CCD1A,
	PatrolBullets_OnCollisionEnter2D_m4CF9C65267915ECBEAD1CF7B38CC1E85D63C8FF8,
	PatrolBullets__ctor_mB8E659F44688B51D1EE44E1B2EDC42EE9F1BCA05,
	pathfindingMonster_Start_mC3EA7174909E38EE78FE2E5DB394DA01AA027CB5,
	pathfindingMonster_Update_mB4BC4EE85998B4CD6077956A1FD8BCE64F2824DB,
	pathfindingMonster__ctor_m9BC200F27CEA1802A1F056F3D7165D1AA6BF96E3,
	patrolandattack_Start_m8D19AEE91EBE18A58C25D451BAADCA9ECD72BD40,
	patrolandattack_Update_m620E83B2EAE6945259EBBA12F175294A29D0D4DB,
	patrolandattack_PlayerDetection_mE0E093751CFF25BC3A46E43CE619973511E96E37,
	patrolandattack_OnDrawGizmos_mCC5D258D34D757317804E29797092881C0313236,
	patrolandattack__ctor_mE27A6E7CFFD282F2C7C7F731FB73548ABCBD16CD,
	sepikwalk_Start_m74A35D23CD9AB70D4862D8C2B4EB3CA23A6595EF,
	sepikwalk_Update_m72FCC0781BB9C49E6E87C0555CE3D7F1A54513CE,
	sepikwalk_move_mC4BCCF4B9C94068AA28C7567DA0392671FBD8583,
	sepikwalk_OnCollisionEnter2D_mD17EAB3559013368220A9220F058A96B9E026B43,
	sepikwalk__ctor_m9ABC7103B213C0A8165578AFDF96EACCBB1BA466,
	shoot_Start_mF5D7A829DFA7BC756DBD1DD8E52A11F134A47C31,
	shoot_Update_m1442464A9FAECA5BA29D383CEC20C43B8CB69FBA,
	shoot_FixedUpdate_m3279C63D80E255ACD4EA9A23A723069129DF17F1,
	shoot__ctor_m36FCAE46441E2DCB7DBAA4F9B0398E8A045D901E,
	shootBomb_Start_m021A1853108881626E17824581D754608B625D99,
	shootBomb_FixedUpdate_m3991FEF0F3D00C85BA12C6DD9291D577FE14618A,
	shootBomb__ctor_m0D78F8673A839341DB28D2508FA9C9E9094A6CBC,
	AnimatedTexture_Start_m971F531771EF3E15A907589C370E23651DEE5BBB,
	AnimatedTexture_NextFrame_mC674616396C363BC25FE982ACA8F5C4F2EFF8FA6,
	AnimatedTexture__ctor_m248560E947A15180F03C79868049A70054439CFD,
	TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226,
	TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E,
	TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1,
	TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3,
	TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385,
	TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4,
	TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B,
	TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788,
	TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A,
	TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076,
	TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D,
	TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA,
	TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB,
	TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5,
	TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70,
	TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11,
	TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC,
	TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44,
	TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC,
	TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53,
	TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66,
	TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43,
	TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77,
	TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8,
	CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7,
	SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2,
	WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C,
	LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018,
	LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0,
	Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C,
	Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F,
	U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73,
	U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E,
	Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4,
	Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376,
	U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561,
	U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2,
	Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046,
	Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5,
	Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77,
	Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516,
	Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE,
	Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9,
	Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C,
	CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E,
	CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610,
	CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373,
	CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3,
	CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124,
	ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA,
	ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3,
	ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC,
	ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8,
	ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2,
	ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469,
	ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64,
	U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B,
	U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362,
	SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F,
	SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5,
	SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697,
	SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB,
	SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F,
	SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3,
	SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3,
	SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86,
	U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF,
	U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A,
	TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4,
	TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB,
	TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12,
	TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3,
	TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720,
	TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21,
	TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1,
	TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C,
	TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF,
	TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A,
	TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D,
	TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0,
	TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B,
	TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363,
	TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C,
	TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B,
	TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE,
	TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943,
	TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD,
	TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0,
	TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66,
	TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF,
	TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A,
	TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB,
	TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910,
	TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691,
	TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D,
	TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F,
	TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F,
	TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2,
	TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33,
	TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E,
	TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944,
	TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647,
	TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84,
	TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB,
	TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF,
	TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE,
	TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7,
	TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF,
	U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9,
	U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261,
	TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0,
	TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA,
	TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50,
	TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712,
	TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8,
	TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534,
	TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4,
	TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848,
	U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C,
	U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F,
	U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8,
	U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27,
	TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046,
	TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF,
	TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E,
	TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B,
	U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3,
	U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6,
	TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB,
	TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA,
	TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1,
	VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC,
	VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66,
	VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42,
	VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52,
	VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F,
	VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825,
	VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846,
	VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97,
	VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549,
	VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16,
	VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE,
	U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF,
	VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2,
	VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA,
	VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2,
	VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE,
	VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E,
	VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599,
	VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62,
	U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E,
	VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5,
	VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971,
	VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4,
	VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1,
	VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5,
	VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87,
	VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC,
	U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD,
	VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C,
	VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC,
	VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884,
	VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C,
	VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F,
	VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED,
	VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2,
	U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107,
	U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F,
	WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0,
	WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4,
	WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB,
	WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2,
	WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099,
	U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60,
	U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25,
	ParticleSceneControls_Awake_m3268C5B5B81D4EE59C85C27AED5CA3956C284DA4,
	ParticleSceneControls_OnDisable_mEB5EAF7BC1928EBEFD83E651459327948255D811,
	ParticleSceneControls_Previous_mC2FE564898A6019238C5611E63B5EC3581CEAA63,
	ParticleSceneControls_Next_m4AB6C18CBE0148C4D354FDCAD5EE45D472577B8E,
	ParticleSceneControls_Update_m7B5894AE70D3733C901BFFF529195AC1467D632B,
	ParticleSceneControls_KeyboardInput_m29C494EC6ACF4EE139686EF47C3A167D1007970F,
	ParticleSceneControls_CheckForGuiCollision_mB8EDC5E34CE1B59986991852009A011D9514A8FB,
	ParticleSceneControls_Select_m123BA8121C7C5C454EF649A13C724F2622B4517A,
	ParticleSceneControls__ctor_mFE76B488C636F213B77193EDFF38F514A3186631,
	ParticleSceneControls__cctor_mBA868BA9BC2400AD1AB420B3D0AB42D863358338,
	DemoParticleSystem__ctor_mA046B73C934441755CA5D57991D7C453645AD8CE,
	DemoParticleSystemList__ctor_mAFBB16272EB2E695826211DD030553401DF6E83B,
	PlaceTargetWithMouse_Update_m28E68FE68FA0185C0D20A6001F4C262868348BE3,
	PlaceTargetWithMouse__ctor_m3582FA7DC3CF8198244F9EB014FE12AC97985FE7,
	SlowMoButton_Start_m8F334286C4BEE772EAA4842F431E01BB4929A04C,
	SlowMoButton_OnDestroy_mE8D51330ED641B12795E984B6F0ECCB00536DBDB,
	SlowMoButton_ChangeSpeed_m0C19576C039AA7BBBC478C87A6C964376BA58EAB,
	SlowMoButton__ctor_m993A85A43C7462445119B63636FD65A6F8C0F6BF,
	UIBlur_get_Color_m37E56FDDFFE88F05C1AE6170EF2F23D3EF00A1FC,
	UIBlur_set_Color_mAE65462AD56D9DEC9E4E9B3FD1DA4B48FE8837EC,
	UIBlur_get_BuildFlipMode_m6074D1333B72835ECE7C82DA6F8755D354CB0E06,
	UIBlur_set_BuildFlipMode_m87C88BDF66A8EDEF80FD3A3C5A33590C8A0B0EC4,
	UIBlur_get_Intensity_m70C8F8524F7CEE8889890DF69F5374225A73B5F4,
	UIBlur_set_Intensity_mA9B78D055CAF1C795D12C7673B4562902C0E8945,
	UIBlur_get_Multiplier_m3D17DC09769C0172C99A00D3C357C23B91DC2136,
	UIBlur_set_Multiplier_m2128C4A1A5573151229751FACE73473DC7F6AE1D,
	UIBlur_get_OnBeginBlur_m4958233556FE6E69B9CFB10D00424A47F2602870,
	UIBlur_set_OnBeginBlur_m807E67E72612CAFF9C83FBA3A71B7A1981809AFB,
	UIBlur_get_OnEndBlur_mF1CC15C438CBC43578415BA52504BA52ACD31E81,
	UIBlur_set_OnEndBlur_m75277025B310055875C5BA20B8BD9EC666F1DAE4,
	UIBlur_get_OnBlurChanged_m8D0199CE8649433E1624FE2DD8B873AD1A94D0E0,
	UIBlur_set_OnBlurChanged_m3936099CD2FA129F2E86CAF978D2F4B71D41FDFC,
	UIBlur_UpdateBlur_m742EA33D6268AC62F517B418AE70EF8D69FB053C,
	UIBlur_SetBlur_m74FEEE5B25F18A974936240A1B48F5CB843F3B31,
	UIBlur_BeginBlur_mAA4C8C9EC13B2346ED694F17DF6D99E51C7ABEA4,
	UIBlur_EndBlur_m8C470ACEF4EEAFCA2023A1C335B1DC0CDC98D456,
	UIBlur_Displayarrow_m6EAC69ACDCAF3164041DC55B77FA280483E3A2A2,
	UIBlur_Start_mDFC44231F35C1B7BF4FEF7BFE2FBDF009B326471,
	UIBlur_SetComponents_mF86FCEE03FAC73A10A42E4D81C62BD03FFAE008B,
	UIBlur_FindMaterial_m37CD281AB7975FF9B2B82735D6083DDB54B273C8,
	UIBlur_UpdateColor_m564C88F0622C2B31F1A50B976B4FFBF1D272144B,
	UIBlur_UpdateIntensity_m3572315EC60F41642B334006B0F8C758C0E7FE32,
	UIBlur_UpdateMultiplier_mB5AA78F2C943181E69938DF30E92C59D43F00DE6,
	UIBlur_UpdateFlipMode_mDD7A50A15EA84DA2F59AAA0138BB7663BAC343E5,
	UIBlur_BeginBlurCoroutine_m22E1A8448AD6438DAA161116C8B9AF7A5A1C7A5B,
	UIBlur_EndBlurCoroutine_m636D49D1F1297AE5D5C1211455CDDABD524FAC35,
	UIBlur__ctor_mA94F5595C4240506E12CF90C0746F0D81824212C,
	BlurChangedEvent__ctor_m5403806F9B87938C93AF8CF3A10C275100A30C5B,
	U3CBeginBlurCoroutineU3Ed__46__ctor_m27D06DFB52FDBF0B49DA318368D0759928E1D1BF,
	U3CBeginBlurCoroutineU3Ed__46_System_IDisposable_Dispose_mF76EBD1A9FC137D2B4030A5EA027BE799790E4BE,
	U3CBeginBlurCoroutineU3Ed__46_MoveNext_mCF71C0A082230AF52104FAEC5FE289586C57A92C,
	U3CBeginBlurCoroutineU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD8D05546E56ECD61023F4A491559495A373F9488,
	U3CBeginBlurCoroutineU3Ed__46_System_Collections_IEnumerator_Reset_mA7BFC75A2A8264EA28152664634CACF8494C525B,
	U3CBeginBlurCoroutineU3Ed__46_System_Collections_IEnumerator_get_Current_m950F37AAD895B5E9640CEC4651660A2682611759,
	U3CEndBlurCoroutineU3Ed__47__ctor_m0AF52C6A23B8451C701EA167E43C7025E0E85856,
	U3CEndBlurCoroutineU3Ed__47_System_IDisposable_Dispose_mA8C03B32893FC08A9DC63FF29DB65C218216B925,
	U3CEndBlurCoroutineU3Ed__47_MoveNext_mB73842F9774403E72A835B269A7FF64F883B9919,
	U3CEndBlurCoroutineU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D4DFA555688E4463316E76AA7186C4650E8B20C,
	U3CEndBlurCoroutineU3Ed__47_System_Collections_IEnumerator_Reset_mF6F0D7B092B4B08D62A8FC1FFE80F1236D268AAF,
	U3CEndBlurCoroutineU3Ed__47_System_Collections_IEnumerator_get_Current_m777A091BCC1224A89508F8FD90276D7D6B1A8E6B,
	MovingImage_Awake_m0D31880152C000DEF08A0E8C3D45DFAFDF3B598C,
	MovingImage_Update_mD1DCF8CC3FED5BEF3B46110E686674A3F505627F,
	MovingImage_Move_m866068146BA793161988CD41AE02AE580E9AE0A6,
	MovingImage_SetTargetCoroutine_m5CAB8DC1170D3816EA911BC37EEB6319647200EC,
	MovingImage_GetRandomTarget_m66C8B7576FB36656899C6F52E8B45369782200C4,
	MovingImage_GetRandomWait_mF76DCC7EFA6456A09EBEA77C28CDE6E2ED7480D8,
	MovingImage__ctor_m73C9F985C03D1AB09E0D72132B0BE532E42B731D,
	U3CSetTargetCoroutineU3Ed__12__ctor_m2DAA4DD13FEF3D7932EC3D4F52022DAA679B411A,
	U3CSetTargetCoroutineU3Ed__12_System_IDisposable_Dispose_m4058148DD9EDFD7B905B0E3FA12486310F63B784,
	U3CSetTargetCoroutineU3Ed__12_MoveNext_mA87788B18112C925B36D387EB036589EA8E54018,
	U3CSetTargetCoroutineU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A22C7DB41C1C1B32FF82AE0C0D7A4A26AC23750,
	U3CSetTargetCoroutineU3Ed__12_System_Collections_IEnumerator_Reset_m4590115325DA446C3B122D94B7DF7088DCB71F38,
	U3CSetTargetCoroutineU3Ed__12_System_Collections_IEnumerator_get_Current_m57C6B9D83546D5B22A1CAEE1B5F4E36334AF53E6,
	UIBlurWithCanvasGroup_Start_m058FF7DCFC3D67A023A77B575BF41AEE54AF23BD,
	UIBlurWithCanvasGroup_SetComponents_m96EE62BFC8705EA45BC5D14FE0CF1C226C32C18D,
	UIBlurWithCanvasGroup_OnBeginBlur_m506912E4B986F28783729995C9771DE52B2976CE,
	UIBlurWithCanvasGroup_OnBlurChanged_m5DA32D6507DA517C29F73B7B0044A539C794D32E,
	UIBlurWithCanvasGroup_OnEndBlur_m149372157F72C96A9CF0859591488F4736FFFB8E,
	UIBlurWithCanvasGroup__ctor_m0D93D37E6B6227863F5FB5FECF082A64733E91D1,
	Demo_Start_m6657AB6877315735FC76B862C074502316882C35,
	Demo_OnGUI_m46E304B45F44828E960CE39B061CFF330BAA8DC5,
	Demo__ctor_m01A972C49C10179F83548F643CFF42C79A93E1B5,
	CameraFollow_Start_m7594BB7CDA736ED26755A569D73CD0974E1ED0AC,
	CameraFollow_Update_m6ABA506E5AFC5631684C34E4C2C3ACCAC1A5E658,
	CameraFollow__ctor_mEB9340C858CE1A69F7E2016F752318396B6B67B5,
	LayerTrigger_OnTriggerExit2D_m8D2DFF92361F34E8EC6CDAF2CFFE69EC67B2772E,
	LayerTrigger__ctor_m4FAD7D9F152317D404D4A254991ABBF4484795B1,
	PropsAltar_OnTriggerEnter2D_m1151A65ECD1659EC2B4557829E68BD2CF05AD0F1,
	PropsAltar_OnTriggerExit2D_m9C4940E7BCF1B43C4E2E328D6FBB31BC7178F0E8,
	PropsAltar_Update_mC55F80C6BC6D2E28B2177C9BB4132695C46BF116,
	PropsAltar__ctor_mCD0C6E769E9A4E769513F9E69968690838137E17,
	SpriteColorAnimation_Start_mD642B1D49DE0FFBB495D0B3C67F38833FB8553C5,
	SpriteColorAnimation_Update_mFA5366D38B7542D73E1120DB4A51A3488BA4E905,
	SpriteColorAnimation__ctor_m686C271C1E3977E544BEBEDEB24308E66EE9F14D,
	TopDownCharacterController_Start_m06C85C7B1F2AA613EF490A0D02747E2448BFA850,
	TopDownCharacterController_Update_mA557EC76CB2058E2B74353C611F9E3290063F856,
	TopDownCharacterController__ctor_m3CC0CCA46D3ACF9D4FA020490EE0CAB07DBB525C,
	AIDestinationSetter2_Start_m5BCEB60D888F8BAA78B448BDD7536DD431995E2A,
	AIDestinationSetter2_OnEnable_mB323EA5D333D6F6BE8BC4DA8705ECF327D5CC28A,
	AIDestinationSetter2_OnDisable_mD1B14D4AB7DE3A3F3296DB039D6CEA0C0B2BC766,
	AIDestinationSetter2_Update_mE3A989203C22682953A23BD8418B089FF36DD97B,
	AIDestinationSetter2__ctor_m670EC8F565336F84C5D30D680AEF03D35528938C,
	AIDestinationSetter3_Start_mAE6545A4FD163030D2F400A84395280E5EA8F033,
	AIDestinationSetter3_OnEnable_m167D845C6296D14A9B7DFCB24288F040177F6C49,
	AIDestinationSetter3_OnDisable_mBCF0ED02563E7216AE9B0537E4DCDB0EA4FB2992,
	AIDestinationSetter3_Update_mB4E3EFAD147420423C8203DAD6534D92AF740AF0,
	AIDestinationSetter3__ctor_mF3779A47BA831FB4AC6500DD0EAAB7E3998B5AD8,
};
static const int32_t s_InvokerIndices[985] = 
{
	3585,
	3585,
	2966,
	2966,
	3585,
	3585,
	3585,
	3585,
	3585,
	3028,
	3512,
	3585,
	2966,
	3585,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3028,
	2966,
	3585,
	3585,
	3585,
	3585,
	3585,
	2946,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	2966,
	2360,
	3585,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	2966,
	3585,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	2966,
	3585,
	3585,
	3585,
	3585,
	2966,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	1909,
	2966,
	3585,
	2367,
	3585,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3003,
	3585,
	3585,
	3585,
	3585,
	2966,
	3585,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	2966,
	3585,
	3585,
	3585,
	3585,
	3585,
	2966,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	2966,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	2966,
	3585,
	3585,
	3585,
	3585,
	1175,
	3585,
	3585,
	3585,
	3585,
	3585,
	3003,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	2966,
	2360,
	2966,
	3585,
	3585,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3585,
	5688,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	1909,
	2966,
	2367,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	2966,
	3512,
	3512,
	3512,
	3585,
	3585,
	3585,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	2966,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3512,
	2966,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3512,
	3585,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	1909,
	2367,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	2966,
	3585,
	3585,
	3585,
	3585,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	2966,
	2966,
	3585,
	3585,
	3585,
	3585,
	2966,
	3585,
	3585,
	3585,
	3585,
	3585,
	2966,
	3585,
	2966,
	3585,
	2966,
	3585,
	3585,
	2966,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	2946,
	2966,
	3585,
	2966,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	5688,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	2966,
	3585,
	3585,
	3585,
	2966,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3512,
	3585,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	2966,
	3512,
	3585,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	1909,
	3512,
	3512,
	3585,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	2966,
	3585,
	3585,
	3585,
	3585,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	2966,
	3512,
	3585,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3512,
	3585,
	3585,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3003,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	2966,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	2966,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	790,
	3585,
	790,
	3585,
	3512,
	2966,
	3512,
	2966,
	3512,
	2966,
	3512,
	2966,
	3512,
	2966,
	3585,
	3585,
	2966,
	2966,
	1602,
	1602,
	1112,
	1112,
	1122,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3585,
	2360,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	2946,
	3585,
	3585,
	3585,
	1602,
	1602,
	1112,
	1112,
	1122,
	3585,
	3585,
	3585,
	3585,
	2966,
	2966,
	3585,
	3585,
	3585,
	3585,
	2966,
	3585,
	2966,
	2966,
	2966,
	2966,
	2946,
	3585,
	3585,
	3585,
	3585,
	2946,
	3585,
	3585,
	2946,
	3585,
	3585,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	2966,
	2360,
	2360,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3512,
	3512,
	3585,
	5688,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3585,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	2966,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	2966,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	2966,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	2966,
	3512,
	3585,
	3585,
	1250,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	2360,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3547,
	2946,
	3585,
	5688,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3451,
	2901,
	3491,
	2946,
	3555,
	3003,
	3555,
	3003,
	3512,
	2966,
	3512,
	2966,
	3512,
	2966,
	3585,
	1058,
	3003,
	3003,
	3585,
	3585,
	3585,
	3512,
	3585,
	3585,
	3585,
	3585,
	2364,
	2364,
	3585,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3512,
	3577,
	3512,
	3585,
	2946,
	3585,
	3547,
	3512,
	3585,
	3512,
	3585,
	3585,
	3585,
	3003,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	2966,
	3585,
	2966,
	2966,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
	3585,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	985,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
