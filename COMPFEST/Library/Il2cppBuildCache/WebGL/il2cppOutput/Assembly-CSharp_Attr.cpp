﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8;
// System.String
struct String_t;
// System.Type
struct Type_t;
// Pathfinding.UniqueComponentAttribute
struct UniqueComponentAttribute_tF88A1BA7A11680CD4D65FF2B2BB70D38B314A64B;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAttackSpinU3Ed__13_tEE4094FC8C3FA8260A9364391BC98173A97F04A1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBeginBlurCoroutineU3Ed__46_t66A4D62ADA1165BE58D4D93D95B8FB28B54A5E3B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBombExplodeU3Ed__13_t4199FBF8EE8FB69FC2E8D1451FA0317074C8EC21_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCanvasVisibleU3Ed__8_tFBE66285DCE7017B4944C80BFF0206EE3F331B90_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CChestOpenU3Ed__13_t0EF608A096EA5B88E7441B285F1CA775DAD69FCC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDelayedRespawnU3Ed__6_t301398457F090A938090B53CE3DF6F3296CC89BA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDelayedRespawnU3Ed__8_t898DB56525F2D9AE983B889DDCC35BAC2B3A492F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDieU3Ed__19_t58BC827D82625F3382F7D1006A44B569D7978E65_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDieU3Ed__27_t7CD29C5A2D9C21135509242D28ACF0A3FC417533_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDieU3Ed__9_tB4DE2C7CC1C2540907ECF2E9768D08FE2FE68284_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDoLoopU3Ed__9_t323C8A0A5985C74C1F651C71592BFFE00295DC35_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDyingU3Ed__11_tD7889E9F82B12975287DAD49DAFF3496615FE6E8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEndBlurCoroutineU3Ed__47_t1A9EDFAA6F9FCD2B99F7462CBE18E5D863D37F4E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFireActionU3Ed__12_t42A2A24BA817A19357851F3502083A1A250B7021_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFlipXYU3Ed__24_tAC33D870E2638F395F4872BB8B7AB326A68B447B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CNotFlipXU3Ed__25_t97BF7638A47B9054628458EF337209FB5E1B47DB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CNotFlipYU3Ed__26_t299D05E533CB08705D83A8436B638C45175F2B9B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnCollisionExit2DU3Ed__22_t1E250C4710DB6AC782B78E32E267E69DB314A2B4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnCollisionExit2DU3Ed__4_t0A7F5C50E870A71D5A066A4183C6EDB3E19B2E6A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRemoveEnemyU3Ed__13_t501D36A6DF6D75CD4BA1F570226BA29791C408CE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRemoveEnemyU3Ed__17_t5A654C70BFCE5C7037FAA9B9B24DFC18C8291E8C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRemoveEnemyU3Ed__20_t6B7DBBF2F07284838A9445F875F2DE0B0B00A0B0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CResetU3Ed__12_t7BA88BBF0C8AA1E7793E5DA6866CD9202A2D4A19_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CResetU3Ed__16_tC91F103945D467258EBCB7653985176BC1BA3134_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CResetU3Ed__19_t8B25B25842B8FF12E2D94BF6AC092846A59A530B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CResetU3Ed__21_t3A09E60FC3785242FEC1CF7ECB8A4168CEEEA729_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSetTargetCoroutineU3Ed__12_tDB1343893EA8FAC6B6FCEA0D4FF1887E41326762_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShadowU3Ed__10_tEA6BB67F648FB2A2A1D1025F5C635A68E1ACA112_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStopAttackU3Ed__12_tA53C09AEBE965C669F3A5CBDB6D421C85EBAD548_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CThawinU3Ed__15_t55349D1A3E9AF40B9525721BC9BC00A5C6763A0C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.HelpURLAttribute::m_Url
	String_t* ___m_Url_0;
	// System.Boolean UnityEngine.HelpURLAttribute::m_Dispatcher
	bool ___m_Dispatcher_1;
	// System.String UnityEngine.HelpURLAttribute::m_DispatchingFieldName
	String_t* ___m_DispatchingFieldName_2;

public:
	inline static int32_t get_offset_of_m_Url_0() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Url_0)); }
	inline String_t* get_m_Url_0() const { return ___m_Url_0; }
	inline String_t** get_address_of_m_Url_0() { return &___m_Url_0; }
	inline void set_m_Url_0(String_t* value)
	{
		___m_Url_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Url_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Dispatcher_1() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Dispatcher_1)); }
	inline bool get_m_Dispatcher_1() const { return ___m_Dispatcher_1; }
	inline bool* get_address_of_m_Dispatcher_1() { return &___m_Dispatcher_1; }
	inline void set_m_Dispatcher_1(bool value)
	{
		___m_Dispatcher_1 = value;
	}

	inline static int32_t get_offset_of_m_DispatchingFieldName_2() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_DispatchingFieldName_2)); }
	inline String_t* get_m_DispatchingFieldName_2() const { return ___m_DispatchingFieldName_2; }
	inline String_t** get_address_of_m_DispatchingFieldName_2() { return &___m_DispatchingFieldName_2; }
	inline void set_m_DispatchingFieldName_2(String_t* value)
	{
		___m_DispatchingFieldName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DispatchingFieldName_2), (void*)value);
	}
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// Pathfinding.UniqueComponentAttribute
struct UniqueComponentAttribute_tF88A1BA7A11680CD4D65FF2B2BB70D38B314A64B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String Pathfinding.UniqueComponentAttribute::tag
	String_t* ___tag_0;

public:
	inline static int32_t get_offset_of_tag_0() { return static_cast<int32_t>(offsetof(UniqueComponentAttribute_tF88A1BA7A11680CD4D65FF2B2BB70D38B314A64B, ___tag_0)); }
	inline String_t* get_tag_0() const { return ___tag_0; }
	inline String_t** get_address_of_tag_0() { return &___tag_0; }
	inline void set_tag_0(String_t* value)
	{
		___tag_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tag_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.SpaceAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HelpURLAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215 (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * __this, String_t* ___url0, const RuntimeMethod* method);
// System.Void Pathfinding.UniqueComponentAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UniqueComponentAttribute__ctor_m8986901ED1E290F6C5A592A4BBBCED447D34B554 (UniqueComponentAttribute_tF88A1BA7A11680CD4D65FF2B2BB70D38B314A64B * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void BombAction_t98D24241EBE24B60FD668DCBF734B4BF85AF55A0_CustomAttributesCacheGenerator_BombAction_BombExplode_mBBCB6A87E2BD076879BE0B674CD60DFEECB12E1A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBombExplodeU3Ed__13_t4199FBF8EE8FB69FC2E8D1451FA0317074C8EC21_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CBombExplodeU3Ed__13_t4199FBF8EE8FB69FC2E8D1451FA0317074C8EC21_0_0_0_var), NULL);
	}
}
static void U3CBombExplodeU3Ed__13_t4199FBF8EE8FB69FC2E8D1451FA0317074C8EC21_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBombExplodeU3Ed__13_t4199FBF8EE8FB69FC2E8D1451FA0317074C8EC21_CustomAttributesCacheGenerator_U3CBombExplodeU3Ed__13__ctor_m923CB05D2C511E33FDCFB085649F5DAA1CCAF7BD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBombExplodeU3Ed__13_t4199FBF8EE8FB69FC2E8D1451FA0317074C8EC21_CustomAttributesCacheGenerator_U3CBombExplodeU3Ed__13_System_IDisposable_Dispose_m15C4650CE46C34DFC8C5830A307D37CC569AAE0F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBombExplodeU3Ed__13_t4199FBF8EE8FB69FC2E8D1451FA0317074C8EC21_CustomAttributesCacheGenerator_U3CBombExplodeU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72951C1ADD21C1776A9F6F9FBBB1784F6CCF401A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBombExplodeU3Ed__13_t4199FBF8EE8FB69FC2E8D1451FA0317074C8EC21_CustomAttributesCacheGenerator_U3CBombExplodeU3Ed__13_System_Collections_IEnumerator_Reset_m623ED216F0519489B9F92EFEB501539FBE710AE2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBombExplodeU3Ed__13_t4199FBF8EE8FB69FC2E8D1451FA0317074C8EC21_CustomAttributesCacheGenerator_U3CBombExplodeU3Ed__13_System_Collections_IEnumerator_get_Current_mD297AFE797BA2B44311CCDB9548E4366A41D37FA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BulletScript_t0B4787136F76B578F89F6D398C85DA98A743E1AD_CustomAttributesCacheGenerator_BulletScript_OnCollisionExit2D_m39DD1005BC9F225C87231175DC010294F39C09B4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnCollisionExit2DU3Ed__4_t0A7F5C50E870A71D5A066A4183C6EDB3E19B2E6A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COnCollisionExit2DU3Ed__4_t0A7F5C50E870A71D5A066A4183C6EDB3E19B2E6A_0_0_0_var), NULL);
	}
}
static void U3COnCollisionExit2DU3Ed__4_t0A7F5C50E870A71D5A066A4183C6EDB3E19B2E6A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnCollisionExit2DU3Ed__4_t0A7F5C50E870A71D5A066A4183C6EDB3E19B2E6A_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__4__ctor_m14AF5686030543F733A7415FDCAD1BF3AB949BEC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCollisionExit2DU3Ed__4_t0A7F5C50E870A71D5A066A4183C6EDB3E19B2E6A_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__4_System_IDisposable_Dispose_m73D6E1249D30E89DDD8894FCC44BF8D01F448D00(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCollisionExit2DU3Ed__4_t0A7F5C50E870A71D5A066A4183C6EDB3E19B2E6A_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A272FB97A0973E2DF45FAE555831D5023594805(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCollisionExit2DU3Ed__4_t0A7F5C50E870A71D5A066A4183C6EDB3E19B2E6A_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__4_System_Collections_IEnumerator_Reset_m9337004461BBF6ECF0723DBD6E0EBDE65CA9ADE2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCollisionExit2DU3Ed__4_t0A7F5C50E870A71D5A066A4183C6EDB3E19B2E6A_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__4_System_Collections_IEnumerator_get_Current_mC32AD54E8C558A329810C8701029A8B9B31E46B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ChestScript_tCF82E2DF0D3418ED5238E8B86468C6AC18BCE1B8_CustomAttributesCacheGenerator_ChestScript_ChestOpen_m61B1715BA969C81616E4C65A3A17BE4015C5C2AC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CChestOpenU3Ed__13_t0EF608A096EA5B88E7441B285F1CA775DAD69FCC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CChestOpenU3Ed__13_t0EF608A096EA5B88E7441B285F1CA775DAD69FCC_0_0_0_var), NULL);
	}
}
static void U3CChestOpenU3Ed__13_t0EF608A096EA5B88E7441B285F1CA775DAD69FCC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CChestOpenU3Ed__13_t0EF608A096EA5B88E7441B285F1CA775DAD69FCC_CustomAttributesCacheGenerator_U3CChestOpenU3Ed__13__ctor_m47FE64B53C9CBFBD01E777FE699028D93D2F4E31(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChestOpenU3Ed__13_t0EF608A096EA5B88E7441B285F1CA775DAD69FCC_CustomAttributesCacheGenerator_U3CChestOpenU3Ed__13_System_IDisposable_Dispose_m3AEB6E997AB70866D1CAC9FD6B4E4461CA83EDF7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChestOpenU3Ed__13_t0EF608A096EA5B88E7441B285F1CA775DAD69FCC_CustomAttributesCacheGenerator_U3CChestOpenU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m773A3042410FA3B4CF8542A5404809996DE56296(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChestOpenU3Ed__13_t0EF608A096EA5B88E7441B285F1CA775DAD69FCC_CustomAttributesCacheGenerator_U3CChestOpenU3Ed__13_System_Collections_IEnumerator_Reset_m0642610982D6DB7B358B9E0AC3349535664FC592(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChestOpenU3Ed__13_t0EF608A096EA5B88E7441B285F1CA775DAD69FCC_CustomAttributesCacheGenerator_U3CChestOpenU3Ed__13_System_Collections_IEnumerator_get_Current_mCED6EEA482BDF5E3F2D25500341C365D9597E9B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void FlashMaterial_t940980625B0FC56B5774C55727E599259C3BC57A_CustomAttributesCacheGenerator_FlashMaterial_DoLoop_m5AC8BAF3E232FAD720F467C4553C008A657F9B93(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDoLoopU3Ed__9_t323C8A0A5985C74C1F651C71592BFFE00295DC35_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDoLoopU3Ed__9_t323C8A0A5985C74C1F651C71592BFFE00295DC35_0_0_0_var), NULL);
	}
}
static void U3CDoLoopU3Ed__9_t323C8A0A5985C74C1F651C71592BFFE00295DC35_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDoLoopU3Ed__9_t323C8A0A5985C74C1F651C71592BFFE00295DC35_CustomAttributesCacheGenerator_U3CDoLoopU3Ed__9__ctor_m80FB7782169D5C9434E6BB332F0875E552A86EE3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDoLoopU3Ed__9_t323C8A0A5985C74C1F651C71592BFFE00295DC35_CustomAttributesCacheGenerator_U3CDoLoopU3Ed__9_System_IDisposable_Dispose_mA1D883D6198A472B861CDD8E69C87389B202711E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDoLoopU3Ed__9_t323C8A0A5985C74C1F651C71592BFFE00295DC35_CustomAttributesCacheGenerator_U3CDoLoopU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m161A940F778B3E1A8EB5FF11524329B4ED2B24E9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDoLoopU3Ed__9_t323C8A0A5985C74C1F651C71592BFFE00295DC35_CustomAttributesCacheGenerator_U3CDoLoopU3Ed__9_System_Collections_IEnumerator_Reset_m03E7421E1933ED063603056D3A8AA90432B2EBA5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDoLoopU3Ed__9_t323C8A0A5985C74C1F651C71592BFFE00295DC35_CustomAttributesCacheGenerator_U3CDoLoopU3Ed__9_System_Collections_IEnumerator_get_Current_m92E236931F2A16F330962D928B271976E1A5AD91(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void FungiMonster_tFDC4C7D802A4BCB3F6ED887AE1E957F08BF11AB9_CustomAttributesCacheGenerator_FungiMonster_Reset_m811B8C555ADE3F36B97FB168CBCF5DD0FA6D62B4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CResetU3Ed__21_t3A09E60FC3785242FEC1CF7ECB8A4168CEEEA729_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CResetU3Ed__21_t3A09E60FC3785242FEC1CF7ECB8A4168CEEEA729_0_0_0_var), NULL);
	}
}
static void U3CResetU3Ed__21_t3A09E60FC3785242FEC1CF7ECB8A4168CEEEA729_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CResetU3Ed__21_t3A09E60FC3785242FEC1CF7ECB8A4168CEEEA729_CustomAttributesCacheGenerator_U3CResetU3Ed__21__ctor_m769F0D7F760AE690DF6B43BEA616B6470E69A540(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetU3Ed__21_t3A09E60FC3785242FEC1CF7ECB8A4168CEEEA729_CustomAttributesCacheGenerator_U3CResetU3Ed__21_System_IDisposable_Dispose_mF3B71B7BD6282934BD973F3FF905DA11CD7AF34A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetU3Ed__21_t3A09E60FC3785242FEC1CF7ECB8A4168CEEEA729_CustomAttributesCacheGenerator_U3CResetU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m40A8093877EEA9DD3CF5DA5BBF315DC68B1FFE7A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetU3Ed__21_t3A09E60FC3785242FEC1CF7ECB8A4168CEEEA729_CustomAttributesCacheGenerator_U3CResetU3Ed__21_System_Collections_IEnumerator_Reset_m476796199DB1FB09AECC198FA41250799FC574AE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetU3Ed__21_t3A09E60FC3785242FEC1CF7ECB8A4168CEEEA729_CustomAttributesCacheGenerator_U3CResetU3Ed__21_System_Collections_IEnumerator_get_Current_mA568910A33A57FBBB89BE715EC8C8E1014881F71(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void LEVEL_t374166D5CEFE5F1CCDC52D08E22E0A4FAFD901E1_CustomAttributesCacheGenerator_LEVEL_CanvasVisible_mA6259459DCA7F69EF82C18AED18304BB8D4214A6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCanvasVisibleU3Ed__8_tFBE66285DCE7017B4944C80BFF0206EE3F331B90_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCanvasVisibleU3Ed__8_tFBE66285DCE7017B4944C80BFF0206EE3F331B90_0_0_0_var), NULL);
	}
}
static void U3CCanvasVisibleU3Ed__8_tFBE66285DCE7017B4944C80BFF0206EE3F331B90_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCanvasVisibleU3Ed__8_tFBE66285DCE7017B4944C80BFF0206EE3F331B90_CustomAttributesCacheGenerator_U3CCanvasVisibleU3Ed__8__ctor_mB747C6375F58AE06D33940068E0429813343D23B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCanvasVisibleU3Ed__8_tFBE66285DCE7017B4944C80BFF0206EE3F331B90_CustomAttributesCacheGenerator_U3CCanvasVisibleU3Ed__8_System_IDisposable_Dispose_m2B04E17B955D1CE7679FF6F2A6CDF01CDA2AA2EF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCanvasVisibleU3Ed__8_tFBE66285DCE7017B4944C80BFF0206EE3F331B90_CustomAttributesCacheGenerator_U3CCanvasVisibleU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC1B4F1E8D5035F6F1FEF26BEF3D2B6D4F16B882D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCanvasVisibleU3Ed__8_tFBE66285DCE7017B4944C80BFF0206EE3F331B90_CustomAttributesCacheGenerator_U3CCanvasVisibleU3Ed__8_System_Collections_IEnumerator_Reset_m60B70917051546AC0423BB783D2146FE7F07A3DC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCanvasVisibleU3Ed__8_tFBE66285DCE7017B4944C80BFF0206EE3F331B90_CustomAttributesCacheGenerator_U3CCanvasVisibleU3Ed__8_System_Collections_IEnumerator_get_Current_m331FFE5EF7E5CBD34180C12B89A4DC1286C16E5D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ParticleStart_tD05588725AE893915D20851D0394B7537B0AAB70_CustomAttributesCacheGenerator_ParticleStart_Dying_m5D0B1E9994445919D0F0185BB8E7E050D71E5AD0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDyingU3Ed__11_tD7889E9F82B12975287DAD49DAFF3496615FE6E8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDyingU3Ed__11_tD7889E9F82B12975287DAD49DAFF3496615FE6E8_0_0_0_var), NULL);
	}
}
static void U3CDyingU3Ed__11_tD7889E9F82B12975287DAD49DAFF3496615FE6E8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDyingU3Ed__11_tD7889E9F82B12975287DAD49DAFF3496615FE6E8_CustomAttributesCacheGenerator_U3CDyingU3Ed__11__ctor_m58D732957B79282AA92ADF5F21813A7E85BCFFF0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDyingU3Ed__11_tD7889E9F82B12975287DAD49DAFF3496615FE6E8_CustomAttributesCacheGenerator_U3CDyingU3Ed__11_System_IDisposable_Dispose_mFA3AC7162FF2307CBFA845DD40FC27ED0BD267FC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDyingU3Ed__11_tD7889E9F82B12975287DAD49DAFF3496615FE6E8_CustomAttributesCacheGenerator_U3CDyingU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51C1911128E46618C6D1050F369891214787B76E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDyingU3Ed__11_tD7889E9F82B12975287DAD49DAFF3496615FE6E8_CustomAttributesCacheGenerator_U3CDyingU3Ed__11_System_Collections_IEnumerator_Reset_m748FB5A362DC188E0B7A3A683E1F523A07E5EBCC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDyingU3Ed__11_tD7889E9F82B12975287DAD49DAFF3496615FE6E8_CustomAttributesCacheGenerator_U3CDyingU3Ed__11_System_Collections_IEnumerator_get_Current_m0DF20F30AB85F89DAC4C200ECF9C50B755DE0651(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Char2DController_tBE830CB8D8053E7D295F07D40B4C637A0EDB6C0A_CustomAttributesCacheGenerator_m_JumpForce(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Char2DController_tBE830CB8D8053E7D295F07D40B4C637A0EDB6C0A_CustomAttributesCacheGenerator_m_CrouchSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void Char2DController_tBE830CB8D8053E7D295F07D40B4C637A0EDB6C0A_CustomAttributesCacheGenerator_m_MovementSmoothing(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 0.300000012f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Char2DController_tBE830CB8D8053E7D295F07D40B4C637A0EDB6C0A_CustomAttributesCacheGenerator_m_AirControl(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Char2DController_tBE830CB8D8053E7D295F07D40B4C637A0EDB6C0A_CustomAttributesCacheGenerator_m_WhatIsGround(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Char2DController_tBE830CB8D8053E7D295F07D40B4C637A0EDB6C0A_CustomAttributesCacheGenerator_m_GroundCheck(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Char2DController_tBE830CB8D8053E7D295F07D40B4C637A0EDB6C0A_CustomAttributesCacheGenerator_m_CeilingCheck(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Char2DController_tBE830CB8D8053E7D295F07D40B4C637A0EDB6C0A_CustomAttributesCacheGenerator_m_CrouchDisableCollider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Char2DController_tBE830CB8D8053E7D295F07D40B4C637A0EDB6C0A_CustomAttributesCacheGenerator_OnLandEvent(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x73"), NULL);
	}
}
static void Enemy2D_tC347E442B3CF83F1584563426C6E202527E5C041_CustomAttributesCacheGenerator_Enemy2D_Die_m3EC84CC04E8DD61905E8434CCEB48F25FD19BB56(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDieU3Ed__9_tB4DE2C7CC1C2540907ECF2E9768D08FE2FE68284_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDieU3Ed__9_tB4DE2C7CC1C2540907ECF2E9768D08FE2FE68284_0_0_0_var), NULL);
	}
}
static void U3CDieU3Ed__9_tB4DE2C7CC1C2540907ECF2E9768D08FE2FE68284_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDieU3Ed__9_tB4DE2C7CC1C2540907ECF2E9768D08FE2FE68284_CustomAttributesCacheGenerator_U3CDieU3Ed__9__ctor_mADE2673E0AC151EEE2C2B8BE752FAF867D70790E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDieU3Ed__9_tB4DE2C7CC1C2540907ECF2E9768D08FE2FE68284_CustomAttributesCacheGenerator_U3CDieU3Ed__9_System_IDisposable_Dispose_m2AE53DE0960F3C4AD19D9B539603A2F844447039(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDieU3Ed__9_tB4DE2C7CC1C2540907ECF2E9768D08FE2FE68284_CustomAttributesCacheGenerator_U3CDieU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51EEC3D1CC93BB56CEB071BFAEA5F897512C2FEB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDieU3Ed__9_tB4DE2C7CC1C2540907ECF2E9768D08FE2FE68284_CustomAttributesCacheGenerator_U3CDieU3Ed__9_System_Collections_IEnumerator_Reset_m8DCFC9CAB164B3F18DC2EDC7E5F35DF765A182E0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDieU3Ed__9_tB4DE2C7CC1C2540907ECF2E9768D08FE2FE68284_CustomAttributesCacheGenerator_U3CDieU3Ed__9_System_Collections_IEnumerator_get_Current_mD3EED8BA14478BD8216B581B28EB81FEE5ECEECD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_CustomAttributesCacheGenerator_Player_OnCollisionExit2D_m3313F2D12F1F1BA58D9E410CE7386136F5008BCF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnCollisionExit2DU3Ed__22_t1E250C4710DB6AC782B78E32E267E69DB314A2B4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COnCollisionExit2DU3Ed__22_t1E250C4710DB6AC782B78E32E267E69DB314A2B4_0_0_0_var), NULL);
	}
}
static void Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_CustomAttributesCacheGenerator_Player_Die_m792E214E2658BA493E201D0C397C9E973457CA82(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDieU3Ed__27_t7CD29C5A2D9C21135509242D28ACF0A3FC417533_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDieU3Ed__27_t7CD29C5A2D9C21135509242D28ACF0A3FC417533_0_0_0_var), NULL);
	}
}
static void U3COnCollisionExit2DU3Ed__22_t1E250C4710DB6AC782B78E32E267E69DB314A2B4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnCollisionExit2DU3Ed__22_t1E250C4710DB6AC782B78E32E267E69DB314A2B4_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__22__ctor_mA8A4F25B6067CE0B9414AB9EC7C05D529E7C8386(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCollisionExit2DU3Ed__22_t1E250C4710DB6AC782B78E32E267E69DB314A2B4_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__22_System_IDisposable_Dispose_mDD17E25B307F29A01D61EF5C767F10C624BA2A2A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCollisionExit2DU3Ed__22_t1E250C4710DB6AC782B78E32E267E69DB314A2B4_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m193C48EB097DA325334C0C839D3B48D9FD1238AF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCollisionExit2DU3Ed__22_t1E250C4710DB6AC782B78E32E267E69DB314A2B4_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__22_System_Collections_IEnumerator_Reset_m772A97B6A8F765C1C881C238E0CA7428687C87E1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnCollisionExit2DU3Ed__22_t1E250C4710DB6AC782B78E32E267E69DB314A2B4_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__22_System_Collections_IEnumerator_get_Current_m2C711895EC942AE7CAFBAA314F76746A7B9CBE34(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDieU3Ed__27_t7CD29C5A2D9C21135509242D28ACF0A3FC417533_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDieU3Ed__27_t7CD29C5A2D9C21135509242D28ACF0A3FC417533_CustomAttributesCacheGenerator_U3CDieU3Ed__27__ctor_m8857762760F3BCE10A3D88DD890FAD638A8D4702(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDieU3Ed__27_t7CD29C5A2D9C21135509242D28ACF0A3FC417533_CustomAttributesCacheGenerator_U3CDieU3Ed__27_System_IDisposable_Dispose_mC61821F0D50A408F101518E86C449AC53FF660C0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDieU3Ed__27_t7CD29C5A2D9C21135509242D28ACF0A3FC417533_CustomAttributesCacheGenerator_U3CDieU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m410D885DBDE33BCAABD4860F2043CF3E36EEBF70(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDieU3Ed__27_t7CD29C5A2D9C21135509242D28ACF0A3FC417533_CustomAttributesCacheGenerator_U3CDieU3Ed__27_System_Collections_IEnumerator_Reset_m4D1BD6D7BA2AF7656BF349668CB2385963FD629F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDieU3Ed__27_t7CD29C5A2D9C21135509242D28ACF0A3FC417533_CustomAttributesCacheGenerator_U3CDieU3Ed__27_System_Collections_IEnumerator_get_Current_m6BDD5E6FB780641B98FA16D648C42FD92BF42C05(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627_CustomAttributesCacheGenerator_Enemy_Reset_m628CD2AF324D1E1AB9149FCED22A7148CFBD056D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CResetU3Ed__19_t8B25B25842B8FF12E2D94BF6AC092846A59A530B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CResetU3Ed__19_t8B25B25842B8FF12E2D94BF6AC092846A59A530B_0_0_0_var), NULL);
	}
}
static void Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627_CustomAttributesCacheGenerator_Enemy_RemoveEnemy_mD2058B454847C883951FBE12A118697333EADF62(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRemoveEnemyU3Ed__20_t6B7DBBF2F07284838A9445F875F2DE0B0B00A0B0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRemoveEnemyU3Ed__20_t6B7DBBF2F07284838A9445F875F2DE0B0B00A0B0_0_0_0_var), NULL);
	}
}
static void U3CResetU3Ed__19_t8B25B25842B8FF12E2D94BF6AC092846A59A530B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CResetU3Ed__19_t8B25B25842B8FF12E2D94BF6AC092846A59A530B_CustomAttributesCacheGenerator_U3CResetU3Ed__19__ctor_m5079434512C1D722152FD57D9692C8186A97D321(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetU3Ed__19_t8B25B25842B8FF12E2D94BF6AC092846A59A530B_CustomAttributesCacheGenerator_U3CResetU3Ed__19_System_IDisposable_Dispose_mC4E423C14CAA780263C2CAD897FB06444822E352(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetU3Ed__19_t8B25B25842B8FF12E2D94BF6AC092846A59A530B_CustomAttributesCacheGenerator_U3CResetU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m165E9A0298191DAED6213B75F722A63C9C93732E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetU3Ed__19_t8B25B25842B8FF12E2D94BF6AC092846A59A530B_CustomAttributesCacheGenerator_U3CResetU3Ed__19_System_Collections_IEnumerator_Reset_m4DFA3ED64ABCD8B3242221434025EC1D302916A7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetU3Ed__19_t8B25B25842B8FF12E2D94BF6AC092846A59A530B_CustomAttributesCacheGenerator_U3CResetU3Ed__19_System_Collections_IEnumerator_get_Current_mED384D8433E87A6CEBC3263205674A78D591C2C9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRemoveEnemyU3Ed__20_t6B7DBBF2F07284838A9445F875F2DE0B0B00A0B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRemoveEnemyU3Ed__20_t6B7DBBF2F07284838A9445F875F2DE0B0B00A0B0_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__20__ctor_m1E6B4D9F3646E854A30388BF4635D465618D628A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRemoveEnemyU3Ed__20_t6B7DBBF2F07284838A9445F875F2DE0B0B00A0B0_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__20_System_IDisposable_Dispose_m3745507487481DEB5DBAF8FA60A0AE9F0E670AB5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRemoveEnemyU3Ed__20_t6B7DBBF2F07284838A9445F875F2DE0B0B00A0B0_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAA95AD573C461C64AEAAA6315CFD544159B3B000(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRemoveEnemyU3Ed__20_t6B7DBBF2F07284838A9445F875F2DE0B0B00A0B0_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__20_System_Collections_IEnumerator_Reset_m56D0635675474E0F012360DB67C043F6DEE882B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRemoveEnemyU3Ed__20_t6B7DBBF2F07284838A9445F875F2DE0B0B00A0B0_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__20_System_Collections_IEnumerator_get_Current_m59443E41B8F69CE7426D70185454AC4A86C7DF3F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Player2_t8C8C4E2564E6A8D88E3760E59336E236B08B4502_CustomAttributesCacheGenerator_Player2_FlipXY_m2E8B59FAB876B6DFD536A9F63F4ACC0F6DA5AE8B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFlipXYU3Ed__24_tAC33D870E2638F395F4872BB8B7AB326A68B447B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFlipXYU3Ed__24_tAC33D870E2638F395F4872BB8B7AB326A68B447B_0_0_0_var), NULL);
	}
}
static void Player2_t8C8C4E2564E6A8D88E3760E59336E236B08B4502_CustomAttributesCacheGenerator_Player2_NotFlipX_m393306343FBC9E3BC569493BAA79BDC717A1B092(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CNotFlipXU3Ed__25_t97BF7638A47B9054628458EF337209FB5E1B47DB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CNotFlipXU3Ed__25_t97BF7638A47B9054628458EF337209FB5E1B47DB_0_0_0_var), NULL);
	}
}
static void Player2_t8C8C4E2564E6A8D88E3760E59336E236B08B4502_CustomAttributesCacheGenerator_Player2_NotFlipY_mFF387819DF73F7157B133CFE1C065061658951FA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CNotFlipYU3Ed__26_t299D05E533CB08705D83A8436B638C45175F2B9B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CNotFlipYU3Ed__26_t299D05E533CB08705D83A8436B638C45175F2B9B_0_0_0_var), NULL);
	}
}
static void U3CFlipXYU3Ed__24_tAC33D870E2638F395F4872BB8B7AB326A68B447B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFlipXYU3Ed__24_tAC33D870E2638F395F4872BB8B7AB326A68B447B_CustomAttributesCacheGenerator_U3CFlipXYU3Ed__24__ctor_m131DB1A9C643FD92F6AF8100B1673A68E413C1C9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFlipXYU3Ed__24_tAC33D870E2638F395F4872BB8B7AB326A68B447B_CustomAttributesCacheGenerator_U3CFlipXYU3Ed__24_System_IDisposable_Dispose_m10B3BC1FED6E3FC8BCD0EB206E1AC28669F2EA2A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFlipXYU3Ed__24_tAC33D870E2638F395F4872BB8B7AB326A68B447B_CustomAttributesCacheGenerator_U3CFlipXYU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAD58057BA5483CE0D43CE4B62009B70AF0DE2EB0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFlipXYU3Ed__24_tAC33D870E2638F395F4872BB8B7AB326A68B447B_CustomAttributesCacheGenerator_U3CFlipXYU3Ed__24_System_Collections_IEnumerator_Reset_mA87B6F0AEC5F1895B83ABC2DE5FC1ED6DD4BE20D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFlipXYU3Ed__24_tAC33D870E2638F395F4872BB8B7AB326A68B447B_CustomAttributesCacheGenerator_U3CFlipXYU3Ed__24_System_Collections_IEnumerator_get_Current_m24CEFA38C44FD2871AC176AA4DA6C3F8AF5EAF8A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNotFlipXU3Ed__25_t97BF7638A47B9054628458EF337209FB5E1B47DB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CNotFlipXU3Ed__25_t97BF7638A47B9054628458EF337209FB5E1B47DB_CustomAttributesCacheGenerator_U3CNotFlipXU3Ed__25__ctor_m94F2F0163265685222ADFB187F09580678CA2145(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNotFlipXU3Ed__25_t97BF7638A47B9054628458EF337209FB5E1B47DB_CustomAttributesCacheGenerator_U3CNotFlipXU3Ed__25_System_IDisposable_Dispose_m6F4A9B4FE98D2DD965AF928F40AA6562E71DA52B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNotFlipXU3Ed__25_t97BF7638A47B9054628458EF337209FB5E1B47DB_CustomAttributesCacheGenerator_U3CNotFlipXU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13CA1D64DE97A4EBEBACA8356720DD9FE5756AB4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNotFlipXU3Ed__25_t97BF7638A47B9054628458EF337209FB5E1B47DB_CustomAttributesCacheGenerator_U3CNotFlipXU3Ed__25_System_Collections_IEnumerator_Reset_m0E72C3BD6F7C5880500F39E88ECF4D8832FEF217(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNotFlipXU3Ed__25_t97BF7638A47B9054628458EF337209FB5E1B47DB_CustomAttributesCacheGenerator_U3CNotFlipXU3Ed__25_System_Collections_IEnumerator_get_Current_m307B7C1CAA37E1DAD29DD63844B00D7A5AC947C0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNotFlipYU3Ed__26_t299D05E533CB08705D83A8436B638C45175F2B9B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CNotFlipYU3Ed__26_t299D05E533CB08705D83A8436B638C45175F2B9B_CustomAttributesCacheGenerator_U3CNotFlipYU3Ed__26__ctor_mD5791BAD4289B9AEA13D346B0D15A4085077458C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNotFlipYU3Ed__26_t299D05E533CB08705D83A8436B638C45175F2B9B_CustomAttributesCacheGenerator_U3CNotFlipYU3Ed__26_System_IDisposable_Dispose_m92088D58772A6C6FAEF4B7208F110704E6488A2B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNotFlipYU3Ed__26_t299D05E533CB08705D83A8436B638C45175F2B9B_CustomAttributesCacheGenerator_U3CNotFlipYU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35E9597F83EF82C1BC20F307C6E63FE93EDA7307(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNotFlipYU3Ed__26_t299D05E533CB08705D83A8436B638C45175F2B9B_CustomAttributesCacheGenerator_U3CNotFlipYU3Ed__26_System_Collections_IEnumerator_Reset_mC6D8CCA93FB0987B4081DED3D535729334345AF4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNotFlipYU3Ed__26_t299D05E533CB08705D83A8436B638C45175F2B9B_CustomAttributesCacheGenerator_U3CNotFlipYU3Ed__26_System_Collections_IEnumerator_get_Current_mE496955C82262A2E1E8BFDDFE55AB76A7ED97D97(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SwordAttack_tF24D56ACEA6B660502BE16C22D0BE37FB1FD42E4_CustomAttributesCacheGenerator_SwordAttack_StopAttack_mE7DCA01D4E303DF75B5D784FDE348862D10618C9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStopAttackU3Ed__12_tA53C09AEBE965C669F3A5CBDB6D421C85EBAD548_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStopAttackU3Ed__12_tA53C09AEBE965C669F3A5CBDB6D421C85EBAD548_0_0_0_var), NULL);
	}
}
static void U3CStopAttackU3Ed__12_tA53C09AEBE965C669F3A5CBDB6D421C85EBAD548_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStopAttackU3Ed__12_tA53C09AEBE965C669F3A5CBDB6D421C85EBAD548_CustomAttributesCacheGenerator_U3CStopAttackU3Ed__12__ctor_mC892CCDD9137521226BFDB42F756F498ED9D9DE4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopAttackU3Ed__12_tA53C09AEBE965C669F3A5CBDB6D421C85EBAD548_CustomAttributesCacheGenerator_U3CStopAttackU3Ed__12_System_IDisposable_Dispose_m050550BE33CA586AD6A3AFC4553AF767A80B7E0A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopAttackU3Ed__12_tA53C09AEBE965C669F3A5CBDB6D421C85EBAD548_CustomAttributesCacheGenerator_U3CStopAttackU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD070A2C7A382D082F76C43E10B370F5A161A1568(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopAttackU3Ed__12_tA53C09AEBE965C669F3A5CBDB6D421C85EBAD548_CustomAttributesCacheGenerator_U3CStopAttackU3Ed__12_System_Collections_IEnumerator_Reset_m7EB90774CFA286159F440225FF0844A9CA318451(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopAttackU3Ed__12_tA53C09AEBE965C669F3A5CBDB6D421C85EBAD548_CustomAttributesCacheGenerator_U3CStopAttackU3Ed__12_System_Collections_IEnumerator_get_Current_m8C32386D0F688C60F58C83D55D8232476A3FE9B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ScriptPengaturGame_tEA62B98743EF089268A3DE2E49A6365A6212462F_CustomAttributesCacheGenerator_ScriptPengaturGame_DelayedRespawn_mFAB0633263E6564AF5BB4EF59FABE7202696771E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDelayedRespawnU3Ed__8_t898DB56525F2D9AE983B889DDCC35BAC2B3A492F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDelayedRespawnU3Ed__8_t898DB56525F2D9AE983B889DDCC35BAC2B3A492F_0_0_0_var), NULL);
	}
}
static void U3CDelayedRespawnU3Ed__8_t898DB56525F2D9AE983B889DDCC35BAC2B3A492F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayedRespawnU3Ed__8_t898DB56525F2D9AE983B889DDCC35BAC2B3A492F_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__8__ctor_m40CD1912AF394DC2AC923A52C94357E0DF450D2B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedRespawnU3Ed__8_t898DB56525F2D9AE983B889DDCC35BAC2B3A492F_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__8_System_IDisposable_Dispose_m870C5BECEB2FDF41D71A48F1252484616984C08D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedRespawnU3Ed__8_t898DB56525F2D9AE983B889DDCC35BAC2B3A492F_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m704FE12609434A38D732427EB89149F0E243013B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedRespawnU3Ed__8_t898DB56525F2D9AE983B889DDCC35BAC2B3A492F_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__8_System_Collections_IEnumerator_Reset_mD5F85137107697D53140B274A3025147FCC06881(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedRespawnU3Ed__8_t898DB56525F2D9AE983B889DDCC35BAC2B3A492F_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__8_System_Collections_IEnumerator_get_Current_mAA4466DE52655D1610F3E77D6DFF672364ADD55D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void RandomPathEnemy_tF79996B7E634EA635BBE8DE42D9231BF223ECAA2_CustomAttributesCacheGenerator_RandomPathEnemy_Reset_m7AC5BB1855F12972ADA3278EE482A1C25BFFB8DD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CResetU3Ed__16_tC91F103945D467258EBCB7653985176BC1BA3134_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CResetU3Ed__16_tC91F103945D467258EBCB7653985176BC1BA3134_0_0_0_var), NULL);
	}
}
static void RandomPathEnemy_tF79996B7E634EA635BBE8DE42D9231BF223ECAA2_CustomAttributesCacheGenerator_RandomPathEnemy_RemoveEnemy_m3F3AA696BB27EC03B23500BA53FDA5BEE9652759(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRemoveEnemyU3Ed__17_t5A654C70BFCE5C7037FAA9B9B24DFC18C8291E8C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRemoveEnemyU3Ed__17_t5A654C70BFCE5C7037FAA9B9B24DFC18C8291E8C_0_0_0_var), NULL);
	}
}
static void U3CResetU3Ed__16_tC91F103945D467258EBCB7653985176BC1BA3134_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CResetU3Ed__16_tC91F103945D467258EBCB7653985176BC1BA3134_CustomAttributesCacheGenerator_U3CResetU3Ed__16__ctor_m4A61B7D86F5085B4169CE600699751AD297884B6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetU3Ed__16_tC91F103945D467258EBCB7653985176BC1BA3134_CustomAttributesCacheGenerator_U3CResetU3Ed__16_System_IDisposable_Dispose_mA222C493C8FD94E8FD1C9D3F7472859F5BACCB04(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetU3Ed__16_tC91F103945D467258EBCB7653985176BC1BA3134_CustomAttributesCacheGenerator_U3CResetU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A2CB645B1B0484C48CB991CDE86500798FAA66E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetU3Ed__16_tC91F103945D467258EBCB7653985176BC1BA3134_CustomAttributesCacheGenerator_U3CResetU3Ed__16_System_Collections_IEnumerator_Reset_m29FBCED23D03FA82F30CEC93EAD8BF33BA588ADF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetU3Ed__16_tC91F103945D467258EBCB7653985176BC1BA3134_CustomAttributesCacheGenerator_U3CResetU3Ed__16_System_Collections_IEnumerator_get_Current_m6A8464C3A44F86288FDC802B734BD79679BD8AC6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRemoveEnemyU3Ed__17_t5A654C70BFCE5C7037FAA9B9B24DFC18C8291E8C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRemoveEnemyU3Ed__17_t5A654C70BFCE5C7037FAA9B9B24DFC18C8291E8C_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__17__ctor_mDF5707187B155BFF83BB4292A37DC36184AE56EF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRemoveEnemyU3Ed__17_t5A654C70BFCE5C7037FAA9B9B24DFC18C8291E8C_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__17_System_IDisposable_Dispose_mBE1FBD45087F9AFB7A1697B658CB87630B689714(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRemoveEnemyU3Ed__17_t5A654C70BFCE5C7037FAA9B9B24DFC18C8291E8C_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC87690098968F6C42184EE1080E4E20289A85F33(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRemoveEnemyU3Ed__17_t5A654C70BFCE5C7037FAA9B9B24DFC18C8291E8C_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__17_System_Collections_IEnumerator_Reset_m50CF5CF07AD930842A88145B8EB6089F69155A83(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRemoveEnemyU3Ed__17_t5A654C70BFCE5C7037FAA9B9B24DFC18C8291E8C_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__17_System_Collections_IEnumerator_get_Current_m1EEF8BCDA0EE7B9BFB461F31DF62C5AA9CE89EC5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Rotation_t494A089B49580F1EC9172DD688C6EC94B1E4EEEE_CustomAttributesCacheGenerator_Rotation_AttackSpin_mFCC4707E6C3F1686B797441B2CABE55A56772514(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAttackSpinU3Ed__13_tEE4094FC8C3FA8260A9364391BC98173A97F04A1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAttackSpinU3Ed__13_tEE4094FC8C3FA8260A9364391BC98173A97F04A1_0_0_0_var), NULL);
	}
}
static void U3CAttackSpinU3Ed__13_tEE4094FC8C3FA8260A9364391BC98173A97F04A1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAttackSpinU3Ed__13_tEE4094FC8C3FA8260A9364391BC98173A97F04A1_CustomAttributesCacheGenerator_U3CAttackSpinU3Ed__13__ctor_m4EA58DFAB7F07180EE63578CF3FE454562B21F9A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackSpinU3Ed__13_tEE4094FC8C3FA8260A9364391BC98173A97F04A1_CustomAttributesCacheGenerator_U3CAttackSpinU3Ed__13_System_IDisposable_Dispose_m809F021C7039F1A4EA346723F825E2BEFFC7BB89(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackSpinU3Ed__13_tEE4094FC8C3FA8260A9364391BC98173A97F04A1_CustomAttributesCacheGenerator_U3CAttackSpinU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m460DF86C141E5B679E0E6964C89FFE1E7EECFD7D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackSpinU3Ed__13_tEE4094FC8C3FA8260A9364391BC98173A97F04A1_CustomAttributesCacheGenerator_U3CAttackSpinU3Ed__13_System_Collections_IEnumerator_Reset_mC4DAB2FCF9E95B419ED3BD42CC01B3DAC3C6B5A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackSpinU3Ed__13_tEE4094FC8C3FA8260A9364391BC98173A97F04A1_CustomAttributesCacheGenerator_U3CAttackSpinU3Ed__13_System_Collections_IEnumerator_get_Current_m48BD155E14859DB7FAA98B221A434C12740E8852(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PengaturGame_t790449E6E929F602A9CE7511FFC69F12ECD6D99C_CustomAttributesCacheGenerator_PengaturGame_Die_mB7093D4526DA560D2C9393D4984E3892F75B2574(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDieU3Ed__19_t58BC827D82625F3382F7D1006A44B569D7978E65_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDieU3Ed__19_t58BC827D82625F3382F7D1006A44B569D7978E65_0_0_0_var), NULL);
	}
}
static void U3CDieU3Ed__19_t58BC827D82625F3382F7D1006A44B569D7978E65_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDieU3Ed__19_t58BC827D82625F3382F7D1006A44B569D7978E65_CustomAttributesCacheGenerator_U3CDieU3Ed__19__ctor_mCD289E975B415CDFE7F03327361CA771A9ACC945(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDieU3Ed__19_t58BC827D82625F3382F7D1006A44B569D7978E65_CustomAttributesCacheGenerator_U3CDieU3Ed__19_System_IDisposable_Dispose_mA8D3F31A7FA44478E9DB61A959929BB795A8F824(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDieU3Ed__19_t58BC827D82625F3382F7D1006A44B569D7978E65_CustomAttributesCacheGenerator_U3CDieU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5DB6F0BF77F660F069C9F3A2A60580D58BC136BF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDieU3Ed__19_t58BC827D82625F3382F7D1006A44B569D7978E65_CustomAttributesCacheGenerator_U3CDieU3Ed__19_System_Collections_IEnumerator_Reset_m6C4B4656507A3C75D780F9043623876E56B280C6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDieU3Ed__19_t58BC827D82625F3382F7D1006A44B569D7978E65_CustomAttributesCacheGenerator_U3CDieU3Ed__19_System_Collections_IEnumerator_get_Current_m42A1443B0A7558052BB999AEA2950E2422012F8D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ScriptPengaturGameTutor_tEF7501C6320507C1A8DCED14B9FC409A77A2D7D6_CustomAttributesCacheGenerator_ScriptPengaturGameTutor_DelayedRespawn_m951994477B5FC95D9C1824E4C044B548A9F12869(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDelayedRespawnU3Ed__6_t301398457F090A938090B53CE3DF6F3296CC89BA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDelayedRespawnU3Ed__6_t301398457F090A938090B53CE3DF6F3296CC89BA_0_0_0_var), NULL);
	}
}
static void U3CDelayedRespawnU3Ed__6_t301398457F090A938090B53CE3DF6F3296CC89BA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayedRespawnU3Ed__6_t301398457F090A938090B53CE3DF6F3296CC89BA_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__6__ctor_m832D756A5E55217164F680E64AF1B43041752E1E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedRespawnU3Ed__6_t301398457F090A938090B53CE3DF6F3296CC89BA_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__6_System_IDisposable_Dispose_m3ACCA8E0A4F478BFD532407AAF5C4458EB6D8131(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedRespawnU3Ed__6_t301398457F090A938090B53CE3DF6F3296CC89BA_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m48E50B3154FC7795127B06517938CAB13B89D71A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedRespawnU3Ed__6_t301398457F090A938090B53CE3DF6F3296CC89BA_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__6_System_Collections_IEnumerator_Reset_m814E90D8FD4532733B793DA5FDAF9FF3A57B2CDC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedRespawnU3Ed__6_t301398457F090A938090B53CE3DF6F3296CC89BA_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__6_System_Collections_IEnumerator_get_Current_m4949E5B7B143356AED4CB5677BBD70C93385687E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ShadowMonster_t7EC06E58CE8A62512943B468400CC576CBF9E99F_CustomAttributesCacheGenerator_ShadowMonster_Shadow_m62273387B5D608A04893DADA04C1DF233B336B48(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShadowU3Ed__10_tEA6BB67F648FB2A2A1D1025F5C635A68E1ACA112_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShadowU3Ed__10_tEA6BB67F648FB2A2A1D1025F5C635A68E1ACA112_0_0_0_var), NULL);
	}
}
static void U3CShadowU3Ed__10_tEA6BB67F648FB2A2A1D1025F5C635A68E1ACA112_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShadowU3Ed__10_tEA6BB67F648FB2A2A1D1025F5C635A68E1ACA112_CustomAttributesCacheGenerator_U3CShadowU3Ed__10__ctor_m09156293353FA2A514EC009449AC2E4F8A7BA07E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShadowU3Ed__10_tEA6BB67F648FB2A2A1D1025F5C635A68E1ACA112_CustomAttributesCacheGenerator_U3CShadowU3Ed__10_System_IDisposable_Dispose_m7D1378040012088EFC8545115F4E26B8BE9AD013(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShadowU3Ed__10_tEA6BB67F648FB2A2A1D1025F5C635A68E1ACA112_CustomAttributesCacheGenerator_U3CShadowU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m158249148A41952D3D36E40F719C17F88C16E8F2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShadowU3Ed__10_tEA6BB67F648FB2A2A1D1025F5C635A68E1ACA112_CustomAttributesCacheGenerator_U3CShadowU3Ed__10_System_Collections_IEnumerator_Reset_mC690E70BB7DD3145DC2A8F2E682A15819094A724(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShadowU3Ed__10_tEA6BB67F648FB2A2A1D1025F5C635A68E1ACA112_CustomAttributesCacheGenerator_U3CShadowU3Ed__10_System_Collections_IEnumerator_get_Current_m3B88444077C83B3FD6A4913F0ABB99A182145512(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BackgroundScroller_t0A4783DD437DF3FDAF8B2C4B1EAE1074EC074D01_CustomAttributesCacheGenerator_scrollSpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -1.0f, 1.0f, NULL);
	}
}
static void StaticEnemy_t95B3339AD78C31858F9A2E5603371F34B3937D6C_CustomAttributesCacheGenerator_StaticEnemy_Reset_m4A777D62276DC732D3191E4F48D267CF16B3D9A7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CResetU3Ed__12_t7BA88BBF0C8AA1E7793E5DA6866CD9202A2D4A19_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CResetU3Ed__12_t7BA88BBF0C8AA1E7793E5DA6866CD9202A2D4A19_0_0_0_var), NULL);
	}
}
static void StaticEnemy_t95B3339AD78C31858F9A2E5603371F34B3937D6C_CustomAttributesCacheGenerator_StaticEnemy_RemoveEnemy_mF7FE9C88D58ADC37D2418A9AE8005C915DA206A6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRemoveEnemyU3Ed__13_t501D36A6DF6D75CD4BA1F570226BA29791C408CE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRemoveEnemyU3Ed__13_t501D36A6DF6D75CD4BA1F570226BA29791C408CE_0_0_0_var), NULL);
	}
}
static void U3CResetU3Ed__12_t7BA88BBF0C8AA1E7793E5DA6866CD9202A2D4A19_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CResetU3Ed__12_t7BA88BBF0C8AA1E7793E5DA6866CD9202A2D4A19_CustomAttributesCacheGenerator_U3CResetU3Ed__12__ctor_m3BDA33E3CBC8E74B1D63ECE7624726D2F0ED5B33(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetU3Ed__12_t7BA88BBF0C8AA1E7793E5DA6866CD9202A2D4A19_CustomAttributesCacheGenerator_U3CResetU3Ed__12_System_IDisposable_Dispose_m3A795EBB07033590127C289B6B5F5E49A5E567FC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetU3Ed__12_t7BA88BBF0C8AA1E7793E5DA6866CD9202A2D4A19_CustomAttributesCacheGenerator_U3CResetU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1061A8FD8BBDC1BD073A7C5D7EE759104350B430(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetU3Ed__12_t7BA88BBF0C8AA1E7793E5DA6866CD9202A2D4A19_CustomAttributesCacheGenerator_U3CResetU3Ed__12_System_Collections_IEnumerator_Reset_mF82998DB5D5B49AFD9CE752D7397D9B67C176AB3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetU3Ed__12_t7BA88BBF0C8AA1E7793E5DA6866CD9202A2D4A19_CustomAttributesCacheGenerator_U3CResetU3Ed__12_System_Collections_IEnumerator_get_Current_m13B64BC583A8FE1918C67F176750728CD09C888F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRemoveEnemyU3Ed__13_t501D36A6DF6D75CD4BA1F570226BA29791C408CE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRemoveEnemyU3Ed__13_t501D36A6DF6D75CD4BA1F570226BA29791C408CE_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__13__ctor_mFE8832B3C4256C83BAC43813904C447C2929A4F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRemoveEnemyU3Ed__13_t501D36A6DF6D75CD4BA1F570226BA29791C408CE_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__13_System_IDisposable_Dispose_mF1DA594D7D588A39061219178E77EC059C949BEB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRemoveEnemyU3Ed__13_t501D36A6DF6D75CD4BA1F570226BA29791C408CE_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m439C009B65DD373B6730486E9589933915789BA3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRemoveEnemyU3Ed__13_t501D36A6DF6D75CD4BA1F570226BA29791C408CE_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__13_System_Collections_IEnumerator_Reset_m245ABFC3107FFFC827EBDCCF997886EECB6439E8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRemoveEnemyU3Ed__13_t501D36A6DF6D75CD4BA1F570226BA29791C408CE_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__13_System_Collections_IEnumerator_get_Current_m650FDB7D270B20CA532F4E1E0486C1783B84A4B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DropdownSample_t925F66C7435D75374A0E26DE15B4F1D95697F0DF_CustomAttributesCacheGenerator_text(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DropdownSample_t925F66C7435D75374A0E26DE15B4F1D95697F0DF_CustomAttributesCacheGenerator_dropdownWithoutPlaceholder(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DropdownSample_t925F66C7435D75374A0E26DE15B4F1D95697F0DF_CustomAttributesCacheGenerator_dropdownWithPlaceholder(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EnvMapAnimator_t7C30A094A3786710A896AD9A6FA46B13EFEB72FD_CustomAttributesCacheGenerator_EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ThwainMonster_tB8C199321ACEE54640562BAE36FED77704CE51E3_CustomAttributesCacheGenerator_ThwainMonster_Thawin_m6C86C7E85FA477214831CC8EF072EE5F6EF506C3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CThawinU3Ed__15_t55349D1A3E9AF40B9525721BC9BC00A5C6763A0C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CThawinU3Ed__15_t55349D1A3E9AF40B9525721BC9BC00A5C6763A0C_0_0_0_var), NULL);
	}
}
static void U3CThawinU3Ed__15_t55349D1A3E9AF40B9525721BC9BC00A5C6763A0C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CThawinU3Ed__15_t55349D1A3E9AF40B9525721BC9BC00A5C6763A0C_CustomAttributesCacheGenerator_U3CThawinU3Ed__15__ctor_mEF2892EA017B7E62EF004982AE3F56F38E6980A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CThawinU3Ed__15_t55349D1A3E9AF40B9525721BC9BC00A5C6763A0C_CustomAttributesCacheGenerator_U3CThawinU3Ed__15_System_IDisposable_Dispose_m6DF0994E7AD74DDC24DEAD3FAB93CB232F26AFDD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CThawinU3Ed__15_t55349D1A3E9AF40B9525721BC9BC00A5C6763A0C_CustomAttributesCacheGenerator_U3CThawinU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF6D7461EC85545A6F83AB1D9F38638DCCCB7BDE3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CThawinU3Ed__15_t55349D1A3E9AF40B9525721BC9BC00A5C6763A0C_CustomAttributesCacheGenerator_U3CThawinU3Ed__15_System_Collections_IEnumerator_Reset_m4C12473748C08E79BAC8333D134EE3E2E140773D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CThawinU3Ed__15_t55349D1A3E9AF40B9525721BC9BC00A5C6763A0C_CustomAttributesCacheGenerator_U3CThawinU3Ed__15_System_Collections_IEnumerator_get_Current_mC850258D3CE2A05A9A334F09705A22526894714D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void bullerAround_t8B0D8062BA22026378CCE3BB162576CD1DEF3B0C_CustomAttributesCacheGenerator_bullerAround_FireAction_mC13433AB56F7308581EBBD3C26B46E252AD6E812(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFireActionU3Ed__12_t42A2A24BA817A19357851F3502083A1A250B7021_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFireActionU3Ed__12_t42A2A24BA817A19357851F3502083A1A250B7021_0_0_0_var), NULL);
	}
}
static void U3CFireActionU3Ed__12_t42A2A24BA817A19357851F3502083A1A250B7021_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFireActionU3Ed__12_t42A2A24BA817A19357851F3502083A1A250B7021_CustomAttributesCacheGenerator_U3CFireActionU3Ed__12__ctor_m4D9A49C5E7CB1A36B37E5ADB70C420D347F72159(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFireActionU3Ed__12_t42A2A24BA817A19357851F3502083A1A250B7021_CustomAttributesCacheGenerator_U3CFireActionU3Ed__12_System_IDisposable_Dispose_m071496B05C3F27B2E7F7D606A63C698F0A69D2CF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFireActionU3Ed__12_t42A2A24BA817A19357851F3502083A1A250B7021_CustomAttributesCacheGenerator_U3CFireActionU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF851ED90860DB287F90CA430F16C4DB28016597E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFireActionU3Ed__12_t42A2A24BA817A19357851F3502083A1A250B7021_CustomAttributesCacheGenerator_U3CFireActionU3Ed__12_System_Collections_IEnumerator_Reset_mE82756C227D55D51245493565A1CF7FF96305500(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFireActionU3Ed__12_t42A2A24BA817A19357851F3502083A1A250B7021_CustomAttributesCacheGenerator_U3CFireActionU3Ed__12_System_Collections_IEnumerator_get_Current_m5DE0834A1C06844043D41DBF2DB921DDE4C22A04(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void AIPatrol_t545F247B50644DC6ACDD6C4AE97E1FD947DBEBBA_CustomAttributesCacheGenerator_mustPatrol(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnCharacterSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnSpriteSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnWordSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnLineSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnLinkSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934_CustomAttributesCacheGenerator_Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B_CustomAttributesCacheGenerator_Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8_CustomAttributesCacheGenerator_ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_0_0_0_var), NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07_CustomAttributesCacheGenerator_SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_0_0_0_var), NULL);
	}
}
static void U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TeleType_t92B5ED4FE818F912A562F085CE035F6B42834934_CustomAttributesCacheGenerator_TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289_CustomAttributesCacheGenerator_TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_0_0_0_var), NULL);
	}
}
static void TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289_CustomAttributesCacheGenerator_TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_0_0_0_var), NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200_CustomAttributesCacheGenerator_TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_0_0_0_var), NULL);
	}
}
static void TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200_CustomAttributesCacheGenerator_TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_0_0_0_var), NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374_CustomAttributesCacheGenerator_VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_0_0_0_var), NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A_CustomAttributesCacheGenerator_VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_0_0_0_var), NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A_CustomAttributesCacheGenerator_VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_0_0_0_var), NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9_CustomAttributesCacheGenerator_VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_0_0_0_var), NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163_CustomAttributesCacheGenerator_VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96_CustomAttributesCacheGenerator_WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_0_0_0_var), NULL);
	}
}
static void U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void FlipMode_t2F88797CCAC3141191EB25B280B2947573026B2D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void UIBlur_t164597816B074D3FA54558A54238652E5EDA8248_CustomAttributesCacheGenerator__color(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIBlur_t164597816B074D3FA54558A54238652E5EDA8248_CustomAttributesCacheGenerator__buildFlipMode(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIBlur_t164597816B074D3FA54558A54238652E5EDA8248_CustomAttributesCacheGenerator__intensity(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void UIBlur_t164597816B074D3FA54558A54238652E5EDA8248_CustomAttributesCacheGenerator__multiplier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void UIBlur_t164597816B074D3FA54558A54238652E5EDA8248_CustomAttributesCacheGenerator__onBeginBlur(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIBlur_t164597816B074D3FA54558A54238652E5EDA8248_CustomAttributesCacheGenerator__onEndBlur(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIBlur_t164597816B074D3FA54558A54238652E5EDA8248_CustomAttributesCacheGenerator__onBlurChanged(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIBlur_t164597816B074D3FA54558A54238652E5EDA8248_CustomAttributesCacheGenerator_UIBlur_BeginBlurCoroutine_m22E1A8448AD6438DAA161116C8B9AF7A5A1C7A5B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBeginBlurCoroutineU3Ed__46_t66A4D62ADA1165BE58D4D93D95B8FB28B54A5E3B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CBeginBlurCoroutineU3Ed__46_t66A4D62ADA1165BE58D4D93D95B8FB28B54A5E3B_0_0_0_var), NULL);
	}
}
static void UIBlur_t164597816B074D3FA54558A54238652E5EDA8248_CustomAttributesCacheGenerator_UIBlur_EndBlurCoroutine_m636D49D1F1297AE5D5C1211455CDDABD524FAC35(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEndBlurCoroutineU3Ed__47_t1A9EDFAA6F9FCD2B99F7462CBE18E5D863D37F4E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CEndBlurCoroutineU3Ed__47_t1A9EDFAA6F9FCD2B99F7462CBE18E5D863D37F4E_0_0_0_var), NULL);
	}
}
static void U3CBeginBlurCoroutineU3Ed__46_t66A4D62ADA1165BE58D4D93D95B8FB28B54A5E3B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBeginBlurCoroutineU3Ed__46_t66A4D62ADA1165BE58D4D93D95B8FB28B54A5E3B_CustomAttributesCacheGenerator_U3CBeginBlurCoroutineU3Ed__46__ctor_m27D06DFB52FDBF0B49DA318368D0759928E1D1BF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginBlurCoroutineU3Ed__46_t66A4D62ADA1165BE58D4D93D95B8FB28B54A5E3B_CustomAttributesCacheGenerator_U3CBeginBlurCoroutineU3Ed__46_System_IDisposable_Dispose_mF76EBD1A9FC137D2B4030A5EA027BE799790E4BE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginBlurCoroutineU3Ed__46_t66A4D62ADA1165BE58D4D93D95B8FB28B54A5E3B_CustomAttributesCacheGenerator_U3CBeginBlurCoroutineU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD8D05546E56ECD61023F4A491559495A373F9488(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginBlurCoroutineU3Ed__46_t66A4D62ADA1165BE58D4D93D95B8FB28B54A5E3B_CustomAttributesCacheGenerator_U3CBeginBlurCoroutineU3Ed__46_System_Collections_IEnumerator_Reset_mA7BFC75A2A8264EA28152664634CACF8494C525B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginBlurCoroutineU3Ed__46_t66A4D62ADA1165BE58D4D93D95B8FB28B54A5E3B_CustomAttributesCacheGenerator_U3CBeginBlurCoroutineU3Ed__46_System_Collections_IEnumerator_get_Current_m950F37AAD895B5E9640CEC4651660A2682611759(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEndBlurCoroutineU3Ed__47_t1A9EDFAA6F9FCD2B99F7462CBE18E5D863D37F4E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEndBlurCoroutineU3Ed__47_t1A9EDFAA6F9FCD2B99F7462CBE18E5D863D37F4E_CustomAttributesCacheGenerator_U3CEndBlurCoroutineU3Ed__47__ctor_m0AF52C6A23B8451C701EA167E43C7025E0E85856(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEndBlurCoroutineU3Ed__47_t1A9EDFAA6F9FCD2B99F7462CBE18E5D863D37F4E_CustomAttributesCacheGenerator_U3CEndBlurCoroutineU3Ed__47_System_IDisposable_Dispose_mA8C03B32893FC08A9DC63FF29DB65C218216B925(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEndBlurCoroutineU3Ed__47_t1A9EDFAA6F9FCD2B99F7462CBE18E5D863D37F4E_CustomAttributesCacheGenerator_U3CEndBlurCoroutineU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D4DFA555688E4463316E76AA7186C4650E8B20C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEndBlurCoroutineU3Ed__47_t1A9EDFAA6F9FCD2B99F7462CBE18E5D863D37F4E_CustomAttributesCacheGenerator_U3CEndBlurCoroutineU3Ed__47_System_Collections_IEnumerator_Reset_mF6F0D7B092B4B08D62A8FC1FFE80F1236D268AAF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEndBlurCoroutineU3Ed__47_t1A9EDFAA6F9FCD2B99F7462CBE18E5D863D37F4E_CustomAttributesCacheGenerator_U3CEndBlurCoroutineU3Ed__47_System_Collections_IEnumerator_get_Current_m777A091BCC1224A89508F8FD90276D7D6B1A8E6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MovingImage_tC249A1D9DE0AB6B003F1222309FBFFA6092ADFAC_CustomAttributesCacheGenerator__minX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MovingImage_tC249A1D9DE0AB6B003F1222309FBFFA6092ADFAC_CustomAttributesCacheGenerator__maxX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MovingImage_tC249A1D9DE0AB6B003F1222309FBFFA6092ADFAC_CustomAttributesCacheGenerator__minY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MovingImage_tC249A1D9DE0AB6B003F1222309FBFFA6092ADFAC_CustomAttributesCacheGenerator__maxY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MovingImage_tC249A1D9DE0AB6B003F1222309FBFFA6092ADFAC_CustomAttributesCacheGenerator__minTime(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MovingImage_tC249A1D9DE0AB6B003F1222309FBFFA6092ADFAC_CustomAttributesCacheGenerator__maxTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MovingImage_tC249A1D9DE0AB6B003F1222309FBFFA6092ADFAC_CustomAttributesCacheGenerator__speed(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MovingImage_tC249A1D9DE0AB6B003F1222309FBFFA6092ADFAC_CustomAttributesCacheGenerator_MovingImage_SetTargetCoroutine_m5CAB8DC1170D3816EA911BC37EEB6319647200EC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSetTargetCoroutineU3Ed__12_tDB1343893EA8FAC6B6FCEA0D4FF1887E41326762_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSetTargetCoroutineU3Ed__12_tDB1343893EA8FAC6B6FCEA0D4FF1887E41326762_0_0_0_var), NULL);
	}
}
static void U3CSetTargetCoroutineU3Ed__12_tDB1343893EA8FAC6B6FCEA0D4FF1887E41326762_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSetTargetCoroutineU3Ed__12_tDB1343893EA8FAC6B6FCEA0D4FF1887E41326762_CustomAttributesCacheGenerator_U3CSetTargetCoroutineU3Ed__12__ctor_m2DAA4DD13FEF3D7932EC3D4F52022DAA679B411A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetTargetCoroutineU3Ed__12_tDB1343893EA8FAC6B6FCEA0D4FF1887E41326762_CustomAttributesCacheGenerator_U3CSetTargetCoroutineU3Ed__12_System_IDisposable_Dispose_m4058148DD9EDFD7B905B0E3FA12486310F63B784(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetTargetCoroutineU3Ed__12_tDB1343893EA8FAC6B6FCEA0D4FF1887E41326762_CustomAttributesCacheGenerator_U3CSetTargetCoroutineU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A22C7DB41C1C1B32FF82AE0C0D7A4A26AC23750(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetTargetCoroutineU3Ed__12_tDB1343893EA8FAC6B6FCEA0D4FF1887E41326762_CustomAttributesCacheGenerator_U3CSetTargetCoroutineU3Ed__12_System_Collections_IEnumerator_Reset_m4590115325DA446C3B122D94B7DF7088DCB71F38(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetTargetCoroutineU3Ed__12_tDB1343893EA8FAC6B6FCEA0D4FF1887E41326762_CustomAttributesCacheGenerator_U3CSetTargetCoroutineU3Ed__12_System_Collections_IEnumerator_get_Current_m57C6B9D83546D5B22A1CAEE1B5F4E36334AF53E6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Demo_tB78549B056D29F6158519F8D5055F1A8695F8C9A_CustomAttributesCacheGenerator_blackKeySprites(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Demo_tB78549B056D29F6158519F8D5055F1A8695F8C9A_CustomAttributesCacheGenerator_whiteKeySprites(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Demo_tB78549B056D29F6158519F8D5055F1A8695F8C9A_CustomAttributesCacheGenerator_blackButtonDisplay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Demo_tB78549B056D29F6158519F8D5055F1A8695F8C9A_CustomAttributesCacheGenerator_whiteButtonDisplay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Demo_tB78549B056D29F6158519F8D5055F1A8695F8C9A_CustomAttributesCacheGenerator_blackKeys(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Demo_tB78549B056D29F6158519F8D5055F1A8695F8C9A_CustomAttributesCacheGenerator_whiteKeys(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AIDestinationSetter2_t933D4395EDE240FB20F4B75AAF08C18003A7320A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x3A\x2F\x2F\x61\x72\x6F\x6E\x67\x72\x61\x6E\x62\x65\x72\x67\x2E\x63\x6F\x6D\x2F\x61\x73\x74\x61\x72\x2F\x64\x6F\x63\x73\x2F\x63\x6C\x61\x73\x73\x5F\x70\x61\x74\x68\x66\x69\x6E\x64\x69\x6E\x67\x5F\x31\x5F\x31\x5F\x61\x5F\x69\x5F\x64\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E\x5F\x73\x65\x74\x74\x65\x72\x2E\x70\x68\x70"), NULL);
	}
	{
		UniqueComponentAttribute_tF88A1BA7A11680CD4D65FF2B2BB70D38B314A64B * tmp = (UniqueComponentAttribute_tF88A1BA7A11680CD4D65FF2B2BB70D38B314A64B *)cache->attributes[1];
		UniqueComponentAttribute__ctor_m8986901ED1E290F6C5A592A4BBBCED447D34B554(tmp, NULL);
		tmp->set_tag_0(il2cpp_codegen_string_new_wrapper("\x61\x69\x2E\x64\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E"));
	}
}
static void AIDestinationSetter3_tB2087A5864AE9CD0AA1246F7177C002D97CBBBA5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x3A\x2F\x2F\x61\x72\x6F\x6E\x67\x72\x61\x6E\x62\x65\x72\x67\x2E\x63\x6F\x6D\x2F\x61\x73\x74\x61\x72\x2F\x64\x6F\x63\x73\x2F\x63\x6C\x61\x73\x73\x5F\x70\x61\x74\x68\x66\x69\x6E\x64\x69\x6E\x67\x5F\x31\x5F\x31\x5F\x61\x5F\x69\x5F\x64\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E\x5F\x73\x65\x74\x74\x65\x72\x2E\x70\x68\x70"), NULL);
	}
	{
		UniqueComponentAttribute_tF88A1BA7A11680CD4D65FF2B2BB70D38B314A64B * tmp = (UniqueComponentAttribute_tF88A1BA7A11680CD4D65FF2B2BB70D38B314A64B *)cache->attributes[1];
		UniqueComponentAttribute__ctor_m8986901ED1E290F6C5A592A4BBBCED447D34B554(tmp, NULL);
		tmp->set_tag_0(il2cpp_codegen_string_new_wrapper("\x61\x69\x2E\x64\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E"));
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[366] = 
{
	U3CBombExplodeU3Ed__13_t4199FBF8EE8FB69FC2E8D1451FA0317074C8EC21_CustomAttributesCacheGenerator,
	U3COnCollisionExit2DU3Ed__4_t0A7F5C50E870A71D5A066A4183C6EDB3E19B2E6A_CustomAttributesCacheGenerator,
	U3CChestOpenU3Ed__13_t0EF608A096EA5B88E7441B285F1CA775DAD69FCC_CustomAttributesCacheGenerator,
	U3CDoLoopU3Ed__9_t323C8A0A5985C74C1F651C71592BFFE00295DC35_CustomAttributesCacheGenerator,
	U3CResetU3Ed__21_t3A09E60FC3785242FEC1CF7ECB8A4168CEEEA729_CustomAttributesCacheGenerator,
	U3CCanvasVisibleU3Ed__8_tFBE66285DCE7017B4944C80BFF0206EE3F331B90_CustomAttributesCacheGenerator,
	U3CDyingU3Ed__11_tD7889E9F82B12975287DAD49DAFF3496615FE6E8_CustomAttributesCacheGenerator,
	U3CDieU3Ed__9_tB4DE2C7CC1C2540907ECF2E9768D08FE2FE68284_CustomAttributesCacheGenerator,
	U3COnCollisionExit2DU3Ed__22_t1E250C4710DB6AC782B78E32E267E69DB314A2B4_CustomAttributesCacheGenerator,
	U3CDieU3Ed__27_t7CD29C5A2D9C21135509242D28ACF0A3FC417533_CustomAttributesCacheGenerator,
	U3CResetU3Ed__19_t8B25B25842B8FF12E2D94BF6AC092846A59A530B_CustomAttributesCacheGenerator,
	U3CRemoveEnemyU3Ed__20_t6B7DBBF2F07284838A9445F875F2DE0B0B00A0B0_CustomAttributesCacheGenerator,
	U3CFlipXYU3Ed__24_tAC33D870E2638F395F4872BB8B7AB326A68B447B_CustomAttributesCacheGenerator,
	U3CNotFlipXU3Ed__25_t97BF7638A47B9054628458EF337209FB5E1B47DB_CustomAttributesCacheGenerator,
	U3CNotFlipYU3Ed__26_t299D05E533CB08705D83A8436B638C45175F2B9B_CustomAttributesCacheGenerator,
	U3CStopAttackU3Ed__12_tA53C09AEBE965C669F3A5CBDB6D421C85EBAD548_CustomAttributesCacheGenerator,
	U3CDelayedRespawnU3Ed__8_t898DB56525F2D9AE983B889DDCC35BAC2B3A492F_CustomAttributesCacheGenerator,
	U3CResetU3Ed__16_tC91F103945D467258EBCB7653985176BC1BA3134_CustomAttributesCacheGenerator,
	U3CRemoveEnemyU3Ed__17_t5A654C70BFCE5C7037FAA9B9B24DFC18C8291E8C_CustomAttributesCacheGenerator,
	U3CAttackSpinU3Ed__13_tEE4094FC8C3FA8260A9364391BC98173A97F04A1_CustomAttributesCacheGenerator,
	U3CDieU3Ed__19_t58BC827D82625F3382F7D1006A44B569D7978E65_CustomAttributesCacheGenerator,
	U3CDelayedRespawnU3Ed__6_t301398457F090A938090B53CE3DF6F3296CC89BA_CustomAttributesCacheGenerator,
	U3CShadowU3Ed__10_tEA6BB67F648FB2A2A1D1025F5C635A68E1ACA112_CustomAttributesCacheGenerator,
	U3CResetU3Ed__12_t7BA88BBF0C8AA1E7793E5DA6866CD9202A2D4A19_CustomAttributesCacheGenerator,
	U3CRemoveEnemyU3Ed__13_t501D36A6DF6D75CD4BA1F570226BA29791C408CE_CustomAttributesCacheGenerator,
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator,
	U3CThawinU3Ed__15_t55349D1A3E9AF40B9525721BC9BC00A5C6763A0C_CustomAttributesCacheGenerator,
	U3CFireActionU3Ed__12_t42A2A24BA817A19357851F3502083A1A250B7021_CustomAttributesCacheGenerator,
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator,
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator,
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator,
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator,
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator,
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator,
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator,
	U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator,
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator,
	FlipMode_t2F88797CCAC3141191EB25B280B2947573026B2D_CustomAttributesCacheGenerator,
	U3CBeginBlurCoroutineU3Ed__46_t66A4D62ADA1165BE58D4D93D95B8FB28B54A5E3B_CustomAttributesCacheGenerator,
	U3CEndBlurCoroutineU3Ed__47_t1A9EDFAA6F9FCD2B99F7462CBE18E5D863D37F4E_CustomAttributesCacheGenerator,
	U3CSetTargetCoroutineU3Ed__12_tDB1343893EA8FAC6B6FCEA0D4FF1887E41326762_CustomAttributesCacheGenerator,
	AIDestinationSetter2_t933D4395EDE240FB20F4B75AAF08C18003A7320A_CustomAttributesCacheGenerator,
	AIDestinationSetter3_tB2087A5864AE9CD0AA1246F7177C002D97CBBBA5_CustomAttributesCacheGenerator,
	Char2DController_tBE830CB8D8053E7D295F07D40B4C637A0EDB6C0A_CustomAttributesCacheGenerator_m_JumpForce,
	Char2DController_tBE830CB8D8053E7D295F07D40B4C637A0EDB6C0A_CustomAttributesCacheGenerator_m_CrouchSpeed,
	Char2DController_tBE830CB8D8053E7D295F07D40B4C637A0EDB6C0A_CustomAttributesCacheGenerator_m_MovementSmoothing,
	Char2DController_tBE830CB8D8053E7D295F07D40B4C637A0EDB6C0A_CustomAttributesCacheGenerator_m_AirControl,
	Char2DController_tBE830CB8D8053E7D295F07D40B4C637A0EDB6C0A_CustomAttributesCacheGenerator_m_WhatIsGround,
	Char2DController_tBE830CB8D8053E7D295F07D40B4C637A0EDB6C0A_CustomAttributesCacheGenerator_m_GroundCheck,
	Char2DController_tBE830CB8D8053E7D295F07D40B4C637A0EDB6C0A_CustomAttributesCacheGenerator_m_CeilingCheck,
	Char2DController_tBE830CB8D8053E7D295F07D40B4C637A0EDB6C0A_CustomAttributesCacheGenerator_m_CrouchDisableCollider,
	Char2DController_tBE830CB8D8053E7D295F07D40B4C637A0EDB6C0A_CustomAttributesCacheGenerator_OnLandEvent,
	BackgroundScroller_t0A4783DD437DF3FDAF8B2C4B1EAE1074EC074D01_CustomAttributesCacheGenerator_scrollSpeed,
	DropdownSample_t925F66C7435D75374A0E26DE15B4F1D95697F0DF_CustomAttributesCacheGenerator_text,
	DropdownSample_t925F66C7435D75374A0E26DE15B4F1D95697F0DF_CustomAttributesCacheGenerator_dropdownWithoutPlaceholder,
	DropdownSample_t925F66C7435D75374A0E26DE15B4F1D95697F0DF_CustomAttributesCacheGenerator_dropdownWithPlaceholder,
	AIPatrol_t545F247B50644DC6ACDD6C4AE97E1FD947DBEBBA_CustomAttributesCacheGenerator_mustPatrol,
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnCharacterSelection,
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnSpriteSelection,
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnWordSelection,
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnLineSelection,
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnLinkSelection,
	UIBlur_t164597816B074D3FA54558A54238652E5EDA8248_CustomAttributesCacheGenerator__color,
	UIBlur_t164597816B074D3FA54558A54238652E5EDA8248_CustomAttributesCacheGenerator__buildFlipMode,
	UIBlur_t164597816B074D3FA54558A54238652E5EDA8248_CustomAttributesCacheGenerator__intensity,
	UIBlur_t164597816B074D3FA54558A54238652E5EDA8248_CustomAttributesCacheGenerator__multiplier,
	UIBlur_t164597816B074D3FA54558A54238652E5EDA8248_CustomAttributesCacheGenerator__onBeginBlur,
	UIBlur_t164597816B074D3FA54558A54238652E5EDA8248_CustomAttributesCacheGenerator__onEndBlur,
	UIBlur_t164597816B074D3FA54558A54238652E5EDA8248_CustomAttributesCacheGenerator__onBlurChanged,
	MovingImage_tC249A1D9DE0AB6B003F1222309FBFFA6092ADFAC_CustomAttributesCacheGenerator__minX,
	MovingImage_tC249A1D9DE0AB6B003F1222309FBFFA6092ADFAC_CustomAttributesCacheGenerator__maxX,
	MovingImage_tC249A1D9DE0AB6B003F1222309FBFFA6092ADFAC_CustomAttributesCacheGenerator__minY,
	MovingImage_tC249A1D9DE0AB6B003F1222309FBFFA6092ADFAC_CustomAttributesCacheGenerator__maxY,
	MovingImage_tC249A1D9DE0AB6B003F1222309FBFFA6092ADFAC_CustomAttributesCacheGenerator__minTime,
	MovingImage_tC249A1D9DE0AB6B003F1222309FBFFA6092ADFAC_CustomAttributesCacheGenerator__maxTime,
	MovingImage_tC249A1D9DE0AB6B003F1222309FBFFA6092ADFAC_CustomAttributesCacheGenerator__speed,
	Demo_tB78549B056D29F6158519F8D5055F1A8695F8C9A_CustomAttributesCacheGenerator_blackKeySprites,
	Demo_tB78549B056D29F6158519F8D5055F1A8695F8C9A_CustomAttributesCacheGenerator_whiteKeySprites,
	Demo_tB78549B056D29F6158519F8D5055F1A8695F8C9A_CustomAttributesCacheGenerator_blackButtonDisplay,
	Demo_tB78549B056D29F6158519F8D5055F1A8695F8C9A_CustomAttributesCacheGenerator_whiteButtonDisplay,
	Demo_tB78549B056D29F6158519F8D5055F1A8695F8C9A_CustomAttributesCacheGenerator_blackKeys,
	Demo_tB78549B056D29F6158519F8D5055F1A8695F8C9A_CustomAttributesCacheGenerator_whiteKeys,
	BombAction_t98D24241EBE24B60FD668DCBF734B4BF85AF55A0_CustomAttributesCacheGenerator_BombAction_BombExplode_mBBCB6A87E2BD076879BE0B674CD60DFEECB12E1A,
	U3CBombExplodeU3Ed__13_t4199FBF8EE8FB69FC2E8D1451FA0317074C8EC21_CustomAttributesCacheGenerator_U3CBombExplodeU3Ed__13__ctor_m923CB05D2C511E33FDCFB085649F5DAA1CCAF7BD,
	U3CBombExplodeU3Ed__13_t4199FBF8EE8FB69FC2E8D1451FA0317074C8EC21_CustomAttributesCacheGenerator_U3CBombExplodeU3Ed__13_System_IDisposable_Dispose_m15C4650CE46C34DFC8C5830A307D37CC569AAE0F,
	U3CBombExplodeU3Ed__13_t4199FBF8EE8FB69FC2E8D1451FA0317074C8EC21_CustomAttributesCacheGenerator_U3CBombExplodeU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72951C1ADD21C1776A9F6F9FBBB1784F6CCF401A,
	U3CBombExplodeU3Ed__13_t4199FBF8EE8FB69FC2E8D1451FA0317074C8EC21_CustomAttributesCacheGenerator_U3CBombExplodeU3Ed__13_System_Collections_IEnumerator_Reset_m623ED216F0519489B9F92EFEB501539FBE710AE2,
	U3CBombExplodeU3Ed__13_t4199FBF8EE8FB69FC2E8D1451FA0317074C8EC21_CustomAttributesCacheGenerator_U3CBombExplodeU3Ed__13_System_Collections_IEnumerator_get_Current_mD297AFE797BA2B44311CCDB9548E4366A41D37FA,
	BulletScript_t0B4787136F76B578F89F6D398C85DA98A743E1AD_CustomAttributesCacheGenerator_BulletScript_OnCollisionExit2D_m39DD1005BC9F225C87231175DC010294F39C09B4,
	U3COnCollisionExit2DU3Ed__4_t0A7F5C50E870A71D5A066A4183C6EDB3E19B2E6A_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__4__ctor_m14AF5686030543F733A7415FDCAD1BF3AB949BEC,
	U3COnCollisionExit2DU3Ed__4_t0A7F5C50E870A71D5A066A4183C6EDB3E19B2E6A_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__4_System_IDisposable_Dispose_m73D6E1249D30E89DDD8894FCC44BF8D01F448D00,
	U3COnCollisionExit2DU3Ed__4_t0A7F5C50E870A71D5A066A4183C6EDB3E19B2E6A_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A272FB97A0973E2DF45FAE555831D5023594805,
	U3COnCollisionExit2DU3Ed__4_t0A7F5C50E870A71D5A066A4183C6EDB3E19B2E6A_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__4_System_Collections_IEnumerator_Reset_m9337004461BBF6ECF0723DBD6E0EBDE65CA9ADE2,
	U3COnCollisionExit2DU3Ed__4_t0A7F5C50E870A71D5A066A4183C6EDB3E19B2E6A_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__4_System_Collections_IEnumerator_get_Current_mC32AD54E8C558A329810C8701029A8B9B31E46B1,
	ChestScript_tCF82E2DF0D3418ED5238E8B86468C6AC18BCE1B8_CustomAttributesCacheGenerator_ChestScript_ChestOpen_m61B1715BA969C81616E4C65A3A17BE4015C5C2AC,
	U3CChestOpenU3Ed__13_t0EF608A096EA5B88E7441B285F1CA775DAD69FCC_CustomAttributesCacheGenerator_U3CChestOpenU3Ed__13__ctor_m47FE64B53C9CBFBD01E777FE699028D93D2F4E31,
	U3CChestOpenU3Ed__13_t0EF608A096EA5B88E7441B285F1CA775DAD69FCC_CustomAttributesCacheGenerator_U3CChestOpenU3Ed__13_System_IDisposable_Dispose_m3AEB6E997AB70866D1CAC9FD6B4E4461CA83EDF7,
	U3CChestOpenU3Ed__13_t0EF608A096EA5B88E7441B285F1CA775DAD69FCC_CustomAttributesCacheGenerator_U3CChestOpenU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m773A3042410FA3B4CF8542A5404809996DE56296,
	U3CChestOpenU3Ed__13_t0EF608A096EA5B88E7441B285F1CA775DAD69FCC_CustomAttributesCacheGenerator_U3CChestOpenU3Ed__13_System_Collections_IEnumerator_Reset_m0642610982D6DB7B358B9E0AC3349535664FC592,
	U3CChestOpenU3Ed__13_t0EF608A096EA5B88E7441B285F1CA775DAD69FCC_CustomAttributesCacheGenerator_U3CChestOpenU3Ed__13_System_Collections_IEnumerator_get_Current_mCED6EEA482BDF5E3F2D25500341C365D9597E9B5,
	FlashMaterial_t940980625B0FC56B5774C55727E599259C3BC57A_CustomAttributesCacheGenerator_FlashMaterial_DoLoop_m5AC8BAF3E232FAD720F467C4553C008A657F9B93,
	U3CDoLoopU3Ed__9_t323C8A0A5985C74C1F651C71592BFFE00295DC35_CustomAttributesCacheGenerator_U3CDoLoopU3Ed__9__ctor_m80FB7782169D5C9434E6BB332F0875E552A86EE3,
	U3CDoLoopU3Ed__9_t323C8A0A5985C74C1F651C71592BFFE00295DC35_CustomAttributesCacheGenerator_U3CDoLoopU3Ed__9_System_IDisposable_Dispose_mA1D883D6198A472B861CDD8E69C87389B202711E,
	U3CDoLoopU3Ed__9_t323C8A0A5985C74C1F651C71592BFFE00295DC35_CustomAttributesCacheGenerator_U3CDoLoopU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m161A940F778B3E1A8EB5FF11524329B4ED2B24E9,
	U3CDoLoopU3Ed__9_t323C8A0A5985C74C1F651C71592BFFE00295DC35_CustomAttributesCacheGenerator_U3CDoLoopU3Ed__9_System_Collections_IEnumerator_Reset_m03E7421E1933ED063603056D3A8AA90432B2EBA5,
	U3CDoLoopU3Ed__9_t323C8A0A5985C74C1F651C71592BFFE00295DC35_CustomAttributesCacheGenerator_U3CDoLoopU3Ed__9_System_Collections_IEnumerator_get_Current_m92E236931F2A16F330962D928B271976E1A5AD91,
	FungiMonster_tFDC4C7D802A4BCB3F6ED887AE1E957F08BF11AB9_CustomAttributesCacheGenerator_FungiMonster_Reset_m811B8C555ADE3F36B97FB168CBCF5DD0FA6D62B4,
	U3CResetU3Ed__21_t3A09E60FC3785242FEC1CF7ECB8A4168CEEEA729_CustomAttributesCacheGenerator_U3CResetU3Ed__21__ctor_m769F0D7F760AE690DF6B43BEA616B6470E69A540,
	U3CResetU3Ed__21_t3A09E60FC3785242FEC1CF7ECB8A4168CEEEA729_CustomAttributesCacheGenerator_U3CResetU3Ed__21_System_IDisposable_Dispose_mF3B71B7BD6282934BD973F3FF905DA11CD7AF34A,
	U3CResetU3Ed__21_t3A09E60FC3785242FEC1CF7ECB8A4168CEEEA729_CustomAttributesCacheGenerator_U3CResetU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m40A8093877EEA9DD3CF5DA5BBF315DC68B1FFE7A,
	U3CResetU3Ed__21_t3A09E60FC3785242FEC1CF7ECB8A4168CEEEA729_CustomAttributesCacheGenerator_U3CResetU3Ed__21_System_Collections_IEnumerator_Reset_m476796199DB1FB09AECC198FA41250799FC574AE,
	U3CResetU3Ed__21_t3A09E60FC3785242FEC1CF7ECB8A4168CEEEA729_CustomAttributesCacheGenerator_U3CResetU3Ed__21_System_Collections_IEnumerator_get_Current_mA568910A33A57FBBB89BE715EC8C8E1014881F71,
	LEVEL_t374166D5CEFE5F1CCDC52D08E22E0A4FAFD901E1_CustomAttributesCacheGenerator_LEVEL_CanvasVisible_mA6259459DCA7F69EF82C18AED18304BB8D4214A6,
	U3CCanvasVisibleU3Ed__8_tFBE66285DCE7017B4944C80BFF0206EE3F331B90_CustomAttributesCacheGenerator_U3CCanvasVisibleU3Ed__8__ctor_mB747C6375F58AE06D33940068E0429813343D23B,
	U3CCanvasVisibleU3Ed__8_tFBE66285DCE7017B4944C80BFF0206EE3F331B90_CustomAttributesCacheGenerator_U3CCanvasVisibleU3Ed__8_System_IDisposable_Dispose_m2B04E17B955D1CE7679FF6F2A6CDF01CDA2AA2EF,
	U3CCanvasVisibleU3Ed__8_tFBE66285DCE7017B4944C80BFF0206EE3F331B90_CustomAttributesCacheGenerator_U3CCanvasVisibleU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC1B4F1E8D5035F6F1FEF26BEF3D2B6D4F16B882D,
	U3CCanvasVisibleU3Ed__8_tFBE66285DCE7017B4944C80BFF0206EE3F331B90_CustomAttributesCacheGenerator_U3CCanvasVisibleU3Ed__8_System_Collections_IEnumerator_Reset_m60B70917051546AC0423BB783D2146FE7F07A3DC,
	U3CCanvasVisibleU3Ed__8_tFBE66285DCE7017B4944C80BFF0206EE3F331B90_CustomAttributesCacheGenerator_U3CCanvasVisibleU3Ed__8_System_Collections_IEnumerator_get_Current_m331FFE5EF7E5CBD34180C12B89A4DC1286C16E5D,
	ParticleStart_tD05588725AE893915D20851D0394B7537B0AAB70_CustomAttributesCacheGenerator_ParticleStart_Dying_m5D0B1E9994445919D0F0185BB8E7E050D71E5AD0,
	U3CDyingU3Ed__11_tD7889E9F82B12975287DAD49DAFF3496615FE6E8_CustomAttributesCacheGenerator_U3CDyingU3Ed__11__ctor_m58D732957B79282AA92ADF5F21813A7E85BCFFF0,
	U3CDyingU3Ed__11_tD7889E9F82B12975287DAD49DAFF3496615FE6E8_CustomAttributesCacheGenerator_U3CDyingU3Ed__11_System_IDisposable_Dispose_mFA3AC7162FF2307CBFA845DD40FC27ED0BD267FC,
	U3CDyingU3Ed__11_tD7889E9F82B12975287DAD49DAFF3496615FE6E8_CustomAttributesCacheGenerator_U3CDyingU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51C1911128E46618C6D1050F369891214787B76E,
	U3CDyingU3Ed__11_tD7889E9F82B12975287DAD49DAFF3496615FE6E8_CustomAttributesCacheGenerator_U3CDyingU3Ed__11_System_Collections_IEnumerator_Reset_m748FB5A362DC188E0B7A3A683E1F523A07E5EBCC,
	U3CDyingU3Ed__11_tD7889E9F82B12975287DAD49DAFF3496615FE6E8_CustomAttributesCacheGenerator_U3CDyingU3Ed__11_System_Collections_IEnumerator_get_Current_m0DF20F30AB85F89DAC4C200ECF9C50B755DE0651,
	Enemy2D_tC347E442B3CF83F1584563426C6E202527E5C041_CustomAttributesCacheGenerator_Enemy2D_Die_m3EC84CC04E8DD61905E8434CCEB48F25FD19BB56,
	U3CDieU3Ed__9_tB4DE2C7CC1C2540907ECF2E9768D08FE2FE68284_CustomAttributesCacheGenerator_U3CDieU3Ed__9__ctor_mADE2673E0AC151EEE2C2B8BE752FAF867D70790E,
	U3CDieU3Ed__9_tB4DE2C7CC1C2540907ECF2E9768D08FE2FE68284_CustomAttributesCacheGenerator_U3CDieU3Ed__9_System_IDisposable_Dispose_m2AE53DE0960F3C4AD19D9B539603A2F844447039,
	U3CDieU3Ed__9_tB4DE2C7CC1C2540907ECF2E9768D08FE2FE68284_CustomAttributesCacheGenerator_U3CDieU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51EEC3D1CC93BB56CEB071BFAEA5F897512C2FEB,
	U3CDieU3Ed__9_tB4DE2C7CC1C2540907ECF2E9768D08FE2FE68284_CustomAttributesCacheGenerator_U3CDieU3Ed__9_System_Collections_IEnumerator_Reset_m8DCFC9CAB164B3F18DC2EDC7E5F35DF765A182E0,
	U3CDieU3Ed__9_tB4DE2C7CC1C2540907ECF2E9768D08FE2FE68284_CustomAttributesCacheGenerator_U3CDieU3Ed__9_System_Collections_IEnumerator_get_Current_mD3EED8BA14478BD8216B581B28EB81FEE5ECEECD,
	Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_CustomAttributesCacheGenerator_Player_OnCollisionExit2D_m3313F2D12F1F1BA58D9E410CE7386136F5008BCF,
	Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_CustomAttributesCacheGenerator_Player_Die_m792E214E2658BA493E201D0C397C9E973457CA82,
	U3COnCollisionExit2DU3Ed__22_t1E250C4710DB6AC782B78E32E267E69DB314A2B4_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__22__ctor_mA8A4F25B6067CE0B9414AB9EC7C05D529E7C8386,
	U3COnCollisionExit2DU3Ed__22_t1E250C4710DB6AC782B78E32E267E69DB314A2B4_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__22_System_IDisposable_Dispose_mDD17E25B307F29A01D61EF5C767F10C624BA2A2A,
	U3COnCollisionExit2DU3Ed__22_t1E250C4710DB6AC782B78E32E267E69DB314A2B4_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m193C48EB097DA325334C0C839D3B48D9FD1238AF,
	U3COnCollisionExit2DU3Ed__22_t1E250C4710DB6AC782B78E32E267E69DB314A2B4_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__22_System_Collections_IEnumerator_Reset_m772A97B6A8F765C1C881C238E0CA7428687C87E1,
	U3COnCollisionExit2DU3Ed__22_t1E250C4710DB6AC782B78E32E267E69DB314A2B4_CustomAttributesCacheGenerator_U3COnCollisionExit2DU3Ed__22_System_Collections_IEnumerator_get_Current_m2C711895EC942AE7CAFBAA314F76746A7B9CBE34,
	U3CDieU3Ed__27_t7CD29C5A2D9C21135509242D28ACF0A3FC417533_CustomAttributesCacheGenerator_U3CDieU3Ed__27__ctor_m8857762760F3BCE10A3D88DD890FAD638A8D4702,
	U3CDieU3Ed__27_t7CD29C5A2D9C21135509242D28ACF0A3FC417533_CustomAttributesCacheGenerator_U3CDieU3Ed__27_System_IDisposable_Dispose_mC61821F0D50A408F101518E86C449AC53FF660C0,
	U3CDieU3Ed__27_t7CD29C5A2D9C21135509242D28ACF0A3FC417533_CustomAttributesCacheGenerator_U3CDieU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m410D885DBDE33BCAABD4860F2043CF3E36EEBF70,
	U3CDieU3Ed__27_t7CD29C5A2D9C21135509242D28ACF0A3FC417533_CustomAttributesCacheGenerator_U3CDieU3Ed__27_System_Collections_IEnumerator_Reset_m4D1BD6D7BA2AF7656BF349668CB2385963FD629F,
	U3CDieU3Ed__27_t7CD29C5A2D9C21135509242D28ACF0A3FC417533_CustomAttributesCacheGenerator_U3CDieU3Ed__27_System_Collections_IEnumerator_get_Current_m6BDD5E6FB780641B98FA16D648C42FD92BF42C05,
	Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627_CustomAttributesCacheGenerator_Enemy_Reset_m628CD2AF324D1E1AB9149FCED22A7148CFBD056D,
	Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627_CustomAttributesCacheGenerator_Enemy_RemoveEnemy_mD2058B454847C883951FBE12A118697333EADF62,
	U3CResetU3Ed__19_t8B25B25842B8FF12E2D94BF6AC092846A59A530B_CustomAttributesCacheGenerator_U3CResetU3Ed__19__ctor_m5079434512C1D722152FD57D9692C8186A97D321,
	U3CResetU3Ed__19_t8B25B25842B8FF12E2D94BF6AC092846A59A530B_CustomAttributesCacheGenerator_U3CResetU3Ed__19_System_IDisposable_Dispose_mC4E423C14CAA780263C2CAD897FB06444822E352,
	U3CResetU3Ed__19_t8B25B25842B8FF12E2D94BF6AC092846A59A530B_CustomAttributesCacheGenerator_U3CResetU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m165E9A0298191DAED6213B75F722A63C9C93732E,
	U3CResetU3Ed__19_t8B25B25842B8FF12E2D94BF6AC092846A59A530B_CustomAttributesCacheGenerator_U3CResetU3Ed__19_System_Collections_IEnumerator_Reset_m4DFA3ED64ABCD8B3242221434025EC1D302916A7,
	U3CResetU3Ed__19_t8B25B25842B8FF12E2D94BF6AC092846A59A530B_CustomAttributesCacheGenerator_U3CResetU3Ed__19_System_Collections_IEnumerator_get_Current_mED384D8433E87A6CEBC3263205674A78D591C2C9,
	U3CRemoveEnemyU3Ed__20_t6B7DBBF2F07284838A9445F875F2DE0B0B00A0B0_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__20__ctor_m1E6B4D9F3646E854A30388BF4635D465618D628A,
	U3CRemoveEnemyU3Ed__20_t6B7DBBF2F07284838A9445F875F2DE0B0B00A0B0_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__20_System_IDisposable_Dispose_m3745507487481DEB5DBAF8FA60A0AE9F0E670AB5,
	U3CRemoveEnemyU3Ed__20_t6B7DBBF2F07284838A9445F875F2DE0B0B00A0B0_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAA95AD573C461C64AEAAA6315CFD544159B3B000,
	U3CRemoveEnemyU3Ed__20_t6B7DBBF2F07284838A9445F875F2DE0B0B00A0B0_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__20_System_Collections_IEnumerator_Reset_m56D0635675474E0F012360DB67C043F6DEE882B5,
	U3CRemoveEnemyU3Ed__20_t6B7DBBF2F07284838A9445F875F2DE0B0B00A0B0_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__20_System_Collections_IEnumerator_get_Current_m59443E41B8F69CE7426D70185454AC4A86C7DF3F,
	Player2_t8C8C4E2564E6A8D88E3760E59336E236B08B4502_CustomAttributesCacheGenerator_Player2_FlipXY_m2E8B59FAB876B6DFD536A9F63F4ACC0F6DA5AE8B,
	Player2_t8C8C4E2564E6A8D88E3760E59336E236B08B4502_CustomAttributesCacheGenerator_Player2_NotFlipX_m393306343FBC9E3BC569493BAA79BDC717A1B092,
	Player2_t8C8C4E2564E6A8D88E3760E59336E236B08B4502_CustomAttributesCacheGenerator_Player2_NotFlipY_mFF387819DF73F7157B133CFE1C065061658951FA,
	U3CFlipXYU3Ed__24_tAC33D870E2638F395F4872BB8B7AB326A68B447B_CustomAttributesCacheGenerator_U3CFlipXYU3Ed__24__ctor_m131DB1A9C643FD92F6AF8100B1673A68E413C1C9,
	U3CFlipXYU3Ed__24_tAC33D870E2638F395F4872BB8B7AB326A68B447B_CustomAttributesCacheGenerator_U3CFlipXYU3Ed__24_System_IDisposable_Dispose_m10B3BC1FED6E3FC8BCD0EB206E1AC28669F2EA2A,
	U3CFlipXYU3Ed__24_tAC33D870E2638F395F4872BB8B7AB326A68B447B_CustomAttributesCacheGenerator_U3CFlipXYU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAD58057BA5483CE0D43CE4B62009B70AF0DE2EB0,
	U3CFlipXYU3Ed__24_tAC33D870E2638F395F4872BB8B7AB326A68B447B_CustomAttributesCacheGenerator_U3CFlipXYU3Ed__24_System_Collections_IEnumerator_Reset_mA87B6F0AEC5F1895B83ABC2DE5FC1ED6DD4BE20D,
	U3CFlipXYU3Ed__24_tAC33D870E2638F395F4872BB8B7AB326A68B447B_CustomAttributesCacheGenerator_U3CFlipXYU3Ed__24_System_Collections_IEnumerator_get_Current_m24CEFA38C44FD2871AC176AA4DA6C3F8AF5EAF8A,
	U3CNotFlipXU3Ed__25_t97BF7638A47B9054628458EF337209FB5E1B47DB_CustomAttributesCacheGenerator_U3CNotFlipXU3Ed__25__ctor_m94F2F0163265685222ADFB187F09580678CA2145,
	U3CNotFlipXU3Ed__25_t97BF7638A47B9054628458EF337209FB5E1B47DB_CustomAttributesCacheGenerator_U3CNotFlipXU3Ed__25_System_IDisposable_Dispose_m6F4A9B4FE98D2DD965AF928F40AA6562E71DA52B,
	U3CNotFlipXU3Ed__25_t97BF7638A47B9054628458EF337209FB5E1B47DB_CustomAttributesCacheGenerator_U3CNotFlipXU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13CA1D64DE97A4EBEBACA8356720DD9FE5756AB4,
	U3CNotFlipXU3Ed__25_t97BF7638A47B9054628458EF337209FB5E1B47DB_CustomAttributesCacheGenerator_U3CNotFlipXU3Ed__25_System_Collections_IEnumerator_Reset_m0E72C3BD6F7C5880500F39E88ECF4D8832FEF217,
	U3CNotFlipXU3Ed__25_t97BF7638A47B9054628458EF337209FB5E1B47DB_CustomAttributesCacheGenerator_U3CNotFlipXU3Ed__25_System_Collections_IEnumerator_get_Current_m307B7C1CAA37E1DAD29DD63844B00D7A5AC947C0,
	U3CNotFlipYU3Ed__26_t299D05E533CB08705D83A8436B638C45175F2B9B_CustomAttributesCacheGenerator_U3CNotFlipYU3Ed__26__ctor_mD5791BAD4289B9AEA13D346B0D15A4085077458C,
	U3CNotFlipYU3Ed__26_t299D05E533CB08705D83A8436B638C45175F2B9B_CustomAttributesCacheGenerator_U3CNotFlipYU3Ed__26_System_IDisposable_Dispose_m92088D58772A6C6FAEF4B7208F110704E6488A2B,
	U3CNotFlipYU3Ed__26_t299D05E533CB08705D83A8436B638C45175F2B9B_CustomAttributesCacheGenerator_U3CNotFlipYU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35E9597F83EF82C1BC20F307C6E63FE93EDA7307,
	U3CNotFlipYU3Ed__26_t299D05E533CB08705D83A8436B638C45175F2B9B_CustomAttributesCacheGenerator_U3CNotFlipYU3Ed__26_System_Collections_IEnumerator_Reset_mC6D8CCA93FB0987B4081DED3D535729334345AF4,
	U3CNotFlipYU3Ed__26_t299D05E533CB08705D83A8436B638C45175F2B9B_CustomAttributesCacheGenerator_U3CNotFlipYU3Ed__26_System_Collections_IEnumerator_get_Current_mE496955C82262A2E1E8BFDDFE55AB76A7ED97D97,
	SwordAttack_tF24D56ACEA6B660502BE16C22D0BE37FB1FD42E4_CustomAttributesCacheGenerator_SwordAttack_StopAttack_mE7DCA01D4E303DF75B5D784FDE348862D10618C9,
	U3CStopAttackU3Ed__12_tA53C09AEBE965C669F3A5CBDB6D421C85EBAD548_CustomAttributesCacheGenerator_U3CStopAttackU3Ed__12__ctor_mC892CCDD9137521226BFDB42F756F498ED9D9DE4,
	U3CStopAttackU3Ed__12_tA53C09AEBE965C669F3A5CBDB6D421C85EBAD548_CustomAttributesCacheGenerator_U3CStopAttackU3Ed__12_System_IDisposable_Dispose_m050550BE33CA586AD6A3AFC4553AF767A80B7E0A,
	U3CStopAttackU3Ed__12_tA53C09AEBE965C669F3A5CBDB6D421C85EBAD548_CustomAttributesCacheGenerator_U3CStopAttackU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD070A2C7A382D082F76C43E10B370F5A161A1568,
	U3CStopAttackU3Ed__12_tA53C09AEBE965C669F3A5CBDB6D421C85EBAD548_CustomAttributesCacheGenerator_U3CStopAttackU3Ed__12_System_Collections_IEnumerator_Reset_m7EB90774CFA286159F440225FF0844A9CA318451,
	U3CStopAttackU3Ed__12_tA53C09AEBE965C669F3A5CBDB6D421C85EBAD548_CustomAttributesCacheGenerator_U3CStopAttackU3Ed__12_System_Collections_IEnumerator_get_Current_m8C32386D0F688C60F58C83D55D8232476A3FE9B2,
	ScriptPengaturGame_tEA62B98743EF089268A3DE2E49A6365A6212462F_CustomAttributesCacheGenerator_ScriptPengaturGame_DelayedRespawn_mFAB0633263E6564AF5BB4EF59FABE7202696771E,
	U3CDelayedRespawnU3Ed__8_t898DB56525F2D9AE983B889DDCC35BAC2B3A492F_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__8__ctor_m40CD1912AF394DC2AC923A52C94357E0DF450D2B,
	U3CDelayedRespawnU3Ed__8_t898DB56525F2D9AE983B889DDCC35BAC2B3A492F_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__8_System_IDisposable_Dispose_m870C5BECEB2FDF41D71A48F1252484616984C08D,
	U3CDelayedRespawnU3Ed__8_t898DB56525F2D9AE983B889DDCC35BAC2B3A492F_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m704FE12609434A38D732427EB89149F0E243013B,
	U3CDelayedRespawnU3Ed__8_t898DB56525F2D9AE983B889DDCC35BAC2B3A492F_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__8_System_Collections_IEnumerator_Reset_mD5F85137107697D53140B274A3025147FCC06881,
	U3CDelayedRespawnU3Ed__8_t898DB56525F2D9AE983B889DDCC35BAC2B3A492F_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__8_System_Collections_IEnumerator_get_Current_mAA4466DE52655D1610F3E77D6DFF672364ADD55D,
	RandomPathEnemy_tF79996B7E634EA635BBE8DE42D9231BF223ECAA2_CustomAttributesCacheGenerator_RandomPathEnemy_Reset_m7AC5BB1855F12972ADA3278EE482A1C25BFFB8DD,
	RandomPathEnemy_tF79996B7E634EA635BBE8DE42D9231BF223ECAA2_CustomAttributesCacheGenerator_RandomPathEnemy_RemoveEnemy_m3F3AA696BB27EC03B23500BA53FDA5BEE9652759,
	U3CResetU3Ed__16_tC91F103945D467258EBCB7653985176BC1BA3134_CustomAttributesCacheGenerator_U3CResetU3Ed__16__ctor_m4A61B7D86F5085B4169CE600699751AD297884B6,
	U3CResetU3Ed__16_tC91F103945D467258EBCB7653985176BC1BA3134_CustomAttributesCacheGenerator_U3CResetU3Ed__16_System_IDisposable_Dispose_mA222C493C8FD94E8FD1C9D3F7472859F5BACCB04,
	U3CResetU3Ed__16_tC91F103945D467258EBCB7653985176BC1BA3134_CustomAttributesCacheGenerator_U3CResetU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A2CB645B1B0484C48CB991CDE86500798FAA66E,
	U3CResetU3Ed__16_tC91F103945D467258EBCB7653985176BC1BA3134_CustomAttributesCacheGenerator_U3CResetU3Ed__16_System_Collections_IEnumerator_Reset_m29FBCED23D03FA82F30CEC93EAD8BF33BA588ADF,
	U3CResetU3Ed__16_tC91F103945D467258EBCB7653985176BC1BA3134_CustomAttributesCacheGenerator_U3CResetU3Ed__16_System_Collections_IEnumerator_get_Current_m6A8464C3A44F86288FDC802B734BD79679BD8AC6,
	U3CRemoveEnemyU3Ed__17_t5A654C70BFCE5C7037FAA9B9B24DFC18C8291E8C_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__17__ctor_mDF5707187B155BFF83BB4292A37DC36184AE56EF,
	U3CRemoveEnemyU3Ed__17_t5A654C70BFCE5C7037FAA9B9B24DFC18C8291E8C_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__17_System_IDisposable_Dispose_mBE1FBD45087F9AFB7A1697B658CB87630B689714,
	U3CRemoveEnemyU3Ed__17_t5A654C70BFCE5C7037FAA9B9B24DFC18C8291E8C_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC87690098968F6C42184EE1080E4E20289A85F33,
	U3CRemoveEnemyU3Ed__17_t5A654C70BFCE5C7037FAA9B9B24DFC18C8291E8C_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__17_System_Collections_IEnumerator_Reset_m50CF5CF07AD930842A88145B8EB6089F69155A83,
	U3CRemoveEnemyU3Ed__17_t5A654C70BFCE5C7037FAA9B9B24DFC18C8291E8C_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__17_System_Collections_IEnumerator_get_Current_m1EEF8BCDA0EE7B9BFB461F31DF62C5AA9CE89EC5,
	Rotation_t494A089B49580F1EC9172DD688C6EC94B1E4EEEE_CustomAttributesCacheGenerator_Rotation_AttackSpin_mFCC4707E6C3F1686B797441B2CABE55A56772514,
	U3CAttackSpinU3Ed__13_tEE4094FC8C3FA8260A9364391BC98173A97F04A1_CustomAttributesCacheGenerator_U3CAttackSpinU3Ed__13__ctor_m4EA58DFAB7F07180EE63578CF3FE454562B21F9A,
	U3CAttackSpinU3Ed__13_tEE4094FC8C3FA8260A9364391BC98173A97F04A1_CustomAttributesCacheGenerator_U3CAttackSpinU3Ed__13_System_IDisposable_Dispose_m809F021C7039F1A4EA346723F825E2BEFFC7BB89,
	U3CAttackSpinU3Ed__13_tEE4094FC8C3FA8260A9364391BC98173A97F04A1_CustomAttributesCacheGenerator_U3CAttackSpinU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m460DF86C141E5B679E0E6964C89FFE1E7EECFD7D,
	U3CAttackSpinU3Ed__13_tEE4094FC8C3FA8260A9364391BC98173A97F04A1_CustomAttributesCacheGenerator_U3CAttackSpinU3Ed__13_System_Collections_IEnumerator_Reset_mC4DAB2FCF9E95B419ED3BD42CC01B3DAC3C6B5A8,
	U3CAttackSpinU3Ed__13_tEE4094FC8C3FA8260A9364391BC98173A97F04A1_CustomAttributesCacheGenerator_U3CAttackSpinU3Ed__13_System_Collections_IEnumerator_get_Current_m48BD155E14859DB7FAA98B221A434C12740E8852,
	PengaturGame_t790449E6E929F602A9CE7511FFC69F12ECD6D99C_CustomAttributesCacheGenerator_PengaturGame_Die_mB7093D4526DA560D2C9393D4984E3892F75B2574,
	U3CDieU3Ed__19_t58BC827D82625F3382F7D1006A44B569D7978E65_CustomAttributesCacheGenerator_U3CDieU3Ed__19__ctor_mCD289E975B415CDFE7F03327361CA771A9ACC945,
	U3CDieU3Ed__19_t58BC827D82625F3382F7D1006A44B569D7978E65_CustomAttributesCacheGenerator_U3CDieU3Ed__19_System_IDisposable_Dispose_mA8D3F31A7FA44478E9DB61A959929BB795A8F824,
	U3CDieU3Ed__19_t58BC827D82625F3382F7D1006A44B569D7978E65_CustomAttributesCacheGenerator_U3CDieU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5DB6F0BF77F660F069C9F3A2A60580D58BC136BF,
	U3CDieU3Ed__19_t58BC827D82625F3382F7D1006A44B569D7978E65_CustomAttributesCacheGenerator_U3CDieU3Ed__19_System_Collections_IEnumerator_Reset_m6C4B4656507A3C75D780F9043623876E56B280C6,
	U3CDieU3Ed__19_t58BC827D82625F3382F7D1006A44B569D7978E65_CustomAttributesCacheGenerator_U3CDieU3Ed__19_System_Collections_IEnumerator_get_Current_m42A1443B0A7558052BB999AEA2950E2422012F8D,
	ScriptPengaturGameTutor_tEF7501C6320507C1A8DCED14B9FC409A77A2D7D6_CustomAttributesCacheGenerator_ScriptPengaturGameTutor_DelayedRespawn_m951994477B5FC95D9C1824E4C044B548A9F12869,
	U3CDelayedRespawnU3Ed__6_t301398457F090A938090B53CE3DF6F3296CC89BA_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__6__ctor_m832D756A5E55217164F680E64AF1B43041752E1E,
	U3CDelayedRespawnU3Ed__6_t301398457F090A938090B53CE3DF6F3296CC89BA_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__6_System_IDisposable_Dispose_m3ACCA8E0A4F478BFD532407AAF5C4458EB6D8131,
	U3CDelayedRespawnU3Ed__6_t301398457F090A938090B53CE3DF6F3296CC89BA_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m48E50B3154FC7795127B06517938CAB13B89D71A,
	U3CDelayedRespawnU3Ed__6_t301398457F090A938090B53CE3DF6F3296CC89BA_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__6_System_Collections_IEnumerator_Reset_m814E90D8FD4532733B793DA5FDAF9FF3A57B2CDC,
	U3CDelayedRespawnU3Ed__6_t301398457F090A938090B53CE3DF6F3296CC89BA_CustomAttributesCacheGenerator_U3CDelayedRespawnU3Ed__6_System_Collections_IEnumerator_get_Current_m4949E5B7B143356AED4CB5677BBD70C93385687E,
	ShadowMonster_t7EC06E58CE8A62512943B468400CC576CBF9E99F_CustomAttributesCacheGenerator_ShadowMonster_Shadow_m62273387B5D608A04893DADA04C1DF233B336B48,
	U3CShadowU3Ed__10_tEA6BB67F648FB2A2A1D1025F5C635A68E1ACA112_CustomAttributesCacheGenerator_U3CShadowU3Ed__10__ctor_m09156293353FA2A514EC009449AC2E4F8A7BA07E,
	U3CShadowU3Ed__10_tEA6BB67F648FB2A2A1D1025F5C635A68E1ACA112_CustomAttributesCacheGenerator_U3CShadowU3Ed__10_System_IDisposable_Dispose_m7D1378040012088EFC8545115F4E26B8BE9AD013,
	U3CShadowU3Ed__10_tEA6BB67F648FB2A2A1D1025F5C635A68E1ACA112_CustomAttributesCacheGenerator_U3CShadowU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m158249148A41952D3D36E40F719C17F88C16E8F2,
	U3CShadowU3Ed__10_tEA6BB67F648FB2A2A1D1025F5C635A68E1ACA112_CustomAttributesCacheGenerator_U3CShadowU3Ed__10_System_Collections_IEnumerator_Reset_mC690E70BB7DD3145DC2A8F2E682A15819094A724,
	U3CShadowU3Ed__10_tEA6BB67F648FB2A2A1D1025F5C635A68E1ACA112_CustomAttributesCacheGenerator_U3CShadowU3Ed__10_System_Collections_IEnumerator_get_Current_m3B88444077C83B3FD6A4913F0ABB99A182145512,
	StaticEnemy_t95B3339AD78C31858F9A2E5603371F34B3937D6C_CustomAttributesCacheGenerator_StaticEnemy_Reset_m4A777D62276DC732D3191E4F48D267CF16B3D9A7,
	StaticEnemy_t95B3339AD78C31858F9A2E5603371F34B3937D6C_CustomAttributesCacheGenerator_StaticEnemy_RemoveEnemy_mF7FE9C88D58ADC37D2418A9AE8005C915DA206A6,
	U3CResetU3Ed__12_t7BA88BBF0C8AA1E7793E5DA6866CD9202A2D4A19_CustomAttributesCacheGenerator_U3CResetU3Ed__12__ctor_m3BDA33E3CBC8E74B1D63ECE7624726D2F0ED5B33,
	U3CResetU3Ed__12_t7BA88BBF0C8AA1E7793E5DA6866CD9202A2D4A19_CustomAttributesCacheGenerator_U3CResetU3Ed__12_System_IDisposable_Dispose_m3A795EBB07033590127C289B6B5F5E49A5E567FC,
	U3CResetU3Ed__12_t7BA88BBF0C8AA1E7793E5DA6866CD9202A2D4A19_CustomAttributesCacheGenerator_U3CResetU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1061A8FD8BBDC1BD073A7C5D7EE759104350B430,
	U3CResetU3Ed__12_t7BA88BBF0C8AA1E7793E5DA6866CD9202A2D4A19_CustomAttributesCacheGenerator_U3CResetU3Ed__12_System_Collections_IEnumerator_Reset_mF82998DB5D5B49AFD9CE752D7397D9B67C176AB3,
	U3CResetU3Ed__12_t7BA88BBF0C8AA1E7793E5DA6866CD9202A2D4A19_CustomAttributesCacheGenerator_U3CResetU3Ed__12_System_Collections_IEnumerator_get_Current_m13B64BC583A8FE1918C67F176750728CD09C888F,
	U3CRemoveEnemyU3Ed__13_t501D36A6DF6D75CD4BA1F570226BA29791C408CE_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__13__ctor_mFE8832B3C4256C83BAC43813904C447C2929A4F0,
	U3CRemoveEnemyU3Ed__13_t501D36A6DF6D75CD4BA1F570226BA29791C408CE_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__13_System_IDisposable_Dispose_mF1DA594D7D588A39061219178E77EC059C949BEB,
	U3CRemoveEnemyU3Ed__13_t501D36A6DF6D75CD4BA1F570226BA29791C408CE_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m439C009B65DD373B6730486E9589933915789BA3,
	U3CRemoveEnemyU3Ed__13_t501D36A6DF6D75CD4BA1F570226BA29791C408CE_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__13_System_Collections_IEnumerator_Reset_m245ABFC3107FFFC827EBDCCF997886EECB6439E8,
	U3CRemoveEnemyU3Ed__13_t501D36A6DF6D75CD4BA1F570226BA29791C408CE_CustomAttributesCacheGenerator_U3CRemoveEnemyU3Ed__13_System_Collections_IEnumerator_get_Current_m650FDB7D270B20CA532F4E1E0486C1783B84A4B1,
	EnvMapAnimator_t7C30A094A3786710A896AD9A6FA46B13EFEB72FD_CustomAttributesCacheGenerator_EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE,
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23,
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B,
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E,
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB,
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF,
	ThwainMonster_tB8C199321ACEE54640562BAE36FED77704CE51E3_CustomAttributesCacheGenerator_ThwainMonster_Thawin_m6C86C7E85FA477214831CC8EF072EE5F6EF506C3,
	U3CThawinU3Ed__15_t55349D1A3E9AF40B9525721BC9BC00A5C6763A0C_CustomAttributesCacheGenerator_U3CThawinU3Ed__15__ctor_mEF2892EA017B7E62EF004982AE3F56F38E6980A1,
	U3CThawinU3Ed__15_t55349D1A3E9AF40B9525721BC9BC00A5C6763A0C_CustomAttributesCacheGenerator_U3CThawinU3Ed__15_System_IDisposable_Dispose_m6DF0994E7AD74DDC24DEAD3FAB93CB232F26AFDD,
	U3CThawinU3Ed__15_t55349D1A3E9AF40B9525721BC9BC00A5C6763A0C_CustomAttributesCacheGenerator_U3CThawinU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF6D7461EC85545A6F83AB1D9F38638DCCCB7BDE3,
	U3CThawinU3Ed__15_t55349D1A3E9AF40B9525721BC9BC00A5C6763A0C_CustomAttributesCacheGenerator_U3CThawinU3Ed__15_System_Collections_IEnumerator_Reset_m4C12473748C08E79BAC8333D134EE3E2E140773D,
	U3CThawinU3Ed__15_t55349D1A3E9AF40B9525721BC9BC00A5C6763A0C_CustomAttributesCacheGenerator_U3CThawinU3Ed__15_System_Collections_IEnumerator_get_Current_mC850258D3CE2A05A9A334F09705A22526894714D,
	bullerAround_t8B0D8062BA22026378CCE3BB162576CD1DEF3B0C_CustomAttributesCacheGenerator_bullerAround_FireAction_mC13433AB56F7308581EBBD3C26B46E252AD6E812,
	U3CFireActionU3Ed__12_t42A2A24BA817A19357851F3502083A1A250B7021_CustomAttributesCacheGenerator_U3CFireActionU3Ed__12__ctor_m4D9A49C5E7CB1A36B37E5ADB70C420D347F72159,
	U3CFireActionU3Ed__12_t42A2A24BA817A19357851F3502083A1A250B7021_CustomAttributesCacheGenerator_U3CFireActionU3Ed__12_System_IDisposable_Dispose_m071496B05C3F27B2E7F7D606A63C698F0A69D2CF,
	U3CFireActionU3Ed__12_t42A2A24BA817A19357851F3502083A1A250B7021_CustomAttributesCacheGenerator_U3CFireActionU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF851ED90860DB287F90CA430F16C4DB28016597E,
	U3CFireActionU3Ed__12_t42A2A24BA817A19357851F3502083A1A250B7021_CustomAttributesCacheGenerator_U3CFireActionU3Ed__12_System_Collections_IEnumerator_Reset_mE82756C227D55D51245493565A1CF7FF96305500,
	U3CFireActionU3Ed__12_t42A2A24BA817A19357851F3502083A1A250B7021_CustomAttributesCacheGenerator_U3CFireActionU3Ed__12_System_Collections_IEnumerator_get_Current_m5DE0834A1C06844043D41DBF2DB921DDE4C22A04,
	Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934_CustomAttributesCacheGenerator_Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C,
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB,
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73,
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591,
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B,
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E,
	Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B_CustomAttributesCacheGenerator_Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4,
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651,
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561,
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD,
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA,
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2,
	ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8_CustomAttributesCacheGenerator_ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469,
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A,
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B,
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390,
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B,
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362,
	SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07_CustomAttributesCacheGenerator_SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3,
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F,
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF,
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1,
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F,
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A,
	TeleType_t92B5ED4FE818F912A562F085CE035F6B42834934_CustomAttributesCacheGenerator_TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7,
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629,
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9,
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B,
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8,
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261,
	TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289_CustomAttributesCacheGenerator_TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534,
	TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289_CustomAttributesCacheGenerator_TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4,
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D,
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C,
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83,
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691,
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F,
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0,
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8,
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB,
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F,
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27,
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200_CustomAttributesCacheGenerator_TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7,
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200_CustomAttributesCacheGenerator_TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B,
	U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20,
	U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3,
	U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598,
	U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5,
	U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6,
	VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374_CustomAttributesCacheGenerator_VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42,
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F,
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007,
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51,
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932,
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52,
	VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A_CustomAttributesCacheGenerator_VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16,
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8,
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099,
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479,
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D,
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF,
	VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A_CustomAttributesCacheGenerator_VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599,
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2,
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81,
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE,
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA,
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E,
	VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9_CustomAttributesCacheGenerator_VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87,
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760,
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC,
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D,
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB,
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD,
	VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163_CustomAttributesCacheGenerator_VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED,
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2,
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F,
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9,
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C,
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F,
	WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96_CustomAttributesCacheGenerator_WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2,
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49,
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60,
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1,
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535,
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25,
	UIBlur_t164597816B074D3FA54558A54238652E5EDA8248_CustomAttributesCacheGenerator_UIBlur_BeginBlurCoroutine_m22E1A8448AD6438DAA161116C8B9AF7A5A1C7A5B,
	UIBlur_t164597816B074D3FA54558A54238652E5EDA8248_CustomAttributesCacheGenerator_UIBlur_EndBlurCoroutine_m636D49D1F1297AE5D5C1211455CDDABD524FAC35,
	U3CBeginBlurCoroutineU3Ed__46_t66A4D62ADA1165BE58D4D93D95B8FB28B54A5E3B_CustomAttributesCacheGenerator_U3CBeginBlurCoroutineU3Ed__46__ctor_m27D06DFB52FDBF0B49DA318368D0759928E1D1BF,
	U3CBeginBlurCoroutineU3Ed__46_t66A4D62ADA1165BE58D4D93D95B8FB28B54A5E3B_CustomAttributesCacheGenerator_U3CBeginBlurCoroutineU3Ed__46_System_IDisposable_Dispose_mF76EBD1A9FC137D2B4030A5EA027BE799790E4BE,
	U3CBeginBlurCoroutineU3Ed__46_t66A4D62ADA1165BE58D4D93D95B8FB28B54A5E3B_CustomAttributesCacheGenerator_U3CBeginBlurCoroutineU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD8D05546E56ECD61023F4A491559495A373F9488,
	U3CBeginBlurCoroutineU3Ed__46_t66A4D62ADA1165BE58D4D93D95B8FB28B54A5E3B_CustomAttributesCacheGenerator_U3CBeginBlurCoroutineU3Ed__46_System_Collections_IEnumerator_Reset_mA7BFC75A2A8264EA28152664634CACF8494C525B,
	U3CBeginBlurCoroutineU3Ed__46_t66A4D62ADA1165BE58D4D93D95B8FB28B54A5E3B_CustomAttributesCacheGenerator_U3CBeginBlurCoroutineU3Ed__46_System_Collections_IEnumerator_get_Current_m950F37AAD895B5E9640CEC4651660A2682611759,
	U3CEndBlurCoroutineU3Ed__47_t1A9EDFAA6F9FCD2B99F7462CBE18E5D863D37F4E_CustomAttributesCacheGenerator_U3CEndBlurCoroutineU3Ed__47__ctor_m0AF52C6A23B8451C701EA167E43C7025E0E85856,
	U3CEndBlurCoroutineU3Ed__47_t1A9EDFAA6F9FCD2B99F7462CBE18E5D863D37F4E_CustomAttributesCacheGenerator_U3CEndBlurCoroutineU3Ed__47_System_IDisposable_Dispose_mA8C03B32893FC08A9DC63FF29DB65C218216B925,
	U3CEndBlurCoroutineU3Ed__47_t1A9EDFAA6F9FCD2B99F7462CBE18E5D863D37F4E_CustomAttributesCacheGenerator_U3CEndBlurCoroutineU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D4DFA555688E4463316E76AA7186C4650E8B20C,
	U3CEndBlurCoroutineU3Ed__47_t1A9EDFAA6F9FCD2B99F7462CBE18E5D863D37F4E_CustomAttributesCacheGenerator_U3CEndBlurCoroutineU3Ed__47_System_Collections_IEnumerator_Reset_mF6F0D7B092B4B08D62A8FC1FFE80F1236D268AAF,
	U3CEndBlurCoroutineU3Ed__47_t1A9EDFAA6F9FCD2B99F7462CBE18E5D863D37F4E_CustomAttributesCacheGenerator_U3CEndBlurCoroutineU3Ed__47_System_Collections_IEnumerator_get_Current_m777A091BCC1224A89508F8FD90276D7D6B1A8E6B,
	MovingImage_tC249A1D9DE0AB6B003F1222309FBFFA6092ADFAC_CustomAttributesCacheGenerator_MovingImage_SetTargetCoroutine_m5CAB8DC1170D3816EA911BC37EEB6319647200EC,
	U3CSetTargetCoroutineU3Ed__12_tDB1343893EA8FAC6B6FCEA0D4FF1887E41326762_CustomAttributesCacheGenerator_U3CSetTargetCoroutineU3Ed__12__ctor_m2DAA4DD13FEF3D7932EC3D4F52022DAA679B411A,
	U3CSetTargetCoroutineU3Ed__12_tDB1343893EA8FAC6B6FCEA0D4FF1887E41326762_CustomAttributesCacheGenerator_U3CSetTargetCoroutineU3Ed__12_System_IDisposable_Dispose_m4058148DD9EDFD7B905B0E3FA12486310F63B784,
	U3CSetTargetCoroutineU3Ed__12_tDB1343893EA8FAC6B6FCEA0D4FF1887E41326762_CustomAttributesCacheGenerator_U3CSetTargetCoroutineU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A22C7DB41C1C1B32FF82AE0C0D7A4A26AC23750,
	U3CSetTargetCoroutineU3Ed__12_tDB1343893EA8FAC6B6FCEA0D4FF1887E41326762_CustomAttributesCacheGenerator_U3CSetTargetCoroutineU3Ed__12_System_Collections_IEnumerator_Reset_m4590115325DA446C3B122D94B7DF7088DCB71F38,
	U3CSetTargetCoroutineU3Ed__12_tDB1343893EA8FAC6B6FCEA0D4FF1887E41326762_CustomAttributesCacheGenerator_U3CSetTargetCoroutineU3Ed__12_System_Collections_IEnumerator_get_Current_m57C6B9D83546D5B22A1CAEE1B5F4E36334AF53E6,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
